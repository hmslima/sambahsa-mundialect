Sambahsa

Mundialect

Complete Grammar

![](Pictures/10000201000004960000047067F9E3243AFA1A59.png){width="6.835cm"
height="6.613cm"}

Henrique Matheus da Silva Lima

Revision: Dr. Olivier Simon

  ------------------------- ----------------------------------------------------------------------------------------------
  **Version:** 0.220 Beta   ![](Pictures/10000000000001F4000000B07F43F2EF6B26141B.png){width="3.074cm" height="1.083cm"}
  ------------------------- ----------------------------------------------------------------------------------------------

IMPORTANT NOTES ABOUT LEGAL ISSUES

This grammar is licensed under the Creative Commons CC-BY 4.0 license.

![](Pictures/10000000000001F4000000B07F43F2EF6B26141B.png){width="3.074cm"
height="1.083cm"}

You are free to:

**Share –** copy and redistribute the material in any medium or format

**Adapt –** remix, transform, and build upon the material for any
purpose, even commercially.

You must give appropriate credit, provide a link to the license, and
indicate if changes were made. You may do so in any reasonable manner,
but not in any way that suggests the licensor endorses you or your use.

You may not apply legal terms or technological measures that legally
restrict others from doing anything the license permits.

Here is the link for more information:
<https://creativecommons.org/licenses/by/4.0/deed.en>

The Sambahsa language authorship is of Dr. Olivier Simon, the language's
use is free for anyone. For the elaboration of this grammar, *The
Grammar Of Sambahsa-Mundialect In English* by Dr. Olivier Simon was
used. Some subchapters of this grammar are practically a transcription
of Olivier's book. Dr. Simon has given me permission to do so, even
considering the license of this book. I've also utilized many examples
from *The Grammar Of Sambahsa-Mundialect In English* and others that Dr.
Simon made for me.

It's very important to inform the reader that **the language itself is
not under a Creative Commons license or anything like it**, the language
is under the traditional Copyright license in which Dr. Olivier Simon
has all property rights. As was said, **the language is free**, you can
translate your works, or produce original works, without the need of Dr.
Olivier Simon's permission, even it is for commercial purposes; in other
words, you can write books in Sambahsa and sell them without problems.
Enjoy this beautiful language.

**This is a beta version of the grammar, but it already can be used.**

THIS GRAMMAR WAS TRANSLATED FROM BRAZILIAN PORTUGUESE TO ENGLISH!
WAITING FOR AVAILABLE NATIVE SPEAKERS TO MAKE A REVISION.

ACKNOWLEDGEMENTS

I thank to Dr. Olivier Simon for making himself available for answering
my questions and giving me many examples, many examples of this grammar
are from him. I must thank him twice, because he had all the patience in
the world for me. He is almost the coauthor of this book.

I thank to Cale Short for making the revision of the English of some few
parts of this grammar.

Also a thanks to Sylvain Auclair, Martín Rincón Botero and Justin
Gagnon, who helped me a lot.

[]{#anchor}INTRODUCTION
=======================

**Sambahsa Mundialect** is an auxiliary language created by the French
Dr. Olivier Simon and launched in the internet on July of 2007, its base
is Proto-Indo-European, language spoken about 5000 years ago, whose
daughter languages widespread from Southern Russia up to the British
Isles and India. Sambahsa counts with many contributions from other
languages, especially Arabic, but also Chinese, Indonesian and many
others.

By being an auxiliary language, Sambahsa is much more easier than any
national language like German or Spanish, but Sambahsa is a bit more
challenging if it's compared to other auxiliary languages because
Sambahsa is a bit complicated at the beginning, but after that, the
learning process shows itself incredibly simple and the advantages of
its complexity will make all the effort be worth.

With what the language looks alike? The interesting of Sambahsa is that
it is so natural that you don't see it as a constructed language, but as
a national language like any other. As Dave MacLeod has said in his
preface for the book *The Grammar Of Sambahsa-Mundialect In English*, “I
always imagined Sambahsa to be an example of a language that could have
existed somewhere around present-day Armenia, where a kingdom using a
descendant of Proto-Indo-European using it has been influenced over the
centuries by its Persian, Turkish and Arab neighbors, as well as various
countries from the east. At times it feels a bit like Bulgarian, at
other times like Persian, and sometimes similar to German as well”.

Sambahsa has a flag for representing it.

![](Pictures/10000201000007D00000053C3165E76E4F454897.png){width="8.334cm"
height="5.584cm"}

The brown color represents the ground, since many Indo-European
languages derive their words for “person” from words related to clay,
like the word “earthling”. The white color was chosen because it
contrasts the better. The double circles represent the wheel because it
is an important innovation made by humans, the square represents the
vehicle (car, chariot…), the “T”s represent the thill, the lines
represent the axe and the bigger circle represents the Sun as well as
the Circle of Life.

The “movement” that supports Sambahsa is not linked to any kind of
thought: people from all kinds, ethnicity, places, believes, genres,
sexual orientations, social classes and ideologies may use this language
as they please.

[]{#anchor-1}WHY LEARN AN AUXILIARY LANGUAGE?
---------------------------------------------

You ask me: “but we already have English as an international language,
why learn an artificial language?”. Before answer this question, I must
remember that we are speaking here about a language for a world context,
if I'll work in China or, at least, stay in touch with Chineses, I will
have to learn Mandarin or Cantonese, it's obvious. What I am talking
about here is: if I enter in a room where there are a Paquistanese, a
Russian, a Brazilian, an Argentinian and a Japanese, all of them only
can have a communication if they know a common language, because only a
very few people have time and disposition to learn five different
languages; in current days it's expected that all these people speak
English for making possible a communication. This is the the heart of
the matter.

The problem with a national language – no matter whether it is English,
Spanish, French or Mandarin – is that it's spent much time *(and also
money in many cases)* learning it. Of course that for those who likes to
learn languages or will live in a foreign country, it's a **fabulous**
investment, even learning a “less important” language is a good
investment if it pleases you and it may be a professional differential
in the future. But when the matter is a language that serve as bridge
between different people, the scenario is different.

I like to say that when two persons communicate with each other through
the []{#anchor-2}language of a determined country it is like calculate
using roman numerals. It's perfectly possible calculate with roman
numerals, peoples did that for centuries, but it's much more efficient
to calculate with indo-Arabic numerals, which in our case would be a
language that you learn in months instead one that you learn in years
and is fully of traps. I said “language of a determined country” because
people learn – in the international communication scope – the English
variant from United States of America or England, but never the English
from Jamaica.

Other negative point in using the language of another country as
international language is that, somewhat, it corroborates the supremacy
of this country. In the modern times was used French because France was
the most influential nation of Europe, since the XX century we use
English because the anglophone countries have acquired great economic
and military power. Do some decades hence we will have to communicate
with each other in Mandarin or in the language of the country that
“rules” the world?

### []{#anchor-3}SO ARE YOU AGAINST THE ENGLISH LANGUAGE?

My answer for this question is a loud “**NO**”! English is considered as
official language in countries of all the continents, not to mention
that the United States and England are great exporters of culture, it’s
because of it and other factors that many people support the idea that
English still will be used as lingua franca for a good amount of time
even if the United States lose its hegemony. And personally I think that
English is a very beautiful language.

Despite all the critics made, English still is the most widespread
language in the globe; in practically all schools of the world with a
minimal of infrastructure the English language is taught; unlike
English, no auxiliary language had the support of a State and companies
to propagate it on the world. We know that currently a work will be much
better exposed if it is made available in English.

It’s important to learn English just as it's important to learn Spanish,
Russian, Esperanto, Mandarin, Portuguese, etc, because although we
defend that the using of an auxiliary language as *lingua franca* would
be much more efficient, the actual international language is the
polyglotism, if you want to be something near to a true “citizen of the
world”, be a polyglot!

But an auxiliary language surely would help us immensely. A neutral
language that can be learned in matter of months would greatly reduce
language barriers. Why not be more efficient in the scope of the
international communication?

[]{#anchor-4}BUT WHY LEARN SAMBAHSA?
------------------------------------

Before I meet Sambahsa I've already known other auxiliary languages, but
I was not fully satisfied with them, not because they are inefficient,
but they have certain aspects, due their enormous simplicity, that
displeased me.

Sambahsa has success in having the regularity – and facility – of an
auxiliary language and the naturality and native beauty of a national
language. Sambahsa is as good as English or Portuguese for making poetry
or music and the imported proper nouns and names of “exotic” things from
other languages don't need, in most cases, to have its spelling or
pronounce drastically changed to adequate the standards of the language,
the “difficulties” in importing these words will be the same that occurs
in any national language like English or Portuguese.

Sambahsa is also a finished language, it means that you don't have to
worry whether what you've learned today will change in five years. What
you write today in Sambahsa will be understood even in a century.

But for not having more than 100 years as other auxiliary languages,
Sambahsa doesn't have a community with thousand of speakers yet, but
this scenario can change, every auxiliary language started with only one
person, but can exist thousands of speakers if more people get
interested for the language

[]{#anchor-5}IS SAMBAHSA DIFFICULT?
-----------------------------------

It's undeniable that in the **beginning** **– and only at the beginning
–** Sambahsa is a bit more challenging if it is compared to other
auxiliary languages, but it's like Robert Winter said in his “Sambahsa:
Guide to Pronouns and Articles”, *“Sambahsa is right on the limit of the
degree of difficulty that is practical for an international auxiliary
language”*.

**Sambahsa undoubtedly is easy**, you only have to know what are you
priorities of study, *you have to know how to study*! Doesn't make sense
decorate all declinative cases from dative if you use them a very few
times *(and maybe only a few of them, even if you only use Sambahsa in
all days of your life)*! Also it's necessary have in mind that it's not
because it is an easy language that you will be writing philosophical
treatises in three weeks, but maybe you can do it in six or eight
months.

People also say that the language is excessively prolix, sometimes
having two or three invariable words for each one of the same type in
our language. Indeed Sambahsa doesn't economize in its invariable words,
a prolix language like Sambahsa pays the price of demanding a bit more
of study, but, by contrast, it allows the best expression of the
thought. Frankly, without realizing you will get used to these words, I
say this from experience.

People complains that Sambahsa has difficult sounds to learn, but all
languages have different sounds to be learned, even other auxiliary
language that tries to be the simplest. Soon after there are some of the
the languages most studied by Americans and their sounds that don't
exist in the General American English *(the sounds in italic are used in
Sambahsa)*.

**Spanish *****(European Castilian)***** –** \[β\], *\[x\]*, \[ɲ\],
\[ɣ\]

**German –** *\[ç\]*, *\[ʁ\]*, *\[x\]*, \[ʏ\], *\[y\]*, *\[ø\]*, \[œ\]

**French –** \[ɲ\], *\[ʁ\]*, \[ɥ\], \[œ\], *\[ø\]*, *\[y\]*, \[ɑ̃\],
\[ɛ̃\], \[œ̃\], \[ɔ̃\]

Maybe Sambahsa could have simpler sounds *(simple for who?)* or less
“extra sounds”, but wouldn't the language be poorer? Even Esperanto has
sounds that defies the Americans like \[x\] of the letter “ĥ”. The
sounds are easily learnable and I'll show you that you can be
pronouncing all of them in a week.

Other complaint is that Sambahsa bases its vocabulaty upon loanwords
instead compound words, I explain: in Sambahsa the adjective “beautiful”
is “bell” while “ugly” is “biaur”; someone could say that, for example,
an affix that inverts the meaning of the word would be more interesting
because it would eliminate the need of memorizing hundreds of words.
Indeed it's a resource with its advantages, but, on the other hand,
sentences with compound sentences take a time to be analyzed, so, after
analyzing the pros and cons, it's more interesting and practical a word
like “biaur” than “antibell”. And frankly, until you master the grammar,
you will have memorized all the most relevant words.

**Everything has a price:** many auxiliary languages try the possible to
be the simplest by adopting very simple orthographies: one letter, one
sound; all nouns having only one ending, all adjectives having only one
ending, etc. The advantage of not having a too complex orthography is
that the learning curve is very low, but the disadvantage is that from
this simplicity it's created a little problem with importation of words
of other languages, which will have to be drastically modified *(as in
orthography as in pronounce)* in order to make them fit to the
language's rules. Other problem of this simplicity is the little freedom
for the creation of poetry and music. I am not saying that very simple
auxiliary languages are ugly, since this is very subjective and a person
with a crystalline voice and talent can create the best song of the
world through the simplest and “ugliest” language. What I am saying is
that it's paid a price for the excess of simplification, and the
contrary is true too, it's paid a price to avoid all these problems of
the simple languages. The question is, what is the price you are willing
to pay?

Many complain that very simple auxiliary languages offers little poetic
freedom, others will say that more complex auxiliary languages are
unnecessarily difficult.

Y[]{#anchor-6}ou are damned if you do and damned if you don't

[]{#anchor-7}THE SAMBAHSA'S VOCABULARY
--------------------------------------

Just below there is a list of languages that had contributed for the
Sambahsa vocabulary, the percentage of borrowed words and some examples
between parentheses:

**Proto-Indo-European –** 44.28 % *(skadh, paursk, potnia) *

**Latin –** 15 % *(facil, question, caise) *

**Germanic family –** 9.5 % *(apter, buk, rogv) *

**French –** 6.21 % *(journal, adresse, place) *

**Greek –** 4.64 % *(pharmacia, ieftin, papier) *

**Romance family –** 3.95 % *(important, visite, torte) *

**Arabic –** 3.42 % *(lakin, mutawassit, hatta) *

**English –** 1.45 % *(film, sport, wagon) *

**Slavic family –** 1.28 % *(lige, grance, vessel) *

**Italian –** 1 % *(autostrad, valise, dusch) *

**Indo-iranian/Persian –** 0.92 % *(naft, ris, hevd) *

**German –** 0.78 % *(dank, postamt, vurst) *

**Chinese –** 0.71 % *(gienxin, yui, saan)*

**Other languages or families –** 6.86 %

About the vocabulary that came from the reconstructed words of
Proto-Indo-European, it's important to say that some words can not be
guaranteed as “common Proto-Indo-European pure”, Proto-Indo-European
divided into many linguistic families, but words in common among these
languages may not necessarily have their origin in Proto-Indo-European.
For example, the words “long” and “pisk” are found in the Romance
languages *(“longo” and “peixe” in the case of Portuguese)* and German
languages *(“long” and “fish” in the case of English)*, but it's
uncertain whether their origin was in Proto-Indo-European, linguistic
families continued to exchange words when Proto-Indo-European split up.

How some words can have “multiple origins”:

**Amlak –** means “asset(s)” *(finance)*, it corresponds to the Arabic
“أملاك”, to the Turkish “emlak” and to the Persian “املاک”.

**Schut –** means “hornless”, it corresponds to the Romanian “Șut”, to
the Bulgarian/Serbo-croatian “šut” and to the ao Albanian “shut”.

**Geong –** means “palace-fortress”, it corresponds to the character
“城”, which is read as *chéng* in Mandarim pinyin, *jō* in Japanese
goon, *seong* in Korean and *thành* in Vietnamese.

**Potire –** means “pitcher”, it corresponds to the Old Greek “ποτήρ”,
to the Serbo-croatian “путир”, to the Russian “потир” and to the
Albanian “potir”.

More examples of words of languages whose contribuition is not very
significant, but have their mark in Sambahsa:

**Portuguese –** banan, mingo, namor

**Spanish –** chocolat, ghitarr(a), salg, vanilia

**Baltic –** biaur, tik

**Malay –** kye *(also from slav; from Malay-Indonesian comes “ke” and
from Russian with some Slavic languages comes “k(o)”) *

**Scandinavian –** leik, lyt, tiel

**Sanskrit –** bahsa, nagor

**Universal –** mama

**Celtic –** sapoun, brigv *(also from Germanic)*, brugs *(also from
Italian) *

**Arawak –** tabak

**Nahuatl –** tomat

[]{#anchor-8}METHODOLOGY OF TEACHING
====================================

I structured the grammar in the following way: I avoid the presentation
of a new content that demands the previous knowledge of other
no-mentioned subject, for example: if for learning the future verbal
tense it's necessary to know the infinitive, I will teach the infinitive
before the future tense; if for learning the declination is necessary
some basic grammar knowledge, I will bring back those teachings from
middle school's classes. I believe that this way is the less annoying
for the beginner.

This book was made in the way that it serves the student without any
previous knowledge, in other words, I am assuming you don't know another
language and you even don't know certain basic grammar terms. Thus a
student can read this grammar without the necessity of consulting other
books, except dictionaries. *The goal of this grammar is to be
accessible to everyone!*

**What I am going to say is important, so pay attention!** When you
study, don't be in a hurry for decorating all cases in the accusative or
all the list of prepositions once, take it easy (!), since you will use
some words more times than other ones. No grammar was made to be read
just once, but many times. Now that you are starting, read the chapters
only once, twice at most, then go on to the next chapter. After
finishing the reading of the grammar, start reading texts, they will
help you to increase your vocabulary and improve you grammar knowledge,
since you will have to consult the grammar for understanding how that
sentence was made.

It can seem strange, but I started this grammar when I was in the
beginning of my studies in the language, in my second or third week of
learning, not when I already had a good mastering over Sambahsa. I had
two reasons to do that: the act of making a grammar demands a great
responsibility, what boosted me in my studies, thanks to this work I
could learn things more quickly, that was a very interesting exercise;
the second reason is that the person who knows the first difficulties of
a beginner is other beginner, if I had decided to make this grammar much
time after, maybe I could forget certain difficulties that I had and I
wouldn't address them in this grammar.

[]{#anchor-9}ACTUALLY YOU DON'T NEED TO LEARN EVERYTHING
--------------------------------------------------------

One of the goals of this grammar is being the most complete as possible,
in other words, the goal is to cover the most basic until the elements
that will only rarely appear in the most erudite literature. There will
be things here like the optional forms of the indicative of present,
that rarely are used, but this grammar has the obligation of presenting
it. Focus only in what is necessary for you!

The language also has a very rich phonology, there are sounds that
doesn't exist in English, but can be learned in a short time. I made the
possible to explain the sounds, since per comparisons with other
languages until per what movements of tongue and mouth are made. But if
you really have difficulty with some sounds, there is no problem, you
can use a similar sound, very improbably someone wont understand you,
but endeavor a bit for learning all sounds, it'll be a learning that
will serve for other languages as you will see.

[]{#anchor-10}TIPS FOR STUDYING A LANGUAGE
------------------------------------------

In these years studying languages I've learned some things, they are
tips that are useful for any language, not only Sambahsa:

-   When you read, listen, write or say a word, directly associate this
    word to its idea. I'll explain: some people, when read “nebh”,
    associate this word to the English word “cloud” and then they make
    association to a cloud. Don't do this, train your mind to don't need
    an intermediate, try to directly associate the word to the idea or
    object.
-   Don't stick to the grammar, read the grammar once or twice then go
    to the texts, you have to see the language in the practice. With the
    time you will absorb the grammar rules while acquire vocabulary.
    When, in a text, you don't know how certain grammatical construction
    was made, you come back to the grammar.
-   Use the language since the first day. Even only in your mind, give a
    “good morning” in Sambahsa, create simple sentences for daily
    situations. Try to use the language in the beginning as possible,
    even you don't know the most basic prepositions.

[]{#anchor-11}ALPHABET AND PRONUNCIATION
========================================

Maybe you take fright because of the size of this chapter, indeed it is
a bit big if compared to other grammars in their chapters of alphabet
and pronunciation, but it's for a good reason. In the study of a
language, the first thing you have to learn are the sounds, it's
important that, by learning a new word, you store the words in you
memory with the correct pronunciation, that's why I will not spare
explanations in this chapter.

The alphabet is composed by 26 Latin letters, they are: a, b, c, d, e,
f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z. The
orthography is a bit complex, but still regular.

For representing the sounds there is the *Sambahsa Phonetic
Transcription* (SPT), an alphabet whose words are always between curly
brackets { } in all this grammar, the letters in UPPERCASE inside the
brackets represent the accentuated syllables and a colon (:) informs
that the vowel before this is lengthened. Be aware that the explanations
about pronunciation are based in the General American English.

Initially the SPT was made between square brackets \[ \], but I've
choosen curly brackets { } because the square brackets already are used
by the other system of phonetic transcription that I'll talk soon after.

In addiction there is something called *International Phonetic Alphabet*
(IPA), it's like the SPT, but IPA is used for all languages while SPT is
only for Sambahsa. The letters of this alphabet always will lie inside
slashes / / or square brackets \[ \] in this grammar. I will use the IPA
to explain the SPT, because if you have access to the sounds of the IPA,
you have a secure source of how the Sambahsa sounds are pronounced.
**But throughout the grammar, outside the “alphabet and pronunciation”
chapter, I practically will only use SPT! Actually you don't need to
learn IPA, but it would be a very useful resource for you.**

The difference between slashes and square brackets in IPA is that the
slashes indicate a more simple transcription while square brackets
indicate a more precise transcription.

You ask me: why we don't only use IPA if this one is official and more
known while SPT only applies to Sambahsa? There are two reasons:

-   SPT is easer to type in computers, what facilitates when someone
    wanna explain the pronunciation in internet or in any typed work.
-   Sambahsa was made for people of all the world, one letter of SPT can
    encompass several similar sounds of IPA. For example: {r}
    represents, preferably, the sounds /ʀ/ and /r/, but it also can
    represent the sound /ɾ/; {o} represents the sounds /o/ and /ɔ/.

Before we go on, some observations:

-   Probably many sounds here will be new for you. Don't worry, all the
    sounds not found in the English language will be explained with all
    details as possible.
-   Keep in mind that the phonetic descriptions were based on the
    English of the United States of America, more precisely the General
    American.
-   The United States of America is a continental country and has some
    linguistic variation. So, if you find that the phonetic description
    of a word is unfamiliar, maybe I am referring to the way of speaking
    of other part of the United States. That's why IPA is very useful
    here, you can know the exact sound.
-   Sambahsa is very rich in what concerns to sounds, if you find that
    some sound is very complicate, you can use a similar sound in its
    place if you think that the meaning of the word will not be twisted
    and make confusion in the conversation. The important is that you
    use Sambahsa!
-   You don't need to use IPA if you don't want, since all the sounds
    will be explained. The IPA is there for serving as additional
    resource for you.
-   Even you have never heard about IPA, you can easily use it. There
    are many resources that shows the sounds of the IPA and some ones
    even teaches you how to do them. Copy and paste the IPA letter that
    you want to learn in your favorite search engine, then see the
    options of websites and files that treats about the pronunciation of
    these sounds. It's not so difficult.

    -   As I've said before: the IPA practically only will be used in
        this “alphabet and pronunciation” chapter, in the rest of the
        book I will use only SPT! The IPA is only a secure guide for you
        learning SPT!

-   If the IPA wont be clear for you, I put some words from our English
    language transcribed in these alphabets to you comprehend them well.

I’ll do my best to explain you the sounds, but since even within USA the
English language may vary, it’s more safe you take a IPA chart with
sounds (you can find one in Wikipedia) to know how they look like.

**{a} –** /a/ – something like the first part of “i” in “h**i**de” or
“br**i**de”, whose sound actually is \[a[]{#anchor-12}ɪ̯\],
[]{#anchor-13}[]{#anchor-14}but without the second part
\[…[]{#anchor-15}ɪ̯\]. []{#anchor-16}Pay attention that {a} is not like
the “a” in “f**a**ther” or “**a**rm”, whose “a” is /ɑː/. It's like the
“a” in the French word “p**a**tte” or in the eastern andalusian Spanish
“m**a**dres”

**{ä} –** /ɛ/ – like “e” in “dr**e**ss” and “m**e**t” or “ea” in
“br**ea**d”.

**{b} –** /b/ – like “b” in “**b**ad”.

**{c} –** /ʃ/ – like “sh” in “**sh**oe” or “cra**sh**”.

**{d} –** /d/ – like “d” in “**d**ay”.

**{e} –** /e/ – something like the first part of “a” in “l**a**ke”, “ai”
in “p**ai**d” or “ei” in “r**ei**n”, whose sound actually is /eɪ/, but
without the semivowel /…ɪ/. It's like the “é” in the French word
“beaut**é**” or the “ee” in the standard German “S**ee**le”.

**{ë} –** /ə/ – like “a” in “**a**gain” or “u” in “syr**u**p”.

**{f} –** /f/ – like “f” in “**f**ather”.

**{g} –** /g/ – like “g” in “**g**irl” or “**g**oat”.

**{h} –** /h/ – like “h” in “**h**orse”

**{i} –** /i/ – like “y” in “happ**y**”, “ey” in “mon**ey**” or “ie” in
“part**ie**s”.

**{j} –** /[]{#anchor-17}ʒ/ – like “s” “vi**s**ion” or “a**s**ian” or
“g” in “bei**g**e”

**{k} –** /k/ – like “c” in “**c**ut”, “ck” in “ba**ck**” or “k” in
“spea**k**”

**{l} –** /l/ – like “l” in “**l**eft” or “sou**l**”.

**{m} –** /m/ – like “m” in “**m**other”.

**{n} –** /n/ – like “n” in “**n**ight”

**{o} –** /o/ or /ɔ/ – the /o/ sound is something like the fist part of
“o” in “g**o**at”, whose sound actually is /o[]{#anchor-18}ʊ/, but
without the semivowel /…ʊ/. It's like the “eau” in the French word
“rés**eau**” or the “o” in the standard German word “**o**der”. The /ɔ/
sound is found in “o” of “n**o**rth”.

**{ö} –** /ø/ – like the “ö” from German or the “eux” French.
[]{#anchor-19}[]{#anchor-20}[]{#anchor-21}Don't you know German neither
French? []{#anchor-22}There is no problem, I teach you how to make this
sound. It's how you make the {e} sound, but with the mouth in the form
you make the {o} sound, in other words, speak /e/ with the lips
puckered.

**{p} –** /p/ – like “p” in “**p**each”.

**{q} –** /x/ – like the German “ach-laut”, like “j” in the Spanish word
“**j**ota” or like “ch” in the Scots word “lo**ch**”. Don't you know
German neither Spanish? There is no problem, I teach you how to make
this sound. It's like the {h} sound, but you put the back of your tongue
against your soft palate as you do with {k} and {g}; think in a vintage
kettle.

**{r} –** /[]{#anchor-23}ʀ/ – in situations like in “radh”, “prete” and
“accurat”, where after “r” there is a vowel, you can pronunciate it as
the “r” in “**r**ed” /ɹ/ or “tt” in “le**tt**er” /ɾ/. Actually Sambahsa
admits a wide variety of sounds – includding our “r” (!!!!!) –, but *if*
it’s for elect an official sound, it should be the uvular “r” (/ʀ/) from
German. []{#anchor-24}Don't you know German? []{#anchor-25}There is no
problem, I teach you how to make this sound. Do the following exercise,
gargle a bit of water *(or any other liquid)* in order to you understand
how to vibrate your uvula. The /[]{#anchor-26}ʀ/ sound is made by the
trill of the uvula.

**{r} –** /[]{#anchor-27}ʁ/ – in situations like in “irk”, “amor” and
“absorb”, where just after “r” there is a consonant (not a vowel!) or
this “r” is the last letter of the word, this consonant assumes the
sound \[ʁ\]. The /ʁ/ is very similar the sound /h/ of “**h**orse”, but
there are two differences: you must vibrate your vocal cords and the
place of articulation of /[]{#anchor-28}ʁ/ is the uvula – while the
place of articulation of /h/ is the glottis *(which for some people is
the same that it doesn't have a place of articulation)* –, in other
words, you must put the back of your tongue against the uvula. If it
still is difficult for you, let's try an exercise: when you make the
sound /h/ you don't vibrate your vocal cords, the vibration of the vocal
cords are important, it's that what differentiates the sounds /t/
*(voiceless, without vibration)* and /d/ *(voiced, with vibration)*, /k/
(voiceless) and /g/ *(voiced)*, /s/ *(voiceless)* and /z/ *(voiced)*,
/f/ *(voiceless)* and /v/ *(voiced)*; OK, try to make the sound /h/ be
voiced, in order to make the sound /ɦ/ *(/ɦ/ is just the voiced version
of /h/ if you didn't understand)*; when you be able to make the sound
/ɦ/, then make this sound with the back of the tongue against the uvula,
thus you're be making the sound /ʁ/. Just below some examples in IPA:

spar – /spaʁ/

cherkin – /t͡ʃəʁ'kin/

wir – /wiʁ/

clever – /'klevəʁ/

cort – /koʁt/

amor – /a'moʁ/

cort – /koʁt/

aur – /'aʊʁ/

mer – /meʁ/

air – /ɛʁ/

atelier – /ate'ljeʁ/

ier – /jeʁ/

ierk – /jeʁk/

piurn – /pjuʁn/

oyr – /ojʁ/

ayr – /ajʁ/

butour – /bu'tuːʁ/

alabster – /a'labstəʁ/

gurgule – /'guʁgyl/

eurp – /øʁp/

arbust – /aʁ'bust/

ender – /'endəʁ/

biaur – /bj'aʊʁ/

**{s} **– /s/ – like “s” in “**s**ound” or “c” in “ri**c**e”.

**{t} –** /t/ – like “t” in “**t**o**t**al”.

**{u} –** /u/ – like “oo” in “l**oo**se”.

**{ü} –** /y/ – like the French “u” or the German “ü”.
[]{#anchor-29}[]{#anchor-30}Don't you know German neither French?
[]{#anchor-31}There is no problem, I teach you how to make this sound.
It's very simple, speak the {u} sound, notice the movements you do with
your mouth; now speak the sound {i}, but with the mouth movement of {u},
in other words, speak {i} with a puckered lips.

**{v} **– /v/ – like “v” in “**v**ein”.

**{w} –** /w/ – like “w” in “**w**ater”. If this {w} comes after a vowel
and you find difficult to pronouce it, you may pronounce it as the “w”
in “sho**w**”, whose sound actually is \[ʊ̯\] ({o} in SPT). It's a
semivowel.

**{y} –** /j/ – like “y” in “**y**ou” or the first part of “u” in
“t**u**be” /t**j**u:b/. It's a semivowel.

**{x} –** /ç/ – like the German “ich-laut” or, perhaps, “h” in
“**h**uman”. []{#anchor-32}Don't you know German? []{#anchor-33}There is
no problem, I teach you how to make this sound. Put the back of your
tongue against your hard palate, in the same way you make with the sound
{y}, then you try to pronounce this sound like you would do with {s}.

**{z} –** /z/ – like “z” in “**z**ap”.

**{§} –** /θ/ – like “th” in “bir**th**day” or “**th**in”, but *not*
like in “**th**is” whose sound is /ð/.

**{tc} –** /[]{#anchor-34}t͡ʃ/ – like “ch” in “whi**ch**” or “**ch**eap”

**{dj} –** /[]{#anchor-35}d͡ʒ/ – like “g” in “hu**g**e” or “j” in
“**j**ump”

**{ng} –** /nj/ or /[]{#anchor-36}ŋ/ – like “ng” in “thi**ng**” or
“ki**ng**”, whose sound is /[]{#anchor-37}ŋ/, but *not* like the “n” in
“thi**n**” or “ki**n**”.

**{nk} –** /ŋk/

I suggest you to learn the SPT very well before move on. Because it's a
very important part, I suggest you to make an exception and read this
chapter more than twice, I need that you know it very well.

[]{#anchor-38}SOME ENGLISH WORDS IN IPA AND SPT
-----------------------------------------------

Do you remember that I promised the transcription of some words of ours
to IPA and SPT? If you've reached here without understanding very well
both alphabets, let's see whether with the list below you understand
them. Deliberately I've inserted some words whose sounds don’t exist in
Sambahsa, I did it to make you pay attention to the differences between
the two languages, in these cases there wont a transcription to SPT.

It's important to highlight that the SPT was not made to transcribe
words of other languages that are not Sambahsa (!!!), what we're going
to do here is just a fun for you acquiring more intimacy with the SPT.

For you don't being lost, firstly I show the word, then the
transcription to IPA, which is between square brackets, and then the
transcription in SPT, which is between curly brackets.

**Men –** \[mɛn\] – {män}

**Man –** \[mæn\] – {män} *({*ä*} should be /*ɛ*/, but I think that I
can represent /*æ*/ as {*ä*})*

**Black –** \[blæk\] – {bläk}

**Flower –** \[ˈflaʊ̯ɚ\] – {flAoë} *({*ë*} should be /*ə*/, but I think
that I can represent /*ë*/ as \[*ɚ*\])*

**Fire –** \[ˈfaɪ̯ɚ\] – {fAyë}

**Breathe –** \[bɹiːð\] *(the sound /*ð*/ doesn't exist In Sambahsa)*

**Two –** \[tʰuː\] – {tu:} – *(SPT, as broad IPA transcription, can't
represent diacritics)*

**The –** \[ðə\] or \[ðiː\]

**Understand –** \[ˌʌndɚˈstænd\] – {ondëstÄnd} *(we can consider /*ʌ*/
as {*o*})*

**Letter –** \[ˈlɛɾɚ\] – {lÄrë}

**Turn –** \[tʰɚn\] – {tën}

**Good –** \[gʊd\] – {gud} – *(we can consider /*ʊ*/ as {*u*})*

**Until –** \[ənˈtʰɪɫ\] – {äntIl} – *(we can consider /*ɪ*/ as {*i*} and
/*ɫ*/ as {*l*})*

**Very –** \[ˈvɛɹi\] – {vÄri} *(we can represent /*ɹ*/ as {*r*})*

**Cheek –** \[t͡ʃiːk\] – {tci:k}

**Divide –** \[dɪˈvaɪ̯d\] – {divAyd}

**Thing –** \[θɪŋ\] – {§ing}

**Thin –** \[θɪn\] – {§in}

**North –** \[nɔːɹθ\] – {nor§}

**Father –** \[ˈfɑːðɚ\]

**Situation –** \[ˌsɪt͡ʃuːˈeɪ̯ʃn̩\] – {sItcu:eycn}

**Have –** \[hæv\] – {häv}

**Battleship –** \[ˈbæɾɫ̩ˌʃɪp\] – {bÄrlcip}

**Ring –** \[ɹɪŋ\] – {ring}

**Pleasure –** \[ˈpl̥ɛʒɚ\] – {pläjë}

**Emotion –** \[ɪˈmoʊ̯ʃn̩\] – {imOocn}

**Shy –** \[ʃaɪ̯\] – {cay}

**Human –** \[ˈhjuːmən\] or \[ˈçuːmən\] – {hyU:mën} or {xU:mën}

**Beautiful –** \[ˈbjuːɾɪfɫ̩\] – {byU:rufl}

**Enough –** \[ɪˈnʌf\] – {inOf}

**Kid –** \[kʰɪd\] – {kid}

**Feel –** \[fiːɫ\] – {fi:l}

**Sang –** \[sæŋ\] – {säng}

**Sink –** \[sɪŋk\] – {singk}

**Jam –** \[d͡ʒæm\] – {djäm}

**Hurry –** \[ˈhʌɹi\] or \[ˈhɚ.i\] – {hOri} or {hËi}

**Has –** \[hæz\] – {häz}

**Square –** \[skwɛɹ\] – {skwär}

**Twenty –** \[ˈtw̥ɛnti\] or \[ˈtw̥ɛ̃ɾ̃i\] – {twËnti} *(Sambahsa doens't
have nasal vowels)*

**Horse –** \[hɔːɹs\] – {ho:rs}

**Milk –** \[mɪɫk\] – {milk}

**Queen –** \[kw̥iːn\] – {kwi:n}

**Girl –** \[gɚɫ\] – {gël}

**When –** \[wɛn\] or \[ʍɛn\] – {wän}

**True –** \[tɹ̥uː\] – {tru:}

**Yellow –** \[ˈjɛloʊ̯\] – {yÄlOo}

**Star –** \[stɑːɹ\] – {sta:r} *(we can consider /*ɑ*/ as {*a*})*

**Thought –** \[θɔːt\] – {§o:t}

**Choice –** \[t͡ʃɔɪ̯s\] – {tcoys}

**Go –** \[goʊ̯\] – {gOo}

**Price –** \[pɹ̥aɪ̯s\] – {prays}

**Lot –** \[lɑːt\] – {la:t}

**Red –** \[ɹɛd\] – {räd}

It’s good to remember that the letter {r} from SPT can assume various
sounds, like /r/, /ɾ/, /ʁ/, /ʀ/ or /ɹ/.

[]{#anchor-39}A LAST VISIT TO THE ALPHABET AND THE NAME OF THE LANGUAGE
-----------------------------------------------------------------------

Now that you already know SPT, let's go back to the alphabet for you
know how each letter is pronounced:

a {a}, b {be}, c {tse}, d {de}, e {e}, f {ef}, g {dje}, h {hatc}, i {i},
j {jye}, k {ka}, l {el}, m {em}, n {en}, o {o}, p {pe}, q {ku}, r {er},
s {es}, t {te}, u {u}, v {ve}, w {we}, x {iks}, y {ü}, z {dzed}

Before we go on, it's important to know how our language is pronounced!

**Sambahsa Mundialect –** {sambA:sa mundyAlëkt}

[]{#anchor-40}WE ARE NOT FINISHED YET, SOME CONVENTIONS
-------------------------------------------------------

**Before we go on, a warning: **don't worry with the more complex
pronunciation rules, because I'll show you the pronunciation of each
example in almost all parts of this grammar, so you'll naturally learn
the pronunciation. *Don't memorize anything, understand it*! My advice
is: read this part once or twice – at most – then go on with your
studies. Does the orthography is too difficult? No, but it can be boring
at the beginning for those who are starting and it is not, in my
personal opinion, obligatory at the beginning since I'll dispose the
pronunciation with the examples.

Since here until the final of the **ALPHABET AND PRONUNCIATION**
chapter, the text will be almost a transcription of a chapter of the
book *The Grammar Of Sambahsa-Mundialect In English*, of Dr. Olivier
Simon.

The vowels are represented by V, they are: “a”, “e”, “i”, “o”, “u”. The
semivowels are “w” and “y”, they are represented by C, as well as the
consonants. The letters “w” and “y” can assume the function of vowels
when they're not related to any other vowel.

### []{#anchor-41}THE LETTER “e”:

When it is the stressed []{#anchor-42}syllable or the first letter of
the word, it's pronounced as {e}.

**incandescent {inkandEsënt} –** incandescent

**emigrant {emigrAnt} –** emigrant

It's not pronounced when alone in the final of a word…

**monte {mont} –** mount *(an animal)*

**claviature {klavyatÜr} –** keyboard

[]{#anchor-43}**clientele {klyentEl} – **customers

… or at the end of a word before the letters “s” and “t”.

**crimes {krims} – **crimes

**survivet {survIvd} –** *(he/she/it)* survives

In some cases the loss of “e” can create a confusion or make the word
unpronounceable, in these cases “e” assumes the sound {ë}.

[]{#anchor-44}**storgnet {stOrnyët} –** stunned

*Why do “gn” of “storgnet” is pronounced as {ny}? I explain it later.*

All words ending with “quet” and “ques” are pronounced, respectively, as
{kët} e {kës}.

In all the other cases “e” has the {ë} sound, like in “kohlen”, which is
pronounced as {kO:lën}.

*Why do the pronounce of “o” is lengthened in this word? I am going to
answer you.*

### []{#anchor-45}THE LETTER “h”:

The {h} sound appears when “h” is at the beginning of a word or between
vowels.

**habe {hab} –** have

**rahat {rahAt} –** rest

[]{#anchor-46}When the letter “h” is after a vowel and it is not
followed by other vowel, the letter “h” serves to indicate that the
pronunciation of the vowel must be lengthened.

[]{#anchor-47}**kohlen {kO:lën} –** hiden

**bahsa {bA:sa} –** language

**bah {ba:} –** to say, to speak

Maybe you ask me: “don't would be much more simple to write the vowel
twice to indicate the lengthened of its pronounce? Instead to write
'bahsa', would be much more simpler to write 'baasa'?”

**The answer is:** No! This would produce a different sound, “baasa”
would be pronounced as {baAza}

If the letter “h” is after a diphthong, like in “credeih”, which means
“to believe”, you lengthen the pronunciation of the main vowel then you
release the semivowel.

**addeih {adE:y} –** to add

**wehrgeih {we:rdjE:y} –** to play, to run

“gh”, “bh” e “dh” respectively correspond to {g}, {b} and {d}, the “h”
in these letters serves to indicate that they will not suffer any kind
of modification.

[]{#anchor-48}VOWELS
--------------------

**“eau” {o:} –** /o:/ like in “bureau” {bürO:} *(bureau, office)*

**“aa” {aA} –** /a'a/ – like in “saat” {saAt} *(hour, clock, watch)*

**“ae”, “ae” {ay} –** /aj/ – like “[]{#anchor-49}yoinkjiae” {yoynkjiAy}
*(musical scale)*

**“ai” {ä} –** like in “caise” {käz} *(queijo)*

**“au” {Ao} –** \[aʊ̯\] – like in “Australia” *{aostrAlya} (Australia)*

**“ea” {Ea} *****(***[]{#anchor-50}***at the end of a word****)***** –**
/'ea/ – like in “wakea” {wakEa} *(definitely)*

**“ea” {ëa} *****(at the end of a word)***** –** /əa/ – like in “ocean”
{ots(ë)An} *(ocean) Notice that as the sounds {ë} and {a} are similar,
in practice the “e”, in this case, normally ends being absorbed, in
other words, it is not pronounced. In this case, any final consonant –
except “s” – shifts the stress on “a”, and leads to that pronunciation
different from “wakea”.*

**“ee” {Eë} –** /eə/ – like in “eet” {Eët} *(*(he/she/it)* was)*

**“eo” {Eo} –** \[eʊ̯\] – like in “fianceo” {fyantsEo} *(fiance)*

**“eu” {ö} –** like in “Europe” {örOp} *(Europa)*

**“ie” {i:} *****(when alone at the end of a word***** –** like in
“publie” {publI:} *(publish)*

**“ie” {ye} *****(in all the other casees)***** –** /je/ – like in
“publiet” {publyEt} *(published)*

**“iu” {yu} –** /ju/ – like “iu” in “siuk” {syuk} *(dry)*

**“oe”, “oi”, e “oy” {oy} –** /oj/ – like in “[]{#anchor-51}choengju”
{tcOyngju} *(rice wine)*

**“oo” {oO} –** /o'o/ – like “boot” {boOt} *(boat)*

**“ou” {u:} –** /u:/ – like in “courage” {ku:rAdj} *(courage)*

**“ue” {ü:} –** like in “continue” {kontinÜ:} *(continue)*

**“ui” {wi} **– /wi/ – like in “[]{#anchor-52}tuich” {twitc} *(empty)*

**“uy” {uy} –** /uj/ – like in “[]{#anchor-53}lastruym” {lastrUym}
*(hold *(boat)*)*

**“u” {u} *****(“u” has the sound {u}, but it would be {ü} if there is
the letter “e” between the two following letters)***** –** like in “bur”
{bur} *(ashes)*, but “bureau” is {bürO:}.

[]{#anchor-54}SEMIVOWELS
------------------------

“w” and “y” respectively are {w} and {y} when combined to vowel, but
when isolates the letter “w” is pronounced like a short {u}, more
precisely \[ŭ\] or \[ʊ̆\], and the letter “y” is like {ü}.

[]{#anchor-55}**sehkwnt {sE:kunt} –** (they) follow

**type {tüp} –** guy, type

But when the letter “y” finds itself at the final of a word or followed
by a -s, “y” e “ys” respectively will have the sounds {i} and {is}.

**baby {bAbi} – **baby

**babys {bAbis} –** babies

[]{#anchor-56}CONSONANTS
------------------------

**“sch” {c} –** like in “[]{#anchor-57}muraischmusch” {muräcmUc} *(mush
fly)*

**“ch” {tc} *****(it's {k} when before a consonant)***** –** like in
“cheus” {tcös} *(choose)* and “Christ” {krist} *(Christ)*.

**“gn” {ny} –** /nj/ – like in “gnoh” {nyo:} *(to know)*

**“kh” {q} –** like in “khiter” {qItër} *(evil)*

**“ph” {f} –** like in “philosophia” {filozOfya} *(philosophy)*

**“qu” {kw} *****(when before “a”, “o” e “u”, or {k} when before “e”,
“i” e “y”)***** –** \[kw\] or \[kʷ\] – like in “quod” {kwod} *(what)*
and “quis” {kis} *(who)*.

**“sc” {sk} *****(it's {s} when before “e”, “i” e “y”)***** –** “scutt”
{skut} *(shake)* and “science” {syents} *(science)*

**“sh” {x} –** like in “[]{#anchor-58}shienciu” {xyEntsyu} *(overgrown)*

**“ss” {s} –** like in “permission” {përmisyOn} *(permission)*

**“th” {§} *****(it's{t} when combined with {s}, {c} or {j} *****) –**
like in “[]{#anchor-59}thamf” {§amf} *(stench)* and
“[]{#anchor-60}esthetic” {estEtik} (*aesthetical)*.

**“c” {k} *****(it's {ts} (/t͡s/) when before “e”, “i” e “y”)***** –**
like in “condition” {kondityOn} *(condition)* and “petrificit”
{pëtrifItsit} *(petrified)*

**“g” {g} *****(it's {dj} when before “e”, “i” e “y”)***** –** like in
“gulf” {gulf} *(gulf)* and “large” {lardj} *(large)*.

**“j” {j} –** always {j}.

**“r” {r} –** as there are people from different backgrounds, it's
admited a wide variety of manners of how this sound is made, including
our “r”! The recommended form – but it's not compulsory – it's the way
spoken in Luxembourg or Saarland, /ʀ/. However this sound can take other
forms:

**“rr” /r(r)/ and “rh” /r(h)/ –** like the Spanish “rr” (/r/). Don't you
know Spanish? There is no problem, I teach you how to make this sound.
It's made by the trill of the tongue in the alveolar ridge, but know
that the movement of the tongue is more made by the airflow than the
tongue itself. It's like the /ɾ/ of “be**tt**er”, but you have multiple
vibrations of the tongue instead of a single vibration of /ɾ/.

**“rl” –** like the Japanese “r” (/ɺ/), it may also be pronounced as
/rl/ or /ʁl/. Don't you know Japanese? There is no problem, I explain
it: it's very similar to the /ɾ/ of “be**tt**er”, but you must hold your
tongue during a tiny bit of time in order to the air pass at the
laterals of your tongue instead of the center of the tongue.

**“s” {s} *****({z} when between vowels)***** –** “son” {son} *(son)
and* “decision” {dëtsiziOn} *(decision)*

**“x” {ks} –** /ks/ – it may turn {gz}, if it facilitates the
pronunciation.

**“z” {dz} –** /d͡z/ – like in “[]{#anchor-61}zangir” {dzAndjir}
*(chain)*

[]{#anchor-62}MORE OBSERVATIONS
-------------------------------

Some letters, especially those at the end of the word, may be modified
by the neighbor sounds. Like in the case of “hands” {handz} (hands).

Some consonantes, as well as the vowel {ë}, may be omitted. The word
“franceois” officially is {frantsëOys}, but it may be {fransOys} because
of {t} is inside a consonanl group and the unstressed vowel {ë} is close
to the stressed vowel {o}.

If the verbal form starts with {s*C*}, oi- may be added before this word
for euphonic reasons. For example “skap”, which becomes “oiskap”.

[]{#anchor-63}STRESS ACCENT IN SAMBAHSA
---------------------------------------

[]{#anchor-64}Start analyzing the word since the last syllable

### []{#anchor-65}ALWAYS RECEIVES ACCENTUATION

Vowels before the letter “h” or a double consonant (rr”, “ll”, “tt” …),
by double consonant also is understood the “ck”{k(k)}. The same is said
about a syllable before a final -e. There some examples:

[]{#anchor-66}**Prodah –** {prodA:} *(hand over *(a criminal)*)\
*[]{#anchor-67}*F*[]{#anchor-68}*or you ro understand: if the word would
“proda”, *[]{#anchor-69}*the pronunciation would be {prOda}*

[]{#anchor-70}**Recess –** {rëtsEs} *(recess)*

F[]{#anchor-71}or you ro understand: if the word would “reces”, the
pronunciation would be {rEtsës}

[]{#anchor-72}**Frontdeck –** {frondEk} *(foredeck)*

F[]{#anchor-73}or you ro understand: if the word would “frontdec”, the
pronunciation would be {frOndëk}

**Taslime –** {taslIm} *(surrender)*

The first of two closed vowels, with exception of “i” and “u” as
semivowels.

**Armee – **{armEë} *(army)*

**Australia –** {aostrAlya} *(Australia)*

The final syllable: -in *(but not -ing)*, ey, ie, ui *(when the
pronunciation {wi})*, oCel *(where C is the only consonant)*.

**Hotel –** {hotEl} *(hotel)*

[]{#anchor-74}**Suadin – **{swadIn} *(fair weather)*

**Reling –** {rEling} *(railing)*

**Kierey –** {kyerEy} *(ram) *

The vowels “a”, “o” and “u” when before a consonant or semivowel, with
exception of a sole “s”.

**Cadaloc – **{kadalOk} *(anywhere)*

**Naval –** {navAl} *(naval)*

**Dayluk –** {daylUk} *(continent)*

### []{#anchor-75}[]{#anchor-76}NEVER IS ACCENTUATED

Prefixes.

[]{#anchor-77}***For*****trehc –** {fortrE:k} *(away; to depart for a
trip)*

*Re*cess

***Be*****vid** – {bëvId} *(to show, to prove)*

**For- –** prefix[]{#anchor-78} that means something far

**Re- –** prefix that means repetition

**Be- –** prefix that means something factive

The letter “w” when used as vowel.

**Sehkwnt –** {sE:kunt} *(*(they)* follow)*

A word finishing in -(i)um, -ule e -s.

[]{#anchor-79}**Schives –** {civz} *(to shift)*

[]{#anchor-80}**Territorium –** {territOryum} *(territory)*

**Insule –** {Insül} *(island)*

A alone vowel or semivowel at the end of a word.

**Okwi –** {Okwi} *(eyes)*

**Baby –** {bAbi} *(baby)*

The vowels “e”, “i” and “y” at the end of a word followed by a semivowel
– with the exception of “ey” – or two or diverse consonants, but not the
double ones.

[]{#anchor-81}**Seghel – **{sEgël} *(sail)*

**Tolkit –** {tOlkit} *(*(he/she/it)* talked)*

**Khitert –** {qItërt} *(evil *(noun)*)*

In compound words the stressed syllable stays the same of the original
word.

**Gouverne –** {gu:vErn} *(to govern)*

**Gouvernement –** {gu:vErnëmënt} *(government)*

**Nest –** {nest} *(nest)*

**Corcuksnest –** {korkUksnëst} *(crow's nest)*

These rules don't necessarily apply to proper nouns and the use of
hyphen preserves the accentuation in both sides.

[]{#anchor-82}IF YOU FOUND SOME SOUNDS VERY DIFFICULT
-----------------------------------------------------

I did my best to teach you each sound, but if you really can't learn
some of them – but I believe you can – it's not the end of the world!!!
If you can't pronounce sounds like /x/ or /ç/, no-one you put you in a
jail if you use /ʃ/ and /h/ in their places. If you can't pronounce the
Japanese “r”, use our “r”. The good news is that the difficult sounds
are rarely used, with the exception of the vowels not found in English
language.

The advantage of learning the phonetics of Sambahsa is that you master
the phonetics of many other languages.

[]{#anchor-83}SELLAMAT!
=======================

Sellamat! It's with this word, “sellamat” {selamAt}, that we say
“hello”, you also may say “salut” {salUt}, which have the same function.

In this chapter I'll show you some sentences, maybe you'll deduce the
elements of each one by youself, but, anyway, in posterior chapters I'll
unravel each sentence's element, so don't worry if you don't understand
something in this chapter.

Different from English and most of the auxiliary languages, the Sambahsa
verbs are conjugated. All verbs are regular, with the exception of the
verbs “to be”, “to have” and “to know”, but they are not difficult.
Let's start with some basic sentences:

**Som **[]{#anchor-84}[]{#anchor-85}**John {som **[]{#anchor-86}**djOn}
–** []{#anchor-87}I am John

**Io som **[]{#anchor-88}**John {yo som djOn}***** *****–**
[]{#anchor-89}I am John

**Ego som **[]{#anchor-90}**John {Ego som djOn}***** *****–** I am John
*(more emphasis at “I”)*

Notice that, in the same way as occurs in the Spanish language, in most
cases you may omit the pronoun, because the own verb indicates the used
pronoun. You've just met the verb correspondent to “to be” when linked
to the pronoun “I”. Let's to the next sentences:

**Es **[]{#anchor-91}**Robert {es **[]{#anchor-92}**rObërt } –**
[]{#anchor-93}you are Robert

**Es **[]{#anchor-94}**Linda {es **[]{#anchor-95}**lInda} –**
[]{#anchor-96}you are Linda

**Tu es **[]{#anchor-97}**Robert {tu es **[]{#anchor-98}**rObërt } –**
[]{#anchor-99}you are Robert

**Tu es Linda {tu es lInda} –** you are Linda

**Is est Robert {is est rObërt} –** he are Robert

**Ia est Linda {ya est lInda} –** she are Linda

[]{#anchor-100}**Smos **[]{#anchor-101}**Americans {smos
**[]{#anchor-102}**amerikAns} –** []{#anchor-103}we are Americans

**Wey smos Americans {wey smos amerikAns} –** we are Americans

**Sambahsa est facil {sambA:sa est fAcil} –** Sambahsa is easy

The next cases are fairly interesting:

**Id est gohd {id est go:d} –** it is good *(neutral)*

**El est gohd {el est go:d} –** he/she is good *(undetermined)*

**UNDERSTAND IT VERY WELL:** neutral pronouns refers to things,
undetermined pronouns are used when it's not known the genre or it's not
desired to say the genre. **Pay attention:** from now, all pronouns or
articles that I *don't* indicate as *neutral* or *undetermined*, will
have the masculine genre or the feminine genre, but I can specify
whether the word is related to a male or female being if it's really
necessary, when the word itself can't informe to which gender it is
related. Are we understood?

To make clear what is *neutral* and what is *undetermined*, look the
examples:

**Chair – **to the best of my knowledge, chairs don't have genre, so it
is *neutral*

**Dog –** if we don't know its genre, so it is *undetermined*

**Female dog –** feminine

**Grandfather –** masculine

**Tree –** even it is a living creature, it has not genre *(although you
can argue about dioecious plants)*, so it's *neutral*

**Daughter –** feminine

**Child –** as the genre is not specified, it's *undetermined*

**Ectoplasm – *****like in “the ectoplasm was exteriorized by the
medium”***** –** it's *neutral*, because “ectoplasm” here is considered
as a substance

**Ectoplasm – *****like in “the ectoplasm spoke to me”***** –** it can
be *masculine*, *feminine* or *undetermined*

**Android –** you decide, it depends of the context and point of view of
each one, it can be *neutral*, *undetermined* or even have a genre.

In case of doubt whether the word must be considered as *neutral* or
*undetermined*, use the *undetermided *form.

We've known the basic pronouns and their verbs, now let's see more
pronouns and verbs in the plural, let's use the word “prient” {pryent},
which means “friend”, and “gohd” {go:d}, which means “good”, in our
examples:

**Smos prients {smos pryents} –** []{#anchor-104}we are friends

**Wey smos prients {wey smos pryents} –** we are friends

**Ste prients {ste pryents} –** []{#anchor-105}you are friends

**Yu ste prients {yu ste pryents} –** you are friends

[]{#anchor-106}[]{#anchor-107}**Yu ste prient {yu ste pryent} –** you
are friend *(notice that this one is in the singular)*

**Tu es prient {tu es pryent} –** you are friend *(equal to the sentence
just above, but this is informal)*

**Ies sont prients {yes sont pryents} –**
[]{#anchor-108}[]{#anchor-109}they are friends *(all of masculine
genre)*

**Ias sont prients {yas sont pryents} –**
[]{#anchor-110}[]{#anchor-111}they are friends *(all of feminine genre)*

**Ia sont gohd {ya sont go:d} –** they are goods *(neutral)*

**I sont prients {i sont pryents} –** they are friends *(undetermined)*

I believe I don't need to explain none of these sentences, I only have
to say that when “yu” is used in the singular, it is for formal
situation, with people you have no intimacy, therefore a courtesy
pronoun, and you use “tu” with close friends, family and kids.

Notice that, even in the singular, the pronoun “yu” make use of the verb
“ste”, because Sambahsa works as in French or Russian, the courtesy
pronoun, “yu”, stays in the plural, even when referring to only one
person. An example from French: “Vous êtes un ami/des amis”; an example
from Russian: “[]{#anchor-112}Вы остаётесь моим(и) другом/друзями” (you
remain my friend(s)). Only the context can informe whether the “yu” is
in singular or plural.

Other important observation about the second person of plural “yu”. You
already know that, in most cases, it's not necessary the use of the
pronoun, but in future examples you'll notice that the pronunciation of
the third person of singular and the second person of plural are very
similar, look:

**Is lieubht me {is liÖb*****t***** me} –** he loves me

**Yu lieubhte me {yu liÖb*****t***** me} –** you love me

Did you imagine if didn't exist the pronoun there? That's why the
pronoun “yu” always have to appear.

It's important that you know how to say the verb “there be”, in the
sense of existence. It's is pretty similar to English, I'll use the noun
“anghen” {Angën}, which means “person”, and the adverb “her” {her},
which means “here”, in the following examples:

**Sont anghens her {sont Angëns her} –** []{#anchor-113}there are
persons here

**Ter sont anghens her {ter sont Angëns her} –** there are persons here

Yes, you may use or not the “ter” if you want.

[]{#anchor-114}USEFUL WORDS AND SENTENCES
=========================================

There is no problem whether you don't know how certain sentences were
made, everything will be explained in the next chapters.

**Sellamat {selamAt} –** hello, hi

**Salut {salUt} –** hello, hi

**Ya {ya} –** yes

**Si {si} –** yes *(answer for a negative question)*

**No {no} –** no

**Sell dien {sel dyen} –** good morning

**Sell **[]{#anchor-115}**posmiddien {sel posmiddyEn} –** good afternoon

**Sell vesper {sel vEspër} –** good evening

**Sell noct {sel nokt} –** good night

**Dank {dank} –** []{#anchor-116}thank you

**Mersie {mërsI:} –** thank you

**Spollay dank {spolAy dank} –** thank you very much

**Obligat {obligAt} –** you're welcome *(when someone thanks you)*

**Plais {pläs} –** please

**Kam leitte yu? {kam leyt yu} –** how are you?

[]{#anchor-117}**Sellgumt {selgUmt} –** welcome

**Leito {lEyto} –** fine

**Chao {tcAo} –** []{#anchor-118}good bye

**Khuda hafiz {qUda hAfidz} –** good bye

**Do reviden {do rëvIdën} –** good bye

**Prosit {prosIt} –** good bye *(desiring good luck)*

**Tiel mox {tyel moks} –** see you soon

**Ne gnohm {në nyo:m} –** I don't know

**Excuset me {ekskÜzd me} –** excuse me

**Maaf {maAf} –** sorry

**Pardon {pardOn} –** pardon me

**OK(ey) {ok(Ey)} –** OK

**Tamam {tamAm} – **OK

**Tabrick {tabrIk} –** congratulations

**Sell appetite {sel apëtIt} –** []{#anchor-119}good appetite

**Marba {mArba} –** pleased to meet you

[]{#anchor-120}**Ne bahm maung Sambahsa {ne ba:m mAong sambA:sa} –** I
don’t speak much Sambahsa.

**Kam yarat ste yu? {kam yarAt ste yu} –** how old are you?

**Quod est vies nam? {kwod est vyes nam} –** what's your name?

**Mien nam est … {myen nam est} –** my name is…

**Quetos yu? {kEtos yu} –** where do you come from?

**Io ne prete {yo ne prEt} –** I don't understand
*(*[]{#anchor-121}*what you wanted to say)*

**Maghte yu hehlpe me? {magt yu he:lp me} –** can you help me?

**Ye quod saat? {ye kwod saAt} –** what time?

**Ne ho pretet hol / Ne ho preten hol {ne ho prEtët hol / ne ho prEtën
hol} –** I haven’t understood everything *(what you wanted to say)*

**Aun sibia {Aon sIbja} –** changing the subject; on a different note

Good, at least you can start and finish a conversartion, continue
studying and you'll be able to maintain a conversation.

[]{#anchor-122}THE DECLINATION CASES
====================================

Sambahsa has an interesting caracteristic, it has a system of
declination cases that are: *nominative*, *accusative*, *dative* and
g*enitive*. These cases refers to the pronouns and articles. But what
they are and how they are used?

Before let's remember those English classes of your school time, look
the sentence below:

**The man buys the car:** In this case “the man” is the *subject* of the
sentence because it's him who is *doing* the action, which in this case
is the buying; and “the car” is the *direct object* of the sentence,
because it's the car that is *suffering* the action, it's being
*directly affected* by the verb. Look the sentences below:

**I eat tomato –** “I” ([]{#anchor-123}subject), “tomato”
([]{#anchor-124}direct object)

**He loves Mary –** “he” (subject), “Mary” (direct object)

**People walk on the streets –** “people” (subject). Ops, this sentence
doesn't have a []{#anchor-125}direct object!

**They gave me a message:** we have one more element in this sentence,
it's the indirect object. We can easily recognize the subject, which is
“they”, but which one is the direct object and which one is the indirect
object? Remember, the direct object is the one that directly suffers the
action; what is being given, the person or the message? The message! The
direct object is “a message”. The indirect object is that is *beneficed*
by the action, which is being *indirectly* affected by the verb, which,
in this case, is “me”. Look the sentences below:

**I've sent you the documents –** “I” (subject), “you” (indirect
object), “documents” (direct object)

**She bought a dog for him –** “she”(subject), “dog” (direct object),
“him” (indirect object)

[]{#anchor-126}Now we can see the cases. I want to warn you that I'll
only show you the cases for now, just for you recognize them by sight, I
will only show explanations in sentences after I explain the verbs, when
we'll be more comfortable to make sentences.

[]{#anchor-127}NOMINATIVE CASE
------------------------------

Basically it is the subject of a sentence.

**Ego / io {Ego / yo} –** I

**tu {tu} –** you

**Is {is} –** he

**Ia {ya} –** she

**Id {id} –** it *(neutral)*

**El {el} –** he / she / it []{#anchor-128}*(undetermined)*

**Wey {wey} –** we

**Yu {yu} –** yu

**Ies {yes} –** they []{#anchor-129}[]{#anchor-130}*(masculine)*

**Ias {yas}–** they []{#anchor-131}[]{#anchor-132}*(feminine)*

**Ia {ya} –** they *(neutral)*

**I {i} –** they []{#anchor-133}*(undetermined)*

PAY ATTENTION!!!!!!!! From the words below, just worry to initially
learn the underlined ones. The others you can learn over time, you don't
need be in a hurry!!!!!!

**So {so} –** this *(masculine)*

**Toy {toy} –** these *(masculine)*

**Sa {sa} –** this *(feminine)*

**Tas {tas} –** these *(feminine)*

**Tod {tod} –** this *(neutral)*

**Ta {ta} –** these *(neutral)*

**Tel {tel} –** this []{#anchor-134}*(undetermined)*

**Ti {Ti} –** these* *[]{#anchor-135}*(undetermined)*

**Cis {tsis} –** that []{#anchor-136}*(masculine)*

**Cies {tsyes} –** those []{#anchor-137}*(masculine)*

**Cia {tsya} –** that *(feminine)*

**Cias {tsyas} –** those []{#anchor-138}*(feminine)*

**Cid {tsid} –** that *(neutral)*

**Cia {tsya} –** those *(neutral)*

**Cel {tsel} –** that *(undetermined)*

**Ci {tsi} –** those *(undetermined)*

**Qui {ki} –** who *(masculine / singular)*

**Quis {kis} –** who *(masculine / singular / interrogative)*

**Quoy {kwoy} –** who *(masculine / plural)*

**Qua {kwa} –** who *(feminine / singular)*

**Quas {kwas} –** who []{#anchor-139}*(feminine / plural)*

**Quod {kwod} –** what *(neutral)*

**Qua –** what *(neutral)*

**Quel {kel} –** who []{#anchor-140}*(undetermined)*

**Qui {ki} –** who *(undetermined)*

**Neis {neys} –** none *(masculine / singular)*

**Noy {noy} –** none *(masculine / plural)*

**Nia {nya} –** none *(feminine / plural)*

**Nias {nyas} –** none *(feminine / plural)*

**Neid {neyd} –** []{#anchor-141}none, nothing *(neutral / singular)*

**Nia {nya} –** none, nothing *(neutral / plural*

**Nel {nel} –** none *(undetermined / singular)*

**Nei {ney} –** none* (undetermined / plural)*

**To {to} –** generic pronoun*, when the sense of approximation doesn't
apply, it can be translated as “this” and “that”*

**Quo {kwo} **– the same as “to”, but this one can be translated to
“what”.

Please see the subchapter “How work generic pronouns like “to” and
“quo”?” from the chapter “Common mistakes and questions”.

The pronoun “ego” is the emphatized form of “io”. When we use “ego”
instead “io”, we want to say that there is a certain relevance in the
“I” that is doing or suffering this or that action.

I'll ask you to stop here for a while and study the pronouns (only the
personal pronouns!), because I will use them a lot from here.

About the other cases, it's enough read them once or twice at most, you
don't need record everything now! Follow the same method of study you
made with the nominative case, focus on the personal pronouns and in
those I indicate.

[]{#anchor-142}ACCUSATIVE CASE
------------------------------

Basically is the direct object of a sentence.

**Nominative case –** accusative case

**Ego / io – **me {me}

**Tu **– te {te}

**Is – **iom {yom}

**Ia –** iam {yam}

**Id –** id {id}

**El –** el {el}

**Wey –** nos {nos}

**Yu –** vos {vos}

**Ies –** iens {yens}

**Ias – **ians {yans}

**Ia –** ia {ya}

**I –** i {i}

[]{#anchor-143}Don't be in a hurry to learn the pronouns from below!

**So –** tom {tom}

**Toy –** tens {tens}

**Sa –** tam {tam}

**Tas –** tans {tans}

**Tod –** tod

**Ta –** ta

**Tel –** tel

**Ti –** ti

**Cis–** ciom {tsyom}

**Cies –** ciens {tsyens}

**Cia –** ciam {tsyam}

**Cias –** cians {tsyans}

**Cid –** cid

**Cia–** cia

**Cel –** cel

**Ci –** ci

**Qui –** quom {kwom}

**Quis –** quom

**Quoy –** quens {kens}

**Qua –** quam {kwam}

**Quas –** quans {kwans}

**Quod –** quod

**Qua –** qua

**Quel –** quel

**Qui –** qui

**Neis – **niom {nyom}

**Noy** – niens {nyens}

**Nia –** niam {nyam}

**Nias –** nians {nyans}

**Neid** – neid {neyd}

**Nia –** nia {nya}

**Nel –** nel {nel}

**Nei –** nei {ney}

**To – **to

**Quo –** quo

[]{#anchor-144}DATIVE CASE
--------------------------

It's the indirect object of a sentence. **Attention: **often everything
after a preposition is treaten as it is in the accusative case, no
dative!

**Nominative case –** dative case

**Ego/io – **Mi {mi}

**Tu **– Tib {tib}

**Is –** Ei {ey}

**Ia –** Ay {ay}

**Id –** ei {ey}

**El –** al {al}

**Wey –** nos {nos}

**Yu –** vos {vos}

**Ies –** ibs {ibz}

**Ias – **iabs {yabz}

**Ia –** ibs {ibz}

**I –** im {im}

Don't be in a hurry to learn the pronouns from below!

**So – **tei {tey}

**Toy –** tibs {tibz}

**Sa –** tay {tay}

**Tas –** tabs {tabz}

**Tod –** tei {tey}

**Ta –** tibs {tibz}

**Tel –** tal {tal}

**Ti –** tim {tim}

**Cis–** cei {tsey}

**Cies –** cibs {tsibz}

**Cia –** ciay {tsyay}

**Cias –** ciabs {tsyabz}

**Cid –** cei {tsey}

**Cia–** cibs {tsibz}

**Cel –** cial {tsyal}

**Ci –** cim {tsim}

**Qui –** quei {key}

**Quis –** quei

**Quoy –** quibs {kibz}

**Qua –** quay {kway}

**Quas –** quabs {kwabz}

**Quod –** quei

**Qua –** quibs

**Quel –** qual {kwal}

**Qui –** quim {kim}

**Neis – **nei {ney}

**Noy** – neibs {neybz}

**Nia –** niay {nyay}

**Nias –** niabs {nyabz}

**Neid** – nei {ney}

**Nia –** neibs {neybz}

**Nel –** nal {nal}

**Nei –** nim {nim}

**To – **ad to

**Quo –** ad quo

If you forget the dative form of the pronoun, you can use the
preposition “ad”. Some examples below:

**mi =** ad me

**tib =** ad te

**tal =** ad tel

quei = ad quod

[]{#anchor-145}GENITIVE CASE
----------------------------

It's the case of the possessor. Firstly let's see the list of genitives
then let's understand them

**Nominative case –** genitive case

**Is –** ios {yos}

**Ia –** ias {yas}

**Id –** ios {yos}

**El –** al {al}

**Ies –** iom {yom}

**Ias –** iam {yam}

**Ia –** iom {yom}

**I –** im {im}

Don't be in a hurry to learn the pronouns from below!

**So – **tos {tos}

**Toy –** tom {tom}

**Sa –** tas {tas}

**Tas –** tam {tam}

**Tod –** tos

**Ta –** tom

**Tel –** tal {tal}

**Ti –** tim {tim}

**Cis–** cios {tsyos}

**Cies –** ciom {tsyom}

**Cia –** cias {tsyas}

**Cias –** ciam {tsyam}

**Cid –** cios

**Cia–** ciom

**Cel –** cial {tsyal}

**Ci –** cim {tsim}

**Qui –** quos {kwos}

**Quis –** quos

**Quoy –** quom {kwom}

**Qua –** quas {kwas}

**Quas –** quam {kwam}

**Quod –** quos

**Qua –** quom

**Quel –** qual {kwal}

**Qui –** quim {kim}

**Neis – **nios {nyos}

**Noy** – niom {nyom}

**Nia –** nias {nyas}

**Nias –** niam {nyam}

**Neid** – nios {nyos}

**Nia –** niom {nyom}

**Nel –** nal {nal}

**Nei –** nim {nim}

**To –** os to

**Quo –** os quo

You ask me: *may I, for example, substitute “ios” for “os is”, “ias” for
“as ia”, “tal” for “os tel”, “quos” for “os quod” and so on?* It's
preferable that you don't do that.

Let's see some examples

**Id apel ios dru {id Apël yos dru} –** the apple of the tree.

**Id hand al person {id hand al përsOn} –** the hand of the person

**Id augos cios wir – {id Aogos tsyos wir}** **–** the power of that man

**Ia quitances tos munt {ya kitAntsës tos munt} –** the bills of this
month

[]{#anchor-146}DECLINATIONS
===========================

Used as a complement in diverse words.

[]{#anchor-147}SINGULAR
-----------------------

[]{#anchor-148}[]{#anchor-149}[]{#anchor-150}**Nominative masculine –**
-o(s)

[]{#anchor-151}[]{#anchor-152}**Accusative masculine –** -o / -um

[]{#anchor-153}[]{#anchor-154}**Dative masculine –** -i

[]{#anchor-155}[]{#anchor-156}**Genitive masculine –** -(io)s

[]{#anchor-157}[]{#anchor-158}**Nominative feminine –** -a

[]{#anchor-159}[]{#anchor-160}**Accusative feminine –** -u

[]{#anchor-161}[]{#anchor-162}**Dative feminine –** -i

[]{#anchor-163}[]{#anchor-164}**Genitive feminine –** -(ia)s

[]{#anchor-165}[]{#anchor-166}**Nominative neutral –** -o / -um

[]{#anchor-167}[]{#anchor-168}**Accusative neutral –** -o / -um

[]{#anchor-169}[]{#anchor-170}**Dative neutral –** -i

[]{#anchor-171}[]{#anchor-172}**Genitive neutral –** -(io)s

[]{#anchor-173}**Nominative undetermined –** -is

[]{#anchor-174}**Accusative undetermined –** -em

[]{#anchor-175}**Dative undetermined –** -i

[]{#anchor-176}**Genitive undetermined –** -(e)s

[]{#anchor-177}PLURAL
---------------------

**Nominative masculine –** -i

**Accusative masculine –** -ens

**Dative masculine –** -ims

[]{#anchor-178}**Genitive masculine –** -(e)n

**Nominative feminine –** -as

**Accusative feminine –** -ens

**Dative feminine –** -ims

**Genitive feminine –** -(e)n

**Nominative neutral –** -a

**Accusative neutral –** -a

[]{#anchor-179}**Dative neutral –** -ims

**Genitive neutral –** -(e)n

**Nominative undetermined –** -i

**Accusative undetermined –** -ens

**Dative undetermined –** -ims

**Genitive undetermined –** -(e)n

But, how the declinations are used? Sometimes their use is compulsory,
as in the words “vasyo” (everything) and “alyo” (another):

**Vasyas gwens –** all the women

**Alyo wir –** another man

**Vasyi paters –** all the fathers

**Alyo stul –** another chair

**Alya stuls –** another chairs

**Vasyo est correcto –** everything is correct

For euphonics effects or literary proposals, like poetry, you can use
these declinations in other words, for example the indefinite article
“un”, but these declinations often only are applied to the words “vasyo”
and “alyo”. Remember however that those “euphonic” declensions can be
used only if they are compatible with the accentuation of the word

The sentence “un bell pwarn” {un bel pwarn}, which means “a pretty boy”,
may be written as “uno bello pwarn” {Uno bElo pwarn}.

The plural of “bell plaj” may be written as “bella plaja”.

Remember that, also, there is the genitive case, which represents
possession.

**Henrique's book –** Henriques buk

**Matheus' house –** Matheus*io*s dom *(notice that I've used the
complete termination because “Matheus” already finishes with “s”)*

**Woman's son –** gwens son\
**America's beauty –** Americas beauteit

There is no mistery in the use of the declination in the accusative, you
only have to apply in the direct object. About the declination in the
genitive, I'll teach it after you learn the verbs, I think it's better
in this way.

[]{#anchor-180}THE ARTICLES
===========================

[]{#anchor-181}DEFINITE ARTICLES
--------------------------------

A characteristic of Sambahsa that stand out is its definite articles,
the words are the same of the pronouns. How? Look the translations
below:

**The man –** is wir {is wir}

**The woman –** ia gwen {ya gwen}

**The dog –** el kwaun {el kwAon}

**The person –** el anghen {el Angën} *or* el person {el përsOn}

**The person –** is anghen *(masculine person)*

**The country –** id land {id land}

**The people *****(from a determined culture)***** – **id folk {id folk}

**The people *****(low social class)***** –** id popule {id pOpül}

Can you understand how the language works? In the Sambahsa logic you
don't speak “the secreatry” or “the man”, but “she secretary” and “he
man”. Bear in mind that the articles suffers the declination as well as
the pronouns.

***The***** dog loves *****the***** woman – ***el* kwaun lieubht *iam*
gwen {el kwAon liöbt yam gwen}

You could not translate “The dog loves the woman” as “el kwaun lieubht
*ia* gwen” because the definite article that is linked to the
substantive “woman” must suffer the declination to accusative.

Another example, but involving the dative case too:

***The***** *****(female)***** manager sent *****the***** e-mail
*****for the***** *****(male)***** president –** *ia* manager yisit *id*
blixbrev *ei* president.

[]{#anchor-182}INDEFINITE ARTICLE
---------------------------------

The indefinite article is “un”.

**A apple –** un apel {un Apël}

**A wall –** un mur {un mur}

**A boy –** un pwarn {un pwarn}

**A girl –** un bent {un bent}

The Sambahsa word “sem” is not an indefinite article, but I’ll explain
about it now. It’s like our “some”, same as in English it doesn’t change
its number when the substantive is in the plural, look:

**Sem apel {sem Apël} –** some apple

**Sem apels {sem Apëls} –** some apples

But if this “sem” is as a substantive, thus it must receive an
appropriate ending. Just after there is a complex sentence, of course
that you don’t need to understand it now, I only want that you see how
this “sem” can behave as a substantive.

**Sems credeihnt est neid global warmen –** some (people) believe there
is no global warming

[]{#anchor-183}HOW TO SAY THE CONJUNCTIONS “AND” AND “OR”
=========================================================

It's good to teach you right now this two very basic conjunctions,
because until we reach the chapter about conjunctions, maybe we will
have seen some examples with them.

The conjunction “and” is translated as “ed” for Sambahsa.

**He and I – **is ed io

As “or” you need to pay a bit more of attention. Often you'll use “au”:

**He or me – **is au io {is Ao yo}

But when we deal with two clauses, we use the conjunction “we” *(don't
get confused with the English pronoun!)*. I'll use a complex sentence,
but only look at how the conjunction is used:

[]{#anchor-184}My child, you must choose: to play in the park *or* to
swim in the pool

Mien purt, dehlcs chuses: likes in id park *we* snahe in id piscine

[]{#anchor-185}**POSSESSION** 
==============================

Here are the prepositions of possession:

**Masculine in the singular –** os

**Masculine in the plural –** om

**Feminine in the singular –** as

**Feminine in the plural –** am

**Neutral in the singular –** os

**Neutral in the plural –** om

[]{#anchor-186}**Undetermined in the singular –** es

**Undetermined in the plural –** em

Look some examples:

**The death of Louis –** id mohrt os Louis {id mo:rt os lU:is}

**The house of Paul and Peter –** id dom om Paulo ed Peter {id dom om
pAolo ed pEtër}

**The son of Julia –** is son as Julia {is son as jUlya}

**The doll of Lara and Carla –** id pupp am Lara ed Carla {id pup am
lAra ed kArla}

When diverse elements are owners of something, you also may use the word
“sbei” {sbey}, thus “id dom om Paulo ed Peter” and “id pupp am Lara ed
Carla” may be rewritten as:

“Paul ed Peter sbei dom”

“Lara ed Carla sbei pupp”

Other possibility is the use of declinations:

**The death of Louis –** Louisios mohrt {id luIzyos mo:rt}

**The house of Paul and Peter –** Paulos ed Peters dom {id pAolos ed
pEtërs dom}

We could not say “Paulo*io*s ed Peter*io*s dom” because this manner
would change the accentuation of the word, thus would be {paolOyos} and
{petEryos}.

You don’t need the definite article like “*id* Louisios mohrt” or “*id*
Paulos ed Peters dom” because the substantives already are ‘defined’ by
the genitive.

I've showed you these two last examples with declination because I have
the obligation in teaching everything, but would be better you prefer
“id mohrt os Louis” and “id dom om Paulo ed Peter”/“Paulo ed Peter sbei
dom” because of they are simpler. The pronunciation of
“Louisios”{luIzyos}*(or {luIyos} if we take the French pronunciation of
the name “Louis”)* is simply bizarre, although, grammatically speaking,
we may use this word. In the other example some people could not notice
that Paulo is owner of the house too. Prefer the simplest mode, let the
declinations for more appropriate moments.

[]{#anchor-187}DIFFERENCE OF USE OF PREPOSITION OF POSSESSION AND GENITIVE CASE
-------------------------------------------------------------------------------

Maybe you're a bit confuse and []{#anchor-188}wonder why the penultimate
example was not translated as “id mohrt *i*os Louis”. There is a simple
difference between a preposition of possession and a pronoun in the
genitive case.

**Ios –** of the

**Os –** of

**Ias –** of the

**As –** of

**Tos –** of that

Notice that, in the translations of the genitive case, we use the
definite article “the” with the preposition “of”, while the preposition
of possession is only translated to a preposition. I'll show you some
inappropriate sentences for you to understand:

“Id mohrt ios Louis” would be translated to “the death of the Louis” and
“Id apel os dru” would be translated to “the apple of tree”. Do you
understand now?

[]{#anchor-189}OTHER PREPOSITIONS OF POSSESSION
-----------------------------------------------

**Mien {myen} –** my, mine

**Tien {tyen} –** your, yours *(singular)*

**Eys {eys} –** his

**Ays {ays} –** her, hers

**Ids {Idz} –** its *(neutral)*

**Els {elz} –** his / her *(also “hers”)* *(undeterminate)*

**Nies {nyes} –** our, ours

**Noster {nOstër} –** our, ours

**Vies {vyes} –** your, yours []{#anchor-190}*(plural)*

**Voster {vOstër} –** your, yours *(plural)*

**Ir {ir} –** their, theirs *(masculine, feminine, neutral and
undeterminate)*

**Sien {syen} –** his, her, its, their, hers, theirs. *Used when it
refers to the subject of the sentence.*

**Uns {uns} –** of a(n)

**My house –** mien dom {myen dom}

**His city–** eys urb {eys urb}

**Our world –** nies mund {nyes mund} / noster mund {nOstër mund}

**Your land –** vies land {vyes land} / voster land {vOstër land}

“*Nies” and “noster”, as well as “vies” and “voster”, means the same
thing. *

I'll teach you how to use the pronoun “sien” after I teach you the
verbs”.

We may combine these pronouns with the declinations in order to
translate that situations when we use the preposition “of” with some
possessive pronouns.

**… of my men –** … mienen wirs {miEnën wirs}

**… of your son –** … tien(io)s son {tiEn(yo)s son}

**… of our daughters –** … niesen dugters {nyEzën dUgtërs}

**… of our daughter –** … nies(ia)s dugter {nyEz(ya)s dUgtër}

**Example:** the house of your son → id dom tiens son

Of course that you may say something like “id dom os tien son” oo “id
pupp am nies dugters”.

[]{#anchor-191}NAMES OF PLACES WITH CATEGORY
============================================

When we talk about names of places that include their category, like
“City of Prescott” or “Mount Everest”, normally we decide the position
of the category names through the use of the national languages, for
example:

**City of Prescott –** citad Prescott

**Mount Everest –** Mont Everest

**Rock of Gibraltar –** Perwnt Gibraltar

**Casterly Rock –** Casterly Rock

**Liancourt Rocks –** Perwnts ios Liancourt *(this is interesting
because, although its original names are “Dokdo” and “Takeshima”, in the
West it has a different name. Since the name comes from French (“Rochers
Liancourt”, due the french whaling ship that sank in that region in
1849), we say “Perwnts ios Liancourt” instead of “Liancourt Rocks”)*

See the examples below:

**Lieubho brigvs, in mien safers ho kohgnet[^1] maung brigvs: Brigv JK,
Millennium Brigv, Brigv Alexandre III, Charles/Karluv Brigv, Kintai
Brigv ed Brigv os Rialto –** I love bridges, in my travels I've known
many bridges: JK Bridge, Millennium Bridge, Pont Alexandre III, Charles
Bridge, Kintai Bridge and Rialto Bridge.

**Brigv JK =** Ponte Juscelino Kubitschek

**Millennium Brigv =** Millennium Bridge

**Brigv Alexandre III =** Pont Alexandre-III

**Charles/Karluv Brigv =** Karlův most

**Kintai Brigv =** 錦帯橋 Kintai-kyō

**Brigv os Rialto =** Ponte di Rialto

In Sambahsa we don't use the genitive when the name refers to the whole
place because it would be appelation, we don't say “id citad os
Montréal” but “id citad Montréal”, but we have to say “id tribunal os
Montréal” because Montreal is not a tribunal. Other example is the
translation of “Palace of Versailles” *(“Château de Versailles” in the
original)*, which is “Chateau os Versailles” in Sambahsa.

[]{#anchor-192}ADJECTIVES
=========================

An adjective is a word that qualifies a noun, as in “the beautiful
house”, where the adjective “beautiful” qualifies the noun “house”. I
think I'll seize the moment to explain what is an adverb: an adverb
qualifies adjectives and verbs, like in “very fast”, where the adverb
“very” qualifies the adjective “fast”. Are we understood?

The construction of adjectives is similar to English, basically you have
to put the adjective just before the noun. Let's know some adjectives
and nouns that we'll use in our examples:

**Veut {vöt} –** old

**Moll {mol} –** soft

**Pell {pel} –** fur

**Wogh {wog} –** car

**Bell {bel} –** beautiful

**Buland {bulAnd} –** tall

**Amin {amIn} –** reliable *(person)*

**Wassic {wAsik} –** reliable *(things)*

**Yun {yun} –** young

**Smulk {smulk} –** small

**Blou {blu:} –** blue

There the examples:

**Veut wogh –** old car

**Moll pel –** soft fur

Naturally you can make constructions like that:

**Id wogh est veut –** the car is old

Like in English, the adjectives don't receive the -s termination when in
the plural.

**Veut woghs –** old cars

**Moll pels –** soft furs

You may use the adjective alone and make it as a noun:

**The powerful *****woman***** –** []{#anchor-193}ia staur / ia staura

Notice that you need an article and you can make use of a declination,
when possible.

Also it's possible to make adjectives from verbs through participles,
but this I'l explain only in the chapter od participles.

To create adjectives from substantives you may make use of affixes like
-ic or -eus, like in the examples below:

**Bois sont cruor*****ic***** {boys sont krwOrik} –** battles are bloody

**Is est wirt*****ic***** {is est wIrtik} –** he is worthy

**Tod place est danger*****eus***** {tod plAts est dangërÖs} –** this
place is dangerous

**Ia gwen est nerv*****eus***** {ya gwen est nervÖs} –** the woman is
nervous

Don't worry too much about these affixes because many words can be used
as substantives or adjectives without the necessity of modifing the
word, like “infinitive”, which means “infinitive”*(what a big
surprise!)*, in“un infinitive verb”*(a infinitive verb)* or “smad studye
id infinitive”*(let's study the infinitive)*.

A *very important observation* is that the vocabulary of Sambahsa comes
from languages of different families, like derivatives from Latin and
Germanic languages, the rules of making of adjectives from substantives
are adapted according to the origin of the word, sometimes you use other
affix, or other way, which is not through the -ic or -eus. See the
examples below:

[]{#anchor-194}[]{#anchor-195}[]{#anchor-196}[]{#anchor-197}**Cid est un
insuleus land –** []{#anchor-198}that is a insular
country[]{#anchor-199} **{**[]{#anchor-200}**wrong X}**

[]{#anchor-201}**Cid est un insulic land –** []{#anchor-202}that is a
insular country []{#anchor-203}**{ wrong X}**

[]{#anchor-204}[]{#anchor-205}**Cid est un insular land –**
[]{#anchor-206}that is a insular country **{**[]{#anchor-207}**correct
Ѵ}**

[]{#anchor-208}**Som in un problemic situation –** []{#anchor-209}I'm in
a problematic situation []{#anchor-210}**{ wrong X}**

[]{#anchor-211}[]{#anchor-212}**Som in un problematic situation –** I'm
in a problematic situation []{#anchor-213}**{ correct Ѵ}**

[]{#anchor-214}**Un pateric amor –** []{#anchor-215}a paternal love
[]{#anchor-216}**{ wrong X}**

[]{#anchor-217}**Un patruw amor –** a paternal love *(attachment)*
[]{#anchor-218}**{ correct Ѵ}**

The Romance word “amor” has a PIE ancestor meaning “to be bound” or
“attached to” (cf. Greek : omnumi = I swear). So, the real first meaning
of “amor” is “attachment” (“emotional connection between individuals”)

[]{#anchor-219}**Tod buit monstereus –**
[]{#anchor-220}[]{#anchor-221}that was monstrous **{ wrong X}**

[]{#anchor-222}**Tod buit monstrueus –** that was monstrous
[]{#anchor-223}**{ correct Ѵ}**

[]{#anchor-224}**Es baygh bayeus –** []{#anchor-225}you're very fearful
**{acceptable, but prefer the sentence below}**

[]{#anchor-226}**Es baygh bayasen –** you're very fearful **{correct
Ѵ}**

Sometimes may be not interesting to use an affix for transforming a
substantive in an adjective, but a preposition like “os” or “es” may be
a good alternative.

[]{#anchor-227}**Kamo anon es kwaun –** []{#anchor-228}I like food of
dog

**Lieubho likes RPGs os table –** []{#anchor-229}I love playing tabletop
RPGs

Wouldn't make sense to say something like []{#anchor-230}“…kwaunic anon”
or []{#anchor-231}“… []{#anchor-232}tableus RPG”, because “…kwaunic
anon” gives the impression of the food has some canine characteristic,
although the food is for dogs, and “… []{#anchor-233}tableus RPG” gives
the impression that the RPG have a characteristic of table, although the
game normally is played on the table.

The most interestingway of expressing the ideas of the two last examples
is:

**Kamo kwaun-anon –** I like dog food

**Lieubho likes table-RPGs –** I love playing tabletop RPGs

Notice that the “table” of “table-RPG” and the “kwaun” of “kwaun-anon”
are not as adjectives, but as components of a word made throught the
aglutination of other words. If, for example, the “table” of “table-RPG”
were an adjetive, would be possible the sentense “id RPG est table”,
what doesn't make the minor sense. Ah, and you could write “kwaun-anon”
and “table-RPG” without the hyphens, leaving them thus: “kwaun anon” and
“table RPG”.

Know also the []{#anchor-234}[]{#anchor-235}[]{#anchor-236}predicative
adjective over the object, very used in sentenses like the ones of the
below examples:

[]{#anchor-237}**Ho pict mien dom do glend –** I've painted the house
green

**Ia me hat kalen zani –** she has called me adulterous

**Tod anon kwehrt/beuwt me sieug –** this food makes me sick

Seizing the opportunity, let me teach you how to say “Anna called John
(as) Robert”. Well, it's exactly like in English: *Anna kiel John (ka)
“Robert”*. I suggest you to use the “ka” in order to avoid equivocal
appositions.

I'll use the first example, “[]{#anchor-238}ho pict mien dom do glend”,
to explain more about this kind of situation. We couldn't have written
“[]{#anchor-239}ho pict mien dom glend” because the “glend” would be in
the accusative and the translation would be something like “I've painted
my green of house”, it wouldn't make sense, and remember that the
Sambahsa adjectives come before the substantive. Also we couldn't write
“[]{#anchor-240}ho pict mien glend dom”, because the translation is
“I've painted my green house”, which, although it's gramatically
perfect, definitivaly shows the wrong idea.

I think it's pertinent to make some observations about these last
examples, they are very interesting.

Notice that , for building the []{#anchor-241}predicative adjective over
the object in the first example, it was made use of the preposition
“do”, which you'll learn with more details in future chapters, but you
can rewrite these sentenses in other way, like for example: “Ho pict(o)
glend(o) mien dom”, thus the “glend” is not taken to be the attributive
adjective of “dom”.

In the second example I could make the word “zani” a substantive if I
add a indefinite article before this word: “Ia me hat kalen *un* zani”;
the meaning pratically still is the same. But this case is no more a
[]{#anchor-242}predicative adjective over the object, but a *nominal*
adjective over the object.

There is a better way to write the third sentence, it would be making
use of the suffix -eih, which is a factive, you'll learn about affixes
in a specific chapter. The sentense would be thus: “Tod anon sieugeiht
me” *(this food sickens me)*. I didn't use it in the examples because
thus the sentence no more would be a predicative adjective over the
object.

[]{#anchor-243}COMPARISONS
--------------------------

The comparison is made suffixing the -er to the adjective (or -ter in
the case the word finishes with a vowel), but only use this ending when
the accentuation of the word is not altered, in other case use the word
“meis”, which means “more”; “quem” means “than”.

**He is older than me –** is est veuter quem io {is est vÖtër kem yo}

**He is taller than me –** is est meis buland quem io {is est meys
bulAnd kem yo}

Comparisons of equality are made with “tem … quem”, which may be
translated as “as … as”.

**Mary is as beautiful as Elizabeth – **Maria est tem bell quem
Elizabeth

Comparison od inferiority as made with “minter … quem”, which may be
translated to “less … than”.

**Wolves are less reliable than dogs –** wolfs sont minter amin quem
kwauns {wolfs sont mIntër amIn kem kwAons}

A important observation is about the adjective “amin” is that actually
it is for humans, but the other Sambahsa adjective for reliable, which
is “wassic”, is for things. Which one use? You choose, I prefered
“amin”, but wouldn't be wrong to use “wassic”, it depends of the
person's world perspective.

“Meis” and “minter” *(sometimes "mins" instead of "minter", but rare)
*are comparative words, now let's talk about superlatives. But what are
superlatives? Look the two examples below:

[]{#anchor-244}Charles is taller than Michael

Steven is the tallest

Charles is taller than Michael, but it doesn't mean that necessarily
doesn't exist people taller than Charles, the “taller” is a comparative,
it only makes the comparison between two beings. But the second sentence
says that Steven is taller than everybody in the context where he is,
“tallest” is a superlative. Of course the same thought applies to
comparisons of inferiority:

Charles is less tall than Michael

Steven is the less tall

The superlatives are “meist”, which means “most”, and “minst”, which
means “less”. Look the examples below:

[]{#anchor-245}**Matthew is the tallest –** Matthew est is meist buland

**Matthew is the less tall –** Matthew est is minst buland

**I am the most young in my house –** som is yunst in mien dom

**Sharon is the most reliable woman –** Sharon est ia aminst gwen

The adjectives “megil” {mEdjil} *(large)* and “lytil” {lÜtil} *(little)*
are the only ones that have irregular comparative and superlative forms,
they are:

**Comparative:** “meger” {mEdjër} and “lyter” {lÜtër}

**Superlative:** “megst” {megst} and “lytst” {lütst}

[]{#anchor-246}PLURAL
=====================

The plural is easy and intuitive, often the words in plural have a -s
added in their endings, as made in English, this is basically the
standard form.

**Prince {prints} *****(prince)***** –** princes {prIntsës} *(princes)*

**Div {div}***** (god)***** –** divs {divs} *(gods)*

**Kwaun {kwAon} *****(dog)***** –** kwauns {kwAons} *(gods)*

**Land {land} *****(country)***** –** lands {landz} *(countries)*

Words finished with -um have its final replaced by -a.

**Territorium {te(r)ritOryum} *****(territory)***** –** territoria
{te(r)ritOrya} *(territories)*

**Collegium {kolEdjyum} *****(college)***** –** collegia {kol(l)Edjya}
*(colleges)*

For words finished with -es or -os, , these terminations are replaced
respectively for -si e -sa.

**Daumos {dAomos} *****(marvel)***** –** daumsa {dAomsa} *(marvels)*

**Elkos {Elkos} *****(ulcer)***** –** elksa (Elksa) *(ulcers)*

**Bes {bes} *****(boss) *****–** besi {bEzi} *(bosses)*

**Kames {kams} *****(spell)***** –** kamsa {kamsa} *(spells)*

There will situations that it's not possible to add a -s at the end of
the word for pronunciation issues, in these cases it's added the
terminations -i *(for *[]{#anchor-247}*animated beings)* or -a *(for
unanimated beings)*.

**Magv {magv} *****(child)***** – **magvi {mAgvi} *(children)*

**Kwax {kwaks} *****(croack)***** –** kwaxa {kwAksa} *(croacks)*

**Urx {urks} *****(bear)***** –** urxi {Urksi} *(bears)*

**Rawaj {rawAj} *****(currency unit)***** –** rawaja {rawAja} *(currency
units)*

**Aux {Aoks} *****(ox)***** –** auxi {Aoksi} *(oxen)*

**Kwas {kwas} *****(cough)***** –** kwasa {kwAza} *(cough)*

**Musch {muc} *****(fly)***** –** muschi {mUci} *(flies)*

*The only exception is “ok” (eye), whose plural can be “oks” {oks} or
“okwi” {okwi}.*

Some examples with articles:

**The men –** ies wirs {yes wirs}

**The women –** ias gwens {yas gwens}

**The cars –** ia woghs {ya wogz}

**The persons –** i anghens {i Angëns}

**The dogs –** i kwauns {i kwAons}

**The people –** i leuds {i lödz}

Please notice that even the articles are modified, while “the men” is
“ies wirs”, “the man” is “is wir”.

For finishing, an example of plural with declination:

**Plur millions brasileiren –** several millions of Brazilians

Notice that was written “brasileiren” instead “brasileirs”, but I could
use anyone without problems, but if you use “brasilers”, don't forget to
use the “em”, that would be “Plur millions em brasileirs vahnt…”

[]{#anchor-248}SOME, FEW, MANY AND OTHERS
=========================================

**Nothing more –** neideti {neydEti}

**Nothing at all –** []{#anchor-249}khich {qitc}

**No-one, nobody –** []{#anchor-250}neanghen {neAngën}, nimen {nImën}

**Some –** sem {sem}

**Someone – **semanghen {semAngën}

**Something –** semject {semjekt}

**Many, much, very, a lot *****(adjctive ou adverb)***** –** baygh
{bayg}

**Many, much *****(coloquial adjective) *****–** maung {mAong}

**Many, much, a lot *****(literal adjective)***** –** pelu {pElu}

**Much, very *****(adverb) *****–** meg {meg}

**Many *****(adjective)***** –** mult {mult}

**Too (much/many) *****(adverb or adjective)***** –** pior {pyor}

**Quite *****(adverb)***** – **destull {dëstUl}

**Several –** plur {plur}

**More *****(adverb or adjective) (comparative)***** –** meis {meys}

**Less *****(adverb or adjective)***** *****(comparative)***** –**
minter {mIntër}

**Most *****(adverb or adjective) (superlative)***** –** meist {meyst}

**Less** ***(adverb)***** *****(superlative)***** **– minst {minst}

**So much, so many – **tant {tant}

**Each *****(more than two)***** –** ielg {yElg}

**Each *****(of two)*****, either –** Ieter {yEtër}

**All the –** vasyi {vAzyi} *(masculine & undetermined nominative
plural)*

**All the –** vasyas {vAzyas} *(feminine nominative plural)*

**All the –** vasya {vAzya} *(neutral plural nominative & accusative)*

**Some, a few–** oik {oyk}

**Little, few *****(adverb)***** –** pau {pAo}

**Few *****(adjective)***** –** pauk {pAok}

**A little *****(adverb or adjective)***** –** lyt {lüt}

**Small –** smulk {smulk}

**Large –** large {lardj}

**Half *****(this one as adverb)*****, semi- – **pwol {pwol}

**One and half –** pwolter {pwOltër}

**The other (one) –** alter {Altër} *(*[]{#anchor-251}*it's not
necessary the article)*

**Another –** alyo {Alyo} *(it's not necessary the article)*

**Another person, someone else –** alyanghen {alyAngën}

**Both –** bo

**All both –** amb(o)

**Either… or… –** auter… au… {Aotër… Ao…}

**Whether, if *****(before the choose of two)***** –** kweter {kwEtër}

**Neither… nor… –** neter… ni… {nEtër… ni…}

**Neither, none of both –** neuter {nÖtër}

**One of two –** oiter {Oytër}

**Which of both? –** quoter? {kwOtër}

**A(n) *****(indefinide article refering to the member of a pair)*****
–** uter {Utër}

**Little by little –** []{#anchor-252}lyt ed lyt {lüt ed lüt}

**Some days –** sem diens {sem dyens}

**Few hors –** pauk hors {pAok hors}

**She is a bit heavy –** Ia est lyt []{#anchor-253}gwaur {ya est lüt
gwAur}

**The other book –** alter buk {Altër buk}

**Another book –** alyo buk {Alyo buk}

[]{#anchor-254}ARE YOU A BIT CONFUSE WITH SO MANY “MUCHS” AND “FEWS”?
---------------------------------------------------------------------

You must observe what is adjective, what is adverb and what is both, it
is very important! Let's see the implications of that:

“Baygh” may not be used before an adjective followed by a substantive, a
sentence like “baygh smulk magvi” would mean what? “many small children”
or “very small children”? Remember that “baygh” can be used as adjective
as adverb, so there is the doubt of what word it qualifies. In such
cases the ideal is to use a more specific word, one that is only an
adjective or only an adverb. *“Baygh” exists to avoid the repetition of
“maung” in a text*.

Look: “maung belli leuds” {mAong bEli lödz}, I know that this sentence
means “many beautiful persons”, but *not* “very beautiful persons”,
because of “maung” only serves as adjective.

I believe you already understand it. Some good examples for you to fix
it in your mind:

***Maung***** smulk magvi –** []{#anchor-255}many small children

***Meg***** smulk magvi –** []{#anchor-256}very small children

**Tod land hat *****pauk***** bella plaja –** []{#anchor-257}this
country has few beautiful beachs

**Tod land hat *****pau***** bella plaja –** this country has not-very
beautiful beachs

You can see that some of the above-mentionned words work sometimes as an
adjective, and sometimes as an adverb. What makes the difference? It is
an adjective when it refers to the quantity of the substantive, but it's
an adverb when it refers to the quality of the adjective.

**Volo lyt cofie –** I want a little of coffee

**Lyt-ye bitter cofie –** Slightly bitter coffee

To differentiate them when there is a risk of confusion, use the
hyphenated ending “-ye” for adverbs, and, if possible, the “euphonic”
declensional endings for adjectives.

About “destull”:

What differentiates “destull” of words like “meg” ou “baygh” is that
“destull” is in an inferior level, see the examples below for you to
understand:

**Brasilu est meg/baygh bell –** Brazil is very beautiful *(in the
superior category of beauty)*

**Brasilu est destull bell –** Brazil is quite beautiful *(it is
beautiful, but there are other countries that are more beautiful)*

You can also translate “destull” as “kinda”.

[]{#anchor-258}NUMBERS
======================

Here are the numbers from 0 to 10:

**0 – zero {dzEro} –** zero

**1 – oin {oyn} –** one

**2 – dwo {dwo} –** two

**3 – tri {tri} –** three

**4 – quar {kwar} –** four

**5 – penk(we) {penk(wë)} –** five

**6 – six {siks} –** six

**7 – sept(a) {sEpt(a)} –** seven

**8 – oct(o) {Okt(o)} –** eight

**9 – nev {nev} –** nine

**10 – dec {dek} –** ten

Between 11 and 19 you add the suffix -dem.

**11 – oindem {Oyndëm} –** eleven

**12 – dwodem {dwOdëm} –** twelve

**13 – tridem {trIdëm} –** thirteen

**14 – quardem {kwArdëm} –** fourteen

**15 – penkdem {pEnkdëm} –** fifteen

**16 – sixdem {sIksdëm} –** sixteen

**17 – septdem {sEptdëm} –** seventeen

**18 – octdem {Oktdëm} –** eighteen

**19 – nevdem {nEvdëm} –** nineteen

For numbers like 20, 30, 40, …, 90 you use the suffix -gim

**20 – dwogim {dwOdjim} –** twenty

**30 – trigim {trIdjim} – **thirty

**40 – quargim {kwArdjim} – **forty

**50 – penkgim {pEnkdjim} –** fifty

**60 – sixgim {sIksdjim} –** sixty

**70 – septgim {sEptdjim} –** seventy

**80 – octgim {Oktdjim} –** eighty

**90 – nevgim {nEvdjim} –** ninety

Here are the numbers 100, 1000, 1000000 and 1000000000:

**100 – cent(om) {tsent}/{tsëntOm} –** one hundred

**1,000 – mil {mil} –** one thousand

**1,000,000 – oin million {oyn milyOn} –** one million

**1,000,000,000 – oin milliard {oyn milyArd} –** one billion

Some examples of numbers:

**23 – dwogim tri –** twenty three

**130 – cent trigim –** one hundred and thirty

**569 – penkcent sixgim nev –** f[]{#anchor-259}ive hundred and
sixty-nine

**1992 – mil nevcent nevgim dwo –** []{#anchor-260}one thousand nine
hundred ninety-two

Cardinal numbers are made with the suffix -t (or -im if the first suffix
for incompatible).

**First –** ~~oint~~ prest *(if it's the first of a group of two, we use
“preter” instead “prest”)*

**Second –** dwot *(or “second”, equal to the English word)*

**Third – **trit

**Fourth–** quart

**Fifth –** penkt

**Sixth –** sixt

**Seventh –** septim

**Eighth –** octim

**Ninth –** nevt

**Tenth –** dect

**Eleventh –** oindemt

Yes, “first” and “second” are irregular, despite be admitted the “dwot”.

Only the last component needs termination:

**Twenty first –** dec prest

Know these too:

**Last –** senst *(superlative)*

**Latter –** senter *(comparative)*

A multiplicator is made with the suffix -(en)s:

**Once –** oins

**Twice –** dwis *(irregular)*

[]{#anchor-261}**Thrice –** tris

We also may use the word “ker”:

**Once –** oin kers

**Twice –** dwo kers

**Thrice –** tri kers

The distributive is made with the suffix -(e)n, but there are irregular
forms:

**1 –** ein

**2 –** dwin *(“duo”) *(for the word “pair” exists the word “pair”,
yeah, it's the same word)

**12 – **douzen *(our “dozen”!)*

**1000 –** tusent

For numbers finished in -dem or -gim, you use -tia instead -(e)n.

**20 –** []{#anchor-262}dwogimtia *(like a score)*

This termination -(e)n also is used for counting nouns that doesn't have
a form in the singular:

**Mi ho kaupen trin bruks –** I’ve bought myself three pairs of pants

If the distributive is used as quantity, then the nouns and adjectives
are in the plural of genitive.

**Un centen wolfen gwiviet in France –** a[]{#anchor-263} hundred wolves
could live in France

Remember of “million” and “milliard”? They only exist in the
distributive form. But when the distributive is followed by another
number, the distributive doesn't more need be in the infinitive.

**1891400 humans –** Oino million octcent nevgim oino mil quarcent
mensci

Parts can be indicated by -del.

**One third –** tridel

But look:

**Half *****(as substantive) *****–** []{#anchor-264}dwidel

**Half *****(as adjective)***** –** pwol *(often used as prefix)*

**Pwolter –** 1,5

The other 0,5 is taken using the ordinal form of the next number after
the “pwol”. Look one more example:

**Pwolpenkt – **4,5

**Pwolnevt –** 8,5

About punctuations in numbers, Sambahsa is different from English, the
fractioned numbers don't receive a dot, but a comma; a number like “two
and a half” is transcribed as 2,5 in Sambahsa. Numbers superior to 999
don't reveice any punctuation, so “one million” wont be transcribed as
1,000,000, but as 1000000.

Math operations:

**2 + 2 = 4 –** dwo plus dwo egal (ad) quar

**2 − 2 = 0 –** dwo minus/ex dwo egal (ad) zero

**2 − 3 = −1 –** dwo minus/ex tri egal (ad) minus oin

**2 ⨯ 2 = 4 –** dwo kers dwo egal (ad) quar

**2 ÷ 2 = 1 –** dwo dividen ab dwo egal (ad) oin

Score or result of voting, like the result of a soccer game where
Barcelona scored two gols and Real Mardid scored one.

**Dwo contra oin pro Barcelona –** two to one to Barcelona

**Oin contra dwo pro Real Madrid –** one to two to Real Madrid

For indicating age, you use the suffix -at:

**Som dwogim sixat –** I am twenty six years old

**Cid monument est milat –** that monument is one thousand years old

[]{#anchor-265}TIME
===================

To indicate the day of a month, we put the “dien” before the cardinal
number of the date,

[]{#anchor-266}Dien quar jul mil septcent septgim six, ia Uniet Stats
bihr independent

On the fourth of July of one thousand seven hundred and seventy-six,
United States became independent

Dien sept september mil octcent dwogim dwo, Brasilu bihsit independent

On the seventh of September of one thousand eight hundred and
eighty-two, Brazil became independent

The days of the week:

**Mingo {mIngo} –** Sunday

**Mundie **[]{#anchor-267}[]{#anchor-268}**{mundI:} –** Monday

**Ardie {ardI:} –** Tuesday

**Credie **[]{#anchor-269}**{krëdI:} –** Wednesday

**Khamsi {qAmsi} –** Thursday

**Juma **[]{#anchor-270}**{jUma} –** Friday

**Sabd {sabd} –** Saturday

Here are the months:

**Januar {januAr} –** January

**Februar {februAr} –** February

**Mart {mart} –** March

**Aprile {aprIl} –** April

**Mai {mä} – **May

**Jun {jun} –** June

**Jul {jul} – **July

**August {aogUst} –** Agust

**September {sëptEmbër} –** September

**October {oktObër} –** October

**November {novEmbër} –** November

**December {dëtsEmbër} –** December

To indicate the hours, the best way is putting the number's hour before
of “saat” {saAt} following the minute's numbers.

**18:49 –** octdem saat quargim nev

**05:00 –** penk saat

To indicate a determined decade, like “the 70's”, you inform the year
more the termination -tias.

**Ia 1970 –** **Ia mil nevcent septgimtias –** the (19)70's

A adverb of period of time may be made per the termination -s.

**I beis, qui wey eiskwmos, appareihnt noct*****s***** –** the bees,
which we want, appears at night

The present period of time may be indicated by the prefix ho-:

**Tonight – **honoct

**This evening –** hovesper

But “today” and “this morning” are made in the following ways:

**Today –** hoyd

**This morning –** Todeghern

A verb that indicates a period of spent time can be made per the prefix
(u)per-.

**Passe ad upernocte in mien dom – **spend the night in my house

**Uperdienam in id farm –** we spent the day in the farm

There the translations for “tomorrow, “yesterday” and their variations:

**Tomorrow –** cras

**Yesterday – **ghes

**After tomorrow –** poscras

**Of yesterday –** ghestern

**Before yesterday –** []{#anchor-271}preghes {prëgEs}

More important words:

**Hourly –** []{#anchor-272}horlong

**Last week –** []{#anchor-273}perhvdi {pEr(h)vdi}

**Every time –** []{#anchor-274}stets *(used in general with
comparatives)*

**In times past –** []{#anchor-275}xiawngja {ksyAwngja}

**Last night –** yarlay {yarlAy}

**Day & night *****(24 horas)***** –** yawm {yAwm}

[]{#anchor-276}CORRELATIVES
===========================

They are words made by affixing certain particles to the interrogative
pronouns or other words, like “anghen” *(person)*, “ject” *(thing)* and
“loc” *(place)*.

**Sem- {sem} – **some

**-quid {kid} –** any *(complete uncertainly, often it’s pejorative)*

> **Is lehct quodquid –** he’s telling anything at all

**-gvonc {gvonk} –** it is between “some” and “any”. *It's freely
inspired by the -cumque from Latin, it's also like the -unque from
Italian (like in “chiunque” and “dovunque”), the -conque from French
(like in “quiconque”), the -gen from German (like in “wegen”).*

> **Ne has clus id dwer. Quelgvonc ghehdiet entre id dom –** you haven't
> closed the door. Anyone/Someone could enter the house

> **Ne has tu dar mathen od mien intelligence est dar staurer quem
> quodgvonc eins? –** haven't you learnt yet that my intelligence is
> still mightier than any sword?

**-kwe {kwë} –** when suffixed to interrogative pronouns corresponds to
the English -ever *(but with an idea of totality in a sentence)*

> **Ir gwelsa brungend quodkwe spalt iom armurs –** their shafts taking
> advantage of any split of the armours

> **Querkwe gwahm, io incontre prients –** whereever I go, I encounter
> friends

**-libt {libd} –** any *(in the sense of “whichever you want” or “at
one's pleasure”)*

> **Cheus quodlibt fustan {tcös kwOdlibt fustAn} – **Choose any skirt
> *(the one you prefer)*

A comparison for better understanding, the two sentences below mean
“any/whichever shoe is useful for me”:

> **Quodquid schou est util pro me:** means that the person doesn’t care
> about the shoe to be worn in his foot

> **Quodkwe schou est util pro me:** it’s more serious

> Another example:

> **Ia sayct quodquid – **she tells anything at all *(it’s a mad woman)*

> **Ia sayct quodkwe –** she says anything *(it means probably that she
> is an intellectual that knows diverse areas of knowledge)*

**Semanghen, semquel {semAngën, semkEl} –** someone

**Quelgvonc {këlgvOnk} –** anyone

**Cadanghen {kadAngën} –** everybody

**Quodquid {kwOdkid} –** anything *(pejorative sense)*

**Quodgvonc {kwodgvOnk} –** anything

**Quodkwe {kwOdkwë} –** whatever

**Quiskwe {kIskwë} –** whoever

*For saying sentences like “I don't see (any)thing”, you can say “ne
vido ject”, it's used “ject” in these cases when in negative sentences.
But a better way of expressing would be “vido neid”* (“I see nothing”)*.
When we talk about persons we use the word “anghen”.*

**Ma? {ma} –** why?

**Itak {itAk} –** that's why

**Kam? {kam} –** how?, like, as *(comparative of equity)* *{this word
also means the verb “like”}*

**Katha {kA§a} –** so, thus

**Ka {ka} –** as (a) *(quality, function)*

**It(han) {it}/{i§An} –** so, this way

[]{#anchor-277}**Zowngschie {dzowngcI:} /dzowŋ'ʃi/ –** anyway

**-med {med} –** *suffixed to a pronoun in the genitive, it has the
meaning of “per/through” + instrument. quosmed {kwOsmëd} (with what? /
by which means?); tosmed {tOsmëd} (with that / through that means)*

**Quayt {kwayt} –** how much, how many *(pronoun)*

**Tant {tant} –** so much, so many *(adverb/adjective)*

**Semanghen, semquis, semqua {semAngën, semkis, semkwA} –** someone

**Fulan {fulAn} –** so and so

**Vasyanghen {vasyAngën} –** everybody

**Nimen, neanghen {nImën, neAngën} –** nobody

**Quer {ker} –** where

**Quetro {kEtro} –** whither

**Quetos / quois {kEtos / kwoys} –** whence

**Her {her} –** here

**Hetro {hEtro} –** hither

**Hetos / Hois {hEtos / hoys} –** hence

**Ter {ter} –** there

**Tetro {tEtro} –** thither

**Tetos / tois {tEtos / toys} –** thence

**Cer {tser} –** yonder

**Cetro {tsEtro} –** to yonder

**Cetos / ciois {tsEtos / tsyoys} –** from yonder

**Semloc/semquer, semtro, semtos {semlOk/semkEr, sEmtro, sEmtos} –**
somewhere

**Quantloc/quanter, quantro, quantos/quantois {kwantlOk/kwAntër,
kwAntro, kwAntos/kwantOys} –** everywhere

**Neidloc/nequer, netro, netos {neydlOk/neker, nEtro, nEtos} –** nowhere

**Alyer, altro, altos/alyois {Alyer, Altro, Altos/alyOys} –** elsewhere

“Left” and “right” are represented respectively by lev(ter) and
dex(ter), naturally that are possible forms like levtro/dextro and
levtos/dextos.

“Outside” and “inside” are represented respectively by “exo” and “eni”
*(irregular words)*, are possible forms like extro/extos and
entro/entos.

An adverb of direction can be made per the suffix -worts, like in “Vasya
fluvs sreunt marworts” {vAsya fluvz srönt marwOrtz} (“all rivers flow to
the sea” or “all rivers flow seawards”). Yeah, -worts equals to -wards.

An adverb of location can be made, rarely, per the termination -i.

**Ghomi {gOmi} –** on the ground

**Urbi {Urbi} –** downtown

**Domi {dOmi} –** in the house

**Hemi {hEmi} –** at home

*“Up” is “ub” and “down é “ghom”*

**Aptos {Aptos} –** from behind

**Unte {Untë} –** “through where?” and “through” or “during”

**Qualg? {kwalg} –** Which?

**Solg, talg {solg, talg} –** Such

**Quod? {kwod} –** What?[]{#anchor-278}

**Semject, semquod {semjEkt, semkwOd} –** something

**Quant(o/um) {kwAnt(o/um)} –** everything

**Khich {qitc} –** nothing at all

Quant- may serve as relative pronoun, you only have to add the
declination terminations to it.

**Quan(do) {kwAn(do)}? –** when?

**Kun {kun} –** when, as

**Kun gnahsim, mien scol dar ne existit –** when I born, my school still
didn1t existed

**Ye id dien kun is mohr, vasyi buir trauric –** On the day he died,
everybody were sad

As relative pronouns, there is no difference between “kun” and
“quan(do)”, but only “quan(do)” may be used in the interrogative form.

**Yando {yAndo} –** sometimes

**Ops {ops} –** often

**Nun {nun} –** nun

**Yant {yant} –** as soon as

**Tun {tun} –** then

“Tun” is equivalent to the Sambahsa expressions: “ye tod tid”, “in tod
condition”, etc

**Semper {sEmpër} –** always

**Naiw(o) {nÄw(o)} –** never

**Nuntos {nUntos} –** from now on

**Tuntos {tUntos} –** for this time/epoch

**Hol {hol} –** whole, integer *(adjective)*

**Alnos {Alnos} –** completely, wholly *(adverb)*

**Ceter {tsEtër} –** all the other

**Ielg {yelg} –** each *(for more than two)*

**Ieter {yEtër} –** either, each *(for a group of two)*

**Cada {kAda} –** each, every

[]{#anchor-279}THE VERBS
========================

Ah, the verbs…

With the exception of three main verbs, “es” {es}, “hab” {hab} and
“woid” {woyd}, all the other verbs are regular, you only need pay
attention to some cases, predicted by the ortographic rules.

It's common, in other languages, the verbs be represented in their
infinitive form, like in dictionaries for example, but in Sambahsa the
standard form of presentation is in the form of *stem*. I want it clear
from now on, the translations from Sambahsa to English will be provided
in the infinitive form, even when the verbs are in their original form
(only the stem). For example: the verb “sedd” is translated to “to sit”
(infinitive in English), but “sedd” is not the infinitive form of the
word, its infinitive form is “sedde”. Don't worry because I'll warn you
when the verb is in the infinitive, I only want that you know that when
I show you something like “linekw = to leave”, don't think that “linekw”
already is the Sambahsa verb in the infinitive form. Are we understood?

Let's meet the irregular verbs:

**Es {es} –** to be

**Hab {hab} –** to have

**Woid {woyd} –** to know

[]{#anchor-280}“Es”, “hab” and “woid” are verbs in the stem form, the
infinitive form of them are respectively “ses”, “habe” and “woide”. You
don't need to worry a lot about “es”, “hab” and “woid” since these verbs
are irregular.

I need to make a special comment about the verb “woid”. It means “to
know”, but there is other verb with the same translation that is “gnoh”.
The difference between “gnoh” and “woid” is that “gnoh” has a broader
meaning than “woid”. “Gnoh” can be translated to “saber” and “conocer”
(Spanish), “wissen” and “kennen” (German), “savoir” and “connaître”
(French), while “woid” only can be translated to “saber”
(Spanish/Portuguese), “wissen” (German) or “savoir” (French).

Taking the Spanish words as reference: “saber” means that you have the
knowing of a fact or information, like in “he knows when to leave” or “I
know Chinese”; “conocer” is for expressing the familiarity with someone,
some place or something, like in “they know Oliver” or “I know Chile”.
The verb that only means “conocer” is “kehgn”. In a resume:

**Woid –** “saber” / “wissen” / “savoir”

**Kehgn –** “conocer” / “kennen” / “connaître”

**Gnoh –** woid + kehgn

[]{#anchor-281}PRESENT
----------------------

You already know the conjugation of the verb “es” in the present, but
[]{#anchor-282}it's no bother to repeat them:

**Som {som} –** *(I)* am

**Es {es} –** *(you)* are *(singular)*

**Est {est} –** *(he/she/it)* is

**Smos {smos} –** *(we)* are

**Ste {ste} –** *(you)* are *(plural)*

**Sont {sont} –** *(they)* are

The verb “ste” is pronounced as {ste} because {st} is not a viable
pronounce.

The pronouns were omitted, but you are entirely free to use them too.
Look the very same conjugation from above, but with the pronouns:

**Io som {yo som} –** I am

**Tu es {tu es} –** you are

**Is/Ia/Id/El est {is/ya/id/el est} –** he/she/it is

**Wey smos {wey smos} –** we are

**Yu Ste {yu ste} –** you are

**Ies/Ias/Ia/I sont {yes/yas/ya/i sont} –** they are

From now I'll omit the pronouns in the conjugations, but know you may
use them! It's just a personal choice of mine.

Now let's see the conjugations of the verbs “hab” and “woid”:

**Ho {ho} –** *(I)* have

**Has {has} –** *(you)* have *(singular)*

**Hat {hat} –** *(he/she/it)* has

**Habmos / Hams {hAbmos / hams} –** *(we)* have

**Yu habte {yu habd} –** *(you)* have *(plural)*

**Habent / Hant {hAbënt / hant} –** *(they)* have

**Woidim {wOydim} –** *(I)* know

**Woidst(a) {wOydst(a)} –** *(you)* know *(singular)*

**Woidit {wOydit} –** *(he/she/it)* knows

**Woidam {woydAm} –** *(we)* know

**Woidat {woydAt} –** *(you)* know *(plural)*

**Woideer {woydEër} –** *(they)* know

The conjugation of woid is not compulsory.

The conjugation of verbs in the present is made with the following
endings:

**1**^**st**^** **[]{#anchor-283}**person singular:** -m *(when after
the sound of vowel or {y})*, -o *(in other cases). If the stem of the
verb ends with -e or if the addition of -o would be incompatible with
the accentuation, add nothing but use the personal pronoun “io”.*

**2**^**nd**^** person singular:** -s

**3**^**rd**^** person singular:** -t

**1**^**st**^** person plural:** -m(o)s *(use the one compatible with
the accentuation)*

**2**^**nd**^** person plural:** -t(e)

**3**^**rd**^** person plural:** -(e)nt, -e *(use the one compatible
with the accentuation)*

Let's see an example:

**I love you –** te lieubho {te lyÖbo}

**You love me –** []{#anchor-284}me lieubhs {me lyÖbs}

**She loves him –** ia lieubht iom {ya lyÖbt yom}

**The dog loves the woman –** el kwaun lieubht iam gwen {el kwAon lyöbt
yam gwen}

**We love them *****(in this case “them” refers to female persons)*****
–** lieubhmos ians {lyÖbmos yans}

**You love us *****(“you” is in the plural)***** –** yu lieubhte nos {yu
lyÖbt nos}

**They love me –** ies lieubhent me {yes lyÖbënt me}

Since the pronouns are often easy to identify (whether they are
nominative, accusative, etc) you are free to change the order of them in
the sentence, but maintain the good sense for avoiding
misunderstandings. You may say too:
[]{#anchor-285}[]{#anchor-286}[]{#anchor-287}[]{#anchor-288}lieubho te,
lieubhs me, []{#anchor-289}io []{#anchor-290}lieubho te, tu lieubhs me,
te []{#anchor-291}lieubho io, me lieubhs tu.

Another example to you understand it well:

***(I)***** learn Sambahsa –** euco Sambahsa {Öko sambA:sa}

***(You)***** learn Sambahsa –** eucs Sambahsa {öks sambA:sa}

***(He/she/it)***** learns Sambahsa –** euct Sambahsa {ökt sambA:sa}

***(We)***** learn Sambahsa –** eucmos Sambahsa {Ökmos sambA:sa}

***(You)***** learn Sambahsa –** yu eucte Sambahsa {yu ökt sambA:sa}

***(They)***** learn Sambahsa –** eucent Sambahsa {Ötsënt sambA:sa}

[]{#anchor-292}BRIEF EXPLANATION ABOUT THE NASAL INFIX
------------------------------------------------------

I will talk about the nasal infix with more details in an appropriate
chapter, but as I need right now that you have a notion about this
subject, I'll show you a quick explanation.

A verb with nasal infix has an *un*stressed “e” together the consonants
“m” or “n”. In all verbal tenses, with the exception of a form of
imperative that uses the own stem form of the verb, this kind of verb
will suffer an alteration, which basically is a lost of the unstressed
“e” and, maybe, some other modifications that I will explain in the
appropriate chapter.

Look how it works:

**Lin*****e*****kw →** linkw

**Preg*****e*****n →** pregn

You may question: but why Sambahsa has this characteristic? Sambahsa
behaves like a natural language, if you analize it well, it's more
pleasant to speak with the things in this way, people naturally would
want to drop this unstressed “e”. Without this rule, the language could
be a bit easer, but it wouldn't be much comfortable to speak.

[]{#anchor-293}PAST TENSE
-------------------------

There are two verbal tenses in the past for the verb “es”, the simple
past and the imperfect, while the first one informs about an action that
takes place in a punctual time in the past, the second one deals with
situations that happened continuously in the past and ended.

Simple past

**Buim {bwim} –** []{#anchor-294}*(I)* was

**Buist(a) {bwIst(a)} –** []{#anchor-295}*(you)* were

**Buit {bwit} –** []{#anchor-296}*(he/she/it)* was

**Buam {bwam} –** []{#anchor-297}*(we)* were

**Buat {bwat} –** []{#anchor-298}*(you)* were

**Buir {bwir} –** []{#anchor-299}*(they)* were

Imperfect past

**Eem {Eëm} –** *(I)* was

**Ees {Eës} –** *(you)* were

**Eet {Eët} –** *(he/she/it)* was

**Eemos {Eëmos} –** *(we)* were

**Yu eete {yu Eët} –** *(you)* were

**Eent {Eënt} –** *(they)* were

Try see these verbs from “imperfect past” like “I used to be”, “you used
to be”, “she was being”, “we were being”.

Both “hab” and “woid” only have the simple past.

**Hiebim {hyEbim} –** *(I)* had

**Hiebst(a) {hyEbst} –** *(you)* had

**Hiebit {hyEbit} –** *(he/she/it)* had

**Hiebam {hyebAm} –** *(we)* had

**Hiebat {hyebAt} –** *(you)* had

**Hiebeer {hyebEër} –** *(they)* had

**Woisim {wOyzim} –** *(I)* knew

**Woisist {wOyzist} –** *(you)* knew

**Woisit {wOyzit} –** *(je/she/it)* knew

**Woisam {woyzAm} –** *(we)* knew

**Woisat {woyzAt} – ***(you)* knew

**Woiseer {woyzEër} –** *(they)* knew

The conjugation of the verb “wois” is not compulsory.

The conjugation of the verbs in the past is made by the following
terminations:

**1**^**st**^** **[]{#anchor-300}**person singular:** -im

**2**^**nd**^** person singular:** -(i)st(a) *(use the one compatible
with the accentuation)*

**3**^**rd**^** person singular:** -it

**1**^**st**^** person plural:** -am

**2**^**nd**^** person plural:** -at

**3**^**rd**^** person plural:** -eer *(it's -r when the verb finishes
with a accentued vowel)*

Between the stem of the verb and the ending of the conjugation, it would
be interesting to use of the -s-, when the stem ends with the sound of
vowel, but it's not mandatory! Look an example below:

[]{#anchor-301}**ghyah {gya:} –** to gape

**ghyah + s + it = ghyahsit {gyA:sit} –** *(he/she/it)* gaped

An example to you understand:

[]{#anchor-302}***(***[]{#anchor-303}***I)***** browed –** brovim
{brOvim}

***(You)***** browed – **brovist {brOvist} *or* brovsta {brOvzta}

***(He/she/it)***** browed –** brovit {brOvit}

***(We)***** browed –** brovam {brovAm}

***(You)***** browed – **brovat {brovAt}

***(They)***** browed –** broveer {brovEër}

“Brovista” is not possible because the “a” would change the accentuation
of the word.

Apropos, “brov” also means the substantive “eyebrow”.

**Attention:** the conjugation of the verbs in the past, whose stems are
altered, is *not* compulsory!

I know that is kinda ridiculous repeat it, but it's in the case still
exists doubts. You may say “io buim”, “wey brovam”, “tu ees”, etc, using
the pronouns. It's the last time I notify you this possibility, my
conjugations are made without the pronouns, but it doesn't mean that
they may not be used! I say the same for any verbal tense.

### []{#anchor-304}ACTUALLY THERE ARE TWO WAYS OF REPRESENTING THE PAST

What you've just seen is the simple past, which the actions started and
ended in the past and ***didn't** let consequences in the present*. The
other form is the perfect present, in which the actions still occurs in
the present or, although the actions were finished in the past, *they
left some consequence in the present*; the present perfect is made
through the verb “habe” + the participle in passive past (as I'll only
teach you the participles in future chapters, I'll limit me to show you
examples here, you don't need to know how to made them now, but I want
that you at least know how to identify them).

Look the examples below to you understand the differences *(notice that
the (a)s are in the simple past while the (b)s are in the present
perfect)*:

**(1a) Is gwivit in Rome unte oct yars –** he lived in Rome for eight
years

**(1b) Is hat gwiwt in Rome unte oct yars –** he has lived in Rome for
wight years

**(2a) Ia ghiet iom **[]{#anchor-305}**gouverneur –** []{#anchor-306}she
met the governor\
**(2b) Ia hat ghaten iom gouverneur –** she has met the governor

**(3a) Lusim mien cleich –** I lost my key

**(3b) Ho lusen mien cleich –** []{#anchor-307}I've lost my key

In (1a) the person lived in Rome in the past, but currently he lives in
other place or he is no more alive; in (1b) he has lived in Rome and
still is there. In (2a) kinda implies the governor doesn't have his
position nowadays; in (2b) we know the governor still is alive and still
has his position. In (3a) I lost my keys in the past, but I already have
found them or it is no more important currently; in (3b) I've lost my
keys, I didn't find them and I still suffer the consequences like I
don't be able to enter into my own house.

[]{#anchor-308}INFINITIVE
-------------------------

You were expecting that I would talk about the future tense, weren't? I
will talk about the future tense in other opportunity because I will
need the infinitive to explain it.

**Ses {ses} –** to be

**Habe {hab} –** to have

**Woide {woyd} –** to know

Verbs finished in an unstressed “e” don't change:

**Accepte {ak(t)sEpt(ë)} –** Accepte *(accepet)*

For verbs with the nasal infix, the unstressed “e” is removed and it's
added the termination -es.

Verbs with “ei”{ey} or “eu”{ö} have these parts replaced respectively
for “i” and “u” and is added the termination -es.

**Linekw {lInëkw}** – linkwes {lInkwës} *(to leave)*

**Dreiv {dreyv} –** drives {drivz} *(to drift)*

[]{#anchor-309}**Ghieul {giÖl} –** ghiules {gyüls} *(to carbonize)*

In all the other cases it's enough to add the final -e when it doesn't
change the accentuation, if the accentuation is altered the word simply
remains unaltered.

[]{#anchor-310}**Garabat {garabAt} –** garabate {garabAt} *(to
scribble)*

**Hinder {hIndër} – **hinder {hIndër} *(to hinder)*

Let's see a practical example:

**Tehrbo kaupe anon {tE:rbo kAop anOn} –** I have to buy food

**Tehrb {te:rb} –** have to

***Kaup {kAop} –** buy*

### []{#anchor-311}THE USE OF THE PREPOSITION “AD” WITH VERBS IN THE INFINITIVE

You've known an example of the following kind:

**Ghohdim kaupe un wogh {gO:dim kAop un wog} –** I could buy a car

**Pitim nices un wir {pItim nItsës un wir} –** I tried to kill a man

But verbs that denotes movement you must use the preposition “ad”, which
you will know with more details in a future chapter. That happens
because Sambahsa infinitives behave like substantives, except those ones
that don't use any article.

**Ia ihsit *****ad***** vide sien son {ya I:sit ad vid syen son} –** she
went []{#anchor-312}to see her son

A situation where the verb is explicitly used as substantive:

**Is hehlpt iom *****ad***** tarjmes id dictionar do portughesche {is
he:lpt yom ad tArjmës id diktyonAr do portugEc} –** he helps him to
translate the dictionary to Portuguese

Othe similar situation, but with two verbs in the infinitive:

**(Tu) has gwiven kay hehlpe *****ad***** cree un nov mund {(tu) has
gwIvën kay he:lp ad crEë un nov mund} –** you have lived to help create
a new world

[]{#anchor-313}FUTURE
---------------------

The conjugations of the three main verbs:

**Sessiem {sesyEm} –** []{#anchor-314}*(I)* will be

**Sessies {sesyEs} –** *(you)* will be

**Sessiet {sesyEt} –** *(he/she/it)* will be

**Sessiemos {sesyEmos} –** *(we)* will be

**Yu sessiete {yu sesyEt} –** *(you)* will be

**Sessient {sesyEnt} –** *(they)* will be

**Habsiem {habsyEm} –** []{#anchor-315}*(I)* will have

**Habsies {habsyEs} –** *(you)* will have

**Habsiet {habsyEt} –** *(he/she/it)* will have

**Habsiem(o)s {habsyEm(o)s} –** *(we)* will have

**Yu habsiete {yu habsyEt} –** *(you)* will have

**Habsient {habsyEnt} –** *(they)* will have

**Woidsiem {woydsyEm} –** []{#anchor-316}*(I)* will know

**Woidsies {woydsyEs} –** *(you* will know

**Woidsiet {woydsyEt} –** *(he/she/it)* will know

**Woidsiem(o)s {woydsyEm(o)s} –** *(we)* will know

**Yu woidsiete {yu woydsyEt} –** *(you)* will know

**Woidsient {woydsyEnt} –** *(they)* will know

The conjugation of the verbs in the future is made in two ways:

In the first one you use the verbal form in the 2^nd^ person singular
plus the particle “ie” and the conjugations of present, look an example
below:

**You permit –** permitts {permItz} *(2*^*nd*^* person singular in the
present)*

[]{#anchor-317}[]{#anchor-318}***(I)***** will permit – **permittsiem
{përmitsyEm}

***(You)***** will permit –** permittsies {permitsyEs}

***(He/she/it)***** will permit –** permittsiet {permitsyEt}

***(We)***** will permit –** permittsiem(o)s {permitsyEm(o)s}

***(You)***** will permit –** yu permittsiete {yu permitsyEt}

***(They)***** will permit –** permittsient {permitsyEnt}

In the second form you use the auxiliar sie- (conjugated!) plus the verb
in the infinitive, look the same example from above conjugated in this
way:

[]{#anchor-319}***(I)***** will permit –** siem permitte {syem permIt}

***(You)***** will permit –** sies permitte {syes permIt}

***(He/she/it)***** will permit –** siet permitte{syet permIt}

***(We)***** will permit –** siem(o)s permitte {syEm(o)s permIt}

***(You)***** will permit –** yu siete permitte {yu syet permIt}

***(They)***** will permit –** sient permitte {syent permIt}

It's a kinda similar to English.

Sambahsa has the negative version of sie-, which works in the same way
*(i.e. only as an independent particle, but never as a suffix)*, it's
the nie-.

***(I)***** wont permit –** niem permitte {nyem permIt}

### []{#anchor-320}THE NEAR FUTURE

For an event that is close to occur, we use the auxiliar vah-. It works
in the following way: we add to this auxiliary the appropriate
conjugation of the present and then we put the verb in the infinitive. I
am going to use the verb “orbat” (to work) in this example.

[]{#anchor-321}***(I)***** am **[]{#anchor-322}**going to work –** vahm
orbate {va:m orbAt}

***(You)***** are going to work –** vahs orbate {va:s orbAt}

***(He/she/it)***** going to work –** vaht orbate {va:t orbAt}

***(We)***** are going to work –** vahmos orbate {vA:mos orbAt}

***(You)***** are going to work –** yu vahte orbate {yu va:t orbAt}

***(They)***** are going to work –** vahnt orbate {vA:nt orbAt}

[]{#anchor-323}FUTURE PERFECT
-----------------------------

They are future events, but that already occurred in the mentioned
facts. Look an example below:

**When I return, he will have already left –** Quando reicsiem, is
habsiet ja likwn

Very similar to English.

[]{#anchor-324}SUBJUNCTIVE
--------------------------

It indicates desire or expectancy.

Only the verb “es” has the complete conjugation in this mode.

**Io sia {yo sya} –** []{#anchor-325}*(that I)* be

**Sias {syas} –** *(that you)* be

**Is/Ia/Id sia {is/ya/id sya} –** *(that he/she/it)* be

**Siam(o)s {syAm(o)s} –** *(that we)* be

**Siate {syat} –** *(that you)* be

**Siant {syant} –** *(that they)* be

All the other verbs only have the singular. The endings are:

**1**^**st**^** **[]{#anchor-326}**person singular:** -a

**2**^**nd**^** person singular:** -as

**3**^**rd**^** person singular:** -a

Per this ending you can make a negative imperative with the prohibitive
adverb “mae” {may}.

**Mae kaupas {may kAopas} –** “Don’t buy!” or “Thou shalt not buy!

[]{#anchor-327}IMPERATIVE
-------------------------

It indicates an order or an ask.

The conjugation of the main verbs:

**Sdi {sdi} –** []{#anchor-328}be

**Estu {Estu} –** *(he/she/it)* be

**Smad ses {smad ses} –** let's be

**Ste {ste} –** be

**Sontu {sOntu} –** *(they)* be

**Hab(e) {hab} –** have

**Smad hab(e) {smad hab} –** let's have

**Habte {habt} –** have

**Woid(e) {woyd} –** know

**Smad woide {smad woyd} –** let's know

**Woidte {woydt} –** know

The imperative of the 2^nd^ person singular corresponds to the verbal
stem, with or without the final -e.

**Leave! –** linekw! {lInëkw} ou linkwe! {linkw}

**Eat this food –** edd tod anon {ed tod anOn}

In the 1^st^ person plural it's used the word “smad” *(let's)* with the
verb in the infinitive.

**Smad linkwes {smad lInkwës} –** let's leave

**Smad edde {smad ed} –** let's eat

In the 2^nd^ person plural, the form is the same used in the simple
present, but without the personal pronoun.

**Linkwte {lInkut} –** *(you)* leave

**Eddte {edt} -** []{#anchor-329}(*you)* eat

[]{#anchor-330}CONDITIONAL
--------------------------

**Esiem {ezyEm} –** []{#anchor-331}*(I)* would be

**Esies {ezyEs} –** *(you)* would be

**Esiet {ezyEt} –** *(he/she/it)* would be

**Esiem(o)s {ezyEm(o)s} –** *(we)* would be

**Yu esiete {yu ezyEt} –** *(you)* would be

**Esient {ezyEnt} –** *(they)* would be

**Habiem {habyEm} –** []{#anchor-332}*(I)* would have

**Habies {habyEs} –** *(you)* would have

**Habiet {habyEt} –** *(he/she/it)* would have

**Habiem(o)s {habyEm(o)s} –** *(we)* would have

**Yu habiete {yu habyEt} –** *(you)* would have

**Habient {habyEnt} –** *(they)* would have

**Woidiem {woydyEm} –** []{#anchor-333}*(I)* would know

**Woidies {woydyEs} –** *(you)* would know

**Woidiet {woydyEt} –** *(he/she/it)* would know

**Woidiem(o)s {woydyEm(o)s} –** *(we)* would know

**Yu woidiete {woydyEt} –** *(you)* would know

**Woidient {woydyEnt} –** *(they)* would know

This verbal mood is made attaching the verbal stem + “ie” + conjugation
of the present.

[]{#anchor-334}***(I)***** would eat –** eddiem {edyEm}

***(You)***** would eat –** eddies {edyEs}

***(He/she/it)***** would eat –** eddiet {edyEt}

***(We)***** would eat –** eddiemos {edyEm(o)s}

***(You)***** would eat –** yu eddiete {yu edyEt}

***(They)***** would eat –** eddient {edyEnt}

There is no problem if the verb ends in -ye:

**Sudye {sUdy(ë)} (to sue) →** sudyiet {sudyEt} (*he/she/it* would sue)

### []{#anchor-335}BUT WHAT ABOUT VERBS THAT ALREADY FINISH WITH “IE”?

Regarding these ones, replace “ie” for “icie”. Let's see the verb
“edifie” {edIfye}, which means “to edify”.

[]{#anchor-336}***(I)***** would edify –** edificiem {edifitsyEm}

***(You)***** would edify –** edificies {edifitsyEs}

***(He/she/it)***** would edify –** edificiet {edifitsyEt}

***(We)***** would edify –** edificiemos {edifitsyEm(o)s}

***(You)***** would edify –** yu edificiete {yu edifitsyEt}

***(They)***** would edify –** edificient {edifitsyEnt}

[]{#anchor-337}CATEGORIES OF VERBAL STEMS
-----------------------------------------

Depending of the kind of verb, the stem may suffer changes, all of them
will be explained in this subchapter. Although I will talk a lot here
about participles, you don't need worry about them, it's still not
necessary to know how create them, it's enough to know that they are
nominal forms of the verb *(like the past passive participle “eaten”
that comes from the verb “to eat”)*.

### []{#anchor-338}NASAL INFIXES

From here to the final of this subchapter of **NASAL INFIXES**, the text
will be almost a transcription of the book *The Grammar Of
Sambahsa-Mundialect In English*, of Dr. Olivier Simon.

Are verbs with nasal infixes those with the following characteristics:

-   An unstressed “e” as last vowel and this “e” is between consonants
-   One of these consonants must be “m” or “n”

Let's work with the following words:

**Linekw {lInëkw} –** to leave

[]{#anchor-339}**Pressem {prEsëm} –** to press

[]{#anchor-340}**Scinesd {sInësd} –** to split *(transitive verb)*

[]{#anchor-341}**Annem {Anëm} –** to breathe

[]{#anchor-342}**Pregen {prEdjën} –** to imbue, to impregnate

In the present tense, and in all the tenses derived from the present
tense, the verbs lose their unstressed “e” when possible. If, with the
deletion of the “e”, a “s” or “ss” join to a consonant, they are deleted
too. Let's see the result of this:

Linkwo, linkws, linkwt, linkwm(o)s,yu linkwte, linkwnt {lInkunt}

Premo, prems, premt, premmos, yu premte, preme(nt)

Scindo, scinds, scindt, scindmos, yu scindte, scinde(nt)

Annmo, annems, annemt, annmmos, yu annemt, annment

(“annmt” and “annms” would be impronouncable)

Pregno {prEnyo}, pregens {prEdjëns}, pregent, pregnems {prEnyëms}, yu
pregent, pregne(nt)

All the verbs, with or without nasal infix, a “e” can be put between the
verbal stem and the conjugation for the word being pronounceble. Look
this case with the verb “storgn”.

Storgno, storgnes, storgnet, storgnems, yu storgnet, storgne(nt)

Verbs that have a unstressed “e” at the end are conjugated in the
following way:

Io entre, entres, entret, entrems, yu entret, entre(nt)

Yes, it's this what you've seen, the verb doesn't is conjugated in the
1^st^ person singular, that's why is compulsory the use of the pronoun
“io”.

Verbs whose last vowel is unstressed and is before a consonant follow
the same pattern:

Io hinder, hinders, hindert, hinderms, yu hindert, hindernt

It's “hinder” but not “hindere” because “hindere” would change the
accentuation {hindEr}

In the past tense and its derivatives, the verb, besides the lose of the
unstressed “e”, loses the nasal infix.

**Linekw –** likwim, likwist, likwit, likwam, likwat, likweer

**Pressem –** pressim, pressist, pressit, pressam, pressat, presseer

**Annem – **annim, annist, annit, annam, annat, anneer

### []{#anchor-343}VERBS WITH “EH” + CONSONANT

In verbs with a vowel “eh” *followed by a consonant*, the “eh” becomes
“oh” when in the past tense and past participle.

**Ghehd –** to be able to, can

**Present:** ghehdo, ghehds, ghehdt, ghehdmos, yu ghehdte, ghehde(nt)

**Infinitive:** ghehde

**Past tense:** io ghohd, tu ghohd…

**Or:** ghohdim, ghohdist/ghohdst(a), ghohdit, ghohdam, ghohdat,
ghohdeer

**Past participle:** ghohdt / ghohden

### []{#anchor-344}VERBS WITH “EU” AND “EI”

Verbs with “eu” or “ei”, within the word, have these parts respectively
replaced by “i” and “u” when used in the infinitive, past tense or past
participle.

**Kheiss –** to feel

**Present:** kheisso, tu kheiss, kheisst, kheissmos, yu kheisste,
kheisse(nt)

“tu” é compulsório antes de “kheiss” porque a terminação não pode ser
ouvida

**Infinitive:** khisses {qIsës}

**Past tense:** io khiss, tu khiss…

**Or:** khissim, khisst(a) / khissist, khissit, khissam, khissat,
khisseer

**Past participle:** khisst / khissen

**Beud –** to appeal to, to make (someone) pay attention (to = dat.), to
demand (something)

**Present:** beudo, beuds, beudt, beudmos, yu beudte, beude(nt)

**Infinitive:** budes {büdz}

**Past tense:** io bud, tu bud…

**Or:** budim, budst(a) / budist, budit, budam, budat, budeer

**Past participle:** budt / buden

**Credeih –** to believe

**Present:** credeihm, credeihs, credeiht, credeihm(o)s, yu credeihte,
credeihnt

**Infinitive:** credihes {krëdI:s}

**Past tense:** io credih, tu credih…

**Or:** credihsim, credihst(a), credihsit, credihsam, credihsat, credihr

**Past participle:** crediht / credihn

### []{#anchor-345}VERBS WITH “A”

Verbs with “a” within the word, even the diphthongs “au” or “ay”, have
their “a” replaced for “ie” in the past tense, but the “a” is maintened
in the past participle.

**nak –** to reach

**Present:** nako, nacs, nact, nakmos, yu nacte, nake(nt)

**Infinitive:** nake

**Past tense:** io niek, tu niek…

**Or:** niekim, niecst(a)/niekist, niekit, niekam, niekat, niekeer

Nakim, nacsta… are possible, but actually they are not used.

**Past participle:** nact / naken

**sayg –** to say

**Present:** saygo, saycs, sayct, saygmos, yu saycte, sayge(nt)

**Infinitive:** sayge

**Past tense:** io sieyg, tu sieycst…

**Or:** sieygim, sieycst(a)/sieygist, sieygit, sieygam, sieygat,
sieygeer

“Sieygim” almost is not used.

**Past participle:** sayct / saygen

**aur –** to hear

**Present:** auro, aurs, aurt, aurm(o)s, yu aurte, aurnt

**Infinitive:** aure

**Past tense:** io ieur, tu ieurst…

**Or:** ieurim, ieurst(a)/ieurist, ieurit, ieuram, ieurat, ieureer

**Past participle:** aurt / aur(e)n

### []{#anchor-346}THE VON WAHL RULES

The ending of these verbs are modified in the past tense and past
participle:

  ----------------- --------------------
  Original          After modification
  -d                -s
  -dd / -tt         -ss
  -rt / -rr / -rg   -rs
  -lg               -ls
  -ct               -x
  ----------------- --------------------

Verbs ending in -v follow the same procediment of -t, but only for their
past participle. If the -v comes after “o” or “u”, it falls, otherwise
it will be replaced for -w.

**Solv –** solwt

**Lav –** lawt

In other forms the -v disappears.

**mov – **mot

**Clud –** to close

**Present:** cludo, cluds, cludt, cludmos, yu cludte, clude(nt)

**Infinitive:** clude

**Past tense:** io clus, tu clusst…

**Or:** clusim, clusst(a)/clusist, clusit, clusam, clusat, cluseer

**Past participle:** clus / cluden

**Sedd –** to sit

**Present:** seddo, sedds, seddt, seddmos, yu seddte, sedde(nt)

**Infinitive:** sedde

**Past tense**: io sess, tu sesst…

**Or:** sessim, sesst(a)/sessist, sessit, sessam, sessat, sesseer

**Past participle:** sess / sedden

**Permitt –** to allow

**Present:** permitto, permitts, permitt, permittmos, yu permitte,
permitte(nt)

**Infinitive:** permitte

**Past tense:** io permiss, tu permisst…

**Or:** permissim, permisst(a)/permissist, permissit, permissam,
permissat, permisseer

**Past participle:** permiss / permitten

**Volg –** to turn (oneself)

**Present:** volgo, volcs, volct, volgmos, yu volcte, volge(nt)

**Infinitive:** volge

**Past tense: **io vols, tu volsst…

**Or:** volsim, volsst(a)/volsist, volsit, volsam, volsat, volseer

**Past participle:** vols / volgen

**Curr –** to run

**Present:** curro, currs, currt, currm(o)s, yu currte, curre(nt)

**Infinitive:** curre

**Past tense:** io curs, tu curst…

***Or:** cursim, curst(a)/cursist, cursit, cursam, cursat, curseer*

**Past participle:** curs / curren

### []{#anchor-347}ENDING WITH A STRESSED VOWEL

Verbs ending with a stressed vowel, or with a -h just after the vowel,
follow the pattern below:

**Gwah –** to go to

**Present:** gwahm, gwahs, gwaht, gwahm(o)s, yu gwahte, gwahnt

**Infinitive:** gwahe

**Past tense:** gwahsim, gwahst(a)/gwahsist, gwahsit, gwahsam, gwahsat,
gwahr

**Past participle:** gwaht / gwahn

### []{#anchor-348}THE OTHER VERBS

They follow the rules normally and request the conjugation in the past
tense.

**Duc –** to lead

**Present:** duco, ducs, duct, ducmos, yu ducte, ducent

**Infinitive: **duce {düts}

**Past tense:** duxim, ducst(a)/duxist, duxit, duxam, duxat, duxeer

You may say “ducim”, “ducit”… too. “duxim” was chosen here for
etimological reasons, because Latin have dux- as the past of duc-.

**Past participle: **[]{#anchor-349}duct / ducen

The accentuation must stay in the same syllable for all persons in the
present tense.

In all the verbal moods, with exception of the nasal inffixes, the
unstressed “e” between two consonants, when this range is at the end of
the word, can be dropped if the junction of these two consonants doesn't
change the pronounciation of the first consonant.

[]{#anchor-350}VERBAL AFFIXES
-----------------------------

In the examples below I will use words in the gerund and I will make use
of reflexive pronouns, don't worry about them, only focus in the verbal
affixe and how they change the verbs. Later I will talk about gerund and
reflexive pronouns.

**bi- –** to begin to *(*[]{#anchor-351}*inchoative)*

**Id luce biattract insects –** The light begins to attract insects

**na- –** to keep on + *gerund (continuative)*

**Is nieudh urgent-ye namove –** He needed urgently to keep moving

**re- –** *as the re- of English*

**Ho tohrben rescribe mien buk –** I have had to rewrite my book

**vi- –** *corresponds to the English adverb “finally”*

**Is viemers ex id wed –** He finally emerged out of the water

**za- –** to stop doing

**Ibs sclavs buit permitten za-ermes –** The slaves were allowed to stop
rowing

### []{#anchor-352}AFFIXES THAT CHANGE THE TENSE OF THE VERB

**ee- –** used to

**Hermann ee(-)gwaht id scol siens urb –** Hermann used to go to the
school of his town

Notice that the verb “gwaht” is in the present tense

**sa(l)- –** to be about to *+ verb* *(always written with hyphen)*

**Is wir sa-khierk –** The man was about to drown

**-skw –** to intend to do, to like to *(desiderative)* *(may be added
to verbs if it doesn't modify the accentuation of the verb)*

**Ies nauts gwahskweer id maykhana –** The sailors wanted to go to the
inn

**-eih –** *factive*

**Kaupeihm iom tod wogh –** I make him buy this car

Verbs ended in “ie” have this part replaced by “iceih”. Stems in “ei”
change this part for “i” because of euphonic reasons.

Pay attention that no all verb ended in “eih” is a factive, “credeih”
and “vergeih” are two examples.

This affix can be added to adjectives. “rudh” means “red”, “rudheih”
means “make (something) red”.

[]{#anchor-353}SOME LITERARY VERBAL FORMS
-----------------------------------------

Due its Indo-European heritage, Sambahsa has some verbal forms that are
found only in the literary use.

### []{#anchor-354}OPTIONAL ENDINGS OF THE PRESENT OF THE INDICATIVE

If it is compatible with the accentuation, the verbs can receive the
following terminations:

**1**^**st**^** **[]{#anchor-355}**person singular:** -mi

**2**^**nd**^** person singular:** -si

**3**^**rd**^** person singular:** -ti

**3**^**rd**^** person plural:** -nti

**The verb “ses”:** esmi, essi, esti, sonti

The verb must be in the initial position of the sentence and this
sentence must not contain adverb, even a negation! This verbal formation
only must describe the events that actually occurs, no for general
statements. These consitions are rarely fulfilled.

[]{#anchor-356}[]{#anchor-357}**Stahnti medsu iens peinds –** here they
stand amidst the enemies

**Acquiseihmi denars kay likes videogames –** I gain money for playing
videogames

### []{#anchor-358}OLD FORMS OF THE IMPERATIVE

In the 2^nd^ person singular of the imperative, an option may be the use
of the suffix -di to the stem of the verb. Don't forget in changing “eu”
and “ei” for respectively “u” and “i” and observe the nasal infix.

**Kludi! –** listen! *instead* “kleu(e)!”

**Ihdi! –** go! *instead* “eih(e)!”

An imperative in the 3^rd^ persons is possible by suffixing the -u to
the conjugated verb, if it doesn't change the accentuation.

**Is maurdher nehct –** the murderer perishs

**Is maurdher nehctu! –** let the murderer perish!

**I slougs behrnt gwaur bermens –** the servants carry heavy burdens

**I slougs behrntu gwaur bermens! –** let the servants carry heavy
burdens!

It's possible to use the stem of the verb with the ending -e and a
subject in the sentence:

**Gwive is roy! –** long live the king!

### []{#anchor-359}PARTICIPLE IN THE FUTURE

To make the participle, it's used the sintetic form of the future plus
the ending -nd.

**gwehmsie + nd = **[]{#anchor-360}**gwehmsiend –** []{#anchor-361}*what
or who* will be coming

The near future is marked with the ending “tur”, for making it:

-   Add -ur to the verbs in participle that follow the Von Wahl rules,
    or those ones that end with “v”.

**Cedd –** cessur (going to yield)

**Emov –** emotur (going to emote)

-   Add -ur to the 3^rd^ person singular in the present.

**Baht –** bahtur (going to speak)

The active future of the verb “ses” is “butur”.

### []{#anchor-362}THE OLD FORMATIONS OF THE INFINITIVE

Formations of the old infinitive are possible in two ways: following the
same base of the active future seen before (the use of the passive
participle in “t” or of the 3^rd^ person singular of present), if it
doesn't modify the accentuation.

The first formation ends with -(t) and express the idea of proposal.

**Abgwahsit pinctum in id widu –** he goes off to paint in the
timberland

**Gwehmo essum con vos –** I come to eat with you

Substantived verbs.

**Deictum exact reuls sienims almens –** showing exact rules to one’s
pupils

**sienims =** “sien” + dative plural “ims”

The other endings ends with -tu and may be translated as “to be …-ed”.
It appears often after qualitative adjectives.

**Un garden amat spehctu –** A garden *(that is)* pleasant to be looked
at

Sometimes it appears as a ajective of obligation.

**Ia kwehrtu opsa** – the tasks to be done

### []{#anchor-363}THE DURATIVE

The durative suffix -neu corresponds to actions that started in the past
and continues until the present, it includes situations where normally
it would be used words like “since” or “for”.

**Stahneum her pon trigim minutes –** I’ve been standing here for thirty
minutes

The past tense is made with ee-.

**Eeghangneut apter iom pon Orleans –** he had been walking behind him
since Orleans

### []{#anchor-364}THE EVENTIVE

It means “not to stop doing”, it consists in repeat the fist consonant
(or sC, Cw, Cy or Cv) before the stem with the addiction of i- or ei-.
The imperfect tense is made with ee-.

**Dehm *****(to tame/to put upright)***** –** didehm *(to tame/to put
upright* without stoping*)*

### []{#anchor-365}THE INTENSIVE

It means “to do little by little”. It consists in the duplication od the
stem (with the deletion of clusives in the middle). Rarely used. The
imperfect tense is made with ee-.

**Wehrt →** wehrwehrt

### []{#anchor-366}THE ITERATIVE

It means “to start to keep on doing”. There is apofonies to be observed:

**eh:** oh

**ei:** oi

**eu:** ou

You use the suffix -ye. The imperfect tense is made with ee-.

[]{#anchor-367}**Kwehr- sien itner –** to make one's way

**Kwohrye- sien itner –** *it indicates that the action was stopped for
a time (for example: for the agent to rest) and it continues this action
later. It wouldn't be like in “rekwehr- sien itner”, which indicates
that all the action is being remade.*

### []{#anchor-368}THE PERFECT

It represents an action made in the past whose effects or consequences
still have an influence in the present. It's made preffixing the first
letter of the stem followed by the “e” and then the verb conjugated in
the past.

[]{#anchor-369}**Lelikwst id vetus wastu –** thou hast left the olde
city []{#anchor-370}*(“lelikwst” comes from the verb “linekw”)*

[]{#anchor-371}MORE ABOUT THE USE OF THE DECLINATIONS
=====================================================

As you already know the verbs, I feel more comfortable to teach you more
about the declinations.

[]{#anchor-372}ACCUSATIVE
-------------------------

It’s very easy, some examples:

**I have all the husbands –** ho vasyens mann(en)s

**I am sorry, but I’ve began to love another woman –** maaf, bet ho
biliubht alyu gwen

**biliubht =** bi (verbal affix that means “to begin to”) + liubht
(participle in the past)

[]{#anchor-373}DATIVE
---------------------

**Manoel gives honey to the bears –** Manoel daht mielt im urxims\
**I’ve bought a present to my wife –** ho kaupen un hadia mieni esori\
**I make furnitures for the houses –** kwehro meublar ibs domims

[]{#anchor-374}ADVERBS
======================

Adverbs are words that qualifies verbs and adjectives, like in “traveled
quietly” and “ate a lot”, where the verbs “traveled” and “ate” are
qualified by the adverbs “quietly” and “a lot”. For making a adverb you
have to add the suffix -ye (with hyphen!!!). Look some examples:

**End –** end, final

**End-ye –** finally

**Enpace –** peaceful

**Enpace-ye –** peacefully

It’s can be said that the -ye is like the -ly of English. But, as in
English, not all adverbs ends with the same termination, because some
words are adverbs by itselves, look some examples:

**Tik –** only

**Ops –** often

**Just –** just

***Ho just nicen un wir –** I’ve just killed a man*

**Just barwakt –** just in time

**It(han) {it / i§An} –** so, this way

**Bfuyi {bfUyi} –** forever

**Sigwra {sIgura} –** definitely, surely

**Oku {Oku} –** quickly

**Ja** **{ja} –** already

**Semper {sEmpër} –** always

**Tun {tun} –** then

**(Ya)schi {(yA)ci} –** too

Adverbs also can be made by adding the preffix a- to substantives.

**Part –** part

**Top(p) –** top

**Apart –** apart

**Atop –** atop

Adverbs of quality like “baygh” and “pior” may behave like adjectives
when they refer to a substantive, they may have (it’s not compulsory) a
declesional final, but as an adverb (with the possible adjunction of the
-ye) when they refer to an attributive adjective.

**Piora kowpic chifans sont vierdnic pro sieune –** too many copious
meals are harmful for health

**Pior-ye kowpic chifans sont vierdnic pro sieune –** too much copious
meals are harmful for health

[]{#anchor-375}GERUND
=====================

In order to make the gerund, it’s used the active participle plus the
suffix -ye (with hyphen!). It’s very simple, but English speakers must
pay a bit of attention here, because you already saw that the sentence
“I am eating” is translated as “som eddend”, but no “som eddend-ye”!
With the verb “ses”, you only use the active participle in the present,
in all the other cases you use the gerund. Look some examples:

**He ate the cake looking at me –** Is essit id torte spehcend-ye me

**They *****(women)***** died working –** Ias mohreer orbatend-ye

If I’ve wrote “Is essit id torte spehcend me” *(without the -ye in the
gerund)* it would mean that is the cake that is looking me, I hope that
with this example you’ve learned how to difference better the gerund of
the active participle in the present.

If it facilitates the understanding: the two examples above may be
translated like “he ate the cake *while* he looked at me” and “they died
*while* they are working”.

[]{#anchor-376}REFLEXIVE PRONOUNS AND THE POSSESSIVE PRONOUN “SIEN”
===================================================================

The pronouns are: “se” (accusative), “sib” (dative) and “sien”
(possessive pronoun).

**Se vidmos in id specule –** we see ourselves in the mirror

**“…”, sib sieyg is lytil prince –** “…”, said the little prince to
himself

The pronoun “sien” refers to the subject of the sentence, but never
appears together the nominal group of the subject.

**Un pater alt sien purts –** a father raises his children

**Julio ed eyso prient siefreer do Brasilu –** Julio and his friend
traveled to Brazil

Notice that it couldn’t be “Julio ed sien prient”, remember of what was
said before.

The “self” is translated as -swo, look:

**Gnohdi teswo –** Know thyself

Ah, it’s opportune to say that “each other” is translated to “mutu” in
Sambahsa.

**Martin ed eyso prient tolke con mutu in Sambahsa –** Martin and his
friend talk to each other in Sambahsa

[]{#anchor-377}RELATIVE PRONOUN AND INTEGRANT CONJUNCTION
=========================================================

Look the sentences below, look at the use of the word “that”:

\(1) This is the horse *that* I bought

\(2) I had a daughter *that* wanted to be singer

\(3) I’ve said *that* I don’t like coffee

\(4) He is so lazy *that* his parents put wheels in his bed

Although they are the same word, they have different functions.

The “that”s of the sentences (1) and (2) are relative pronouns, because
they are related to some substantive used before, in the case of the
sentence (1) it's “horse” and in the sentence (2) it's “daughter”.

The “that”s of the sentences (3) and (4) are integrant conjunctions,
because they start a new sentence.

Perhaps you already know the relative pronouns, because they are in that
list of cases of declination, only pay attention about what case to use,
because the “that” of the sentence (1) is a direct object (of the verb
“bought”) while the “that” of the sentence (2) is a subject.

**This is the horse *****that***** I bought – **Tel est el cavall *quel*
kieupim

**I had a daughter *****that***** wanted to be singer –** hiebim un
dugter *qua* iskwit ses sehngver

The integrant conjuction is often translated as “od”.

**I’ve said *****that***** I don’t like coffee –** Ho saygen *od* ne
kamo cofie

**He thought *****that***** I am from Africa –** is mohnit *od* som ex
Africa

As sometimes only a conjunction is not clear enough to indicate whether
what was said is a desire or a claim, there is also the integrant
conjunction “ke” (or “kem”), which express a desire and may be used in
the place of “od”.

**I desire that he learns Sambahsa –** vanscho kem is euct Sambahsa

**That our team win the enemy team and become the european soccer
champion –** Ke nies swoin vinct id peindswoin ed biht Europes football
champion

**Let’s fake that the glass have become so soft as gauze to we can go
the on the other side –** smad simule kem id glas hat biht tem moll quem
kaz kay ghehdiemos lites ocolo

**I ask you to leave –** te prehgo ke linkws

[]{#anchor-378}THE RELATIVE PRONOUNS “YO(S)”, “YA” AND “YOD”
------------------------------------------------------------

I thought a lot if I really should bring this subject here, because, as
you’ll see, maybe you will never use them, but as this is a *complete*
grammar, I felt in the obligation of explaining them.

Basically the relation is:

**Qui –** yo(s)

**Qua – **ya

**Quod –** yod

The relative pronouns yo(s)/ya/yod are to make independent sentences,
how? Look the example below:

**Latif, gwivt-yo in Afghanistan, baht Sambahsa –** Latif, who lives in
Afghanistan, speaks Sambahsa

They actually are enclitics, you'll know more about them in a
appropriate chapter, it's enough to know that they must be suffixed –
with hyphen – just after the verb.

Notice that even we remove the “gwivt-yo in Afghanistan” (“who lives in
Afghanistan”), the sentence wouldn’t lose its meaning, once “Latif baht
Sambahsa” still pass the same message? We can understand the relative
pronouns yo(s)/ya/yod insert an information merely additional that –
gramatically speaking – can be omitted.

Now let’s see an example that the relative pronouns yo(s)/ya/yod
wouldn’t be useful.

**Id bahsa, quod Latif baht, est Sambahsa –** the language, which Latif
speaks, is Sambahsa

If we remove the “quod Latif baht” (“which Latif speaks”), the sentence
would lose its original meaning.

You would question me: “but I could use a lot these relative pronouns
that you say be rarely used, because I make a lot of sentences where
they fit”.

But the detail is that you could use the pronouns qui/qua/quod in the
place of yo(s)/ya/yod, would be perfectly valid a sentence like: “Latif,
qui gwivt in Afghanistan, baht Sambahsa”

The only reason to use the relative pronouns yo(s)/ya/yod would be for
literary proposals.

[]{#anchor-379}QUESTIONS
========================

There are diverse ways for making a question, the first one is adding
the word “kwe” {kwe} at the beginning of the sentence.

**Kwe ghehdo ghende tod? –** can I hold it?

**Kwe siem ghehdo ghende tod? –** could I hold it?

**Kwe ter sont leuds her? –** are there people here?

**Kwe sont leuds her? –** are there people here?

The “kwe” also may be used at the end of question together the “no” with
the role of “isn’t”, “aren’t”, “wasn’t”, etc.

**Safersiemos do ia Uniet Stats, kwe no? –** we will travel to United
States, wont?

You also can make a question inverting the position of the main verb.

**Ghehdo io ghende tod? –** can I hold it?

**Siem io ghehde ghende tod? –** can I hold it?

Notice that is necessary the use of the pronoun.

**Sont ter leuds her? –** are there people here?

Notice that is necessary the use of the “ter”.

Questions with “what”, “who”, “when” and other similar pronouns, you can
make a question in the same way you do in English.

**Quis ste yu? –** who are you? *(question made for a male person)*

**Quando safersies –** when you travel?

[]{#anchor-380}NEGATION
=======================

In order to answer a question in a negative way, you can simply say
“no”.

To create a negative verb, it’s enough to put the “ne” before the verb.

**Ne eddo leckereits –** I don’t eat candies

The “mae” {may} is a prohibitive.

**Mae eddo leckereits –** I shouldn’t eat sweeties

**Mae eddas leckereits –** “don’t eat sweeties” or “thou shalt not eat
sweeties” *(don’t you understand the reason of the ending -as in the
verb “edd”? Remember the reason in the chapter about the subjunctive
verb)*

To facilitate, just remember those biblical commandments:

**Mae neicas –** You shall not murder

We may not forget the verb “I am not able to” that is “khako” {qAko}.

**Khako kaupe un wogh –** I can’t buy a car

“Khak” is the antonymous of “ghehd”.

[]{#anchor-381}ABOUT THE USE OF THE “SI”
----------------------------------------

In order to have not doubts regarding the use of the “si”, which means
“yes” and is used to answer negative questions. Observe the following
situation:

**Ne has tu purts? –** don’t you have children?

Si

The “si” doesn’t confirm the thought of who asks, in other words.

**Ne has tu purts? –** don’t you have children?

**Si, ho purts –** yes, I have children

[]{#anchor-382}AVOIDING REPETITIONS
===================================

In a conversation, may occur the necessity of refering to a substantive
already used just before, to avoid the repetition of words, Sambahsa
offers basically the same resources as English. Look the examples:

If you like tea, I can give you *some*

Sei tu kams chay, vahm tib schehnke *sem*

I bring the tea and its cups.

Please, give me *one*!

Bringho id chay ed ia tasses.

Plais, schehnk mi *uno*!

A course of faculty is composed by diverse subjects, and *each one* has
a certain quantity of hours

Un facultat curs est composen ab/med multa matiers, ed *ielg* hat un
certain quantitat om hors

Take your book and pass *mine*

Ghendte vies buk ed anancte mi *mieno*

You could say just “mien” or “id mien”, but “mieno” makes the message
more clear

Which T-shirt are you going to use?

I am going to use *my newer one*

Qualg tischert vahte yu vehse?

Vahm vehse *mien nov(er)*

My little fish is like of the neighbor*’s one* before die

Mien lytil pisk est lik *tal* ios nieber pre mehre

Here the complete sentence: Mien lytil pisk est lik al pisk ios nieber
pre mehre

[]{#anchor-383}PARTICIPLES
==========================

Participles are the nominal form of the verb.

[]{#anchor-384}ACTIVE PRESENT
-----------------------------

For forming the active participle of the present, it’s enough to add the
suffix -(e)nd *(-(e)nt is possible too, but rarely used because it can
make confusion with the third person of the plural)*.

Using the verb “to be” plus the participle plus the active participle of
the present for making sentences like:

**Id sol eet bleigend –** the sun was shining

**Id bleigend sol –** the shining sun

**Es –** esend

**Hab –** Habend

**Woid –** woidend

In theory, the participles can be used as substantives, but actually it
never occurs, what happens is the use of endings like -(e)nt and -(a)nt,
like in “president”, “studyent” and “immigrant”.

[]{#anchor-385}ACTIVE PAST
--------------------------

For forming the active participle of the past, it’s enough to add the
suffix -us or -vs *(it’ll depend of what gives the better pronunciation
to the word and of its accentuational patterns)*.

**Dank spollay ob gwehmus hetro –** thank you very much for coming here

**Ia minst antslehnkus lands –** the less developed countries

**Es –** esus

**Hab –** Habus

**Woid –** woidus

[]{#anchor-386}PASSIVE PRESENT
------------------------------

From the passive participles, Sambahsa uses only the past, but it’s
possible to use the passive present in composed words. The suffix is
-men with the stem of the verb in the present.

**Al –** raise *(in the sense of raise a child)*

**Almen –** pupil

[]{#anchor-387}PASSIVE PAST
---------------------------

For forming the passive participle, it’s enough to add the suffix -t or
-(e)n. I judge this participle the most interesting and you understand
why.

**Es –** est / esen *(been)*

**Hab –** habt / haben *(had)*

**Woid –** wois / woiden *(known)*

### []{#anchor-388}OTHER WAY OF EXPRESSING THE PAST TENSE, THE PRESENT PERFECT

It’s the same as in English but Sambahsa tenses can refer to actions
that don't last anymore, even if they still have consequences.

**Ho edden –** I’ve eaten

**Ho kaupen –** I’ve bought

**Has liubhen / liubht –** you’ve loved

**Habte yu edden? / –** have you eaten?

**Habte yu kaupen? / –** have you bought?

### []{#anchor-389}A WAY OF MAKING ADJECTIVES WITH VERBS

Let’s use the following verbs and substantives in the examples:

**Sneigv {sneygv} –** snow

**Calive {kalIv} –** cabin

**Lyegher {lyEgër} –** layer

**Tenu {tEnu} –** thin

**Myehrs {mye:rs} – **to forget

**Covehr {kovE:r} –** to cover

**Covohr {kovO:r} –** *verb “covehr” in the past. In the next chapter
I’ll explain why it occurs.*

Now let’s see the application of the participles:

**Myohrsen land –** forgotten land

**Uno sneigvcovohrn calive –** a snow-covered cabin

**Un calive covohrno med un tenu sneigvlyegher –** A cabin covered with
a thin snow-layer

[]{#anchor-390}CONJUNCTIONS AND OTHER INVARIABLE WORDS
======================================================

Both in this chapter an in the next one, the chapter about the
prepositions, I’ll work in the following way: I’ll show some direct
translations of the words, but in the examples I may represent these
invariable Sambahsa words from per words or expressions of the English
language that I didn’t show in the direct translation. How? I’ll talk
about the invariable word “schowi”, which is translated as “therefore,
consequently”, but in the examples I did not []{#anchor-391}exactly use
this translation! I did it deliberately with the goal of showing you
that you have to learn the idea behind the words, not merely their
direct translations!

Don’t be afraid with the size of this chapter, I only aimed to put *all*
the invariable words here, in order to this grammar be a good reference
material.

**Aiw(o) {Äw(o)} –** ever

**Ays kays eet id bellst quod is hieb aiwo vis –** her hair was the most
beautiful he had ever seen

**Est stragn od tod ilaj aiw hieb esen usen –** it's strange that this
(medical) treatment has ever been used

As substantive, “aiwo” means “age”, i.e. “period of time, History”, like
in “Petraiwo”, which means “Stone Age”.

**Id memorandum comprindt oino iom meist exhaustive playcts aiwo signen
ab bo lands, markend uno major wehnd in ira relations –** the memorandum
comprises one of the most exhaustive agreements ever signed by both
countries, highlighting a major turning point in their relations

**Id khakst film aiwo –** the worst movie ever

**Agar {agAr} –** if, in case

**In 2005, id Chinese Parlament hat widen un "anti-secession" leg
autorisend silahneud agar Taywan declariet sien independence –** in
2005, the Chinese Parliament passed an "anti-secession" act which
authorizes the use of force in case that Taiwan would declare its
independence

**Albatt(a) {albAt(a)} –** certainly, indeed

**Albatt Sambahsa ne est id meist facil bahsa –** certainly, Sambahsa is
not the easiest language

**Als {als} –** otherwise, else

**Kwe semanghen als volt anon? –** does someone else want some food?

**Also {Also} –** also, so

**Ne ho denars kay safer, also tehrbo studye –** I don't have money to
travel, also I have to study

**Maria ghi hieb neid magho kay lises ments. Also ia contentit-se med
abgires ed chehxe proscher cada face –** Maria indeed had no powers to
read minds. So she contented herself with turning away and looking
closer at each face

**Is fauran mohn de stuppe sieno nas; also sternues ne lambh iom –** he
suddenly thought about stopping up his nose; also, he wasn't caught by
sneezings

**Amalan {amalAn} –** practically

**Ti bell animals sont amalan extinct –** these beautiful animals are

practically extinct.

**An {an} –** whether, if, that* (introducing an interrogation, a
doubt)*

***Ne woidim an poitto drehnke hoyd –** I don't know if I have the right
to drink (alcohol) today. *

***Ia ihsit ad vide an ays wogh hieb likwt id reparation service –** She
went off to see if her car had left the reparation service*

***Daumo an i insects tant kame plukes ambh kiers –** I wonder whether
insects so much like to fly around candles*

**Anter {Antër} –** rather

**Sieycst “torche”, bet kad eiskws sayge anter “torte” –** you said
“torche”, bet perhaps that you rather mean “cake”

**Api {Api} –** but, now *(to introduce a counter-argument)*

**Est saygen od pisks ne maghe ses daht im leuds, i dehlge bihe bedarst
ad piskes, api leuds ne hant access ei fluv –** it is said that fish(es)
cannot be given to people, they must be taught how to fish, but people
don't have any access to the river

**Gulf lands eiskwnt conserve ir teutisk ed religieus traditions. Api,
id interpretation ios Coran ed ia reuls in gwis ios scharia
inferioreihnt gwens dia wirs –** the Gulf countries want to keep their
tribal and religious traditions. Now, the interpretation of the Coran
and the rules in force of the Sharia make women inferior towards men

**Aproposs {apropOs} –** by the way, a propos

**Dank ob aurdhens id meja. Aproposs, kwohrst tien almentask? –** thanks
for putting the table in order. By the way, did you do your (school)
homework?

**Som un serieus christian ed, aproposs, som gai. Sem leuds pohnd
difficil ghabe to –** I'm a serious Christian and, by the way, I'm gay.
Some people found this difficult to understand

**Ar {ar} –** for *(used when “ghi” is not convenient)*

**Kartvelia est ayt un perodhkala os Occident ar, pos dwo secules os
Russiano militaro presence, Moskva hat dohlgen evacue in 2005 sien
sensta militar bases –** Georgia (the Caucasian country) is considered
an advanced bastion of the West for, after two centuries of Russian
military presence, Moscow had to evacuate in 2005 its last military
bases

Notice that the use of “ghi” would not be practical because of the long
complement “pos dwo secules os Russiano militaro presence”

**Arasih {arazI:} –** accidentally

**Arasih ho sorben un bei –** accidentally I've swallowed a bee

**Au {Ao} –** or *(separate substantives, adjectives and verbs)*

**Dah ei un apel au un vinber –** give him an apple or a grape

**Aus {Aos} –** early

***Is gwohmit aus –** he came early*

**Autah {aotA:} –** or *(it's just stronger than “au”)*

***China est tienxia dwot plautsto Stat yeji id land superficie, autah
trit au quart plautsto yeji id total superficie, sekwent id meid methode
–** China is the world's second largest state by land area, or either
the third or fourth-largest by total area, depending on the method of
measurement*

**Auti {Aoti} –** or … too, or … also

***Tod militar potential ghehdt ses nudt eni id quader om UNO au NATO
operations, auti ob id maidehsa om certain lands au organisations –**
this military potential can be used whithin the framework of UNO or NATO
operations, or else because of the call for help of certain countries or
organisations*

**Bad {bad} –** at last, finally

**Bad gwahsiem hem –** at last I'll go home

**Ia gwenak me spohc med sien okwi meg-ye ghyanen ob staunos, pre bad
sprehge –** the young woman looked at me with her eyes wide-opened in
amazement, before she finally talked to me

**Ne … bad {ne … bad} –** not … yet

**Ne ho bad perichohxen id hol Sambahsa-Portughesche kamus –** I haven't
yet proofread the whole Sambahsa-Portuguese lexicon

perichohxen = peri + chohxen (past tense of “chehx”)

**Bariem {baryEm} –** at least

**Som orm, bet bariem weiko in un riche land –** I'm poor, but at least
I live in a rich country

**Besonters {bezOntërs} –** especially

***Kamo magvi, besonters i tamijdars –** I like children, especially the
well-educated ones*

Extra: “those well-educated” would be “ti tamijdar”, because “ti” here
replaces the substantive.

**Bet {bet} –** but

**Ho un bell gvibh, bet ne som noroct –** I have a beautiful wife, but
I'm not happy

**Bfuyi {bfUyi} –** non-stop, continuously

**El Manticore henslit iom bfuyi –** the Manticore harassed him
continously

**Biadet {byAdët} –** usually

**Biadet eihm hem jumas –** usually I go home on Fridays

**Bilax {bilAks} –** on the contrary

**Ne orbato con bandits, bilax, tik orbato con honeste leuds –** I don't
work with bandits, on the contrary, I only work with honest people

**(Bil)hassa {(bil)hAsa} –** mostly

**Bilhassa ghango in id forest nocts –** I walk in the forest mostly at
night

**Cadadien {kadadyEn} –** daily

**Cadadien puwno id dom –** I clean up the house every day

**Casu quo {kAzu kwo} –** if need be

**Casu quo, pehrnsiem id dom, men neti sessiem makrouse –** if
necessary, I will sell the house, but I won't be indebted anymore

**Chiowdeo {tcyowdEo} –** exactly

**Quod volst sayge chiowdeo? –** what did you mean exactly?

**Chunke {tcunk} –** since, as *(conditional)*

**Chunke yu xeihte id magh os kyukes iom Mighelekwo, ne ghehdiete yu
sprehge iom quer wehst eys poti? –** since you hold the power to summon
the Horse of Mist, couldn't you ask him where his master is now?

**Circa {tsIrka} –** about, approximately

**Is est circa dwo meters buland –** he's approximately two meters tall

**Com(samen) {kom(sAmën)} –** together

**Hovesper, sessiemos com –** tonight, we'll be together

**Daanistah {daanista:} –** knowingly, intentionally

**Daanistah brohgim id machine –** I broke the machine intentionally

**Dalg {dalg} –** far

**Weiko dalg –** I live far (from here)

**Dalger {dAldjër} –** farther, further *(in the spatial sense, never
temporal)*

**Tetos dalger, vidsies un phar –** farther from there, you'll see a
lighthouse

**Seghlim dalger kay trehve id noroc **– I sailed farther to find
happiness

**Dar {dar} –** still *(as an adverb)*

**Quan gwahm lict, mien mann dar est wehrgend –** When I go to bed, my
husband is still working

**Dat {dat} –** given that

**Dat id noct hieb gwohmen, is Marjban ess oin lembas pre swehpe –**
given that the night had come, the Ranger (from Middle Earth of Tolkien)
ate one lembas before sleeping

**Dat is hieb nia denars kay tules un eustwort porm, Tosten dohlgit
linkwes id urb –** given that he had no money to afford a fare to the
east, Ithacus had to leave the city

**Daydey {daydEy} –** in general, generally

**Daydey ia semens teukent quan (sont) madhen –** in general, seeds do
sprout when they've turned wet

Ps.: in this case, writting only “…quan madhen” is not gramatically
incorrect, but it's preferable the use of the verb in the middle for
clarity

**Dayim {dayIm} –** constantly

***Is fabric orbater premt ia scruvs dayim –** the factory worker
constantly*

presses the screws

It's synonyms of “bfuyi”

**Dexios {dEksjos} –** briskly

**Id Caroline snohg dexios inter ia enflammen vracks ed intercepit oino
iom feugend vecels –** the Caroline (a warship) sneaked skillfully
between the flamed wrecks and intercepted one of the fleeing vessels

**Dind {dind} –** then, after that

***Gwahsim id mercat, dind gwahsim kyrk –** I went to market, then I
went to church*

**Iran speht ithan crisces sien financial ressurces –** Iran hopes to
increase this way its financial resources

It's synonyms of “poskwo” and “pos to”

**Diu {dyu} –** long time, long *(temporal adverb)*

**Unte baygh diu, ho esen her –** during a long time, I've been here

Diutos is est un prisoner – he's been a prisoner for a long time

**Diuper {dyÜpër} –** long ago

**Diuper ia gwivit in mien dom –** long ago, she lived in my house

**Ed {ed} –** and

**Io ed mien son –** Me and my son

**Entrim {Entrim} –** meanwhile

**Vahm soke discret-ye, bet entrim, to dehlct remane inter nos –** I'm
gonna investigate discretely, but meanwhile, that must remain between us

**Esdi {Ezdi} –** even if

**Ne weiko in tod dom esdi i payghent me –** I don't live in that house
even if they pay me

**I men Chineses name ir land Zhōngguó, quo hat dahn id Sambahsa
Giungkwok, esdi tod nam est neter nudt in id official nam ios Popules
Respublic China, ni in tod ios Respublic China (Taywan). China de gwehmt
ex id nam ios megil cesar Qin Shi Huangdi (246-210 pre JC) –** Chinese
do call their country Zhōngguó, what has given the Sambahsa Giungkwok,
even if this name is neither used in the official name of the People's
Republic of China, nor in Taiwan's one. “China” comes from the name of
the great emperor Qin Shi Huangdi (246-210 B.C.)

If you didn’t understand the use of “… men (…) de …”, in the chapter
“Enclitics and Proclitics” there is an explanation.

**Eni {Eni} –** within, inside

***El kwaun est eni id dom –** the dog is inside the house*

**Eti {Eti} –** moreover, furthermore, also

**Ne eddo hamburger, eti som vegetarian –** I don't eat hamburger,
moreover I'm a vegan

**En id plan ios edifice. Tod plan est ja veut. Kad id edifice hat est
modifiet mulayim-ye tuntos. Eti khact ses vis ep id an fulan dwer est
ghyanen au cluden –** here is the plan of the building. This plan is
already old. Perhaps that the building was slightly modified since this
time. Moreover, we cannot see whether any door is open or closed

**Tod vestibule compris eti dwo dwers, uter wester, alter euster –**
this entrance room comprised furthermore two doors, one to the west, the
other one to the east

**Exo {Ekso} –** outside

**El kwaun est exo id dom –** the dog is outside the house

We can also say: “el kwaun est exter id dom”

**Fauran {faorAn} –** immediately

**Kehrzsiem mien kays fauran, iey(it) is orbater ei bes –** I will cut
my hair immediately, the worker said to the boss

**Filan {filAn} –** so & so

**Ia religieus autoritats hant tolcto kay woide kweter filan buk ios
Bible eet we ne inspiret ab Div –** the religious authorities have
discussed to know whether this or that book of the Bible was inspired by
God or not

**Fujatan {fujatAn} –** suddenly

**Fujatan eem trigimat –** suddenly I turned thirty

**Gairn {gärn} –** of goodwill

**Is commander reservit id access ibs “inner kyals” (kam is gairn kiel
ia) sibswo –** the commander reserved the access to the “inner rooms”
(as he liked to call them) to himself

**Gontro {gOntro} (gon + tro) –** aside *(adverb of movement)*

**Reusch gontro! –** rush to the side

**Per noroc is ekwos ios hussar, se kheissend protietragen ghomtro ab id
ansia is colonel dier, movit gontro, ed it id longo miech ios saber os
gwaur cavalerie os Fabrice slid engwn id vest ios hussar ed passit alnos
sub eys okwi –** by chance the horse of the hussard, feeling tugged down
by the rein the colonel was holding firmly, moved to the side, and, this
way, the long blade of Fabricio's sabre of heavy cavalry slid along the
dress of the hussard and passed just under his very eyes

**Ghi {gi} –** *without any definite meaning, this word frequently
appears in second position and serves to emphasize the word before. It
is often suffixed to a pronoun, an adverb, as long as it is phonetically
compatible. Can mean “also, then, for…”*

**Is ne kieup id wogh, isghi ne hieb denars –** he didn't buy the car,
for he had no money

**Eiskwo woide id ghi payghen pris –** I'd like to know the (actually)
paid price

**Id probleme tom prabhils est od pauk ghi anghens brunge ia –** the
problem with these rules is that (indeed) few people benefit from them

***Gwaru {gwAru} –*** seriously, heavily* (when referring to a wound)*

**Id ecosysteme os Amazonia buit gwaru taraght ab illegal reuydens –**
the ecosystem of Amazonia was heavily upset by illegal exploitations

**Hatta {hAta} –** even *(adverb)*

**Hatta i smulkst aranks ghehde nices –** even the smallest spiders can
kill

**Pedro ne hat hatta smautert tod film –** Pedro hasn't even watched
this movie

**Tu hatta ghyienst id hadia –** you even opened the present

**Hatta habend-ye piten, ne kamyabim –** even having tried, I didn't
succeed

**Magho sayge od hatta Suomi hat falls os foll gwow siuge –** I can tell
that even Finland has cases of mad cow disease

**Hakan {hakAn} –** really, truly

***Is hakan lieubht te –** he really loves you*

**Iawod {yawOd} –** provided that, let's hope that

**Hol grupps bihnt autoriset ad page, conservend ir bahsa, ir mores, ir
social organisation iawod obedeihnt ia loys ios Roman Stat –** whole
groups are authorized to settle down, keeping their language, their
customs, their social organisation, provided that they abide by the laws
of the Roman State

**Ib {ib} –** lest

**Eti, un ieuster daysa iom opnos tehrbiet bihe instohlen in multa
lands, ibo vide udbrehge grave social troubles –** moreover, a fairer
sharing of wealth should be set up in many lands, lest (we) see grave
social troubles break up

**Ilhali {ilhAli} –** whereas

**Ilhali id recognition ios inherent decos vasyims members ios
menscfamilia ed iren egal edinalienable rects constituet id sul om lure,
justice ed pace tienxia –** whereas recognition of the inherent dignity
and of the equal and inalienable rights of all members of the human
family is the foundation of freedom, justice and peace in the world

**Inkaptos {inkAptos} –** from the beginning/start

**Sayg mi quanto wakyit inkaptos –** tell me everything that happened
from the beginning

***Intant {intAnt} –** in the meantime*

**Bet intant, kwehr quodlibt –** but in the meantime, do what you want

**Iter {Itër} –** once again, oncemore, anew

**Som con mien família iter –** I'm once again with my family

**In unisson {in unisOn} –** in unison

**I brasileirs obswihr is corrumepen president in unisson –** all
together/like a single man, the Brazilians booed at the corrupted
president

**Ja {ja} –** already

**Tu biscripst todeghern ed tu ja finihst id wehrg –** you began to
write this morning and already you finished the work

**Jaldi {jAldi} –** fast, quickly *(notion of velocity)*

**Ia cursit meg jaldi quando ia eet yuner –** she ran more quickly when
she was younger

**Id nivell ios wed est steighend jaldi –** the level of the water is
rising quickly

**Ka {ka} –** as (a) *(when referring to a quality)*

**Tod werd wehrct ka adjective ed adverb –** this word works as an
adjective and as an adverb

**Kad {kad} –** maybe, perhaps that

**Kad wehdsiem iam –** Perhaps I will marry her

**Kafi {kAfi} –** enough

**Sat! {sat} –** enough!

**Ho edden kafi –** I've eaten enough

**Kam {kam} –** how?, in what manner ?, like

**Kam has tu arriven her? –** how have you arrived here?

**Kam leits tu? –** how are you?

**Som kam tu –** I am like you

**Kam adet {kam Adët} –** as usual

**Kam adet ia oisbud aus –** as usual she woke up early

**Kamsei {kamsEy} –** as if

**Is ee-sispehct me kamsei is esiet/eet un lion sispehcend un owu –** he
didn't stop looking at me as if he was/were a lion looking at a sheep

**Kariban {karibAn} –** soon, shortly

**Linkwsiemos (hetos) kariban –** we'll soon leave (from here)

**Kathalika {ka§alIka} –** likewise

**Tony ed Sandro sont kerabs, kathalika sont Otavio ed Clarissa –** Tony
and Sandro are relatives, likewise are Otavio and Clarissa

**Kay {kay} –** (in order) to

**Gwahsiem weir kay defende mien land –** I'll go to war to defend my
country

**Eiskwo un wogh kay safer –** I want a car in order to travel

**Khaliban {qalibAn} –** mainly

**Is orbietit khaliban ka swobod orbater –** he mainly worked as a
freelancer

**Kheptenn {qëptEn} –** quite, definitely

**Is kheptenn est gai –** he's definitely gay

**Kjiawxieng {kjyawksyEng} –** by chance, incidentally

**Kjiawxieng kwe has cigarettes? –** by chance, do you have some
cigarettes?

**Kongcio {kOngtsyo} –** from now on, hereinafter, henceforth

**Kongcio sessiem un gohd pater, promitto –** henceforth I'll be a good
father, I promise

[]{#anchor-392}“Nuntos” has the same meaning and often is most used,
since the “kongcio” seems be antiquated.

**Kwecto {kwEcto} –** apparently, as it seems

**Id phial, quei poskwo dahsim mien attention, kwecto pwolpohld med un
cruorrudh liqueur –** the phial, to which I next turned my attention,
might have been about half-full of a blood-red liquor

**Hatta eys prientias kwecto kihr ep un samliko catholicitat os suabuhsa
–** even his friendships seemed to be founded in a similar catholicity
of good-nature

**Kweid {kweyd} –** even if, though *(pass the idea of: under the price
of/it is necessary that)*

**Iaschi EU lands maghe bihe tenten ab bringhes wahid-ye securitara
responses ei probleme os terrorisme, kweid biht limitet id bungos iom
civil lures –** the EU countries themselves may be tempted to bring
security-only responses to the problem of terrorism, even (under the
price of) if the function of the civil freedoms gets limited

**Kweter {kwEtër} –** whether.* It indicates the choice or the doubt
between two options*

***Ay buit impossible tarctum kweter ia hieb vanien in id hava we ia
hieb curren baygh oku in id bosc***** **(“tarctum” is an optional
antiquated form of infinitive)** –** *it was impossible to her to
conjecture whether she has vanished in the air or she had run very
quickly into the wood*

***Quo ia druve-ye gnohskwit eet kweter el stohng we ne –** what she
really wanted to know *(about an insect)* was whether it stung or not*

**Lakin {lakIn} –** however, nevertheless

**Kamo te, lakin tu dehlcs change –** I like you, however you've got to
change

**Libter {lIbtër} –** willingly, with pleasure

**Libter kwehrsiem tod pro te –** with pleasure I shall do that for you

**Lika {lIka} –** the same *(adverb)*, alike

**Daydey, ho piten vergihes werds qua swehnient pior lika alyi –** in
general, I have tried to avoid words that sounded too much the same as
another (one)

**Makar {makAr} –** even though, although

**Makar ei saygo, is ne kaurt de –** even though I tell him, he doesn't
care

**Makar io ielgv denars, na kwahsim spare –** even though I earned
money, I failed to spare

**Makar kauro, ne kwahm vergihes howkscheces –** even though I care, I
fail to avoid potholes

**Makhsus {mAqsus} –** on purpose, intentionally

**Makhsus ho scriben id texte samt erros –** deliberately I've written
the text with mistakes

**Mathalan {ma§alAn} –** for example, for instance

**Kamo aw fantasia buks, mathalan “Is Lytil Prince” ed Lovecrafts buks
–** I like old fantasy books, for example “The Little Prince” and
Lovecraft's books.

**Meist-ye {meyst ye} –** at the most, at the latest

**Eiskwo meist-ye dwo purts, ne meis quem to –** I want two children at
the most, no more than that

**Men {men} –** but (less strong than “bet”)

**Men, weidwos, to ne est tien fault –** But, of course, this is not
your fault

**Menxu {mEnksu} –** while *(conjunction)*

**Eem in alyo land menxu mien land eet invaden –** I was in another
country while my country was invaded

**Minst-ye, lytst-ye –** at the least *(“tehrb” can be used here)*

**Naudhsies minst-ye six hevds kay plane adequat-ye –** you shall
require at the least seven weeks in order to plan adequately

**Mudam {mudAm} –** constantly, steadily, continually, continuously

**Myen machine orbat mudam –** my machine works continually

***Id seuy fallt mudam –** the rain is continually falling*

**Mutlak {mutlAk} –** absolutely

**Ia est mutlak khiter –** she is absolutely evil

**Mox(u) {mOks(u)} –** soon

**Vidsiem te mox –** I shall see you soon

**Naiw(o) {nÄw(o)} –** never

**Naiwo likwim mien land –** I never left my country

**Naturelika {natürëlIka} –** naturally

**Naturelika kamo uces bahsas –** naturally I enjoy learning languages

**Nepunei {nepÜney} –** with impunity

**“Niem permitte od quoy serve iom Demon-Roy safernt nepunei unte mien
land, Castelian”, grohm Beruh –** “I won't allow that those who serve
the Demon-King travel unpunished through my country, Castelian”, did
Beruh roar

**Neti {nEti} –** not … anymore, nevermore, never again, no more

**Neti eddo her –** I eat here no more

**Nib(o) {nIb(o)} –** unless

**Orbatsiem in id farm nibo kamyabo in un public concurs –** I will work
at the farm unless I pass a public exam

**Nisbatan {nisbatAn} –** relatively

**Ia ruines sont nisbatan salver quem id forest –** the ruins are
relatively safer than the forest

**Nun {nun} –** now

**Som noroct nun –** now I'm happy

**Nundiens {nundyEns} –** nowadays

**Nundiens, leuds sont suagramat –** nowadays, people are well literate

**Nuper {nÜpër} –** recently

**Gnahsit nupe –** (He/she) was born recently

**Nuptos {nUptos} –** not long ago

**Nuptos mien dugter ghiemt –** recently my daughter married

**Oku {Oku} –** quickly

**Gwehm oku –** come quickly

**Ne ghehdo antwehrde tib oku –** I cannot answer quickly to you

**Okwivid-ye {okwivId-ye} –** obviously

**Okwivid-ye i ne surviveer –** obviously they didn't survive

**Payn {payn} –** hardly

**Payn kwahsiemos fuges –** We could hardly flee

**Perodh {perOd} –** forward

**Ghango perodh –** I walk forward

**Plus {plus} –** more (+), additional, more

**Dwo plus dwo est egal ad quar –** 2 + 2 = 4

**Ho addihn plus mathmoun ad mien buk –** I've added more contents to my
book

In the latter sentence, you could have used “meis” instead of “plus”.

**Poskwo {pOskwo} –** afterwards, then

**Ghamsiemos poskwo stajernsiemos un dom –** we will marry and we will
rent a house afterwards

**Prevst {prevst} –** once (*in the very past)*, before

**Visim un fee prevst –** I saw a fairy once in the past

**Protiapo {protyApo} –** against the current

**Snahm protiapo –** I swim against the stream

**Punor {punOr} –** however, on the other hand *(opposition)*

**Habe purts est gohd, punor dehlcs dedie tien hol gwiv pro i –** having
children is good, but, on the other hand, you must dedicate your life to
them

**Quayque {kwAyk(ë)} –** (al)though

**Eddo mult leckereits, quayque som diabetic –** I eat a lot of sweets,
though I'm diabetic

**Quasi {kwAzi} –** nearly, almost

**Quasi mohrim honoct –** I nearly died tonight

**Quodlibt {kwOdlibd} –** anyone/thing you like/want

**Vols tu un orange, un banana au un mankay? Quodlibt –** do you want an
orange, a banana or a mango ? Anyone you want

**Quoterlibt {kwOtërlibd} –** *same as “quodlibt”, but for a choice
between two options*

**Vols tu un orange au un mankay? Quoterlibt –** do you want an orange
or a mango ? Anyone you want

**Saat-ye {saAt ye} –** clockwise *(adverb)*

**Id wogh gwaht saat-ye –** the car runs clockwise

**Sammel {sAmël} –** at the same time

**Nies purts vanier sammel –** our children vanished at the same time

**Sat {sat} –** enough

**Essim sat –** I ate enough

**Schawxwen {xAwkswën} –** momentarily

***Eem schawxwen dusiht –** I turned momentarily dizzy*

**Schowi {cOwi} –** therefore, consequently

**Id institutional division iom maghs est schowi necessar iri mutual
control –** the institutional division of the powers is by consequence
necessary to their mutual control

**Sei {sey} –** if *(introducing a condition or suposition)*

**Sei seuyt, mansiem domi –** if it rains, I'll stay at home

**Kaupsiem tien hadia, bet sei tien pater ne payght mi, cheidsiem con
iom –** I shall buy your present, but if your father doesn't pay me,
I'll have an argument with him

**Sekwent {sEkwënt} –** following, according to

**Sekwent ids Constitution, official bahsa Ukraines est ukrainsk –**
according to its constitution, the official language of Ukraine is
Ukrainian

**Sekwent id Tyrk statistic institut, id population ios landios mikdier
74,7 millions leuden in 2011 –** according to the Turkish statistical
institute, the country's population amounted to 74,7 million people in
2011

**Seni {sEni} –** apart, separately, asunder

**Crohscim seni ud mien braters ed swesters –** I grew up separately
from my brothers and sisters

**Ser {ser} –** seriously* (when refering to injuries, wounds…)*

***Buim ser vurnt unte id accident –** I was severely injured during the
accident*

**Serter {sErtër} –** later

**Wano wakt nun, ghehdsiemos vide mutu serter –** I'm lacking time for
the moment, we can see each other later

**Shayad {xayAd} –** probably

**Credeihm od ne ter sessient meis large ubnuwa nuntos. Shayad naudhsiem
kaure tik de id –** I think that there won't be larger updates from now
on. Probably I'll have to care about it

**Sigwra {sIgura} –** certainly, sure

**Sigwra eucsiem Sambahsa –** I will certainly learn Sambahsa

**Sonst {sonst} –** if not, or else

**Is ne hat daken vies message, sonst habiet gwohmt –** he didn't get
your message, or else he would have come

**Sontern {sOntërn} –** but *(after a negation)*

**“Ne ho saygen od neid est gohder,” jawieb is Roy, “sontern od neid est
meis lecker –** “I haven't said that nothing is better,” replied the
King, “but that nothing is more delicious”

**Stayg {stayg} =** fujutan *(both can be used as adjectives)*

**Strax {straks} =** fauran

**Tadrijan {tadrIjan} –** gradually

**Tadrijan i beis construgent ir alvey –** little by little do the bees
build their hive

**Taiper {tÄpër} –** presently

**Taiper id mund est baygh dangereus –** the world is presently very
dangerous

**Takriban {takribAn} –** about, approximately

**Ho takriban penkwe milliards in id bank –** I have approximately five
millions at the bank

**Tan(do) {tAn(do)} –** as long as

**“Ghehdo te hehlpe ad kwehre id sam”, is iey “tan es asli-ye zakir” –**
“I can help you to do the same”, he said, “as long as you are faithfully
pious”

**Tienxia {tyEnksya} –** all around the world

**Tienxia est khitert, betschi tienxia est karam –** rhere's evil all
around the world, but there is also goodness all around the world

**Tik {tik} –** only

**Som tik octat –** I'm only eight years old

**Towsraen {towsrAyn} –** randomly, at random

**Ia numers prehpent towsraen –** the numbers come up at random

**Tsay {tsay} –** back

**I peinds gwehment tsay –** The enemies are coming back

**Tun {tun} –** then *(temporal adverb)*

**Tun el magv visit un piurneus serpent ep id charagah –** then the
child saw a snake on the grass

**Wa {wa} –** and/or *(to indicate alternative names)*

**Feira de Santana wa “Princesse Urb” est id second mierst urb os Bahia
–** Feira de Santana or “Princess City” is the second largest city of
Bahia

**Wakea {wakEa} –** definitely

**Is est wakea is meist preparet **– he's definitely the best prepared
one

**Way {way} –** alas, unfortunately

**Way ia mohrit –** alas, she died

**We {we} –** or *(separes clauses)*

***Ne woidim an kaupo un wogh we io safer do Tyrkia –** I don't know if
I buy a car or travel to Turkey*

**Weidwos {wEydwos} –** of course

**Weidwos od Sambahsa est facil –** of course that Sambahsa is easy

**Yadi {yAdi} –** if only

***Yadi Jorge hieb esen perodhsedd, hol esiet different –** if only
Jorge had been the chairman, everything would be different*

**Yani {yAni} –** that is to say *(to bring a precision)*

**Babys sont pur, yani, i ne hant synt –** babies are pure, that is to
say, they haven't sin

**…i hant neid synt –** …they have no sin

**(Ya)schi {(yA)ci} –** too, also *(“-schi” can be suffixed to a
pronoun, an article or another invariable word if it's phonetically
compatible and if it's the word concerned by the repetition)*

**Iaschi buit aunstohmen ad mohrt –** she also was sentenced to death

**Cavalls yaschi ghehdent ses usen ka transport forme –** horses also
can be used as a means of transportation

**Yed {yed} –** yet, however

**Yed, is postalion ne gwohmit –** yet, the postman didn't come

**Yeji {yEji} –** according to

**Yeji id Traiteit os Amritsar, is Radja os Jammu, Gulab Singh, bihsit
is nov wanak os Kashmir –** pursuant to the Amritsar Traiteit, the Ralah
of Jammu, Gulab Singh, became the new ruler of Kashmir

**Strehcend uper circa 9,6 millions km², China est tienxia dwot plautsto
Stat yeji id land superficie –** spanning over around 9,6 millions km²,
China is the second or third largest country of the world according to
land surface

**Yeji Einstein, dwo jects sont aunfin, id universe ed mensc cacavania
–** according to Einstein, two things are endless, the universe and
human stupidity

**Yunyun {yunyUn} –** and so on

**Un cyclist safert 20Km in id prest hor, 16 in id second yunyun –** a
cycliste travels 20 kmin the first hour, 16 in the second one, and so on

[]{#anchor-393}SOME DOUBTS THAT CAN APPEAR
------------------------------------------

I've tried to explain the conjunctions well in the examples, I've even
used very similar examples in order to evidence the differences, but
some doubts can remain. In this subchapter I try to predict some of them
and I try to answer them.

### []{#anchor-394}WHAT'S THE DIFFERENCE BETWEEN “BET” AND “MEN”?

While “bet”, the most common, presents a real opposition, “men” doesn't
present real oposition. Look the examples below:

**(1) Ne eddo **[]{#anchor-395}**leckereits, MEN ne som diabetic –** I
don't eat candies, but I am not diabetic

**(2) Ia orbat in un kiekwrschop, MEN ia ne est veterinar –** she works
in a pet shop, but she is not veterinar

**(3) Kamo sports, BET ne som un athlete –** I like sports, but I am not
an athlete

**(4) Gwivs in id eust, BET eiskws gwive in id west –** you live in the
east, but you want to live in the west

Observe that, in (1), if I don't eat candies, it doesn't necessarly mean
that I am diabetic; in (2) the woman works in a pet shop, but she
doesn't have to be veterinar to work there. But in (3), if I say that I
like sports, many people may presume that I am an athlete; in (4) the
person lives in the east, so it's assumed that he/she wanted to live
there, but in reality his/her desire is otherside.

Don't worry so much, because the choose between “men” and “bet” is very
*subjective*, some can say that if you work in a pet shop, much probably
you are a veterinar, thus we should use “bet” instead “men” in (2). In
(3), if you live in a place where people like sports a lot, but it's
common that almost nobody practice such activities, “men” can be used.

[]{#anchor-396}PREPOSITIONS
===========================

Prepositions are words that establish the relationship, of subordinative
type, between two terms of a sentence. Most of the Sambahsa prepositions
can be used as conjunctions too. The prepositions are:

**Ab {ab} –** by *(after a verb in the passive).* *This preposition
becomes “af” when before words that begins with “h”. It also can means
“as of” or “starting from”*

**Id torte buit edden ab me –** the pie was eaten by me

**Af hoyd –** from today

**Ad {ad} –** at. *Very useful when the dative cannot be used.*

**Yeiso un blixbrev ad Henrique –** I send an e-mail to Henrique

***Ia ihsit ad vide an ays wogh hieb likwt id reparation service –** she
went off to see if her car had left the reparation service*

**Ant {ant} –** before, in front of

**Ant id scol –** in front of the school

**Ant id thron** – in front of the throne

**Apo {Apo} –** off. *It can be reduced to “ap” when before vowels.*

**Grand Britain lyehct apo id Europay dayluk –** Great Britain is
located off the European mainland

**Apter {Aptër} –** behind

**Apter id dru –** behind the trees

**Apter iam mater –** behind the mother

**Id urb est apter id forest –** the city is behind the forest

**Aun {Aon} –** without

**Som aun chifan –** I'm without meal

**Bayna {bAyna} –** among

**Smos bayna i peinds –** we're among the enemies

**Id vallee est bayna ia ghyors –** the valle is among the mountains

**Is planit id competition bayna i daskals –** he planned the
competition among the teachers

**Bi {bi} –** at *(workplace, shop), in (the work of an author), by
(idea *

*of non-permanent proximity)*. *This preposition often merges with
pronouns in the dative and accusative forms. bi + ei = bei; bi + ay =
bay; bi + el = bil; bi + im = bim.*

**Fruits sont kaupen bi mercat –** fruits are bought at the market

**Manskwo bi mien sokwi ep tod walu –** I want to stand by my companion
on that battlefield

**Sei yed id dayi progress dehlct ses, bi Kant kam bi Condorcet, id
norme quod permitt judce Historia… –** if however the idea of progress
must be, in Kant as in Condorcet, the norm that allows to judge History…

**Bila {bIla} –** in lack of, failing

**Bila satisfacend communicationzariyas ed efficienta
transportsnetwehrgs, ia electoral campagnes sont difficil ductu –** in
lack of satisfying communication means and transportation networks,
electoral campaigns are hard to carry on

**Cis {tsis} –** on this side (where I am)

**Cis tod fluv –** on this side of the river

**Con {kon} –** with *(idea of company)*

**Som con te –** I'm with you.

**Safersiem con iom –** I will travel with him

**Weiko con mien esor –** I live with my wife

**Tolko con Olivier –** I'm talking with Olivier

**Contra {kOntra} –** against

**I katuer contra nos –** they fought against us

**Circa {tsIrka} –** about, approximately

**Is est circa dwo meters buland–** he's approximately two meters high

**Dayir {dAyir} –** concerning

**Ia conflicts dayir wed bihnt schowi imminent –** by consequence,
conflicts about water are getting imminent

**De {de} –** about

**Tod buk est de drus –** this book is about trees

**Dia {dya} –** towards *(in a moral, figurative sense)*

**Id majoritat os Kashmirs population esiet nuntos ryowkhowo dia
independence –** the majority of Kashmir's population would be from now
on favorable towards independence

**Som allergic dia penicillin –** I'm allergical at penicillin

**Do {do} –** (in)to

**Wey safersiemos do Espania –** we will travel (in)to Spain

***Ho adapten sem reconstruct nams em antique hindeuropay divs do
Sambahsa –** I have adapted some reconstructed names of antique
Indo-european gods into Sambahsa*

**Due {dü:} –** due to

**Due id seuy, khako linkwes mien dom –** because of (due to) the rain,

I can't leave my house

**En {en} –** here is/are *+ acc*

***En id antwehrd –** here is the answer*

**Ender {Endër} –** beneath, underneath *(doesn't imply contact)*

**Ter est un kwaun ender id meja –** there is a dog beneath the table

**Ender nebhs sont id land ed id mar –** bthe mouth beneath the
noseeneath the clouds there are the land and the sea

**Id stohm ender id nas –** the mouth beneath the nose

**Endo {Endo} –** within, inside *(with movement)*

**El monster fugit endo antro –** the monster fled inside the cave-den

**Engwn {Engun} –** along

**Tod permitt mette precis-ye engwn ia walls ed flors **– this allows to
mow with precision along the remparts and the flowers

**Eni {Eni} –** within

**Eni mien corpos ter sont vehrms –** there are worms inside my body

**Ep {ep} –** on *(touches the object)*. *Before “h” this preposition
becomes “ef”, like is done with “ab”*

**Ter est un rat ep id meja –** there is a rat on the table

**Epter {Eptër} –** just over *(ex: a garment on another one, or looking
over a shoulder)*

**Ia dohm-se ep id bors om sien peds, ed, glanzend epter id leizdo ios
mur, ayso spect incontrit tod uns buland wir –** she stood upright on
the tip of her feet, and, glancing over the edge of the wall, her look
met the one of a tall man

**Ex {eks} –** out of, from (within)

**Som ex Brasilu –** I am from Brazil

**Un helm ex stal –** a helmet (made) of steel

**El kwaun gwehmt ex id dom –** the dog comes out of the house

Look the difference between “ex” and “exo”

***El kwaun est ex****o**** id dom –** the dog is outside the house*

When is not possible to express the idea of “made of” or “from certain
place”, the preposition “ex” passes the idea of movement.

**In {in} –** in

**Som in id strad X –** I am in the street X

**Ghehdo bahe in dwodem bahsas –** I can speak in twelve languages

**Credeihm in fees –** I believe in fairies

**Instet {instEt} –** instead (of)

**Instet swehpe, ma ne orbats? –** instead of sleeping, why don't you
work?

**Inter {Intër} –** between

**Ia bent est inter dwo geronts –** the girl is between two old men

***Id joyel eet inter ia colunns –** the jewel was between the columns*

**Kata {kAta} –** from above, down

**Mathalan, ia pinegs hangend kata id mur nieb id ogwn quanta tengier
gwiv –** for example, the paintings on the wall near the fire all seemed
to be alive

**Kye {kye} –** in the direction of. *This preposition merges itself
with the following determinat or the pronoun in the 3*^*rd*^* person,
it's not rare to find something like kyid (kye + id).*

**Vahm gwahe kye mien dom –** I shall go soon in the direction of my
house

**Is fallit kya dubes ios hayd –** he fell towards the depths of Hell

**To ne est bad perfection, sontern to est un itner kye –** this is not
yet perfection, but this is a way in (that) direction

**Kyant {kyant} = kye + ant –** towards* (coming in the opposite
direction of)*

**Io vis iom gwehme kyant me –** I saw him coming towards me

**Med {med} –** with *(an instrument)*.

**Is magician construgit un castell med sien magia –** the magician
built a castle with his magics

**Saferam med wogh –** we traveled in a car

**Med alya werds –** with other words

**Medsu {mEdsu} –** amidst, in the middle of

**Eduarda khiek sib stambhes glihes medsu sien dakrus –** Eduarda
couldn't help herself from laughing in the middle of her tears

**Arriveer bad in vid unios mier menegh medsu quod is Lion ed is Unicorn
eent katuend –** they eventually arrived in sight of a big crowd in the
middle of which the Lion and the Unicorn were fighting

**Mustathna {mustA§na} –** apart from

**3.729.500 leuds gwiveer dien 1sto Januar 2015 in Kartvelia (mustathna
Abkhasia ed Sud-Iristan) –** 3.729.500 people were living on the 1st of
January 2015 in Georgia (not counting Abkhazia and South Ossetia)

**Na {na} –** in continuation to

**J. K. Rowling scripsit alya Harry Potter buks na “The Philosopher's
Stone” –** J. K. Rowling wrote other Harry Potter books in continuation
to “The Philosopher's Stone”

**Nespekent {nëspEkënt} –** despite

**Nespekent sien situation, Sophia biey meis pro iom quem pro se ye cid
moment –** despite her situation, Sophi feared more for him than for
herself at this moment

**Nieb {nyeb} –** next to, on the side of, beside

**Id dru est nieb id dom –** the tree is by the house

**Ob {ob} –** because (of)

**Eddo sabzi ob id suasmehct –** I eat vegetables because they taste
good

**Dank ob id antwehrd –** thanks for the answer

**Ois {oys} –** coming from, having its origin in. *Sometimes it is
written as “oys”.*

**Tuntos, ad pelu Stats ois id decolonisation moliet trehve ir tula –**
from this time, many States born out of the decolonization had
difficulties to find their balance

I think that is better I explain what “ad” is doing in “ad pelu Stats”
for those who didn't understand. In the example was used the verb
“molie”, which means “to be difficult to”, that's why was necessary the
use of “ad” for indicating the indirect object. The real subject of
“moliet” is “trehve”.

**Pati {pAti} –** including

**Ceters hieb esen lusen unte id naufrage, pati vasya mehmens os eys
gwito tuntro –** all other things had been lost during the shipwreck,
including all the memories of his life until then

***Sayg mi tien adresse, pati tien postal code –** tell me your address,
inclunding your postal code*

**Per {per} –** through

**Skapam per cid passage –** we escaped through this passage

**Presaygo id future per mien cristall ball –** I foretell the future
through my cristal ball

**Dec kilometers per hor –** ten kilometers per hour

**(Per)ambh {(për)Amb} –** around

***Piends sont ambh nos –** enemies are around us*

**Ia ghyors perambh id vallee –** the mountains around the valley

Ter est wed perambh id insule – There is water around the island

**Ploisko {plOysko} –** except

**Eddo quant genos os miems, ploisko pork –** I eat all kind of meat,
except pork

**Po {po} –** for *(in exchange of, in search for)*

**Io exchange un apel po un banane –** I exchange an apple for a banana

**Pon {pon} –** since, for

**Som her pon id auror –** I'm here since dawn

**Pon kamdiu reidneute yu in plen diewo? –** for how long have you been
riding in full daylight? (notice the use of the durative -neu-, I'll
talk about it in the appropriate chapter)

**Pos {pos} –** after

**Tolko tib pos io arrive –** I (will) talk to you after I (will) arrive

**Ho corregen werd pos werd –** I've corrected word by word

**Pre {pre} –** before *(in time)*

**Tolko tib pre linkwo –** I talk to you before I leave

**Pri {pri} –** by, along *(idea of movement)*

**Is vis un bloudun reider qui galoppit pri iom dextos levtro –** he saw
a blue-clad rider who was galopped before him from the right to the left

**Quando yu gwahsiete pri id cocin –** when you'll go by the kitchen

**El shamyu snahsit pri me –** the shark was swimming along me

**Pro {pro} –** for (the benefit of), to, per

**Kieupim un cavall pro te –** I bought a horse for you

**Eiskwo un wogh pro miena safers –** I want a car for my travels

**El behrger mohrit pro vies gwits –** the saviour died for your lives

**Dec kilometers pro hor –** ten kilometers per hour

**To est nearyo pro me –** that's Greek to me / it's double Dutch

**Prod {prod} –** ahead (of)

**Noster chef semper ghanct prod nies grupp –** our boss always walks
ahead the group

**Prokwe(m) {prOkwë(m)} –** near, close to

**El animal est prokwe me –** the animal is close to me

**Prosch {proc} –** close(r) to *(idea of approaching)*

**Id wogho gwehmt prosch me –** the car is approaching me

**Protie {protI:} =** contra

**I katueer protiev nos –** they fought against us

**Protiev {protyEv} = **Protie

**Samt {samt} –** with *(circumstance, description)*

**Un wir samt blou okwi –** a blue-eyed man

**Cid familia gwivt samt maungo noroc –** this family lives with a lot
of happiness

**Cheuso mien werds samt kaur –** I choose my words carefully

**Sekwos {sEkwos} –** following

**Sekwos id swodetermination referendum os 1sto Jul 1962, Algeria
proclamet sien independence –** following the self-determination
referendum of July 1st, 1962, Algeria proclaims its independence

**Speit {speyt} –** despite *(with a idea of disappointment)(is a
substantive as well)*

**Id tank, speit gwaur leusa, tadrijan upertrehsit id squadron –** the
tank, despite heavy losses, managed to get through his squadron

**Sub {sub} –** under *(often implies contact)*

**Ter est un arank sub id meja –** there's a spider under the table

**Sub un peruca ter est un calv cap –** under a wig there's a bald head

**Subpoena {subpOyna} –** under fear of *(penalty)*

**Uno mukhla al est tun dahn kay linkwes id land subpoena expulsion –**
A deadline is then given to him/her in order to leave the country under
threat of expulsion

**Suisraen {swisrAyn} –** notwithstanding

**Diplomatia ghehdeiht ia Stats suisraen ia differences iren
constitutional ed social systemes, do mutual comprehension –** diplomacy
allows States, notwithstanding the differences of their constitutional
and social systemes, to reach mutual understanding

**Suppose {supOzë} –** supposing that

**Suppose mathalan yu subscripte un contract bi un pharmaceutic firma
kay behrge vos ud diabetes –** let's suppose, for example, that you sign
a contract with a pharmaceutical firm to keep you from diabetes

**Swod {swod} –** in the way of, according to the custom of

***Swod iens atavs (salaf) sokwis ios Prophet –** In the way of the
ancestors (salaf) who followed the Prophet*

**Tiel {tyel}–** till. *But before a verb we use “hin(a)”.*

**Id verdia start ex id urb ed continuet tiel id mar –** the highway
starts from the city and runs to the sea

**Cursim tiel mien dom –** I ran up to my house

**Trans {trans} –** beyond

**Trans tod fluv –** beyond the river

**Trans destruge doms, hurcan Irma hat nicen leuds –** beyond the
destruction of houses, the hurricane Irma has killed people

**Ud {ud} –** from

**Un brev ud Marta –** a letter from Marta

**Ia kieupit un dom med denars ud ays pater –** she bought a house with
money from her father

**Ulter {Ultër} –** besides, moreover, in addition to

**Ulter id marche wehlen ios roy Huan, is conservit precieus-ye id royal
titule yeji quod is poiss mutalbe id noble nam Vasconcelos –** in
addition to the march order of King Huan, he kept preciously the royal
title according to which he had the right to claim the noble name
Vasconcelos

**Uper {Üpër} –** over *(it doesn't touche the object)*

**Un plav pleuct uper id tor –** a plane is flying over the tower

**Unte {Untë} –** during, in the space of *(temporal or spatial
meaning)*

**Unte id wer –** during the spring

**Unte id hall –** within the hall

**I construgeer id tor unte ia yars –** they build the tower during the
years

***Unte tod wakt, in Washington –** during this time, in Washington*

**Sub id schock, is Waktprince remien aunmov unte un khvil –** Under the
shock, the Prince of Time remained motionless for a while

**Vice {vits} –** at the place of *(it can also be used as a prefix)*

**Henrique ne hat ghohden dake ia buks. Vice to, Olivier hat ei yist ia
odt-documents –** Henrique could not get the books. Instead of this,
Olivier sent him the odt-documents

**Witerom {witërOm} –** opposite to *(it's also an adverb)*

**Witerom id dikhliz, is daskal skohpt me –** on the opposite end of the
gangway, the teacher was waiting for me

**Yant {yant} –** as soon as *(can be used with substantives)*

**Yant ho denars, kaupo un leuyk tib –** as soon as I get money, I buy
you a toy

**Ye {ye} –** *it doesn't have a defined meaning, it expresses a
circumstance or condition, you use this preposition when no other is
useful, it is our Joker card*

**Ye mien surprise –** at my surprise

**Ye mieno mayn –** in my opinion

**Io arrive do hem ye noct –** I arrive at the house at night

**Za {dza} –** in the back of

**Za te! –** in your back!

[]{#anchor-397}SOME DOUBTS THAT CAN APPEAR
------------------------------------------

I've tried to explain the prepositions well in the examples, I've even
used very similar examples in order to evidence the differences, but
some doubts can remain. In this subchapter I try to predict some of them
and I try to answer them.

### []{#anchor-398}DIFFERENCE BETWEEN “MED” AND “PER”

What's the difference between the twho sentences below?

**Is magician hat construgen un castell med sien magia –** the magician
has built a castle with his magic

**Is magician hat construgen un castell per sien magia – **the magician
has built a castle through his magic

The second sentence is gramatically correct, but it gives the impression
that the magician has build the castle with him in other dimension or
distant place, while in the first sentence the magician has build the
castle in the place where he was. Look whether with the example below
you can understand:

Glego per id fenster med mien durbin iam bikini-vehsend gwenak

I ogle through the window with my binoculars the bikini-weared young
woman

Did you notice why my second example *(the one of the magician who has
built the castle “per” his magic)* was strange?

### []{#anchor-399}DIFFERENCE BETWEEN “POS” AND “TRANS”

“Pos” is initially an adverb, in other words, if you say a sentence like
the one below:

**Id urb est pos id forest –** the city is after the forest

The sentece above means something like: first it's necessary cross the
forest before reachingthe city.

### []{#anchor-400}DIFFERENCE BETWEEN “EX” AND “UD”

Let's see three examples:

**Som ex ia Uniet Stats –** I am from the United States

**Un helm ex stal –** a helmet of steel

**Un brev ud Klara –** a letter from Klara

The “ex”, when not in the sense of “out”, is used for something made
from the mentioned object or came from it, like a helmet of steel or a
person from a country. The “ud” is used for things that didn't come from
within the mencioned object, like we don't expect that the letter was
made from the body of Klara, so we use “ud” instead “ex”.

### []{#anchor-401}DIFFERENCE BETWEEN “VICE” AND “INSTET”

As prepositions both means the same thing, but “instet” can be also a
adverb while “vice” also can be a prefix, like in “vice-president”.

Henrique ne hat ghohden dake ia buks. *Instet*, Olivier hat ei yist ia
odt-documents

Henrique ne hat ghohden dake ia buks. *Vice to*, Olivier hat ei yist ia
odt-documents

[]{#anchor-402}ENCLITICS AND PROCLITICS
=======================================

Some of these words are not easy to be translated, but this chapter will
teach you how to use them. Enclitics are words put after another word
while proclitics are words put before another word.

The enclitic “tun” is the demonstrative of “kun” or “quando”.

**Herodotos extendt tod nam do id continental hinterland, beuwend it
Asia id trit part ios gnoht mundios tun –** Herodot extends this name to
the continental hinterland, making thus of Asia the third part of the
known world then* (at that time)*

**Id Occidental nam ios land gwehmt sigwra ex id Medieval Chinese
pronunciation tom khanjis, quod tun buit launto do id bahsa Indonesias
ka “Jepang”, ed dind ab Portughesche nauts –** the Occidental name of
the country surely comes from the Medieval Chinese pronounciation of
those hanzis, which was then borrowed into Indonesian as “Jepang”, and
afterwards by Portuguese sailors

When you have to use some clitic, but you haven’t a word with the
convenient accentuation that supports the clitic, you can use “nu” as a
support, like in “nughi” *(nu + ghi)* or with the relative pronouns
“yos”, “ya”, “yod”, “yel” *(for example: nu- yos, nu-yel)*.

**Chanda ex Zambia, nu-yos suabaht Sambahsa, gnoht maung bahsas –**
Chanda from Zambia, who speaks well Sambahsa, knows many languages

“ge” and “tar” are like “ghi”, but as, sometimes, the repetition of the
particle “ghi” occurs very repeatedly, it's used these two particles.
“ge” emphasizes personal pronouns and articles, while “tar” emphasizes
interrogative pronouns.

**Ia gehnsiet un son, ed ei dahsies id nam Jesus; is ge salvsiet sien
popule ex idsa synts –** she will conceive a son, whom she will give the
name Jesus; it’s him who will save his people from its sins

**Yed, isge ieg kam weysmee –** however, he did like we (did)

-ge is connected to a personal pronoun, in this sentence of the example
-ge is here to emphasize the opposition with “wey-smee”

**Quod tar tengicit is? –** what did he (really) look like?

Often “tar” and “ge” are not directly translated to another language,
especially “tar”

An excellent example with “nu” and “tar”:

**Sei nu id luce quod est in te est temos, kam tar megil sessiet tod
temos! –** if, therefore, the light that is in thee be darkness, how
great is that darkness!

The enclitic “pet” reinforces identity, often translated as our “very”.

**Henrique weict in idpet centre ios citad –** Henrique lives in the
very center of the city

To write “…in idswo centre…” would be strange because it means “… in the
center itself …”

**Id brev hieb gwohmt unte idpet dwer ios laboratorium –** the letter
had come through the very door of the laboratory

**Est un prientlik geront quospet ghalv buit trohven medsu eys bustan
–** it's a friendly old man whose very skull was found in the middle of
his garden

**I reuschus leuds eent ipet parents ias magv –** the people who had
rushed were *(precisely)* the parents of the child *(after that child
had an accident)*

*ipet = i *(the []{#anchor-403}definite article)* + pet*

When “pet” is used after a verb, it has the sense of “to be in the
capacity of”.

**Sei yu plais steighte con me do mien fiaker, duco-pet vos ad eys
baytel –** if you accept to get in with me into my cab, I *(can)* drive
you to his home

With adverbs of time and space the word “kye”, as an enclitic,
reinforces the adverb.

**… ed terkye id sinister masse uns bina forstilb ids ghebel uper id
strad –** … and just at that point, a certain sinister block of building
thrust forward its gable on the street

**Ed mehnte ghi od is nos duxit tei stet querkye staht id dwer?! –** and
where do you think he carried us but to that place with the door?!

**“Nunkye”, bahsit alter, antwehrdte mi: “quois gnohte yu me?” –** “And
now”, said the other, you answer me: “how did you know me?”

Notice that “antwehrdte” and “gnohte yu” refer to one person, not to a
group as it may look at a first moment. It's becauste it's used the
pronoun of cortesy “yu”, indeed you only can know it by the context.

**Tunkye eet circum nev saat ios aghyern, ed id prest nebule ios saison
–** it was by this time about nine in the morning, and the first fog of
the season

**Gheskye ia ne eevevis iom pon quasi dwo munts –** it was nearly two
months since she had seen him till yesterday

The enclitic “wert” indicates reported speech.

**Sambahsa estwert baygh interessant –** Sambahsa - it is said - is very
interesting

**Sekwent qui ghieng pri iens unte ir mingo promenades, bowert sieyg
neid –** it was reported by those who encountered them in their Sunday
walks, that they said nothing

**Sekwent tom mertikwol, ia bent comwert hieb haben meis dekhschat quem
gvol –** well, the child was not much the worse, more frightened,
according to the Sawbones

**Iswert liek stets meis confinit-se in id practis uper id laboratorium,
quer is hatta swohp yando –** the doctor, it appeared, now more than
ever confined himself to the cabinet over the laboratory, where he would
sometimes even sleep *(in the preceding sentence of the translation,
what we know about the doctor is reported by his butler)*

The proclitic “proe” is often translated as “already”.

**Kun id nam os Hyde buit swohrt, Ustad Utterson proe-akowsicit, bet kun
is chohx id klor, is khiek etidwoie –** when Hyde's name was uttered,
Mr. Utterson was already listening, but when he checked the bit of wood,
he no longer doubted it

**Mien kerd hat tem aghnuet quem ioschi proe-plangiem –** I came away
with that upon my heart, that I could have wept too

**… proeghi tetyohc pelu dwogimtias –** … a thing that had befallen many
scores of times

The conjunction “… men (…) de …” or “… nu (…) de …” shows an opposition
between two sentences:

**Olivier men weict in France; Henrique de in Brasilu –** Olivier lives
*(on his side)* in France; Henrique *(for what regards him)* in Brazil

**Central ed Eust Asia nu est befolct ab mongoloids. Indonesia de
superpont uni aborigine substrat malays populations –** Central & East
Asia is populated *(on its side)* by mongoloids. Indonesia *(on its
side)* superposes malay populations to an aborigine substrate

uni = un + i = ad un (dative singular)

“… men (…) de …” and “… nu (…) de …” have the very same role.

But when “nu” is used alone, that’s to say, it’s not used in conjunction
with “de”, it’s an adverb that means “namely”.

**Un public wesnum est un contracto conclus ab un contragend magh, nu id
Stat, ia regional au local autoritats –** a public procurement is a
contract concluded by a contracting authority, namely/that is to say the
State, the regional or local authorities (etc …)

The enclitic affix -schi already was explained and the enclitic affix
-smee will be explained soon.

[]{#anchor-404}USEFUL AFFIXES
=============================

Differently of other auxiliary languages, Sambahsa rely more on borrowed
words than composed words. Sambahsa has so many languages as sources
that is impossible to have a regular system of derivation, but some
useful affixes can be listed.

Some basic rules can be considered in forms derived from the romance
languages, an important characteristic of theirs is that they're based
on the “perfect stem”, which is predicted in the following ways:

For verbs ending with unstressed “e”, it's used -at- instead.

form*e* → form*at*- → formation

For verbs ending with “ie”, it's used -icat- instead.

publ*ie* → publ*icat-* → publication

For verbs ending with “ue”, it's used -ut- instead. It also is applied
to verbs whose past participles in “t” ends with -wt.

constit*ue* → constitut*ut-* → constitution

For verbs ending with “eih”, it's used -it- instead.

add*eih* → add*it-* → addition

The perfect stem of other verbs corresponds to their participles in “t”.

[]{#anchor-405}PREFFIXES
------------------------

**ab- –** away

**Abcurr –** to run away

**apo- – ***indicates the fourth generation*

**Apopater –** great-grandfather

**be- –** *makes transitive verbs as be- in English*

**begh(i)s- –** deprived of

**bfu- –** *negative prefix before words of Sinitic origin.*

**cum- –** *similar to words with the prefix god-*

**Cumpater –** godfather

**Cummater –** godmother

**dus- –** ill-, bad

**Dusmenos –** ill-disposed

**en- –** to put into

**Enquestion –** to put in question, to question

**ender- –** *diminutive of action*

**Endervid –** to catch a glimpse of

**Enderghyan –** to half-open

**eti- – **Indicates the fifth generation

**for- – **corresponds to the adverb astray

**ga- –** before a verb with apophony: indicates the result of an
action; if it has the suffix -os it indicates something pejorative.
Before a substantive it indicates sum*.*

***Gabehrg –** mountain range* (behrg = cliff)

**ken- –** empty of

**mu- –** *may be used to indicate the executer of an action when
preffixed to a word of “muslim” origin*

***Mussafer –** traveller* (safer = to travel)

**muta- –** to change

**Mutamayn –** to change opinion

**Ni- –** *means “down” in a figurative sense*

***Niklad –** to download* (klad = load)

**(oi)s-** – the opposite of en-, it means “to put off/out”

**oischalt –** to switch off

**(en)schalt –** to switch on

**or- –** original, primeval, primitive

**Oraryo – **pre-aryan or proto Indo-European (aryo = arian)

**par- –** completion or fullfilled action

***parkwehr –** to archive* (wehr = to do)

**peri- –** through

**Perigumt –** throughfare (gumt = coming)

**pro- –** indicates the third generation*. Before a verb it means “in
front of” or “preceding” and triggers the dative.*

**Pronepot –** grandnephew

**Is mi proghieng –** he was walking in front of me

**rhayr –** negative preffix used before words of arab origin, but often
is used as a indepentent adverb

**Rhayr yakin –** unsure

**step- –** *family after a second marriage)*

**Steppater –** stepfather

**Stepmater –** stepmother

**Steppurt –** stepson, stepdaughter

**sua- –** well

***Suakwohrt – **well done (kwohrt = past tense of “kwohr”, which means
“done”)*

**ud- –** capacity of doing better than others

**Ho udsnaht iom –** I've outswum him

With the reflexive pronoun “sib”, it indicates the way of getting
something.

**Id mafia sib udtehrct id silence schahiden –** The Mafia gets the
silence of witnesses by threatening them

[]{#anchor-406}SUFFIXES
-----------------------

### []{#anchor-407}EXPRESSING QUALITY OR STATE

In some cases it's added the suffix -e and it's submitted to apophony if
possible.

**Long \[long\] (length) –** longe \[londj\] (lengthness)

**Deub \[döb\] (deep) –** dube \[düb\] (deepness)

**Slab \[slab\] (weak) –** sliebe \[slyeb\] (weakness)

This system only works if there is a phonetic difference between the
adjective and the derived form. For other adjectives is used the suffix
-(e)t.

**Mild \[mild\] (mild) –** mildet \[mIldët\] (pity)

**Mynder \[mÜndër\] (proud) –** Myndert \[mÜndërt\] (pride)

It's possible the use of others affixes like -os and -ia. For words of
romanic origin it's used -or and (i)tat.

### []{#anchor-408}EXPRESSING AN ACTION

If the own stem is not the sufficient, the ending -(e)n may be added
*(or -sa for verbs ending with a stressed vowel sound)*. For verbs of
romance origin is added -(t)ion is added to the verbal stem.

### []{#anchor-409}INDICATING THE PERFORMER OF AN ACTION

The most used is -er, very similar to English. A pratical set is -ant
for who makes the action, -eit for who suffers this action and -at to
indicate the result or object of this action.

Is telephonant kwehrt un telephonat pro iom telephoneit

The caller makes a phone call to the called

### []{#anchor-410}OTHER SUFFIXES

**-ar –** colection. For names of profession it means “maker of”

**-asc –** to become

**-at –** years old

**-av –** in a verbal stem it means “inclined to” (the verbs are
modified because of nasal infix and other reasons)

**-ber –** berry, fruit

***Vinber –** grape* (vin = vin)

**-ble –** possibility. Very similar to the English -ble. If there is a
risk of confusion with other conjugated form, then use -et (or -im if
the -t is not possible)

***Dyehrcet –** glimpsable *(dyehrc = to glimpse)

**-dem –** region. Very similar to English -dom.

***Roydem –** kingdom* (roy = king)

**-eus –** adjetive of quality.* *It corresponds to the -ous from
English

***Bulbeus –** bulbous *(bulbe = bulb)

**-en –** adjective of substance

***Golden –** golden *(gold = gold)

**-fred –** free from

**-ia –** quality, science, country

**-ic –** forms adjectives*. *It corresponds to adjectives that ends in
-ic

***Cubic –** cubic* (cube = cube)

**-iev –** fruit, grain

**-ika –** young *(female) *

***Potnika –** miss* (potnia = lady)

**-iko –** young *(male) *

***Potiko –** galant* (poti = sir)

**-il – **susceptible to, open to.

**-in –** feminin suffix *or* “forest of”

**-(i)sk – **adjective of origin

***Coschmarisk –** nightmarish* (coschmar = nightmare)

**-isme –** study of something, theory, ideology

**-iste –** participants or supporters of a study of something, theory
or ideology

**-ka –** makes the feminine diminutuve in a stressed syllable

**Ritka & Hanko –** Hansel & Gretel

**-ko –***** ***makes the masculine diminutuve in a stressed syllable

**Ritka & Hanko –** Hansel & Gretel

**-lik –** -like

**Ays face est cavallik –** her face is horselike

**-log –** -logist

**Astrolog –** astrologer

**Cardiolog –** cardiologist

**-ment –** corresponds to no-adverb borrowed words that ends in -ment.
For matter of accentuation, this suffix counts as apart substantive
within of a compounded word

**-mon –** who can

**-ner –** masculine suffix

***Eireanner –** irishman* (eirean = pessoa ou coisa da irlanda)

**-nic – **pejorative *(masculine)*

**Drehnknic –** drunkard

**-nica –** pejorative *(feminine)*

**-on –** precision *(with hyphen)*

**Segim mien diemens. Bet ye tod-on momento, ies Orks vols ed
exkardkiesch ir simitars ed dagas –** I cut my bonds. But at this very
moment, the Orcs turned around and pulled out their scimitars and
daggers

**-os –*** *on a verb it means “game of”

**Skeulkos –** esconde-esconde

**-smee –** always on personal pronouns to emphasize oppositions

**Weysmee habmos naiwo likwno nies parents –** We (not you) have never
left our parents

**-ster –** feminine suffix. Use this only there are no problems with
change of accentation

**-ure –** on the perfect stem it means “result” or “quality”

**-went –** maks adjectives, corresponds to the English -ful. For matter
of accentuation, this suffix counts as apart substantive within of a
compounded word.

***Dakruwent –** tearful (dakru = tear)*

[]{#anchor-411}STYLE
====================

Different from the national languages, which have native speakers
inserted in a determined social context, Sambahsa is to be spoken
between people of different backgrounds. A Mongolian will find
difficulties in understanding a person who speaks in the German way,
regardless how the Sambahsa of this second person is grammatically
correct and how his pronunciation is good.

You must beaware to don't translate a sentence like “It's necessary to
repair the house” to “id est necessar urpes id dom”, the correct
translation is “est necessar urpes id dom”, although the better way of
expressing this idea would be “id dom tehrpt bihe urpt”. Work on to
avoid transporting characteristics from English to Sambahsa.

Do you know what is idiotism? It's is a linguistic term used to describe
terms that are only used in a determined language, if they were directly
translated to other language, they wont have any sense. I'll show you
some examples from the French language:

**Cherchez la femme –** police slang for situations when there is a
complication and nobody knows who was the responsible. In a literal
translation it is “look for the woman”

**Pourboire –** it's our tip *(the extra money that we give to the
waiter)*. In direct translation it is “for drinking”.

It's not like the idiotisms are untranslable, it's their direct
translation that is unfeasible. Are understanding what I am trying to
say?

I am not saying that you have to speak a poor and personalityless
Sambahsa! But you have to be aware in avoiding idiotisms and other forms
of speaking that are proper of Americans or British.

**What I am trying to say is:** talk in a simple way, but without lose
the personality that the language allows us to have and the richness
that it offers us.

[]{#anchor-412}CHOOSING NEW WORDS
=================================

The primary source is, surely, what was reconstructed from the ancestor
(Proto-)Indo-European. As not all words were reconstructed, many of them
can be obtained from Indo-European ancient languages like Sanskrit,
Ancient Greek and Latin.

Other sources are borrowed words from, at least, two linguistic branchs,
from West Europe until East Asia.

There is no reason to worry about new words, Sambahsa already has a
great lexicon and new words will come from the absortion of neologisms.
If it's not possible a translation from the existent lexicon in
Sambahsa, can be used the word in its original form, the terms from the
computer science area are a good example about it. Words that imply a
specific cultural item, like a martial arts of a people or a typical
dish, will be used the own word. It's similar what would happen in
English, for example, the word 武術 *(wushu)* was translated in the
nearest way to what is pronounced in the original, respecting the limits
of the English language that doesn't have the intonations of Chinese and
other different sounds.

As you can see, naturally that the new words have to be adapted to the
orthography of Sambahsa, which due its flexibility tries at best
maintain the words in their original form and original pronounce.

[]{#anchor-413}COMMON MISTAKES AND QUESTIONS
============================================

In all this grammar I've tried to explain all the details of the
language, but something that was not understood by someone may always
exist. So I've created this chapter for listing all the mistakes and
doubts I had throughout my learning in Sambahsa, I hope that this
chapter be useful for you.

Obviously that, in future versions of this grammar, questions of other
people will be added here.

[]{#anchor-414}HOW WORK GENERIC PRONOUNS LIKE “TO” AND “QUO”?
-------------------------------------------------------------

Words like “to” and “quo” are generic pronouns that especially refer to
already said things, they are for neutral singular
nominative/accusative. They don’t suffer declination.

Henrique hat creet un nov automatic dictionar. Tod suawehrct!

Here “tod” refers to “automatic dictionar”.

Henrique hat creet un nov automatic dictionar. To est un khauris khabar!

Here “to” sto the previous sentence, to the fact that Henrique has
created a new dictionary.

“To hat duren pior diu!!!!” sieyg is ob impatience.

Here “to” refers to the situation that created this impatience.

**El prient: “Volo kaupe un ieftin deluxe auto, bet ne trehvo semject
interessant in id journal”.**

Serter: Est her un annunce quod correspondt quo tu paurskes

Here “quod” refers to “annunce” and “quo” refers to something that was
not said in the sentence.

Is it possible more generic pronouns? In theory would be possible
pronouns like “cio” and “eno”, but they almost are never used.

[]{#anchor-415}WHAT REALLY MEANS THE VERB “LEIT”
------------------------------------------------

It means “to go, to run” in the figurative sense.

**Is vohs un armur quod baygh duslit ei –** he wore an armor that wasn't
good in him

**duslit = **dus (preffix for “bad” or “ill-”) + lit (past tense of
“leit”)

**In ielg poesis, lit meis au minter dayir pisk –** In each poetry, the
matter was more or less about fish

**Kam leitte yu? –** How are you? *(*or* “how are you going?”)*

**Bet tod conversation leit lyt pior oku –** but this conversation is
going a bit very quickly

**Id grance leit engwn id Rhen –** the boundaries goes along the Rhine

More examples, but with te word “uperleit”:

**Hind uperleit ex un agrent societat eni un democratic quader –** India
makes the transition from a agricultural society within a democratic
framework

**Id economic crosct nilent, uperleitend ex 9% pro yar in 2010 do 6% in
2011 –** The economic growth is slowing down, passing from 9% for year
to 6% in 2011

**Mehnent maghe uperlites fauran do id politic nivell –** They think
*(they)* can pass immediately to the political level

**Serter uperlit is do id camp ios Papp –** Later, he joined the camp of
the Pope

**Ghehdt uperlites id hol territorium uns land –** it can go *(extend)*
beyond the whole territory of a country

[]{#anchor-416}HOW ASK SOMETHING TO SOMEONE
-------------------------------------------

How translate “I asked my father for money”. The most correct
translation is *not* “prohgim denars ad mien pater”, let's see the
problems of this sentence. The sentence in its passive form:

**\* Denars buir prohgen ab me ud mien pater –** money was prayed to me
by my father

Remember that the verb “prehg”, in the sense of “ask something to
someone” is used with the word “ke(m)” and in this case should be used
the “ud” instead “ad”. Let's see how would be the translation of this
absurd sentence.

**\* Prohgim denars ad mien pater –** I prayed to money for my father

Now see the sentence how it should be translated:

**I asked my father for money –** prohgim mien pater ke mi daht denars
*(**literally:** I asked my father that he give me money)*

Notice that “mi” is in the dative case

[]{#anchor-417}HOW TO SAY WHETHER SOMETHING WILL BE DELAYED OR BE LATE?
-----------------------------------------------------------------------

Let's start with the verb “chitay”. You can make sentences like:

**Tod saat chitayt –** this clock is late

**Id machine chitayt pre schalte –** the machine is delaying to start
*(**in direct translation:** “the machine delays before starting”. You
mustn't use “kay” here)*

How the transitive verb “chitay” may be used with persons:

**Id seuy hat chitayt mien prient –** the rain has delayed my friend

You can use the adjective “skept”.

**Mien prient est skept ob id seuy –** my friend is late due the rain

There are several forms of expressing an idea:

**Id machine tehrpt wakt pre biwehrge –** the machine needs time before
it start to work

[]{#anchor-418}VERBS OF DIRECTION LIKE “GWAH”
---------------------------------------------

Verbs like “gwah” (to go to) dispense prepositions, because their direct
objects already informate that they are the direction of movement. The
preposition may serve to informe something more, like in the examples
below:

**Vahm gwahe kye mien dom –** I am going to my house *(I am going to the
house, but I don't plain to enter into it)*

**Vahm gwahe mien dom –** I am going to my house *(I enter into the
houseentro na casa)*

[]{#anchor-419}WHAT'S THE DIFFERENCE BETWEEN “YAKIN” AND “WEIDWOS”?
-------------------------------------------------------------------

While “yakin” is an adjective, “weidwos” is an adverb.

Always pay attention in the dictionary to the gramatical
classificationof the words.

[]{#anchor-420}ABOUT MINOR GRAMMATICAL ISSUES
=============================================

[]{#anchor-421}NAMES OF LANGUAGES AND NATIONALITIES
---------------------------------------------------

Look the following text: “Did you know that Spanish is spoken by more
than five hundred million people? But there are only forty-seven million
Spaniards”.

This very same text in Portuguese: “Você sabia que o espanhol é falado
por mais de quinhentos milhões de pessoas? Mas só existem quarenta e
sete milhões de espanhóis”.

Notice that in English the first letter of the name of the language was
written in capital letter and didn't received a definite article, the
gentile was also written in capital letter. But in Portuguese both the
name of the language and its gentile always has its first letter written
in lowercase, and the name of the language receives article.

Which model Sambahsa follows? Both are valid, write in the way you think
is better.

[]{#anchor-422}WORDS RELATED TO NUMBERS LESSER THAN TWO
-------------------------------------------------------

What's correct, “1,5 liter” or “1,5 liters”? In Sambahsa both are
correct.

[]{#anchor-423}VERBAL CONCORDANCE OF THE VERB “ES”
--------------------------------------------------

Look the following sentences:

\(1) – Id bratriya est i Purts ios Desert 

\(2) – Id bratriya sont i Purts ios Desert

Which one would be the correct in this case, “est” or “sont”? To which
sentence the verb “es” preferably agrees, “Id bratriya” or “i Purts ios
Desert”? In such cases, if there is an element in the plural, the verb
to be used will be in the plural, therefore the correct would be (2).

Yeah, I know that the best way to write this sentence would be “id
bratriya est composen ab i Purts ios Desert”, but I've forced a more
informal sentence in order to show you what to do in situations like
that.

[]{#anchor-424}WORDS AND THEIR MEANINGS
=======================================

Words have layers of meanings, like the word “fortuitous”, which means
“coincidental” or “unplanned”, but some people think that it means
“coincidentaly fortunate” or “unplannedly fortunate”, although a
fortuitous thing can be unfortunate.

If you see something like “milder”, which means “mitigate”, don't assume
that it means “to militate” or “to provide reasons for” because it
actually means “to alleviate”.

When you're consulting the dictionary, be careful with this kind of
situation in order to the message doesn't be passed or received wrongly.
Of course that, in the dictionaries, always there will the effort to
make more-explicative dictionaries, but in doubt consult an English
dictionary for making sure about the meaning of the word.

[]{#anchor-425}VOCABULARY
=========================

You've learned much of what you need for using this beautiful language
called Sambahsa. I'll not be unmodest in affirm that I've passed all
that can be taught, even the auxiliary languages requires volumes and
more volumes if they were taught deeply, but I hope that this grammar
had taught you to use the language in at least 99,90% of the situations.

This last chapter has as goal expand a bit more your vocabulary, it's
obvious that I'll not teach all words, because this book would have to
be also a dictionay for this task be possible, but I want that you
finish this book with a reasonable vocabulary, in order to you can
initiate the reading of texts with a bit of comfort.

There are several subchapters that treats several areas, like human body
and computers, but it will be just basic concepts, there is no
pretension here in to list the names in Sambahsa of all muscles and
bones of the human body or all the names concerning Information
Technology; you'll have to search the words in a dictionary or, in the
case of very specific terms, in dictionaries about a specific area like
dicionaries of medical terms.

Also it's important to pay attention in which subchapter the words are
found in order to you don't have unnecessary doubts about the actual
meaning of the words. I wanna avoid situations like you see the word
“musch”, whose translation is “fly”, and you think that it means the
verb “to fly”; if this word is in the subchapter ANIMALS, probably it
refers to an insect of the order Diptera instead of that action that
airplanes do. Resumming: through the subchapter where the word is found
you can deduce in what context this word can be used.

In order to you expand your vocabulary – and consolidate your knowledges
of grammar – it's essential that you read, listen and practise the
language.

[]{#anchor-426}VERBS
--------------------

*Although the English words are in the infinitive form, the presented
Sambahsa verbs are in their stem form. Exemple: the **infinitive** verb
“to add” is “addihes” in Sambahsa, not “addeih”. Are we understood?*

**Acquiseih {akizE:y} –** to acquire *(like, for example, in a legal
sense)*

**Addeih {adE:y} –** to add

**Adore {adOr} –** to like, to love; to worship

**Al {al} –** to raise *(in the sense of “I raise hens”)*

**Annem {Anëm} –** to breath

**Antwehrd {antwE:rd}–** to answer

**Appareih {aparE:y} –** to appear, to make evident *(this verb is
rarely used, don't confuse with “prehp”)*

**Arrive {arIv} –** to arrive

**Au {Ao} –** to be without

**Aur {Aor} –** to hear

**Ay {ay} –** to consider *(**me ays tien prient** = you consider me as
your friend)*; to say, to tell *(“…” **iey is =** “…” said he)*

**Aygve {aygv} –** to feel ashamed of

**Bah {ba:} –** to say, to speak

**Balbel {bAlbël} –** to chat

**Balnye {bAlnyë} –** to bathe

**Bay {bay} –** to fear

**Behrg {be:rg} –** to save, to put out of danger

**Beud {böd} –** to appeal to *(someone)*, to make* (someone) *pay
attention* (to = dative)*, to demand* (something)*

**Beuw {böw} –** to make grow, to make become; to cultivate *(a
particular culture)*

**Bih {bi:} –** to become

**Brinegh {brInëg} –** to bring

**Bruneg {brÜnëg} –** to enjoy, to profit

**Cheid {tceyd} –** to quarrel

**Cheus {tcös} –** to choose

**Chitay {tcitAy} – **to delay *(for machines and things)*

**Clud {klud} –** to close

**Comprehend {komprEhënd} –** to comprehend *(to have comprehension
for)*

**Credeih {krëdE:y} –** to believe

**Cultive {kultIv} –** to cultive

**Curr {kur} –** to run

**Dagh {dag} – **to switch on, to light

**Dah {da:} –** to give

**Dak {dak} – **to receive, to get

**Daum {dAom} –** to wonder

**Deh {de:} –** to put

**Dehbh {de:b} –** to suit, to fit in

**Dehlg {de:lg} –** must

**Dehm {de:m} –** to put ub; to train *(an animal)*

**Deik {deyk} –** to show, to indicate

**Desire {dezIr} –** to desire

**Destrug {dëstrUg} –** to destroy

**Deulg {dölg} –** to owe

**Disradh {disrAd} –** to advise Y against X

**Tib disradho namore ciom yuwen –** I advise you to don't date that
youngster

**Drehnk {dre:nk} –** to drink* (alcohol)*

**Edd {ed} –** eat

**Eih {e:y} –** to go

**Eiskw {Eyskw} –** to seek, to intend, to want

**Em {em} –** to take *(in the sense of “take a train” or “take a
decision”)*

**(En)schalt {(en)cAlt} –** to switch on *(a machine)*

**Entre {Entrë} –** to enter

**Euc {ök} –** to learn

**Exporte {ekspOrt} –** to export

**Fall {fal} –** to fall

**Faungmoen {faongmOyn} –** to pay a visit to

**Feug {fög} –** to flee (from)

**Fineih {finE:y} –** to finish

**Fleurt {flört} –** to flirt

**Folossie {folosI:} –** to use, make use of

**Fortrehc {fortrE:k} –** to depart, to go off *(for a travel)*

**Gahab {gahAb} –** to spare *(an enemy)*

**Gehn {dje:n} (+ acc) –** to father, to create *(for literary purposes
you can say “bringhes do luce”)*

**Ghab {gab} –** to understand *(a fact or situation)*

**Gham {gam} –** to marry

**Ghang {gang} – **to walk

**Ghat {gat} –** to meet

**Ghehd {ge:d} –** to be able to, can

**Ghend {gend} –** to take *(a thing literally with the hands)*

**Ghohd {go:d} –** *past tense of “ghehd”*

**Ghyan {gyan} –** to open

**Gnah {nya:} –** to be born

**Gnoh {nyo:} –** to know

**Gugheir {gugEyr} –** to mess up, to spoil something

**Gvaedd {gvayd} –** to guess

**Gvehd *****&lt;something&gt; &lt;from someone&gt;*****{gve:d} –** to
pray *(for something from someone, like in “I nowngmins gvehde seuy ud
Div”, which means “the peasants pray for rain from God”)*

**Gwah {gwa:} –** to go to

**Gwehm {gwe:m} –** to come

**Gweup {gwöp} –** to keep, to conserve

**Gwiv {gwiv} –** to live

**Gwohm {gwo:m} – ***past tense of “gwehm”*

**Heih {he:y} –** to reach *(a precise point)*, to hit

**Hock {hok} –** to crouch

**Iey {yey} –** *past tense of “ay”*

**Ih {i:} –** *past tense of “eih”*

**Importe {impOrt} –** to import *(a product)*

**Istifsar {istifsAr} –** to ask *(an information)*

**Jinkdou {jinkdU:} –** to take place, to go on *(in the sense of “the
second movie takes place in China”)*

**Jlampoh {jlampO:} –** to drink *(water)*

**Kam {kam} –** to like

**Kamyab {kamyAb} –** to succeed in

**Kan {kan} –** to play *(an instrument)*

**Kau {kAo} –** to notice, to take note of

**Kaup {kAop} –** to buy

**Kaur {kAor} –** to care *(kaur de = “to care for”)*

**Keul {köl} –** to highlight; to cultivate *(a land)*

**Keung {köng} –** to hesitate, to delay

**Keup {köp} –** to demand, to require

**Khak {qak} –** cannot. *This word also means “bad”.*

**Kheiss {qeys} –** to feel

**Kieup {kyöp} –** *past tense of “kaup”*

**Klehpt {kle:pt} –** to steal

**Kleu {klö} –** to listen to

**Kleuster {klÖstër} –** to listen to *(more carefully)*

**Klu {klu} –** *past tense of “kleu”*

**Kurihen {kurIhën} –** to purchase

**Kussen {kUsën} –** to kiss

**Kwah {kwa:} –** to take in, to gather; to succeed in (doing)

**Kwehk {kwe:k} –** to seem

**Kwehr {kwe:r} –** to do

**Lass {las} –** let

**Leik {leyk} –** to play *(a game)*

**Leis {leys} –** to read

**Leips {leypz} –** to miss *(a deadline, a bus…)*

**Leit {leyt} –** to go, to run *(figurative sense)*

**Lever {lEvër} –** to deliver

**Lieubh {lyÖb} –** to love

***Se en*****lieubh *****in/med/in/… ***** {lyÖb} –** to fall in love

**Linekw {lInëkw} –** to leave

**Lis {lis} –** *past tense of “leis”*

**Lites {lits} –** *infinitive form of “leit”*

**Localise {lokaliz} –** to locate

**Magh {mag} –** can, may

**Maghses {mAgsës} –** may be

**Man {man} –** to stay

**Manage {manAdj} –** to manage (something), to take good care of

**Mank {mank} –** to lack, to be lacking *(intransitive verb)*

**Maximise {maksimIz} –** to maximize

**Mayn {mayn} –** to mean

**Mehld {me:ld} –** to point ou, to signal, announce *(like in “John
signals to his son that the dictionary has errors”);* to pray

**Mehm {me:m} –** to remember

**Mehn {me:n} –** to think

**Meil {meyl} –** to prefer, to like

**Meuk {mök} –** to release

**Miegh {myeg} –** *past tense of “magh”*

**Minimise {minimIz} –** to minimize

**Miss {mis} –** to miss *(someone)*

**Myehrs {mye:rz} –** to forget

**Mohn {mo:n} –** *past tense of “mehn”*

**Mutt {mut}** – to take place, to have grounds to *(in the figurative
sense, like in “I hope that the events take place in this direction”)*,
to go *(in the figurative sense of “how things are going?”)*; to have
reasons to

**Nak {nak} –** to reach

**Namor {namOr} –** to date *(like in “John and Mary are dating”)*

**Naudh {nAod} –** to need, to require

**Nayd {nayd} –** to scoff at

**Neic {neyk} –** to kill

**Niklad {niklAd} – **to download *(a file)*

**Neud {nöd} –** to use

**Nieudh {nyöd} –** *past tense of “naudh”*

**Obsok {obsOk} –** to search *(investigation)*, to rumage
*(investigation)*

**Oisschalt {oyscAlt} –** to switch off *(a machine)*

**Orbat {orbAt} –** to work, to labour

**Paursk {pAorzk} –** to search, to look for

**Pehrd {pe:rd} –** to fart *(with noise)*

**Pehrn {pe:rn} –** to sell

**Pehzd {pe:zd} –** to fart *(with bad smell)*

**Peit {peyt} –** to try

**Permitt {përmIt} –** to allow

**Permiss {përmIs} –** *past tense of “permitt”*

**Phone ad {fon ad} –** to ring, make a call for *(phone call)*

**Plan {plan} –** to plan

**Pleuk {plök} –** to fly

**Poh {po:} –** to drink

**Poitt {poyt} –** to have the right to, to be entitled to

**Prehg *****&lt;someone&gt;***** ke(m) {pre:g &lt;&gt; ke(m)} –** to
ask, to pray *(like in “Henrique prehct Olivier kem (is) ei antwehrdt”,
which means “Henrique asks Olivier for an answer”)*, to talk *(in the
sense of “to talk with someone about a matter”)*

**Prehp {pre:p} –** to appear *(in the sense of become visible)*, to
come into sight

**Preim {preym} –** to receive *(intentional reception, like in “to
receive some friends”)*

**Prete {pret} –** to understand *(a language or what was said)*

**Prodah {prodA:} –** to hand over *(a criminal)*

**Puwen {pÜwën} –** to clean

**Raubh {rAob} –** to steal, to rob

**Recep {rëtsEp} –** to receive *(normally in a more technical sense
like in: here in my village we receive the TV signal from Germany)*

**Reik {reyk} –** to return to, get back to

**Reiss {reys} –** to draw

**Resid {rëzId} –** to reside, to live

**Sagv {sagv} –** to know how to

**Salg {salg} –** to go / get out of

**Salg con {salg kon} –** to get together, to go out with *(it can have
a sense of dating)*

**Sayg {sayg} –** to say

**Schehnk {ce:nk} –** to give, to pour, to pay *(it may refer to a
present)*

**Schikay {cikAy} –** to complain

**Scrib {skrib} –** to write

**Sedd {sed} –** to sit

**Sgwesen {sgwEzën} –** to turn/switch off, to extinguish

**Skand {skand} –** to jump

**Skap {skap} –** to escape, to depart from

**Skeul {sköl} –** to be obliged to, ought to

**Skehpt {ske:pt} –** to wait for

**Sleu {slö} –** to release

**Sisen {sIzën} –** to let to + infinitive verb* (like in “he let me to
use the emblem of his house”)*

**Smauter {smAotër} –** to watch

**Snumeb {snÜmëb} –** ro marry (a man)

**Sok {sok} –** to investigate

**Soll {sol} –** must *(probability)*

**Spar {spar} –** to spare, to save *(like in “save money”)*

**Speh {spe:} –** to hope

**Spehc {spe:k} –** to look at

**Spend {spend} –** to spend

**Spraneg {sprAnëg} –** to spring, to explode, to burst *(transitive
verb)*

**(oi)Sprehg *****&lt;asked person&gt; &lt;question&gt;***** {(oy)sprE:g
&lt;&gt;&lt;&gt;} –** to ask *(someone about a question. Like in “Smith
sprehct Lee quer is weict in Brasilu”, que significa “Smith asks Lee
where he lives in Brazil”)*

**Sprehng {spre:ng} –** to spring, to explode, to burst *(intransitive
verb)*

**Stehm {ste:m} –** to support *(a person, an idea, something)*

**Stehnk {ste:nk} –** to stink

**Studye {stUdye} –** to study

**Substitue {substitÜ:} –** to substitute

**Suicide {switsId} –** to suicide

**Swehk {swe:k} –** to smell

**Tarjem {tArjëm} –** to translate

**Tehl –** support, bear *(both words refer to the idea of support
something like a roof)*; to tolerate

**Tehr {te:r} –** to cross

**Tehrb {te:rb} –** to have to *(in the sense of “I have to do the
dinner” or “I have to work”)*

**Tekhnass {teqnAs} –** to manage to, to handle *(the situation)*, to
get by

**Tengie {tëndjI:} –** to look like

**Teup {töp} –** to hide (oneself)

**Tohrb –** *past tense of “tehrb”*

**Tolk {tolk} –** to talk; to explain, to interpret; to perform

**Trehc {tre:k} –** to displace (oneself)

**Trehv {tre:v} –** to find, to meet *(transitive verb)*

**Ubklad {ubklAd} –** to upload *(a file)*

**Uc {uk} –** *past tense of “euc”*

**Uperleit do {üpërlEyt do} –** to pass to, to go to *(figurative
sense)*

**Use {Üz} –** to use

**Van {van} –** to fetch

**Vansch {vanc} –** to wish, to desire

**Vid {vid} –** to see

**Vol {vol} –** to want

**Wan {wan} –** to need *(something that is missing)*, to request
*(something that is missing)*

**Wehd {we:d} –** to marry (a woman)

**Wehkw {wE:kw} –** to talk to, to express (oneself)

**Wehl {we:l} –** to want/order someone to

**Wehn {we:n} –** to desire, to feel like doing

**Wehrg {we:rg} –** to work

**Wehs {we:s} –** to find (oneself), to be *(this “be” is in the sense
of location)*

**Weik {weyk} –** to inhabit, to dwell

**Yeis {yeys} –** to send

**Yis {yis} –** *past tense of “yeis”*

[]{#anchor-427}ADJECTIVES
-------------------------

**Akster {Akstër} –** lively, strong *(figurative sense: colors; sense,
link in “strong alcohol”)*

**Bell {bel} –** beautiful

**Bert {bert} –** bright

**Biaur {byAor} –** ugly

**Biedan {biedAn} –** unhappy

**Bridek {brIdëk} –** ugly *(like in “bad condition” because of a cloudy
day)*

**Buland {bulAnd} –** tall

**Chald {tcald} –** hot

**Cherkin {tcërkIn} –** ugly, naughty *(it pass an idea of sordidness)*

**Clever {klEvër} –** clever

**Clus/cluden {klus / klÜdën} –** closed

**Cort {kort} –** short

**Deusk {dösk} –** dark

**Difficil {difItsil} –** difficult

**Dorgv {dorgv} –** dear; expensive

**Facil {fAtsil} –** easy

**Ghem {gem} –** low, inferior

**Ghyan(t/en) {gyAn(t/ën)} –** open

**Gohd {go:d} –** good, successful *(by contrast about something of bad
quality)*

**Hog {hog} –** high, superior

**Ieftin {yeftIn} –** cheap

**Jadide {jadId} –** new, not worn out

**Jamile {jamIl} – **nice, pretty

**Kaurd {kAord} –** hard

**Kiest {kyest} –** clean *(pure)*

**Latif {lAtif} –** kind

**Lent {lent} –** slow

**Leur {lör} –** free

**Long {long} –** long

**Madh {mad} –** wet

**Maschkhoul {macqU:l} –** engaged *( borrowed , used)*

**Mliak {mlyak} –** meek, sweet

**Murdar {murdAr} –** dirty

**Muzlim {mUdzlim} –** low, feeble *(figurative sense: intensity)*

**Nert {nert} –** strong *(fisically)*

**Noroct {norOkt} –** happy

**Nov {nov} –** new

**Oku {Oku} –** fast

**Orm {orm} –** poor

**Peigher {pEygër} –** nasty, wicked

**Prev {prev} –** past

**Riche {ritc} –** rich

**Saluber {salÜbër} –** healthy, salubrious

**Sell {sel} –** good *(contra-evil)*, blest

**Siuk {syuk} –** dry

**Slab {slab} –** weak (fisically)

**Smulk {smulk} –** small

**Srig {srig} –** cold

**Staur {stAor} –** powerful

**Stupid {stUpid} –** stupid

**Swad {swad} –** sweet

**Trauric {trAorik} –** sad, mourning

**Trist {trist} –** sad (thing)

**Dussaun {dusAon} –** unhealthy

**Veut {vöt} –** old

**Wahid {wahId} –** only

**Walik {wAlik} –** strong, powerful

**Yui {yuI} –** sad

**Yun {yun} –** young

[]{#anchor-428}BODY
-------------------

**Anus {Anus} –** anus

**Aur {Aor} –** ear

**Badan {badAn} –** organism *(obviously when refering to the body)*

**Bagu {bAgu} –** forearm

**Bemern {bEmërn} –** thigh

**Berd {berd} –** beard

**Brakh {braq} –** arm

**Brov {brov} –** eyebrown

**Cap {kap} –** head

**Chol {tcol} –** forehead

**Cloin {kloyn} –** buttock

**Coll {kol} –** pescoço

**Corpos {kOrpos} –** neck

**Dent {dent} –** tooth

**Dingv {dingv} –** tongue

**Fingher {fIngër} –** finger

**Genu {djEnu} –** knee

**Gharn(a) {gArn(a)} –** gut

**Gian {djyan} –** cheeck

**Glesen {glEzën} –** ankle

**Gumos {gUmos} –** body secretion

**Gurgule {gUrgül} –** throat

**Gventer {gvEntër} –** belly

**Hand {hand} –** hand

**Hank {hank} –** hip

**Jamb {jamb} –** leg

**Kays {kays} –** hair (on the head)

**Kers {kers} –** brain

**Kert {kert} –** heart

**Krew(os) {krEw(os)} –** flesh

**Lid {lid} –** eyelid

**Lige {lidj} –** face

**Lip {lip} –** lip

**Lyekwrnt {lyEkurnt} –** liver

**Mant(u) {mAnt(u)} –** chin

**Mems {mems} –** limb

**Moustache – **mustache

**Muscle {muskl} –** muscle

**Nabh {nab} –** navel

**Nas {nas} –** nose

**Naster {nAstër} –** muzzle

**Nayv {nayv} –** dead body

**Nugver {nUgvër} –** kidney

**Ok {ok} –** eye

**Oklid {Oklid} –** eyelid

**Okwi {Okwi} –** eyes

**Olan {olAn} –** elbow

**Oms {oms} –** shoulder

**Onkh {onk} –** nail *(on a finger)*

**Ors {ors} –** ass

**Ost {ost} –** bone

**Papil {pApil} –** nipple

**Ped {ped} –** foot

**Penis {pEnis} –** penis

**Pizdan {pizdAn} –** woman's chest

**Pod {pod} –** paw

**Poimen {pOymën} –** mother's milk

**Pulmon {pulmOn} –** lung

**Regv {regv} –** back *(spine)*

**Runc {runk} –** wrist

**Sehrg {se:rg} –** blood

**Skeletum {skëlEtum} –** skeleton

**Smokru {smOkru} –** goatee

**Snap {snap} –** beak

**Stohm {sto:m} –** mouth

**Stomak {stomAk} –** stomach

**Veine {veyn} –** vein

**Wagin {wadjIn} –** vagina

**Xiongbu {ksyOngbu} –** chest

**Yowkjitia {yowkjItya} –** body (when refering to a living being)

[]{#anchor-429}COLORS
---------------------

**Albh {alb} –** white

**Argavan {argavAn} –** lilac

**Blou {blu:} –** blue

**Brun {brun} –** brown

**Gehlb {dje:lb} –** yellow

**Glend {glend} –** green

**Greis {greys} –** gray

**Kenek {kEnëk} –** golden yellow

**Kwit {kwit} –** white. As* “albh” as “kwit” means the same thing,
“white”, but as “kwit” comes from “kweit” (purify), “kwit” might pass a
feeling of something more brighty, but both words have the same
meaning.*

**Orange {orAndj} –** orange

**Pemb {pemb} –** pink

**Rudh {rud} –** red

**Sword {sword} –** black

**Violett {violEt} –** purple

[]{#anchor-430}COMPUTING
------------------------

As certain terms, especially acronyms, from the English language have
wide acceptation in the world, maybe it's more convenient to use the
original terms, like VGA, which in Sambahsa is VGT. Well, do what you
think that is the better.

Do you remember what I told about the verbs in the infinitive form? If
not, please go back to the subchapter VERBS in this chapter VOCABULARY.

If you want to translate a software to Sambahsa and don't know whether
you should translate the commands to the infinitive form or imperative
form, see here the explanation of how it must be made:

-   Commands given to the computer by the user are translated in the
    infinitive form. ***Example:** abonne in forum *(subscribe in the
    forum)*; rename archive *(rename file)*; yises blixbrev *(send
    email)
-   Commands given to the user by the computer are translated in the
    imperative form, with the "courtesy" formula of the 2nd person
    plural. ***Example:** contacte nos *(contact us)*; clickte her
    *(click here)*; yeiste nos un blixbrev *(send us an email)

[]{#anchor-431}**Abonn {abOn} –** to subscribe to/in

**Abonnent {abOnënt} –** subscriber

**Abonnment {abOnmënt} –** subscription

**Adresse bar {adrEs bar} –** adress bar

**AI (Artificial Inteligence) {artifitsiAl intëlidjEnts} –** AI
(*Artificial Inteligence*)

**Alat bar {alAt bar} –** toolbar

**Alatengjia {alAtëngjya} –** toolbar

**Algorithme {algorI§m(ë)} –** algorithm

**Alveycomputing {alveykompUting} –** cloud computing

**Analogic {analOdjik} –** analogic

**Antivirus {antivIrus} –** antivirus

**Anyow {anyOw} –** password

**Application {aplikatyOn} –** application

**Archive {artcIv} –** file

**Arrange {arAndj} – **to format (a text)

**Attache {atAtc} –** to attach (a file)

**Aunviel {aonvyEl} –** wireless

**Aunkabel {aonkAbël} –** wireless

**Aurphone {aorfOn} –** headset, headphone

**BD *****(Blou-rai)***** {blu:-rä} –** BD *(Blue-ray)*

**Bichoun {bitcU:n} –** layout

**Binar {binAr} –** binary

**Blixbrev {blIksbrëv} –** e-mail

**Blog {blog} –** Blog

**Bluetooth {blu:tU§} –** bluetooth *(since it's a Registered Trademark,
the Sambahsa word keeps the English pronunciation)*

**Browser {brOwsër} –** web browser

**Bukmark {bukmArk} –** bookmark (of the web browser or page of
document)

**Cadre per secunde {kAdr per sekUnd} –** frame per second

**CD *****(compacto disk)***** {kompAkto disk} –** CD *(compact disk)*

**Cell(ule) {tsEl(ül)} –** cell of a spreadsheet

**(Central) processor {(tsentrAl) protsesOr} –** CPU *(Central
Processing Unit)*

**Channel {tcAnël} –** channel (of radio, television, etc)

**Chip {tcip} –** electronic chip

**Claviatolk {klavyatOlk} –** CHAT

**Claviature {klaviatÜr} –** keyboard

**Cleichwerd {klEytcwërd} –** key-word

**Click {klik} –** click

**Codec {kOdëk} –** codec

**Colunn {kolUn} –** column

**Compiler {kompIlër} –** compiler

**Computer {kompÜtër} –** computer

**Computerdorak {kompütërdorAk} –** computer case, computer chassis,
tower, system unit, cabinet, base unit

**Configuration {konfiguratyOn} –** configuration

**Connect {konEkt} –** to connect *(transitive verb)*

**Cont(o) {kOnt(o)} –** account

**Converter {konvErtër} –** converter

**Convert {konvErt} –** to convert

**Copie {kopI:} –** copy

**Cursive texte {kursIv text} –** italic text

**Daftarnukta {daftarnUkta} –** bullet *(graphic symbol like •, ◦, ‣)*

**Dakhelpage {dakhëlpAdj} –** home page

**Data {dAta} –** dada

**Databank {databAnk} –** databank

**Database {databAz} –** database

**Datenbank {datënbAnk} –** data bank

**Datensklad {datënsklAd} –** DRIVE *(Data storage device)*

**Datia {dAtya} –** file

**Datum {dAtum} –** datum

**DD - darm disk {darm disk} –** HD - disco rígido *(Hard Disk Drive)*

**Defect {dëfEkt} – **BUG

**Desinstalle {dësinstAl} –** to uninstall

**Desfragmente {dësfragmEnt} –** to defragment (a partition)

**Desktop {dësktOp} –** desktop

**Diaporama {dyaporAma} –** diaporama, slide show, slide presentation

**Digital {didjitAl} –** digital

**Disayn {dizAyn} –** design

**Disayner {dizAynër} –** designer

**Diskette {diskEt} –** diskette, floppy disk, floppy disc

**Diskwehr {diskwE:r} –** to undo

**DM *****(darmo memoria)***** {dArmo memOrya} –** ROM *(Read-Only
Memory)*

**Document {dokÜmënt} –** document

**Documentation {dokümëntatyOn} –** documentation

**Dossier {dosyEr} –** folder

**Drucken {drUkën} –** print

**Ducer {dÜtsër} –** driver

**DVD (*****Digital Video Disk*****) {didjitAl vidEo disk} –** DVD
*(Digital Video Disc)*

**E-buk {ebUk} –** e-book

**Ecran {ekrAn} –** screen

**(Ecran)decor {(ekran)dëkOr} –** wallpaper

**Ecransparer {ekranspArër} –** screensaver

**Edeih {edE:y} –** to edit

**Editor {editOr} –** editor

**Emulator {emulatOr} –** emulator

**Enmercatnotes {enmErkatnots} –** release notes

**error {erOr} –** error

**Extension {ekstënsyOn} –** complement, extention, add-on

**Exter ligne {ekstër lInye} –** offline

**Extrag {ekstrAg} –** to cut

**Fenster {fEnstër} –** window

**Format {formAt} –** to format (a hard disc or text)

**Formulire {formulIr} –** form

**Forum {fOrum} –** forum

**Fraumbar {fraombAr} –** scroll bar

**Frequent-ye Anacta Questions (F.A.Q.) {frEkwënt-ye anAkta këstyOns}
–** Frequently Asked Questions (F.A.Q.)

**Funed {fÜnëd} –** to merge (table cells)

**Gadabeih {gadabE:y} –** to format (a text)

**Gadget {gAdjët}–** gadget

**Galerie {galërI:} – **galery

**Ghyanen code {gyAnën kod} –** open source

**Gleim {gleym} –** to paste

**GNI *****(graphic (neuder) interface)***** {grAfik (nÖdër) intërfAts}
–** GUI *(Graphical User Interface)*

**Graphic {grAfik} –** graphic *(adjective)*

**Graphic ambient {grAfik ambyEnt} –** graphical environment

**Graphic pianji {grAfik pyAnji} –** drawing tablet

**Graphique {grafIk} –** graphic *(substantive)*

**Hacker {hAkër} –** hacker

**Hardware {hardwAr} –** hardware

**Hissabpianji {hisabpyAnji} –** spreadsheet

**HDMI *****(Hog-definition multimedia interface)***** {hog dëfinityOn
multimEdya intërfAts} –** HDMI *(High-Definition Multimedia Interface)*

**Hol ecran {hol ekrAn} –** full screen

**Hyperlink {hüpërlInk} –** hiperlink

**IA (integren antplehcensambient) {intEgrën antple:tsënsambyEnt} –**
IDE *(Integrated Development Environment)*

**Icon {ikOn} –** icon

**Indentation {indëntatyOn} –** indentation

**Informatique {informatIk} –** computing

**Infrarudh {infrarUd} –** infrared

**In ligne {in lInye} –** online

**Installe {instAl} –** to install

**Internet {intërnEt} –** internet

**Interpreter {intërprEtër} –** interpreter *(like a Java or Python
interpreter)*

**(Computer) joystick {(compÜtër) joystIk} –** joystick

**Kabel {kAbël} –** cable

**Khatem {qAtëm} –** Game Over

**Klad {klad} –** to load

**Klink {klink} –** tab (of a window, like the tabs of Mozilla Firefox)

**Knop {knop} –** button

**Laptop {laptOp} –** laptop

**Leikstyr {lEykstür} –** game controller

**Leiser {lEyzër} –** reader

**Leur software {lör softwAr} –** free software

**Ligne {lInye} –** line

**Log in {log in} –** login, logon

**Log ex {log eks} –** logout, logoff

**Materplack {matërplAk} –** motherboard

**Mediatolker {medyatOlkër} –** media player

**Memoria carte {memOrya kart} –** memory card

**Metadata {metadAta} –** metadada

**Metadatum {metadAtum} –** metadatum

**Microphone {mikrofOn} –** microphone

**Miniature {miniatÜr} –** thumbnail

**Mobilphone {mobilfOn} –** cell phone

**Modem {mOdëm} –** modem

**Monitor {monitOr} –** monitor

**Mus {mus} –** mouse

**Musictolker {muziktOlkër} –** music player

**Muspad {muspAd} –** mousepad

**Mov {mov} –** to move

**Netwehrg {netwE:rg} –** network

**Niklad {niklAd} –** to download

**Ogwnschirm {Oguncirm} –** firewall

**Oisschalt {oyscAlt} –** turn off

**Operationsysteme {opëratyonsüstEm} –** operating system

**Operative systeme {opëratIv süstEm} –** operating system

**Pack {pak} –** pack *(set of softwares)*

**Panell {panEl} –** panel

**Partition {partityOn} –** partition of a hard disk)

**Personalise {përsonalIz} –** to customize, to personalize

**Pfehrster {pfE:rstër} –** cursor, ponteiro do mouse

**(Computer) pianji {(kompÜtër) pyAnji} –** tablet

**Piwer texte {pIwër text} –** bold text

**Plug-in {plug-In} –** plugin

**Podcast {podkAst} –** podcast

**Post {post} –** post *(substantive)*

**Primark lurhat {primArk lurhAt} –** markup language

Printer {prIntër} – printer

Processor {protsesOr} – processor

Profile {profIl} – profile

Programmation lurhat {programatyOn lurhAt} – programing language

Programme {progrAm} – to program, to code

**Pung {pung} –** button

**Puwen {pÜwën} –** to debug

**RAM *****(random access memoria)***** {randOm ak(t)sEs mëmOrya} –**
RAM *(Random Access Memory)*

**Reclame {rëklAm} –** advertisement

**Recorde {rëkOrd} –** to save (a document or process of a software)

**Recorder {rëkOrdër} –** recorder

**Renam {rënAm} –** to rename

**Retrovert {rëtrovErt} –** to downgrade *(to bring back an old version
or state)*

**Rekwehr {rëkwE:r} –** to redo

**Rewos bar {rEwos bar} –** space bar

**Rinkap {rinkAp} –** to reset

**Roig {royg} – **line *(spreadsheet)*

**Router {rU:tër} –** router

**Salvguarde {salvgwArd} –** backup

**Scanner {skAnër} –** scanner

**Schalter {cAltër} –** on-off switch

**Schrift {crift} –** font *(typeface)*

**Server {sErvër} –** server

**Smartphone {smartfOn} –** smartphone

**Smauter {smAotër} –** to view

**Social netwehrg {sotsiAl nëtwE:rg} –** social network

**Software {softwAr} –** software

**Sokmotor {sokmotOr} –** search engine

**SPAM –** SPAM

**Sroviswor {sroviswOr} –** power supply

**Stahgjia {stA:gjya} –** sidebar

**Strehmen {strE:mën} –** streaming

**Substrichen texte {substrItcën tekst} –** underlined text

**Suppressem {suprEsëm} –** to delete

**Surce code {surts kod} –** source code

**Swoglehmber {swoglE:mbër} –** booting

**Swoncarte {swonkArt} –** sound card

**Swonkwatel {swonkwAtël} –** speaker

**Synchronise {sünkronIz} –** to sync

**Tab(ulation) {tab(ulatyOn)} –** tabulation

**Texte futliar {text futlyAr} –** text box

**Textenbeorbater {tekstënbeorbAtër} –** text processor

**Topic {tOpik} –** topic, thread (of a forum)

**Touche {tu:tc} –** key (of a keyboard)

**Touchecran {tu:tcëkrAn} –** touchscreen

**Trackliste {traklIst} –** playlist

**Transistor {transistOr} –** transistor

**TV carte {TV kart} –** capture card

**Ubklad {ubklAd} –** to upload

**Ubnuw {ubnUw} –** update

**Ubnuwen {ubnÜwën} –** to update

**Ubstiumep {ubstyÜmëp} –** to upgrade

**Uperswehp {üpërswE:p} –** to hibernate

**Utilitat {utilitAt} –** utility

**USB (*****Universal Serial Bus*****) {univërsAl sëriAl bus} –** USB
*(Universal Serial Bus)*

**USB cleich {USB kleytc}–** pendrive

**Vector image {vëktOr imAdj} –** vector image

**Vectorial image {vëktoryAl imAdj} –** vector image

**VGT *****(Video Graphique Tehxen)***** {vidEo grafIk tE:ksën} –** VGA
*(Video Graphics Array)*

**Video {vidEo} –** video

**Videogame {videogAm} –** video game console

**Videocarte {videokArt} –** video card

**Videoleik {videolEyk} –** video game console

**Videotolker {videotOlkër} –** video player

**Wi-fi {wi-fi} –** wi-fi

**Virtual {virtuAl} –** virtual

**Virus {vIrus} –** virus

**Viel {vyel} –** thread, wire

**Voltage-tuler {voltAdj tÜlër} –** power strip, surge protector

**Volume {volÜm} –** volum

**Web {web} –** web

**Webcam {wëbkAm} –** webcam

**Website {wëbsAyt} –** website

**Webtyohc {wëbtyO:k} –** feed *(substantive)*

**Wehrgeih {we:rdjE:y} –** to play, to run *(transitive verb)*

**Windows portfeuyl {windOws portfÖyl} –** Briefcase *(from Microsoft
Windows)*

**Zoum apo {zu:m Apo} –** Zoom out

**Zoum prosch {zu:m proc} –** Zoom in

[]{#anchor-432}ANIMALS
----------------------

**Arank {arAnk} –** spider

**Aux(an) {Aoks / aoksAn} –** ox

**Av {av} –** bird

**Ayg {ayg} –** female goat

**Aygur {aygUr} –** stallion

**Baleina {balEyna} –** whale

**Baul {bAol} –** bat

**Bei {bey} –** bee

**Bock {bok} –** male goat

**Brank {brank} –** beetle

**Cat {kat} –** cat

**Cavall {kavAl} –** horse

**Cuincule {kwInkül} –** rabbit

**Delphin {dëlfIn} –** dolphin

**Gall {gal} –** rooster, cock

**Gaydh {gayd} –** goat *(it can be used as adjective)*

**Ghelon {gëlOn} –** turtle

**Ghimer {gImër} –** young animal *(one winter-old)*

**Gwow {gwow} –** cow, bovine animal

**Katelsqual {katëlskwAl} –** killer whale

**Kiep {kyep} –** ape

**Kierk {kyErk} –** chicken

**Kunia {kUnya} –** she-dog, bitch

**Kwaun {kwAon} –** dog

**Leopard {leopArd} –** leopard

**Lion {lyon} –** lion

**Maymoun {maymU:n} – **monkey

**Murm {murm} –** ant

**Mus {mus} –** mouse

**Musch {muc} –** fly

**Ornd {ornd} –** eagle

**Ow {ow} –** sheep

**Paymen {pAymën} –** *(animal)* breeder

**Pelpel {pElpël} –** butterfly

**Pinguin {pingwIn} –** penguin

**Pisk {pisk} –** fish

**Pork {pork} –** pig

**Rat {rat} –** rat

**Schebeck {cëbEk} –** female monkey

**Scorpion {skorpyOn} –** scorpion

**Serpent {sErpënt} –** serpent, snake

**Shamyu {xAmyu} –** shark

**Sill {sil} –** seal

**Taur {tAor} –** bull

**Tigher {tIgër} –** tiger

**Urx {urks} –** bear

**Vulp(ek) {vUlp(ëk)} –** fox

**Wersi {wErsi} –** young animal *(one spring old)*

**Wolf {wolf} –** wolf

[]{#anchor-433}MEASURES
-----------------------

**Absorpt energia dose {absOrpt enErdjya doz} –** absorbed dosis *(total
ionizing dose)*

**Ampere {ampEr} (A) –** ampere

**Becquerel {bëkErël} (Bq**) – becquerel

**Candela {kandEla} (cd) –** candela

**Catalytic activitat {katalÜtik aktivitAt} –** catalytic activity

**Celsius grade {tsElsyus grAd} (°C) –** Celsius grade

**Dwinegh {dwInëg} –** force *(in context of pressure)*

**Electric capacitat {elEktrik kapatsitAt} –** electric capacity

**Electric charge {elEktrik tcardj} –** electric charge

**Electric conductance {elEktrik konduktAnts} –** electric conductance

**Electric inductance {elEktrik induktAnts} –** electric inductance

**Electric resistence {elEktrik rezistEnts} –** electric resistence

**Electromot force {elëktromot forts} –** electromotive force

**Energia {enErdjya} –** energy

**Equivalent dose {ekivAlënt doz} –** equivalent dose

**Farad {farAd} (F) –** farad

**Force {forts} –** force

**Frequence {frëkEnts} –** frequence

**Gray {gray} (Gy) –** gray

**Gwayder flux {gwAydër fluks} –** luminous flux

**Gwayder intensitat {gwAydër intënsitAt} –** luminous intensity

**Henry {hEnri} (H) –** henry

**Hertz {hertz} (Hz) –** hertz

**Joule {ju:l} (J) –** joule

**Katal {katAl} (kat) –** katal

**Kelvin {kelvIn} (k) –** kelvin

**Kilogramme {kilogrAm} (kg) –** kilogram

**Kweitergwis {kwEytërgwis} –** illuminance

**Liter {lItër} –** liter

**Longe {londj} –** length

**Lumen {lÜmën} (lm) –** lumen

**Lux {luks} (lx) –** lux

**Magnetic induction {manyEtik induktyOn} –** magnetic induction

**Magnetic induction flux {manyEtik induktyOn fluks} –** Magnetic
induction flux

**Masse {mas} –** mass

**Meter {mEtër} (m)–** meter

**Mole {mol} (mol) –** mol

**Newton {nëwtOn} (N) –** newton

**Ohm {o:m} (Ω) –** ohm

**Pascal {paskAl} (Pa) –** pascal

**Plane angule {plan Angül} –** plane angle

**Pressem {prEsëm} –** pressure

**Radian {radyAn} (rad) –** radian

**Radioactivitat {radyoaktivitAt} –** radioactivity

**Secunde {sekUnd} (s) –** second

**Siemens {syEmëns} (S) –** siemens

**Sievert {syEvërt} (Sv) –** sievert

**Solid angule {sOlid Angül} –** solid angle

**Srov {srov} –** electric power

**Steradian {stëradyAn} (sr) – **steradian

**Stieure {styÖr} –** power

**Temperature {tëmpëratÜr} –** temperature

**Tesla {tEsla} (T) –** tesla

**Volt {volt} (V) –** volt

**Wakel {wAkël} –** batch, quantity

**Wakt {wakt} –** time *(duration)*

**Weber {wEbër} (Wb) –** weber

**Wehrg {we:rg} –** work

**Watt {wat} (W) –** watt

[]{#anchor-434}FAMILY
---------------------

**Aja {Aja} –** granny, grandma

**Amma {Ama} –** adoptive mother

**Atta {Ata} –** stepfather

**Bent {bent} –** girl

**Brater {brAtër} –** brother

**Cousin {ku:zIn} –** cousin

**Cummater {kummAtër} –** godmother

**Cumpater {kumpAtër} –** godfather

**Daiwer {dÄwër} –** brother-in-law

**Dugter {dUgtër} –** daughter

**Eln {eln} –** child, puppy

**Esor {ezOr} –** wife

**Ewo {Ewo} –** grandpa, grandad

**Familia {famIlya} –** family

**Fiancee {fyantsEë} –** fiancee

**Fianceo {fyantsEo} –** fiancé

**Gelou {djëlU:} –** sister-in-law

**Gemer {djEmër} –** sister’s husband

**Genealogic dru {djënealOdjik dru} –** family tree

**Grandmater {grandmAtër} –** grandmother

**Grandpater {grandpAtër} –** grandfather

**Gvibh {gvib} –** wife

**Ienter {yEntër} –** husband’s brother’s wife

**Kerab {kërAb} –** relative

**Kerdprient {kërdpryEnt} –** boyfriend

**Kerdprientin {kërdpryentIn} –** girlfriend

**Kweil {kweyl} –** girl

**Mann {man} –** husband

**Mater {mAtër} –** mother

**Matruw {matrUw} –** maternal uncle

**Nanander {nanAndër} –** husband’s sister

**Nepot {nëpOt} –** nephew

**Neptia {nEptya} –** niece

**Nitia {nItya} –** *circle of family and close friends*

**Novsta {nOvsta} –** bride

**Novsto {nOvsto} –** groom

**Oncle {onkl} –** uncle

**Parent {pArënt} –** parent

**Pater {pAtër} –** father

**Prient {pryent} –** friend

**Samgwelbh –** *born of the same mother*

**Siour {syu:r} –** wife’s brother

**Son {son} –** son

**Swelion {swëlyOn} –** wife’s sister’s husband

**Swesgen {swEsdjën} –** sibling

**Swes(ter) {swEs(tër)} –** sister

**Swoker {swOkër} –** father-in-law

**Swokru {swOkru} –** mother-in-law

**Tante {tant} –** aunt

**Vidva {vIdva} –** widow

**Vidvo(s) {vIdvo(s)} –** widower

**Wehdmen {wE:dmën} –** *purchase price of the bride*

[]{#anchor-435}VARIOUS
----------------------

**Alphabet {alfAbd} – **alphabet

**Anua {Anwa} –** old woman

**Aunfin {aonfIn} –** endless, infinite

**Buk {buk} –** book

**Dienkia {dyEnkya} –** domestic electric appliances

**Distance-control {distAnts kontrOl} –** remote-control

**Dru {dru} –** tree

**Fall {fal} –** case *(situation)*

**Forest {forEst} –** forest

**Future {futÜr} –** future *(substantive)*

**Geront {djërOnt} –** old man

**Grammatic {gramAtik} –** grammar

**Gwen {gwen} –** woman

**Gwenak {gwënAk} –** young woman

**Ject {jekt} –** thing

**Ker {ker} –** time, occasion

**Kindergarten {kindërgArtën} –** kindergarten

**Kwehrmen {kwE:mën} –** way *(of doing something)*

**Majoritat {majoritAt} –** majority

**Memoria {mëmOrya}** – memory

**Mensc {mensk} – **human, man *(“man” in the sense of specie)*

**Menue {mënÜ:} – **menu

**Message {mesAdj} –** menssage

**Minoritat {minoritAt} –** minority

**Pieg {pyeg} –** girl

**Present {prEzënt} –** present

**Prev {prev} –** past

**Primar talim {primAr tAlim} –** *stage when the children learns the
basic, like reading and very basic calculation*

**Pwarn {pwarn} –** boy

**Secundar talim {sëkundAr tAlim} –** *stage when teenagers learns the
fundamental*

**Suedos {sÜ:dos} –** property *(quality)*

**Television {tëlëvizyOn} – **television

**Veneg {vEnëg} –** seller

**Videocassette {videokasEt} –** videocassette

**Videoteip {videotEyp} –** videocassette

**Wakt {wakt} –** time *(duration)*

**Wir {wir} –** man

**Yeudmo {yÖdmo} –** man *(as combatent)*

**Yuwen {yÜwën} –** young man

**Zaman {dzamAn} –** epoch, time

[]{#anchor-436}INTERJECTIONS
----------------------------

**Ajaban! {ajabAn} –** gosh!

**Al-hamdulillah! {al hamdulilA:} –** thank you God!

**Aman! {amAn} –** have mercy!

**Anchoa! {antcOa} –** not possible!

**Asafa! {azAfa} –** how awful!

**Bast(a)! {bAst(a)} –** it's enough!

**Bravo! {brAvo} –** bravo!

**Bre! {bre} –** whoa!

**Chao! {tcAo} –** goodbye!

**Chiba! {tcIba} –** *word used to drive a dog*

**Chinchay! –** nothing!

**Coul! {ku:l} –** cool!

**Dank! {dank} –** Thanks!

**E? {ë} –** hein?

**Ehh {e:} –** err *(doubt)*

**Gwivtu! / Gwive! {gw} –** hurray!

**Ha ha ha! {ha ha ha} –** Ha ha ha!

**Hay(te)! {hay(t)} –** go! *(encouragement)*

**Hue! {hü:} –** Gee up! *for a horse*

**Hurrah! {hurA:} –** hooray!

**Iblis! {Iblis} –** deuce! devil!

**Interjection {intërjëktyOn} –** interjection

**Lassamahallah {lasamahalA:} –** God help us!

**Lutfan {lutfAn} –** be nice!

**Maideh {maydE:} –** help! mayday!

**Marba {mArba} –** please to meet you!

**Mersie! {mërsI:} –** thank you!

**O Deiwes! {o dEywës} **– great Gods!

**Pawiropeku! {pawiropEku} –** goodbye! farewell!

**Sayang! {sayAng} –** what a pity!

**Servus! {sErvus} –** at your service!

**Scha! {ca} –** hush!

**Sieune {syön} –** Bless You ! *(to someone who has just sneezed)*

**Sikhtir {sIqtir} –** out!

**Stop! {stop} –** stop!

**Toi! {toy} –** *deception*

**Uff! {uf} –** uff!

**Ups! {ups} –** ops!

**Vedim! {vEdim} –** let's see! *(to bring a counter-argument)*

**Yallah! {yalA:} –** my God!

**Way! {way} –** ai! ui!

**Zinhaar! {dzinhaAr} –** beware! caution!

[]{#anchor-437}REFERENCES
=========================

SIMON, Olivier. RICE, Stephen L (ed.). **The Grammar Of
Sambahsa-Mundialect In English**. 7 ed. Maio 2012.

A Starter to Sambahsa

RICE, Stephen L. **Sambahsa\_Phrasebook**

WINTER, Robert. **Sambahsa: Guide to Pronouns and Articles**.

Many and many talks with Olivier Simon.

LIMA, Henrique Matheus da Silva. **Manual de Alfabeto Fonético
Internacional**. 2017.

OFFICIAL DAKHELPAGE OS SAMBAHSA-MUNDIALECT. **FrontPage**. Available in:
&lt;http://Sambahsa.pbworks.com/w/page/10183084/FrontPage&gt;. Acesso
em: jul. 2016.

OFFICIAL DAKHELPAGE OS SAMBAHSA-MUNDIALECT. **IPA Sambahsa phonetics**.
Available in:
&lt;http://Sambahsa.pbworks.com/w/page/10183089/IPA%20Sambahsa%20phonetics&gt;.
Access in: jul. 2016.

DIARIO DOS CAMPOS. **Idiotismos da Língua Portuguesa**. 01 set. 2012.
Disponível em:
&lt;http://www.diariodoscampos.com.br/variedades/2009/05/idiotismos-da-lingua-portuguesa/1123815/&gt;.
Access in: jul. 2016.

RACHEL. Youtube channel of Rachel's English.
&lt;https://www.youtube.com/user/rachelsenglish&gt;. Access in: jan.
2017.

[^1]: **It’s not “kohgnt” because {ko:nyt} is not pronounceable, that’s
    why we write “kohgnt” {kO:nyët} here**.
