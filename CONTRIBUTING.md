# WHAT DO WE NEED

* Revising the translations of the documents

* Checking whether there is some mistake in writing or content

* Giving suggestions for making the materials more didactic and complete

* Production of new material! :)

# HOW THE DOCUMENTS ARE EDITED?

The contributions will be made in the md files in order to keep track of the modifications, then the changes will be made in the actual documents, for exemple, the grammars are in odt format, so contributor will make changes in the md versions of the grammars and then these changes will be applied in the odt files.

The md files are not "official" because the formatting capacities of MarkDown are very limited and text formatting matters a lot.

We do not accept documents in proprietary formats or fake-open formats *(I looking at you, [OOXML](http://officeopenxml.com/)!)*, so we suggest you to use free software for making your documents, like LibreOffice *(if you don't like LibreOffice, at least use the OpenDocument format)*.
