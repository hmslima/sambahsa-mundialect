Sambahsa

Mundialect

Gramática Completa

![](Pictures/10000201000004960000047067F9E3243AFA1A59.png){width="6.835cm"
height="6.613cm"}

Henrique Matheus da Silva Lima

Revisão: Dr. Olivier Simon

  ----------------------- ---------------------------------------------------------------------------------------------
  **Versão:** 0.32 Beta   ![](Pictures/10000000000001F4000000B07F43F2EF6B26141B.png){width="3.069cm" height="1.08cm"}
  ----------------------- ---------------------------------------------------------------------------------------------

NOTAS IMPORTANTES SOBRE QUESTÕES LEGAIS

Esta gramática é licenciada sob a licença Creative Commons CC-BY 4.0.

![](Pictures/10000000000001F4000000B07F43F2EF6B26141B.png){width="3.069cm"
height="1.08cm"}

Você tem o direito de:

**Compartilhar –** copiar e redistribuir o material em qualquer suporte
ou formato.

**Adaptar –** remixar, transformar, e criar a partir do material para
qualquer fim, mesmo que comercial.

Você deve dar o crédito apropriado, prover um link para a licença e
indicar se mudanças foram feitas. Você deve fazê-lo em qualquer
circunstância razoável, mas de maneira alguma que sugira ao licenciante
a apoiar você ou o seu uso.

Você não pode aplicar termos jurídicos ou medidas de caráter tecnológico
que restrinjam legalmente outros de fazerem algo que a licença permita.

Eis o link para mais informações:
<https://creativecommons.org/licenses/by/4.0/deed.pt_BR>

A língua sambahsa é de autoria do Dr. Olivier Simon, a língua é de uso
livre para qualquer pessoa. Para a elaboração desta gramática foi
utilizado, como referência, o livro []{#anchor}*The Grammar Of
Sambahsa-Mundialect In English* do Dr. Olivier Simon e alguns poucos
subcapítulos desta gramática são praticamente uma tradução desse livro,
que o autor me permitiu fazer, mesmo levando em conta a licença deste
livro. Também utilizei muitos exemplos do livro *The Grammar Of
Sambahsa-Mundialect In English *e outros que o próprio Dr. Olivier Simon
fez para mim.

É muito importante informar que **a língua em si não se encontra sob
nenhuma licença Creative Commons ou similar**, a língua está sob a
tradicional licença Copyright em que o Dr. Olivier Simon detém todos os
direitos reservados. Mas, como já foi dito, **a língua é livre**, você
pode traduzir seus trabalhos, ou produzir trabalhos originais, sem a
necessidade de permissão do Dr. Olivier Simon, mesmo que seja para fins
comerciais; ou seja, você pode escrever livros em sambahsa e vendê-los
sem problemas. Usufrua dessa linda língua.

**Esta é uma versão beta da gramática, mas ela já pode ser usada.**

AGRADECIMENTOS

Agradeço ao Dr. Olivier Simon por ter se disponibilizado em tirar todas
as minhas dúvidas e ter me dado vários exemplos, muitos dos exemplos
desta gramática são dele. Devo agradecê-lo duas vezes, por ele ter tido
toda a paciência do mundo comigo, ele é quase o coautor deste livro.

Agradeço também ao Sylvain Auclair, Martín Rincón Botero e Justin
Gagnon, que me ajudaram bastante.

INTRODUÇÃO
==========

**Sambahsa Mundialect** é uma língua auxiliar criada pelo francês Dr.
Olivier Simon e lançada na internet em julho de 2007, sua base é o
protoindo-europeu, língua falada há cerca de 5000 anos, cujas línguas
filhas se estendem do sul da Rússia até as ilhas britânicas e a Índia. O
sambahsa ainda conta com muitas contribuições de outras línguas, em
especial o árabe, mas também chinês, indonésio e muitas outras.

Por ser uma língua auxiliar, sambahsa é muito mais fácil do que qualquer
outra língua nacional como inglês ou espanhol, mas é um pouco mais
desafiador se comparada a outras línguas auxiliares porque ela se
apresenta um pouco complicada no início, mas depois o processo de
aprendizado é incrivelmente simples e as vantagens de sua maior
complexidade farão todo o esforço valer a pena.

Com o que o sambahsa se parece? O interessante do sambahsa é que ele é
tão natural que você não o vê com uma língua construída, mas sim como
uma língua nacional como qualquer outra. Como Dave MacLeod disse no seu
prefácio para o livro *The Grammar Of Sambahsa-Mundialect In English*, o
sambahsa pode ser imaginado como uma língua que existiu em algum lugar
em volta de onde hoje fica a Armênia, em um reino que tem uma língua
descendente do proto indo-europeu que recebeu influências através dos
séculos dos persas, turcos, vizinhos árabes e de vários países do leste;
algumas vezes a língua se parece com o búlgaro, outras vezes o persa e
também pode lembrar o alemão.

Sambahsa possui uma bandeira para representá-lo.

![](Pictures/10000201000007D00000053C3165E76E4F454897.png){width="8.334cm"
height="5.584cm"}

A cor marrom representa o solo, uma vez que muitas línguas
indo-europeias derivam suas palavras para “pessoa” de palavras
relacionadas ao barro, como por exemplo a palavra “terráqueo”. A cor
branca foi escolhida porque ela contrasta o melhor. Os círculos duplos
representam a roda, pois esta é uma inovação importante feita pelos
humanos, o quadrado representa o veículo (carro, carroça…), os “T”s
representam o varal de veículo, as linhas representam os eixos e o
círculo maior representa o sol bem como o ciclo da vida.

O “movimento” que apoia a língua sambahsa não é ligado a nenhuma forma
de pensamento: pessoas de todos os tipos, etnias, lugares, crenças,
gêneros, orientações sexuais, classes sociais e ideologias podem usar a
língua ao seu bel prazer.

POR QUE ESTUDAR UMA LÍNGUA AUXILIAR?
------------------------------------

Você me pergunta: “mas ora, nós já não temos o inglês, pra que uma
língua artificial?”. Antes de responder a essa pergunta, preciso lembrar
que nós estamos falando aqui sobre uma língua para um contexto mundial,
se eu for trabalhar na China ou, no mínimo, manter um contato com
chineses, terei que aprender mandarim ou cantonês, isso não se discute.
O que eu estou falando aqui é: se eu entrar numa sala onde se encontram
um paquistanês, um russo, um americano, um argentino e um japonês, todos
só poderão se comunicar se souberem uma língua em comum, porque são
poucos os que tem tempo e disposição para aprender cinco línguas
diferentes; nos dias de hoje se espera que todas essas pessoas falem
inglês para que seja possível a comunicação. É aí que está o X da
questão.

O problema de uma língua nacional – não importa se é inglês, espanhol,
francês ou mandarim – é que se gasta muito tempo *(e, em alguns casos,
também dinheiro)* aprendendo-a. É claro que para quem gosta de aprender
idiomas ou vai morar em um país estrangeiro, é um **ótimo**
investimento, até mesmo estudar uma língua “pouco importante” é um bom
investimento se isso te der prazer e ainda pode se tornar um diferencial
no futuro. Mas quando se trata de uma língua que sirva de ponte entre
diferentes povos, o cenário é diferente.

[]{#anchor-1}Gosto de dizer que quando duas pessoas se comunicarem
através da []{#anchor-2}língua de um determinado país é como fazer
cálculos usando algarismos romanos. É perfeitamente possível calcular
com numerais romanos, povos fizeram isso por séculos, mas é muito mais
eficiente calcular com algarismos indo arábicos, que no nosso caso seria
a comunicação com uma língua que se aprende em meses em vez uma que se
aprende em anos e é cheia de armadilhas. Eu disse “língua de um
determinado país” porque as pessoas aprendem – no âmbito da comunicação
internacional – a variedade inglesa dos Estados Unidos ou Inglaterra,
mas jamais o inglês da Jamaica.

Outro ponto negativo de usar a língua de um outro país como idioma
internacional é que, de uma certa forma, corrobora a superioridade desse
país. Nos tempos modernos se usava o francês porque a França era a nação
mais influente da Europa, a partir do século XX usamos o inglês porque
os países anglófonos adquiriram grande poder econômico e militar. Será
que daqui a algumas décadas teremos que nos comunicar em mandarim ou na
língua de quem quer que mande no mundo?

### ENTÃO VOCÊS SÃO CONTRA A LÍNGUA INGLESA?

A minha resposta para essa pergunta é um sonoro “**NÃO**”! O inglês é
tido como língua oficial em países de todos os continentes, sem contar
que os Estados Unidos e a Inglaterra são grandes exportadores de
cultura, é por conta desses e outros fatores que muitos defendem que o
inglês continuará sendo usado como língua franca por um bom tempo mesmo
se os Estados Unidos perderem sua hegemonia. E pessoalmente acho a
língua inglesa muito bonita.

Apesar de todas as críticas feitas, o inglês ainda é a língua mais
disseminada no globo; em praticamente todas as escolas do mundo com um
mínimo de infraestrutura, o inglês é ensinado; ao contrário do inglês,
nenhuma língua auxiliar teve o apoio de um Estado e empresas privadas
para espalhá-la no mundo. Sabemos que, atualmente, um trabalho será
muito melhor exposto se ele for disponibilizado em inglês.

É importante aprender inglês, assim como é importante aprender espanhol,
russo, esperanto, mandarim, alemão, etc, pois embora defendamos que usar
uma língua auxiliar como língua franca seria muito mais eficiente, na
realidade a verdadeira língua internacional é o poliglotismo, se você
quer ser algo próximo a um verdadeiro “cidadão do mundo”, seja um
poliglota!

Mas certamente que uma língua auxiliar nos ajudaria imensamente. Uma
língua neutra que pode ser aprendida em questão de meses diminuiria
consideravelmente as barreiras linguísticas. Por que não sermos mais
eficientes no âmbito da comunicação internacional?

MAS POR QUE ESTUDAR SAMBAHSA?
-----------------------------

Antes de conhecer o sambahsa eu já conhecia outras línguas auxiliares,
mas eu não estava plenamente satisfeito com elas, não porque elas fossem
ineficientes, mas havia nelas certas características, devido a sua
enorme simplicidade, que me desagradavam.

O sambahsa consegue ter a regularidade – e facilidade – de uma língua
auxiliar e a naturalidade e beleza nativa de uma língua nacional. O
sambahsa é tão bom quanto o português ou inglês para fazer poesia ou
música e os nomes próprios e nomes de coisas “exóticas” importados de
outras línguas não precisam, na maioria dos casos, ter sua grafia ou
pronúncia drasticamente modificados para atender as normas da língua, as
“dificuldades” para importar essas palavras serão os mesmos de qualquer
outra língua nacional, como o português ou o inglês.

O sambahsa também é uma língua finalizada, isso significa que você não
precisa se preocupar se o que você aprendeu hoje será alterado daqui a
cinco anos. O que você escrever hoje em sambahsa será entendido mesmo
daqui a um século.

Mas por não ter mais de 100 anos como outras línguas auxiliares, o
sambahsa ainda não tem uma comunidade com dezenas de milhares de
falantes, mas esse cenário pode mudar, toda língua auxiliar começou com
apenas uma pessoa, mas podem existir milhares de falantes se mais gente
se interessar pela língua.

O SAMBAHSA É DIFÍCIL?
---------------------

É inegável que **no início** **– e somente no início –** o sambahsa é um
pouco mais desafiador se comparado a outras línguas auxiliares, mas é
como Robert Winter disse em seu “Sambahsa: Guide to Pronouns and
Articles” *(Sambahsa: Guia para Pronomes e Artigos)*, o sambahsa tem o
limite de dificuldade praticável para uma linguagem auxiliar
internacional.

O **sambahsa é fácil sim**, você só precisa saber quais são as
prioridades no seu estudo, *você tem que saber como estudar*! Não faz
sentido decorar todos os casos de declinação do dativo se você pouco vai
usá-los *(e talvez só alguns poucos deles, mesmo que você passe toda a
sua vida falando conversando em sambahsa)*! Também é preciso ter em
mente de que não é porque se trata de uma língua fácil que você
escreverá tratados filosóficos em três semanas, mas talvez você possa
fazer isso em seis ou oito meses.

Também dizem que a língua é demasiadamente prolixa, às vezes tendo duas
ou três palavras invariáveis para cada uma do mesmo tipo da nossa
língua. Realmente o sambahsa não economiza em suas palavras invariáveis,
uma língua prolixa como sambahsa paga o preço de exigir um pouco mais de
estudo, mas, em compensação, permite a melhor expressão do pensamento.
Francamente, sem perceber você se acostumará com essas palavras, digo
isso por experiência própria.

Reclamam que o sambahsa possui sons difíceis de aprender, mas todas as
línguas possuem sons diferentes, mesmo outras línguas auxiliares que
tentam ao máximo serem simples. Logo a seguir algumas das línguas mais
estudas pelos brasileiros e seus sons que não existem no português
brasileiro padrão *(os sons em itálico são utilizados no sambahsa)*.

**Inglês *****(americano)***** –** \[ɑ\], *\[ə\]*, \[æ\], \[ɜ\], \[ʌ\],
*\[θ\]*, \[ð\], \[ɹ\], *\[ŋ\]*, \[ʍ\], *\[h\]*

**Espanhol *****(castelhano europeu)***** –** \[β\], *\[θ\]*, \[ð\],
*\[x\]*, \[ɲ\], *\[ŋ\]*, \[ɣ\]

**Alemão –** *\[ç\]*, *\[ʁ\]*, *\[x\]*, *\[ə\]*, \[ʏ\], *\[y\]*,
*\[ø\]*, \[œ\], *\[ŋ\]*

**Francês –** *\[ʁ\]*, \[ɥ\], *\[ə\]*, \[œ\], *\[ø\]*, *\[y\]*, \[ɑ̃\],
\[œ̃\], \[ɔ̃\]

Talvez o sambahsa pudesse ter sons mais simples *(mais simples pra
quem?)* ou menos “sons extras”, mas a língua não seria mais pobre? Mesmo
o esperanto possui alguns sons que desafiam os brasileiros, como o \[x\]
da letra “ĥ”. Os sons são facilmente aprendíveis e vou mostrar-lhe que
você pode estar pronunciando todos eles em uma semana.

Outra queixa é que o sambahsa baseia seu vocabulário em palavras
emprestadas em vez de palavras compostas, eu explico: em sambahsa o
adjetivo “belo” é “bell” enquanto o adjetivo “feio” é “biaur”; alguém
poderia dizer que, por exemplo, um afixo que inverte o significado seria
mais interessante e eliminaria a necessidade de decorar centenas de
palavras. De fato é um recurso que tem suas vantagens, mas, por outro
lado, frases com palavras compostas demandam tempo para serem
analisadas, então, após analisar os pós e os contras, é mais
interessante e prático uma palavra como “biaur” do que “antibell”. E
francamente, até você dominar a gramática você já terá decorado todas as
palavras mais relevantes.

**Tudo tem um preço:** muitas línguas auxiliares tentam ser a mais
simples possível, adotando, por exemplo, ortografias muito simples: uma
letra, um som; todos substantivos têm apenas uma terminação, todos os
adjetivos têm apenas uma terminação, etc. A vantagem de não ter uma
ortografia tão complexa é que o aprendizado é muito mais rápido, mas a
desvantagem é que disso é gerado um pequeno problema para a importação
de palavras de outros idiomas, que terão que ser drasticamente alteradas
*(tanto em ortografia quanto em pronúncia)* para estarem de acordo com a
língua. Outro problema dessa simplicidade é a pouca liberdade para
criação de poesia e música. Não estou dizendo que línguas auxiliares
muito simples são feias, até porque isso é muito subjetivo, e uma pessoa
com uma voz cristalina e talento pode criar a melhor música do mundo com
a língua mais “feia” e simples. O que eu quero dizer é que se paga um
preço pelo excesso de simplificação, e o contrário também é verdadeiro,
se paga um preço para evitar todos esses problemas das línguas simples.
A questão é, qual preço você se dispõe a pagar?

Muitos reclamam que línguas auxiliares muito simples oferecem pouca
liberdade poética, outros dirão que línguas auxiliares mais complexas
são desnecessariamente mais difíceis.

Preso por ter cão, preso por não ter cão

O VOCABULÁRIO DO SAMBAHSA
-------------------------

Logo a seguir, a lista de idiomas que contribuíram para o vocabulário do
sambahsa, as percentagens de palavras emprestadas e alguns exemplos
entre parênteses:

**Protoindo-Europeu –** 44,28 % *(skadh, paursk, potnia)*

**Latim –** 15 % *(facil, question, caise)*

**Família germânica –** 9,5 % *(apter, buk, rogv)*

**Francês –** 6,21 % *(journal, adresse, place)*

**Grego –** 4,64 % *(pharmacia, ieftin, papier)*

**Família românica –** 3,95 % *(important, visite, torte)*

**Árabe –** 3,42 % *(lakin, mutawassit, hatta)*

**Inglês –** 1,45 % *(film, sport, wagon)*

**Família eslava –** 1,28 % *(lige, grance, vessel)*

**Italiano –** 1 % *(autostrad, valise, dusch)*

**Indo iraniano/Persa –** 0,92 % *(naft, ris, hevd)*

**Alemão –** 0,78 % *(dank, postamt, vurst)*

**Chinês –** 0,71 % *(gienxin, yui, saan)*

**Outras línguas ou famílias –** 6,86 %

Sobre o vocabulário advindo das palavras reconstruídas do
protoindo-europeu, é importante dizer que algumas não podem ser
garantidas como “puras do protoindo-europeu comum”, o protoindo-europeu
se dividiu em muitas famílias linguísticas, mas palavras em comum entre
essas línguas podem não ser necessariamente oriundas do
protoindo-europeu. Por exemplo, as palavras “long” e “pisk” são
encontradas nas línguas românicas *(“longo” e “peixe” no caso do
português)* e germânicas *(“long” e “fish” no caso do inglês)*, mas é
incerto se sua origem foi no protoindo-europeu, famílias linguísticas
continuaram a trocar palavras quando o protoindo-europeu se dividiu.

Como algumas palavras podem ter “origens múltiplas”:

**Amlak –** significa “ativo” (contabilidade), corresponde ao árabe
“أملاك”, ao turco “emlak” e ao persa “املاک”.

**Schut –** significa “sem corno”, corresponde ao romeno “Șut”, ao
búlgaro/servo-croata “šut” e ao albanês “shut”.

**Geong –** significa “palácio fortaleza”, corresponde ao caractere
“城”, que é lido como *chéng* em pinyin (mandarim latinizado), *jō* em
Goon (japonês latinizado), *seong* em coreano e *thành* em vietnamita.

**Potire –** significa “jarro”, corresponde à palavra do grego antigo
“ποτήρ”, ao servo-croata “путир”, ao russo “потир” e ao albanês “potir”.

Mais exemplos de palavras de línguas cuja contribuição foi pouco
expressiva, mas que tem sua marca no sambahsa:

**Português –** banan, mingo, namor

**Espanhol –** chocolat, ghitarr(a), salg, vanilia

**Báltico –** biaur, tik

**Malaio –** kye *(este também é eslavo; do malaio-indonésio vem “ke” e
do russo mais algumas línguas eslavas vem “k(o)”)*

**Escandinavo –** leik, lyt, tiel

**Sânscrito –** bahsa, nagor

***Universal –*** mama

**Celta –** sapoun, brigv *(também do germânico)*, brugs *(também do
italiano)*

**Aruaque –** tabak

**Náuatle –** tomat

METODOLOGIA DE ENSINO
=====================

Eu estruturei a gramática da seguinte forma: tentei evitar a
apresentação de um novo conteúdo que exija o conhecimento prévio de
outro assunto ainda não mencionado, por exemplo: se para aprender o
tempo verbal do futuro é necessário saber o infinitivo, ensinarei o
infinitivo antes; se para aprender as declinações é necessário alguns
conhecimentos básicos de gramática, terei que buscar aqueles
ensinamentos das aulas de gramática do ensino médio. Acredito que essa
maneira seja a menos irritante para o iniciante.

O livro foi feito de forma que atenda ao estudante sem nenhum
conhecimento prévio, ou seja, estou assumindo aqui que você não conhece
outra língua e, até mesmo, desconhece certos termos básicos de
gramática. Assim, um estudante poderá ler esta gramática sem a
necessidade de consultar outros livros, com exceção de dicionários. *O
objetivo desta gramática é ser acessível a todos!*

**O que vou dizer agora é importante, então preste atenção!** Quando
você for estudar, não tenha pressa em decorar todos os casos acusativos
ou toda a lista de preposições de uma vez, vá com calma (!), não precisa
ter presa, até porque você usará mais uns do que outros. Nenhuma
gramática foi feita para ser lida apenas uma vez, mas sim várias vezes.
Agora que você está começando, leia os capítulos uma vez, ou duas no
máximo, então você parte para o próximo capítulo. Após terminar de ler
esta gramática, leia textos, eles te ajudarão a aumentar seu vocabulário
e aumentarão seu conhecimento da gramática, uma vez que você terá que
consultar a gramática para entender como aquela frase foi feita.

Pode parecer estranho, mas comecei a fazer esta gramática quando eu
estava me iniciando na língua, na minha segunda ou terceira semana de
aprendizado, e não quando eu já tinha um bom domínio no Sambahsa. Tive
dois motivos para ter feito isso: o ato de fazer uma gramática exige uma
responsabilidade muito grande, o que me impulsionou nos meus estudos da
língua, graças a esse trabalho eu pude aprender as coisas muito mais
rapidamente, foi um exercício muito interessante; o segundo motivo é que
o tipo de pessoa que mais sabe das primeiras dificuldades de um
iniciante é outro iniciante, se eu deixasse para fazer esta gramática
muito depois, talvez eu pudesse ter esquecido de certas dificuldades que
tive e eu não as abordaria nesta gramática.

NA VERDADE VOCÊ NÃO PRECISA APRENDER TUDO
-----------------------------------------

Um dos objetivos desta gramática é ser a mais completa possível, ou
seja, o objetivo é abranger do conteúdo mais básico até elementos que só
aparecerão raríssimas vezes nas literaturas mais eruditas. Haverá coisas
aqui como formas opcionais do presente do indicativo, que raramente são
usadas, mas que esta gramática tem a obrigação de apresentar. Foque-se
apenas no que é necessário para você!

A língua também tem uma fonologia bastante rica, há sons que não existem
no português, mas que podem ser aprendidos em pouco tempo. Fiz o
possível para explicar os sons, seja por comparações com outras línguas
como quais movimentos de língua e boca são feitos. Mas se você realmente
sentir muita dificuldade em algum som, não tem problema, você pode usar
um som parecido, muito dificilmente alguém não entenderá o que você quer
dizer, mas, mesmo assim, tente se esforçar um pouco para aprender tudo,
será um aprendizado que valerá para outras línguas.

DICAS PARA ESTUDAR UM IDIOMA
----------------------------

Nesses anos estudando idiomas eu aprendi algumas coisas, são dicas que
valem para qualquer idioma, não só o sambahsa:

-   Quando você ler, ouvir, escrever ou dizer uma palavra, associe-a
    diretamente a ideia. Vou explicar: algumas pessoas, quando leem
    “nebh”, associam essa palavra a palavra portuguesa “nuvem” e então
    fazem a associação a uma nuvem. Não faça isso, treine sua mente para
    não precisar de um intermediário, tente associar a palavra
    diretamente a ideia ou objeto em questão.
-   Não se prenda muito à gramática, dê uma lida ou duas na gramática e
    parta para os textos, você tem que ver a língua na prática. Com o
    tempo você vai absorvendo as regras gramaticais enquanto adquire
    vocabulário. Quando, em um texto, você não souber como determinada
    construção gramatical foi feita, você volta à gramática.
-   Use a língua desde o primeiro dia. Mesmo que mentalmente, dê bom dia
    em sambahsa quando você acordar, crie frases simples para situações
    cotidianas. Tente usar a língua no início na medida do possível,
    mesmo que você ainda não conheça as preposições mais básicas.

ALFABETO E PRONÚNCIA
====================

Talvez você se assuste um pouco por causa do tamanho deste capítulo, de
fato ele é um pouco grande se comparado a outras gramáticas em seus
capítulos de alfabeto e pronúncia, mas é por um bom motivo. No estudo de
uma língua, a primeira coisa que você tem que aprender são os sons, é
importante que, ao aprender uma palavra nova, você a guarde em sua
memória com a sua pronúncia correta, por isso que não pouparei
explicações neste capítulo.

O alfabeto é composto por 26 letras latinas, ela são: a, b, c, d, e, f,
g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z. A ortografia
é um pouco complexa, mas, ainda assim, regular.

Para representar o som existe a *Transcrição Fonética do Sambahsa*
(TFS), um alfabeto cujas palavras estarão sempre entre chaves { } em
toda esta gramática, as letras em CAIXA ALTA dentro dos colchetes
representam a sílaba tônica e dois pontos (:) representam que a vogal
que vem antes dela é pronunciada um pouco mais demoradamente. Esteja
ciente que as explicações sobre pronúncia são baseadas no português
brasileiro, não numa variante da língua portuguesa de qualquer outro
país lusófono! A maioria dos sons existem na língua portuguesa, mas
outros não, quanto a esses últimos eu fiz o possível para explicá-los,
caso minha explicação não seja boa o suficiente, eu indiquei onde
aparecem esses sons em outras línguas.

Inicialmente a TFS era feita entre colchetes \[ \], mas optei por chaves
{ } porque os colchetes já são usados pelo outro sistema de transcrição
fonética que falarei a seguir.

Além do TFS existe algo chamado *Alfabeto Fonético Internacional* (AFI)
(em inglês*, International Phonetic Alphabet (IPA)*), é como o TFS, mas
o AFI é para todas as línguas enquanto a TFS é somente para o sambahsa.
As letras desse alfabeto estarão sempre entre barras / / ou colchetes \[
\]. Usarei o AFI para explicar a TFS, porque se você tiver acesso aos
sons das letras do AFI, terá uma fonte segura de como as letras do
sambahsa são pronunciadas. **Mas no decorrer do livro, fora do capítulo
de “alfabeto e pronúncia”, eu praticamente só usarei a TFS**! **Na
verdade você não precisa aprender o AFI, mas ele seria um recurso muito
útil pra você.**

A diferença entre o uso de barras e colchetes no AFI é que as barras
indicam uma transcrição mais simples enquanto os colchetes indicam uma
transcrição mais precisa.

A propósito, tenho um Manual de Alfabeto Fonético Internacional que,
assim como está gramática, está disponível livremente na internet. Ele
lhe será útil e talvez você o encontre no mesmo lugar onde você
encontrou esta gramática.

Você me pergunta: por que não usamos somente o AFI se este é oficial e
mais conhecido enquanto o TFS se aplica somente ao Sambahsa? Por dois
motivos:

-   A TFS é muito mais fácil de escrever no computador, o que facilita
    na hora de explicar a pronúncia para alguém pela internet ou
    qualquer trabalho digitado.
-   Sambahsa foi feito para pessoas de todo o mundo, uma letra da TFS
    pode englobar diversos sons parecidos do AFI. Por exemplo, o {r}
    representa, preferencialmente, aos sons /ʀ/ ou /r/, mas também pode
    representar o som /ɾ/ que ocorre no português brasileiro; o {o}
    representa aos sons /o/ e /ɔ/.

Antes de continuarmos, algumas observações:

-   Provavelmente muitos sons aqui serão novos para você. Não se
    preocupe, todos os sons não encontrados na língua portuguesa do
    Brasil serão explicados com o maior detalhamento possível.
-   Esteja ciente que as descrições fonéticas são baseadas no português
    brasileiro.
-   O Brasil é um país continental e possui uma certa variação
    linguística, os dialetos do português usados como referência aqui
    são o que é falado em parte da Bahia e o oficialmente considerado
    como dialeto paulista. Embora sejam dialetos diferentes, no que é
    relevante neste livro ambos servirão para explicar a fonética do
    sambahsa.
-   Sambahsa é bastante rico no que concerne a sons, se você achar que
    um som é muito complicado, você pode usar um som parecido no lugar
    se você achar que o significado da palavra não será deturpado e
    causará confusão na conversa. O importante é que você use o
    sambahsa!
-   Você não precisa usar o AFI se não quiser, uma vez que todos os sons
    serão explicados. O AFI está aí para servir como um recurso a mais
    para você.
-   Mesmo que você nunca tenha ouvido falar do AFI, você pode usá-lo
    facilmente. Existe muito material que mostra os sons do AFI e alguns
    até ensinam como fazê-los, embora não haja uma abundância de
    materiais em português. Digite, ou copie e cole, a letra do AFI que
    você deseja aprender no seu site de busca preferido e veja as opções
    de sites e arquivos que tratam sobre a pronúncia desses sons. Não
    tem mistério.

    -   Como eu havia dito antes: o AFI praticamente só será utilizado
        neste capítulo de “Alfabeto e Pronúncia”, no resto do livro
        utilizarei apenas a TFS! O AFI é só um guia seguro para você
        aprender a TFS!

-   Se o AFI não for muito claro para você, no final eu coloquei algumas
    palavras da nossa língua portuguesa transcritas nesses alfabetos
    para que você os compreenda bem.

**{a} –** /a/ – como “a” em “c**a**sa” ou “J**a**ime”.

**{ä} –** /ɛ/ – como “e” em “m**é**dico”, mas *não* como em
“cab**e**lo”.

**{b} –** /b/ – como “b” em “**b**arraca”.

**{c} –** /ʃ/ – como “ch” em “ma**ch**ado”, “x” de “amei**x**a” ou “sh”
de “**sh**ampoo”.

**{d} –** /d/ – como “d” em “**d**a**d**o”.

**{e} –** /e/ – como “e” em “cab**e**lo”, mas *não* como em
“m**é**dico”.

**{ë} –** /ə/ – som conhecido como xevá ou schwa, é como as seguintes
vogais do inglês americano: “e” em “fath**e**r” ou “a” em “**a**gain”.
Não sabe inglês? Não tem problema, eu vou tentar explicar como se faz
esse som. É como se fosse um “a” que vem do fundo da sua garganta, mas é
uma vogal que vem sem nenhum esforço, até mesmo sua boca só se abre de
leve e bem pouco.

**{f} –** /f/ – como “f” em “**f**ilho”.

**{g} –** /g/ – como “g” em “**g**arota” ou “gu” em “**gu**eto”.

**{h} –** /h/ – bem parecido com “r” em “**r**aposa” ou “rr” em
“ca**rr**o”, mas o som dessas consoantes dos exemplos é /ɦ/ *(mas pode
ser /ʁ/, /ɣ/ ou /χ/ a depender da variedade do português que você
fale)*, não o /h/ do sambahsa! O som /h/ é bastante simples. No nosso
/ɦ/ nós vibramos as cordas vocais, no caso do /h/ as cordas vocais não
vibram, se você leu meu Manual de Alfabeto Fonético Internacional você
terá uma ideia de como fazer isso.

**{i} –** /i/ – como “i” em “**i**lha”.

**{j} –** /ʒ/ – como “j” em “**j**anela” ou “g” em “**g**irafa”.

**{k} –** /k/ – como “c” em “**c**asa” ou “qu” de “**qu**ente”.

**{l} –** /l/ – como “l” em “**l**ua”.

**{m} –** /m/ – como “m” em “**m**ãe”.

**{n} –** /n/ – como “n” em “**n**adar”

**{o} –** /o/ ou /ɔ/ – como “o” em “m**o**t**o**r” (som /o/) ou
“**ó**leo” (som /ɔ/), tanto faz.

**{ö} –** /[]{#anchor-3}[]{#anchor-4}ø/ – como o “ö” do alemão ou o
“eux” do francês. Não sabe alemão e nem francês? Não tem problema, eu
explico como se faz esse som. É bem simples, você faz que vai emitir o
som de {e}, mas com a boca como se fosse fazer o som de {o}, ou seja,
fale “ê” com o biquinho de quem fala “ô”.

**{p} –** /p/ – como “p” em “**p**ai”.

**{q} –** /x/ – como o “ach-laut” do alemão ou o “j” da palavra
espanhola “**j**ota”. Não sabe alemão e nem espanhol? Não tem problema,
eu explico como se faz esse som. É como se fosse um som de “rr”, como em
“carroça”, mas a parte de trás da sua língua é posta contra o seu palato
mole como você faz com {k} e {g}; pense numa chaleira daquelas antigas.

**{r} –** /[]{#anchor-5}ʀ/ – em situações como em “radh”, “prete” e
“accurat”, onde depois do “r” há uma vogal, pode-se tranquilamente
pronunciar esse “r” como você faz em “ca**r**o” ou “ba**r**alho”, mas
*não* como em “**r**aposa”. Na verdade o sambahsa admite uma ampla
variedade desse som – inclusive o nosso “r” (/ɾ/) –, mas *se* for para
eleger um som oficial, seria o “r” uvular (/ʀ/) do alemão. Não sabe
alemão? Não tem problema, eu explico como se faz esse som. Enquanto o
nosso “r” é feito através da vibração da ponta da língua, o /ʀ/ é feito
através da vibração da úvula. Mas como fazer esse som? Não é impossível,
na verdade é bem simples, faça o seguinte exercício: gargareje um
pouquinho de água *(ou qualquer outro líquido)*, através desse simples
movimento você vai começar a entender como vibrar sua úvula.

**{r} –** /ʁ/ – em situações como em “irk”, “amor” e “absorb”, onde logo
depois do “r” há uma consoante (não uma vogal!) ou este “r” é a última
letra da palavra, esta consoante assume o som de /ʁ/. O /ʁ/ é muito
similar ao /ɦ/ de “ca**rr**o” ou “**r**ede”, a única diferença é que
enquanto o ponto de articulação de /ɦ/ é na glote *(o que, para alguns,
é o mesmo que não ter ponto de articulação)*, o ponto de articulação de
/ʁ/ é na úvula, ou seja, para você pronunciar o /ʁ/ você coloca a parte
de trás da sua língua contra a úvula e então faz o som de “r” de
“**r**oupa”. Logo abaixo alguns exemplos em AFI.

spar – /spaʁ/\
cherkin – /t͡ʃəʁ'kin/\
wir – /wiʁ/

clever – /'klevəʁ/\
cort – /koʁt/\
amor – /a'moʁ/\
cort – /koʁt/\
aur – /'aʊʁ/\
mer – /meʁ/\
air – /ɛʁ/\
atelier – /ate'ljeʁ/\
ier – /jeʁ/\
ierk – /jeʁk/\
piurn – /pjuʁn/\
oyr – /ojʁ/\
ayr – /ajʁ/\
butour – /bu'tuːʁ/\
alabster – /a'labstəʁ/\
gurgule – /'guʁgyl/\
eurp – /øʁp/\
arbust – /aʁ'bust/\
ender – /'endəʁ/

biaur – /bj'aʊʁ/

**{s} **– /s/ –** **como “s” em “**s**apo” ou “c” em “**c**érebro”.

**{t} –** /t/ – como “t” em “**t**a**t**u”

**{u} –** /u/ – como “u” em “H**u**go” ou “ba**ú**”.

**{ü} –** /y/ – como o “u” do francês ou “ü” do alemão. Não sabe francês
e nem alemão? Não tem problema, eu explico como se faz esse som. É muito
simples, faça o seguinte: fale o “u” do português, observe como se movem
os músculos da sua boca, sua boca não fica com um formato de “o”?
Observou? Agora fale “i”, mas – preste atenção – dessa vez fale “i” com
o movimento de boca que você usa para falar “u”, ou seja, fale “i” com o
biquinho da boca em formato de “o”.

**{v} **– /v/ –** **como “v” em “**v**elho”.

**{w} –** /w/ – como “w” em “ki**w**i” ou “**W**illian”. No caso desse
{w} vier depois de uma vogal e você tiver dificuldade de pronunciá-lo,
você pode pronunciar esse som como o “l” em “a**l**moço” ou o “u” em
“a**u**tomóvel”, cujo som na verdade é \[ʊ̯\] ({o} em TFS), mas *não*
como “u” em “sa**ú**de” ou “ch**u**va”. É uma semivogal.

**{y} –** /j/ – como o “i” em “histór**i**a” ou “sér**i**e”. Você também
pode pronunciar como “i” de “pa**i**” e “o**i**to” *(mas saiba que esses
têm o som de \[ɪ̯\], não \[j\])*, mas *não* como em “p**i**a” ou
“f**i**lha”. É uma semivogal.

**{x} –** /ç/ – como o “ich-laut” do alemão ou o “h” da palavra inglesa
“**h**uman”, é representado pelo “sh” da ortografia do Sambahsa. Não
sabe alemão? Não tem problema, eu explico como se faz esse som. Coloque
a parte de trás de sua língua contra o seu palato duro, da mesma forma
como ocorre com o som {y}, então você tenta pronunciar esse som como
você faria com {s}.

**{z} –** /z/ – como o “z” em “**z**ero” ou “s” em “ca**s**a”.

**{§} –** /θ/ – como o “th” do inglês em “**th**in” ou “bir**th**day”,
mas *não* como em “**th**is” ou “wor**th**” cujo som é /ð/. Não sabe
inglês? Não tem problema, eu explico como se faz esse som. É como se
você fosse fazer o som de “s”, como em “*s*apo” ou “*s*ílaba”, mas com a
ponta da língua entre os dentes da frente.

**{tc} –** /t͡ʃ/ – como o “tch” em “**tch**au” e “a**tch**im”

**{dj} –** /d͡ʒ/ – como o “d” em “**d**ia” ou “a**d**mite”. Essa eu tenho
que dar uma explicação especial porque existem regiões do Brasil em que
a pronúncia do “d” isolado ou junto ao “i” é diferente. Fale “da”, “de”,
“di”, “do” e “du”, repare os movimentos de boca e língua que você fez,
se o “d” do seu “di” é diferente do “d” dos demais, então você realmente
pronuncia “di” como {dji}, mas se você pronuncia o “d” do “di” da mesma
forma que “da”, “de”, “do” e “du” – ou seja, você pronuncia “di” como
{di} –, então os meus exemplos (“dia” e “admite”) não são válidos para
você neste caso, mas eu espero que você tenha entendido como funciona
esse som.

Sugiro que você aprenda muito bem a TFS antes de seguir adiante. Por ser
uma parte muito importante, eu sugiro que você abra uma exceção e a
releia mais de duas vezes, preciso que você fixe bem isso!

ATENÇÃO ESPECIAL PARA BRASILEIROS
---------------------------------

Sobre o {l}:

Quando esta letra *não* modifica uma vogal, ou seja, quando no final de
uma palavra ou depois de uma vogal sem ter outra vogal em seguida, o “l”
*não* é pronunciado como {w}, como nós brasileiros costumamos fazer, mas
sim como fazem os portugueses, em que o “l” é realmente pronunciado como
{l}. Eu explico como você pronuncia esse “l” solitário: observe o
movimento que sua língua faz quando você fala “la”, “le”, “li”, “lo”,
“lu”, pratique esse movimento de língua sem fazer o uso das vogais. Uma
palavra como “auxel” você não pronuncia {Aoksëw}, mas sim {Aoksël}.

Sobre o “d” e o “t”:

A pronúncia dessas letras, em algumas regiões do Brasil, geralmente é
feita dessa maneira:

{da}, {de}, {dji}, {do}, {du}

{ta}, {te}, {tci}, {to}, {tu}

O “d” de “di” é diferente do “d” de “da”, “de”, “do” e du”; digo o mesmo
do “t” de “ti”. Mas não é assim que acontece no sambahsa.

Fique atento, “t” e “d”, quando precedem uma vogal, sempre terão
respectivamente o som de {t} e {d}, então se você vê uma palavra como
“yadi”, que significa “se apenas”, não a pronuncie como {yAdji}, mas sim
como {yAdi}!

Sobre o {m} e o {n}:

Quando sozinhas, ou seja, quando não estão em conjunto com uma vogal,
como em “ma”, “me”, “mi”, “mo”, “mu”, essas consoantes são pronunciadas
separadamente. Vejamos um exemplo: a palavra “vimb”, que significa
“brema” *(uma espécie de peixe)*, não é pronunciada como vĩ-b, mas sim
como vi-m-b, o “m” é pronunciado isoladamente!

Pronunciar apenas o {m} e o {n} sozinhos não é nada difícil, mas pode
exigir um pouquinho de prática, o procedimento para aprender a pronuncia
é praticamente o mesmo que eu dei para o {l} solitário, mas dessa vez
você vai prestar atenção no movimento que sua boca faz.

Veja o porquê de não pronunciar sílabas como “an” ou “am” na forma de um
“ã”.

**Kams music, kans ghitarr –** você gosta de música, você toca violão.

Como você já tem uma noção de como funciona o “m” e o “n” quando
“sozinhos”, me sinto mais confortável para explicar os sons abaixo que
você pode se deparar:

**{ng} –** /nj/ ou /ŋ/ – como o “ng” das palavras inglesas “thi**ng**” e
“ki**ng**”, cujo som é /ŋ/, mas *não* como o “n” das palavras inglesas
“thi**n**” e “ki**n**”. Você fala o “n” com a língua para baixo, esse
movimento por si só faz a língua tapar um pouco a sua garganta *(fique
tranquilo que você não vai ficar sem conseguir respirar)*, de forma que
o som passe pelas suas vias nasais, não é a toa que a letra do AFI “ŋ”
se chama “nasal velar”.

**{nk} –** /ŋk/

ALGUMAS PALAVRAS PORTUGUESAS EM AFI E TFS
-----------------------------------------

Lembra que eu prometi a transcrição de algumas palavras nossas em AFI e
TFS? Se você chegou até aqui sem entender direito esses dois alfabetos,
vamos ver se com a lista abaixo você entende. Propositalmente eu inseri
algumas palavras cujos sons não existem no sambahsa, isso serve para lhe
deixar atento quanto as diferenças das duas línguas, nesses casos não
haverá uma transcrição para o TFS.

É importante frisar que a TFS não foi feita para transcrever palavras de
outras línguas que não o sambahsa (!!!), o que faremos aqui é apenas uma
brincadeira para ver se você ganha mais intimidade com a TFS.

Para que você não se perca, primeiro é mostrado a palavra, em seguida a
transcrição em AFI, que estará entre colchetes, e depois a transcrição
em TFS, que estará entre chaves.

**Morno –** \['moh.nu\] – {mOhnu}

**Avô –** \[a.ˈvo\] – {avO}

**Avó –** \[a.ˈvɔ\] – {avO}

**Cama –** \[ˈkɐ̃.mɐ\] *(não existem vogais nasalizadas no sambahsa)*

**Leila –** \[ˈleɪ̯.lɐ\] – {lEyla} *(podemos representar /*ɐ*/ por
{*a*})*

**Mel –** \[mɛʊ̯\] – {mÄo}

**Meu –** \[meʊ̯\] – {mEo}

**Baixo –** \['baːɪ̯.ʃʊ\] – {bAycu} *(podemos representar /*ʊ*/ por
{*u*})*

**Alto –** \[ˈaʊ̯.tʊ\] – {Aotu}

**Aula –** \[ˈaʊ̯.lɐ\] – {Aola}

**Pai –** \[ˈpaɪ̯\] – {pay}

**Quatro –** \[ˈkʷa.tɾʊ\] – {kwAtru} *(a TFS não pode representar
diacríticos, mas o /*ʷ*/ pode ser facilmente convertido para {*w*})*

**Rio –** \['ɦiu\] – {hIu} – *(podemos considerar /*ɦ*/ como {*h*})*

**Baralho –** \[bɐ.ˈɾa.ʎʊ\] *(não existe o som /*ʎ*/ no sambahsa)*

**Girafa –** \[ʒɪ.ˈɾa.fɐ\] – {jirAfa}

**Magro –** \[ˈma.gɾʊ\] – {mAgru}

**Idade –** \[ɪ.ˈda.d͡ʒ(ɪ)\] – {idAdj(i)} *(podemos representar /*ɪ*/ por
{*i*})*

**Arroz –** \[a.'ɦos\] – {ahOs}

**Coral –** \[ko.'ɾaːʊ̯\] – {korAo}

**Caroço –** \[ka.ˈɾo.sʊ\] – {karOsu}

**Ritmo –** \['çi.t͡ʃi.mʊ\] – {xItcimu}

**Ambidestro –** \[ˌɐ̃.bɪ.ˈdɛs.tɾʊ\]

**Carne –** \['kah.nɪ\] – {kArni}

**Freire –** \[ˈfɾeɪ̯.ɾɪ\] – {frEyri}

**Itália –** \[i.'ta.lʲɐ\], \[i.'ta.ljɐ\] ou \[i.ˈta.ʎjɐ\] – *somente
\[i.'ta.ljɐ\] pode ser transcrito em TFS, que seria {itAlya};
\[i.'ta.lʲɐ\] também poderia ser transcrito para {itAla} se ignorarmos o
diacrítico, mas a palavra não seria bem representada*

**Muro –** \[ˈmu.ɾʊ\] – {mUru}

**Kiwi –** \[ki.'wi\] – {kiwI}

**Água –** \['a.gʷɐ\] – {Agwa}

**Cantor –** \[kɐ̃.'toh\]

**Ordem –** \['ɔh.dẽɪ̯̃\]

**Amar –** \[ɐ̃.'maχ\]

**Amarra –** \[ɐ̃.'ma.ɦɐ\]

**Chineses –** \[ʃĩ.'ne.zɪs\]

Já está muito claro para você que o {r} da TFS pode assumir diversos
sons (*como /ɾ/, /r/, /ʁ/, /ʀ/ ou /ɹ/)*, por isso que pude usá-lo para
representar diferentes sons na lista acima. Portanto, se você for
explicar a fonética do português brasileiro para um estrangeiro
conhecedor da TFS, temo que será melhor usar o AFI.

UMA ÚLTIMA VISITA AO ALFABETO E AO NOME DA LÍNGUA
-------------------------------------------------

Agora que você já sabe a TFS, vamos voltar ao alfabeto para que você
saiba como cada letra é pronunciada:

a {a}, b {be}, c {tse}, d {de}, e {e}, f {ef}, g {dje}, h {hatc}, i {i},
j {jye}, k {ka}, l {el}, m {em}, n {en}, o {o}, p {pe}, q {ku}, r {er},
s {es}, t {te}, u {u}, v {ve}, w {we}, x {iks}, y {ü}, z {dzed}

Antes de continuar, é importante saber como a nossa língua é
pronunciada!

**Sambahsa Mundialect –** {sambA:sa mundyAlëkt}

AINDA NÃO TERMINOU, ALGUMAS CONVENÇÕES
--------------------------------------

**Antes de continuar, um aviso:** não se preocupe tanto, a princípio,
com as regras mais complexas de pronúncia, porque passarei um bom tempo
mostrando nos exemplos a pronúncia de cada palavra, você acabará
aprendendo-as naturalmente. *Não decore, entenda!* Meu conselho é: leia
esta parte uma ou duas vezes – no máximo – e continue seus estudos. A
ortografia é tão difícil assim? Não, mas pode ser chato para quem está
começando e não é algo, a meu ver, obrigatório no início uma vez que eu
irei dispor a pronúncia nos exemplos.

A partir daqui até o final deste capítulo de **ALFABETO E PRONÚNCIA**, o
texto será praticamente uma tradução de um capítulo do livro *The
Grammar Of Sambahsa-Mundialect In English*, do Dr. Olivier Simon.

As vogais são representadas por *V*, elas são: “a”, “e”, “i”, “o”, “u”.
As semivogais são “w” e “y”, elas são representadas por *C* assim como
as consoantes. As letras “w” e “y” podem assumir a função de vogais
quando elas não estão relacionadas a nenhuma vogal.

### A LETRA “e”:

Quando é a sílaba tônica ou a primeira letra da palavra, é pronunciada
como {e}.

**incandescent {inkandEsënt} –** incandescente

**emigrant {emigrAnt} –** emigrante

Não é pronunciada quando sozinha no final de uma palavra…

**monte {mont} –** montar *(um animal)*

**claviature {klavyatÜr} –** teclado

**clientele {klyentEl} –** clientela

… ou no final de uma palavra antes das letras “s” e “t”.

**crimes {krims} – **crimes

**survivet {survIvd} –** *(ele/ela)* sobrevive

Em alguns casos a perda do “e” pode causar confusão ou deixar a palavra
impronunciável, nesse tipo de situação o “e” assume o som de {ë}.

**storgnet {stOrnyët} –** atordoado

*Por que o “gn” de “storgnet” é pronunciado como “ny”? Explico depois.*

Todas as palavras terminadas em “quet” e “ques” são pronunciadas,
respectivamente, como {kët} e {kës}.

Nos demais casos o “e” tem som de {ë}, como em “kohlen”, que se
pronuncia como {kO:lën}.

*Por que a pronúncia do “o” é mais longa nessa palavra? Essa eu já lhe
respondo.*

### A LETRA “h”:

O som de {h} aparece quando o “h” está no início de uma palavra ou entre
vogais.

**habe {hab} –** ter

**rahat {rahAt} – **descansar

Quando letra “h” está depois de uma vogal e não é seguida por mais
nenhuma outra vogal, a letra “h” serve para indicar que a vogal é
pronunciada de forma prolongada.

**kohlen {kO:lën} –** escondido

**bahsa {bA:sa} – **língua, idioma

**bah {ba:} –** falar

Talvez você me pergunte: “ora, não seria mais fácil escrever a vogal
duas vezes para indicar a duração maior da pronúncia? Em vez de escrever
'bahsa', não seria mais simples escrever 'baasa'?”

***A resposta é:** Não! Isso produziria um som diferente, “baasa” seria
pronunciado como {baAza}.*

Se a letra “h” estiver depois de um ditongo, como em “credeih”, que
significa “acreditar”, você prolonga a pronúncia da vogal principal e
então você pronuncia a semivogal.

**addeih {adE:y} –** adicionar

**wehrgeih {we:rdjE:y} –** dar play, rodar, fazer funcionar, operar

“gh”, “bh” e “dh” correspondem respectivamente a {g}, {b} e {d}, o “h”
nessas letras serve para que elas não sofrão nenhum tipo de modificação.

VOGAIS
------

**“eau” {o:} –** /o:/ como em “bureau” {bürO:} *(escritório)*

**“aa” {aA} –** /a'a/ – como em “saat” {saAt} *(tempo)*

**“ae”, “ae” {ay} –** /aj/ – como em “yoinkjiae” {yoynkjiAy} *(escala
musical)*

**“ai” {ä} –** como em “caise” {käz} *(queijo)*

**“au” {Ao} –** []{#anchor-6}\[aʊ̯\] – como em “Australia” *{aostrAlya}
(Austrália)*

**“ea” {Ea} *****(quando no final de uma palavra)***** –** /'ea/ – como
em “wakea” {wakEa} *(definitivamente)*

**“ea” {ëa} *****(quando no meio de uma palavra)***** –** /əa/ – como em
“ocean” {ots(ë)An} *(oceano) Perceba que como os sons de {ë} e {a} são
parecidos, na prática o “e”, deste caso, geralmente acaba sendo
absorvido, ou seja, não é pronunciado. Neste caso, qualquer consoante
final – exceto “s” – muda a acentuação tônica em “a”, e leva a essa
pronúncia diferente de “wakea”.*

**“ee” {Eë} –** /eə/ – como em “eet” {Eët} *(*(ele/ela)* era)*

**“eo” {Eo} –** \[eʊ̯\] – como em “fianceo” {fyantsEo} *(noivo(durante o
noivado))*

**“eu” {ö} –** como em “Europe” {örOp} *(Europa)*

**“ie” {i:} *****(quando sozinha no final de uma palavra***** –** como
em “publie” {publI:} *(publicar)*

**“ie” {ye} *****(nos outros casos)***** –** /je/ – como em “publiet”
{publyEt} *(publicado)*

**“iu” {yu} –** /ju/ – como “iu” em “siuk” {syuk} *(seco(a))*

**“oe”, “oi”, e “oy” {oy} –** /oj/ – como em “choengju” {tcOyngju}
*(álcool de arroz)*

**“oo” {oO} –** /o'o/ – como em “boot” {boOt} *(barco)*

**“ou” {u:} –** /u:/ – como em “courage” {ku:rAdj} *(coragem)*

**“ue” {ü:} –** como em “continue” {kontinÜ:} *(continue)*

**“ui” {wi} **– /wi/ –** **como em “tuich” {twitc} *(vazio)*

**“uy” {uy} –** /uj/ – como em “lastruym” {lastrUym} *(porão de navio de
carga)*

**“u” {u} *****(“u” terá o som de {u}, mas será {ü} se houver a letra
“e” entre as duas letras seguintes)***** –** como em “bur” {bur}
*(cinzas)*, mas “bureau” é {bürO:}.

SEMI VOGAIS
-----------

“w” e “y” são {w} e {y} respectivamente quando junto a vogais, mas
quando isoladas a letra “w” é pronunciada como um {u} curto, mais
precisamente \[ŭ\] ou \[ʊ̆\], e a letra “y” é como {ü}.

**sehkwnt {sE:kunt} –** eles seguem

**type {tüp} –** cara, tipo

Por “tipo” entenda algo como: “não gosto dos tipos que estão andando no
nosso bairro à noite”.

Mas quando a letra “y” se encontra no final de uma palavra ou seguida de
um -s, “y” e “ys” terão respectivamente os sons {i} e {is}.

**baby {bAbi} – **bebê

**babys {bAbis} –** bebês

CONSOANTES
----------

**“sch” {c} –** como em “muraischmusch” {muräcmUc} *(mosca do pântano)*

**“ch” {tc} *****(ou {k} quando antes de uma consoante)***** –** como em
“cheus” {tcös} *(escolher)* e “Christ” {krist} *(Cristo)*.

**“gn” {ny} –** /nj/ – como em “gnoh” {nyo:} *(saber)*

**“kh” {q} –** como em “khiter” {qItër} *(mal)*

**“ph” {f} –** como em “philosophia” {filozOfya} *(filosofia)*

**“qu” {kw} *****(quando antes do “a”, “o” e “u”, ou {k} quando antes de
“e”, “i” e “y”)***** –** \[kw\] ou \[kʷ\] – como em “quod” {kwod} *(o
que)* e “quis” {kis} *(quem)*.

**“sc” {sk} *****({s} quando antes de “e”, “i” e “y”)***** –** “scutt”
{skut} *(sacudir)* e “science” {syents} *(ciência)*

**“sh” {x} –** como em “shienciu” {xyEntsyu} (*coberto de vegetação)*

**“ss” {s} –** como em “permission” {përmisyOn} *(permissão)*

**“th” {§} *****({t} quando junto a {s}, {c} ou {j} *****) –** como em
“thamf” {§amf} *(fedor)* e “esthetic” {estEtik} *(estética)*.

**“c” {k} *****({ts} (/t͡s/) quando antes de “e”, “i” e “y”)***** –**
como em “condition” {kondityOn} (condição) e “petrificit” {pëtrifItsit}
*(petrificado)*

**“g” {g} *****({dj} quando antes de “e”, “i” e “y”)***** –** como em
“gulf” {gulf} *(golfo) *e “large” {lardj} *(grande)*.

**“j” {j} –** sempre {j}.

**“r” {r} –** como existem pessoas de diferentes lugares, admite-se uma
variedade maior de formas de como esse som pode ser feito, inclusive o
nosso /ɾ/! A forma recomendada – mas não compulsória – é a falada em
Luxemburgo ou Sarre, /ʀ/. Entretanto esse som pode assumir outras
formas:

**“rr” /r(r)/ e “rh” /r(h)/ –** como o “rr” (/r/) espanhol. Não sabe
espanhol? É como se fosse o nosso “r”, só que a língua vibra mais vezes
e o movimento da língua é mais causado pela passagem de ar do que pela
língua em si.

**“rl” –** é como o “r” (/ɺ/) do japonês, também pode ser pronunciado
como /rl/ ou /ʁl/. Não sabe japonês? Não tem problema, eu explico: é
muito similar ao /ɾ/ mas você deve segurar a língua só um tiquinho
apenas para que o ar passe pelos lados da língua em vez do centro da
língua.

**“s” {s} *****({z} quando entre vogais)***** –** “son” {son} *(filho)*
e “decision” {dëtsiziOn} *(decisão)*

**“x” {ks} –** /ks/ – pode tornar-se {gz}, se isso facilitar a
pronúncia.

**“z” {dz} –** /d͡z/ – como em “zangir” {dzAndjir} *(corrente)*

MAIS OBSERVAÇÕES
----------------

Algumas letras, principalmente aquelas no final da palavra, podem ser
modificadas pelos sons vizinhos. Como é o caso de “hands” {handz}
(mãos).

Algumas consoantes, assim como a vogal {ë}, podem ser omitidas. A
palavra “franceois” é oficialmente {frantsëOys}, mas pode ser {fransOys}
porque {t} está dentro de um grupo consonantal e a vogal não tônica {ë}
está próxima da vogal tônica {o}.

Se uma forma verbal começa com {s*C*}, pode ser adicionado oi- antes
dessa palavra por razões de eufonização. Como por exemplo “skap” que se
torna “oiskap”.

ACENTO TÔNICO EM SAMBAHSA
-------------------------

Comece analisando a palavra a partir da última sílaba.

### RECEBERÁ SEMPRE ACENTUAÇÃO TÔNICA

Vogais antes da letra “h” ou de uma consoante dupla (rr”, “ll”, “tt” …),
por consoante dupla também se entende o “ck”{k(k)}. O mesmo se diz de
uma sílaba antes de um final -e. Eis alguns exemplos:

**Prodah –** {prodA:} *(entregar)\
Pra você entender: se a palavra fosse “proda”, a pronúncia seria
{prOda}*

**Recess –** {rëtsEs} *(recesso, intervalo, recreio)*

Pra você entender: se a palavra fosse “reces”, a pronúncia seria
{rEtsës}

**Frontdeck –** {frondEk} *(plataforma da parte da frente de um navio)*

Pra você entender: se a palavra fosse “frontdec”, a pronúncia seria
{frOndëk}

**Taslime –** {taslIm} *(rendição)*

A primeira de duas vogais juntas, com exceção de “i” e “u” como
semivogais.

**Armee – **{armEë} *(exército)*

**Australia –** {aostrAlya} *(Austrália)*

A sílaba final: -in *(mas não -ing)*, ey, ie, ui *(quando a pronúncia é
{wi})*, oCel *(na qual C é uma única consoante)*.

**Hotel –** {hotEl} *(hotel)*

**Suadin – **{swadIn} *(bom tempo)*

**Reling –** {rEling} *(corrimão de um navio)*

**Kierey –** {kyerEy} *(carneiro) *

As vogais “a”, “o” e “u” quando antes de uma consoante ou semivogal, com
exceção de um único “s”.

**Cadaloc – **{kadalOk} *(qualquer lugar)*

**Naval –** {navAl} *(naval)*

**Dayluk –** {daylUk} *(continente)*

### NUNCA RECEBERÁ ACENTUAÇÃO TÔNICA

Prefixos.

***For*****trehc –** {fortrE:k} *(partir para uma viagem)*

*Re*cess

***Be*****vid** – {bëvId} *(mostrar, provar)*

**For- –** prefixo que significa longe

**Re- –** prefixo que significa de volta

**Be- –** prefixo que significa significado algo factivo

A letra “w” quando usada como uma vogal.

**Sehkwnt –** {sE:kunt} *(eles seguem)*

Uma palavra terminando em -(i)um, -ule e -s.

**Schives –** {civz} *(mudar, deslocar)*

**Territorium –** {territOryum} *(território)*

**Insule –** {Insül} *(ilha)*

Uma vogal ou semivogal sozinha no final da palavra.

**Okwi –** {Okwi} *(olhos)*

**Baby –** {bAbi} *(bebê)*

As vogais “e”, “i” e “y” no final da palavra seguidas por uma semivogal
- com exceção de “ey” - ou uma ou diversas consoantes, mas não as
duplas.

**Seghel – **{sEgël} *(vela, velejar)*

**Tolkit –** {tOlkit} *(ele/ela falou)*

**Khitert –** {qItërt} *(mal *(substantivo)*)*

Em palavras compostas a sílaba tônica permanece a mesma da palavra
original.

**Gouverne –** {gu:vErn} *(governar)*

**Gouvernement –** {gu:vErnëmënt} *(governo)*

**Nest –** {nest} *(ninho)*

**Corcuksnest –** {korkUksnëst} *(ninho de corvo)*

Essas regras não se aplicam necessariamente a substantivos próprios e o
uso do hífen preserva a acentuação tônica de ambos os lados.

CASO VOCÊ TENHA ACHADO ALGUNS SONS MUITO DIFÍCEIS
-------------------------------------------------

Fiz o meu melhor para ensinar cada som, mas se você realmente não
conseguir aprendê-los – mas eu acredito que você consegue – não será o
fim do mundo!!! Se você não consegue pronunciar o som /ŋ/, não tem
problema em usar o nosso /n/, todo mundo vai entender o que você falar,
digo o mesmo para os variados sons de “r”, se você não consegue
pronunciar o “r” japonês, pronuncia o nosso “r” mesmo; não precisa
esquentar a cabeça! Ninguém colocará você na cadeia se você trocar sons
como /x/ ou /ç/ respectivamente pelos sons /ʃ/ e /h/. Mas, ainda assim,
tente se esforçar um pouco. A boa notícia é que os sons “mais difíceis”
são justamente os mais raros de aparecerem nas palavras!

Uma vantagem em dominar a fonética do sambahsa é que você já domina a
fonética de muitos outros idiomas.

SELLAMAT!
=========

Sellamat! É com essa palavra, “sellamat” {selamAt}, que dizemos “olá”,
você também pode dizer “salut” {salUt} que tem a mesma função. Perceba
que o “ll” tem som de {l}, não do nosso “nh”!

Nesse capítulo lhe apresentarei umas frases, provavelmente você deduzirá
cada um dos elementos delas por si só, mas, de qualquer forma, nos
capítulos posteriores destrincharei cada elemento de uma frase, então
não se preocupe se você não entender alguma coisa neste capítulo.

Diferente do inglês e da maioria das línguas auxiliares, os verbos do
sambahsa são conjugados. Todos os verbos são regulares, com exceção dos
verbos “ser”, “ter” e “saber”, mas não são difíceis. Vamos começar com
algumas frases básicas:

**Som Ricardo {som rikArdo} –** sou Ricardo

**Io som Ricardo {yo som rikArdo}***** *****–** eu sou Ricardo

**Ego som Ricardo {Ego som rikArdo}***** *****–** EU sou Ricardo *(maior
ênfase no “eu”)*

Perceba que, da mesma forma como ocorre no português e espanhol, na
maioria dos casos você poderá omitir o pronome, pois o próprio verbo
indica o pronome utilizado. Você acabou de conhecer o verbo
correspondente a “sou” e os pronomes pessoais para o pronome “eu”. Vamos
para as próximas frases:

**Es Carlos {es kArlos} –** és Carlos/ você é Carlos

**Es Camila {es kamIla} –** és Camila / você é Camila

**Tu es Carlos {tu es kArlos} –** tu és Carlos/ você é Carlos

**Tu es Camila {tu es kamIla} –** tu és Camila / você é Camila

**Is est Carlos {is est kArlos} –** ele é Carlos

**Ia est Camila {ya est kamIla} –** ela é Camila

[]{#anchor-7}[]{#anchor-8}**Smos brasileirs {smos brazilEyrs} –** somos
brasileiros

**Wey smos brasileirs {wey smos brazilEyrs} –** nós somos brasileiros

**Sambahsa est facil {sambA:sa est fAcil} –** sambahsa é fácil

Os próximos casos são bem interessantes:

**Id est gohd {id est go:d} – **ele/ela é bom *(neutro)*

**El est gohd {el est go:d} – **ele/ela é bom *(indeterminado)*

**ENTENDA BEM ISSO:** pronomes neutros se referem a coisas, pronomes
indeterminados são usados quando não se sabe o sexo ou não desejamos
dizê-lo. **Presta atenção:** daqui pra frente, todo pronome ou artigo
que eu *não* indicar como *neutro* ou *indeterminado*, terá o gênero
masculino ou feminino, mas posso especificar se a palavra é relacionada
a um ser masculino ou feminino se isso for realmente necessário, quando
a palavra em si não puder informar a qual gênero ela se refere. Estamos
entendidos?

Para que fique claro o que é *neutro* e o que é *indeterminado*, veja os
exemplos abaixo:

**Cadeira – **até onde eu sei cadeiras não tem sexo, portanto é *neutro*

**Cão –** se não sabemos o sexo, então é *indeterminado*

**Cadela –** feminino

**Avô –** masculino

**Árvore –** ainda que seja um ser vivo, não tem sexo *(embora você
possa argumentar sobre plantas dioicas)*, portanto é *neutro*

**Filha –** feminino

**Criança –** como o sexo não é especificado, é *indeterminado*

**Espírito – *****como em “o espírito do meu pai”***** –** neutro

**Espírito – *****como em “o espírito falou comigo”***** –** pode ser
*masculino*, *feminino* ou *indeterminado*

**Androide –** você decide, vai depender muito do contexto e pontos de
vista de cada um, pode ser *neutro*, *indeterminado* ou até mesmo ter um
sexo específico.

Em caso de dúvida se deve se a palavra deve ser considerada como
*neutra* ou *indeterminada*, utilize a forma *indeterminada*.

Nós conhecemos os pronomes básicos e seus verbos, agora vamos vê mais
pronomes e verbos no plural, vamos usar a palavra “prient” {pryent}, que
significa “amigo”, e “gohd” {go:d}, que significa “bom”, nos nossos
exemplos:

**Smos prients {smos pryents} –** somos amigos

**Wey smos prients {wey smos pryents} –** nós somos amigos

**Ste prients {ste pryents} –** sois amigos / *(vocês)* são amigos

**Yu ste prients {yu ste pryents} –** vocês são amigos

[]{#anchor-9}**Yu ste prient {yu ste pryent} –** você é amigo

**Tu es prient {tu es pryent} –** você é amigo *(informal)*

**Ies sont prients {yes sont pryents} –** eles são amigos *(todos são do
sexo masculino)*

**Ias sont prients {yas sont pryents} –** elas são amigas

**Ia sont gohd {ya sont go:d} –** eles são bons *(neutro)*

**I sont prients {i sont pryents} –** eles são amigos *(indeterminado)*

Acredito que não preciso explicar nenhuma dessas frases com exceção da
frase “Yu ste prient”, o pronome “yu” é usado tanto para “você” quanto
“vocês”. Costuma-se usar o “yu” no singular quando conversamos com uma
pessoa que não temos muita intimidade, sendo assim um pronome de
cortesia, já o “tu” usamos com amigos próximos, familiares e crianças.

[]{#anchor-10}[]{#anchor-11}[]{#anchor-12}Perceba que, mesmo no
singular, o pronome “yu” faz uso do verbo “ste”, pois o sambahsa
funciona como o russo ou o francês, o pronome de cortesia, “yu”, fica no
plural, mesmo quando se refere a apenas uma pessoa. Um exemplo do
francês: “Vous êtes un ami/des amis”; um exemplo do russo:
“[]{#anchor-13}Вы остаётесь моим(и) другом/друзями” (você(s) continua(m)
sendo meu(s) amigo(s)). Somente o contexto pode informar se o “yu” está
no singular ou plural.

Outra observação importante sobre o pronome da 2ª pessoa do plural “yu”.
Você já sabe que, na maioria dos casos, não é necessário o uso do
pronome, mas em exemplos futuros você vai perceber que a pronúncia da 3ª
pessoa do singular e 2ª pessoa do plural é bem semelhante, observe:

**Is lieubht me {is liÖb*****t***** me} –** ele me ama

**Yu lieubhte me {yu liÖb*****t***** me} –** vós amais me

Já pensou se não tivesse o pronome? Por isso que o pronome “yu” quase
sempre tem que aparecer.

É importante que você saiba também como dizer “haver” no sentido de
existir. Vou usar o substantivo “anghen” {Angën}, que significa
“pessoa”, e o advérbio “her” {her}, que significa “aqui”, no exemplo a
seguir:

**Sont anghens her {sont Angëns her} –** há pessoas aqui

**Ter sont anghens her {ter sont Angëns her} –** há pessoas aqui

Sim, você pode usar ou não o “ter” se quiser.

PALAVRAS E FRASES ÚTEIS
=======================

Não tem problema se você não souber como certas frases foram
construídas, tudo será explicado nos próximos capítulo.

**Sellamat {selamAt} –** olá, oi

**Salut {salUt} –** olá, oi

**Ya {ya} –** sim

**Si {si} – **sim *(resposta a uma questão negativa)*

**No {no} –** não

**Sell dien {sel dyen} –** bom dia

**Sell posmiddien {sel posmiddyEn} –** boa tarde

**Sell vesper {sel vEspër} –** boa noite *(ao encontrar alguém)*

**Sell noct {sel nokt} –** boa noite* (ao se despedir)*

**Dank {dank} –** obrigado

**Mersie {mërsI:} –** obrigado

**Spollay dank {spolAy dank} –** muito obrigado

**Obligat {obligAt} – **de nada, não diga isso

**Plais {pläs} –** por favor

**Kam leitte yu? {kam leyt yu} –** como você está?

**Sellgumt {selgUmt} –** bem-vindo(a)

**Leito {lEyto} –** estou bem

**Chao {tcAo} –** Tchau, adeus

**Khuda hafiz {qUda hAfidz} –** Tchau, adeus

**Do reviden {do rëvIdën} –** Tchau, adeus

**Prosit {prosIt} –** Tchau, adeus *(desejando boa sorte)*

**Tiel mox {tyel moks} – **até mais, até mais tarde, até breve

**Ne gnohm {në nyo:m} –** não sei

**Excuset me {ekskÜzd me} –** com licença

**Maaf {maAf} –** desculpe-me

**Pardon {pardOn} –** perdoe-me

**OK(ey) {ok(Ey)} –** OK

**Tamam {tamAm} – **OK

**Tabrick {tabrIk} –** parabéns

**Sell appetite {sel apëtIt} –** bom apetite

**Marba {mArba} –** foi um prazer te conhecer

**Ne bahm maung Sambahsa {ne ba:m mAong sambA:sa} –** não falo sambahsa
muito bem

**Kam yarat ste yu? {kam yarAt ste yu} –** quantos anos você tem?

**Quod est vies nam? {kwod est vyes nam} –** qual é o seu nome?

**Mien nam est … {myen nam est} –** meu nome é

**Quetos yu? {kEtos yu} –** de onde você é?

**Io ne prete {yo ne prEt} –** não entendo *(o que você quer dizer)*

**Maghte yu hehlpe me? {magt yu he:lp me} –** você pode me ajudar?

**Ye quod saat? {ye kwod saAt} –** que horas são?

**Ne ho pretet hol / Ne ho preten hol {ne ho prEtët hol / ne ho prEtën
hol} –** não entendi nada *(do que você disse)*

**Aun sibia {Aon sIbja} –** mudando de assunto

Pronto, pelo menos agora você consegue iniciar e finalizar uma conversa,
continue estudando e você poderá manter uma conversa.

OS CASOS DE DECLINAÇÃO
======================

O sambahsa tem uma característica interessante, ele tem um sistema de
casos de declinação que são 4: *nominativo*, *acusativo*, *dativo* e
*genitivo*. Esses casos se referem aos pronomes e aos artigos. Mas o que
são eles e como eles são usados?

Antes vamos relembrar um pouco das aulas de português, observe a frase
abaixo:

**O homem compra o carro: **Nesse caso “o homem” é o *sujeito* da oração
porque é ele quem está *realizando* a ação, que no caso é “compra”; e “o
carro” é o *objeto direto* da oração, porque está *sofrendo* a ação,
está sendo afetado *diretamente* pelo verbo. Observe as frases abaixo:

**Eu como tomate –** “eu” (sujeito), “tomate” (objeto direto)

**Ele ama Maria –** “ele” (sujeito), “Maria” (objeto direto)

**Pessoas andam na rua –** “pessoas” (sujeito). Opa, essa frase não tem
objeto direto!

**Eles deram-me um recado:** temos mais um elemento nessa frase, o
objeto indireto. Podemos reconhecer facilmente o sujeito, que no caso é
“eles”, mas quem é o objeto direto e quem é o objeto indireto?
Lembre-se, o objeto direto é quem sofre a ação; quem está sendo dado, a
pessoa ou o recado? O recado! O objeto direto é “um recado”. O objeto
indireto é quem se *beneficia* da ação, quem está sendo afetado
*indiretamente* pelo verbo, que no caso é “-me”. Veja as frases abaixo:

**Enviei-lhe os documentos –** “lhe” (objeto indireto), “os documentos”
(objeto direto)

**Ela comprou um cachorro para mim –** Ela (sujeito), “um cachorro”
(objeto direto), “para mim” (objeto indireto)

Agora podemos ver os casos. Quero avisar antes que só vou me limitar a
mostrar os casos por enquanto, apenas para que você os conheça de vista,
eu só vou mostrar aplicações em frases depois que eu ensinar os verbos,
que será quando nós nos sentiremos mais confortáveis para construir
frases.

CASO NOMINATIVO
---------------

Basicamente é o sujeito de uma oração.

**Ego / io {Ego / yo} –** eu

**tu {tu} –** tu

**Is {is} –** ele

**Ia {ya} – **ela

**Id {id} –** ele / ela *(neutro)*

**El {el} –** ele / ela *(indeterminado)*

**Wey {wey} –** nós

**Yu {yu} –** vós

**Ies {yes} –** eles

**Ias {yas}–** elas

**Ia {ya} –** eles / elas *(neutro)*

**I {i} –** eles / elas *(indeterminado)*

ATENÇÃO AQUI!!!!!!!! Dos de baixo, preocupe-se apenas em aprender
primeiramente os que estão sublinhados. Os outros você aprenderá com o
tempo, não precisa ter pressa!!!!!!

**So {so} –** esse, este

**Toy {toy} –** esses, estes

**Sa {sa} –** essa, esta

**Tas {tas} –** essas, estas

**Tod {tod} –** esse, este *(neutro)*

**Ta {ta} –** esses, estes *(neutro)*

**Tel {tel} –** esse, este *(indeterminado)*

**Ti {Ti} –** esses, estes* (indeterminado)*

**Cis {tsis} –** aquele

**Cies {tsyes} –** aqueles

**Cia {tsya} –** aquela

**Cias {tsyas} –** aquelas

**Cid {tsid} –** aquilo, aquele, aquela *(neutro)*

**Cia {tsya} –** aqueles, aquelas *(neutro)*

**Cel {tsel} –** aquele *(indeterminado)*

**Ci {tsi} –** aqueles *(indeterminado)*

**Qui {ki} –** que *(pronome relativo)*, quem, o qual

**Quis {kis} –** que *(pronome interrogativo)*

**Quoy {kwoy} –** os quais

**Qua {kwa} –** que, quem, a qual

**Quas {kwas} –** as quais

**Quod {kwod} –** que, o qual *(neutro)*

**Qua –** quais, os quais *(neutro)*

**Quel {kel} –** que, quem, o qual, a qual *(indeterminado)*

**Qui {ki} –** os quais *(indeterminado)*

**Neis {neys} –** nenhum

**Noy {noy} –** *plural de neis*

**Nia {nya} –** nenhuma

**Nias {nyas} –** *plural de nia*

**Neid {neyd} –** nada, nenhuma coisa *(neutro)*

**Nia {nya} –** *plural de neid (neutro)*

**Nel {nel} –** ninguém *(indeterminado)*

**Nei {ney} –** *plural de nel (indeterminado)*

**To {to} –** *pronome genérico, quando o sentido de aproximação não se
aplica, pode ser traduzido como “isto” e “isso”.*

**Quo {kwo} –** a mesma coisa que “to”, mas este já pode ser traduzido
para “que”.

Por favor, veja o subcapítulo “Como funcionam pronomes genéricos como
‘to’ e ‘quo’” do capítulo “Erros comuns e questões”.

O pronome “ego” é a versão mais enfatizada de “io”. Quando usamos o
“ego” em vez do “io”, queremos dizer que há uma certa relevância no “eu”
que está fazendo ou sofrendo essa ou aquela ação.

Vou pedir a você que você dê uma pausa aqui e estude bem os pronomes
(apenas os pessoais do caso reto!), porque vou usá-los bastante daqui
pra frente.

Quanto aos demais casos, basta lê-los uma ou duas vezes no máximo, não
precisa gravar tudo agora! Siga o mesmo método de estudo que você fez
com o caso nominativo, foque mais nos pronomes pessoais e no que eu
indicar dos outros pronomes.

CASO ACUSATIVO
--------------

Basicamente é o objeto direto da oração.

**Caso nominativo –** Caso acusativo

**Ego / io – **me {me}

**Tu **– te {te}

**Is – **iom {yom}

**Ia –** iam {yam}

**Id –** id {id}

**El –** el {el}

**Wey –** nos {nos}

**Yu –** vos {vos}

**Ies –** iens {yens}

**Ias – **ians {yans}

**Ia –** ia {ya}

**I –** i {i}

Não precisa ter pressa em aprender os de baixo!

**So –** tom {tom}

**Toy –** tens {tens}

**Sa –** tam {tam}

**Tas –** tans {tans}

**Tod –** tod

**Ta –** ta

**Tel –** tel

**Ti –** ti

**Cis–** ciom {tsyom}

**Cies –** ciens {tsyens}

**Cia –** ciam {tsyam}

**Cias –** cians {tsyans}

**Cid –** cid

**Cia–** cia

**Cel –** cel

**Ci –** ci

**Qui –** quom {kwom}

**Quis –** quom

**Quoy –** quens {kens}

**Qua –** quam {kwam}

**Quas –** quans {kwans}

**Quod –** quod

**Qua –** qua

**Quel –** quel

**Qui –** qui

**Neis – **niom {nyom}

**Noy** – niens {nyens}

**Nia –** niam {nyam}

**Nias –** nians {nyans}

**Neid** – neid {neyd}

**Nia –** nia {nya}

**Nel –** nel {nel}

**Nei –** nei {ney}

**To – **to

**Quo –** quo

CASO DATIVO
-----------

É o objeto indireto da oração. **Atenção:** normalmente, tudo que
estiver depois de uma preposição será tratado como se estivesse no caso
acusativo, não dativo!

**Caso nominativo –** Caso dativo

**Ego/io – **Mi {mi}

**Tu **– Tib {tib}

**Is –** Ei {ey}

**Ia –** Ay {ay}

**Id –** ei {ey}

**El –** al {al}

**Wey –** nos {nos}

**Yu –** vos {vos}

**Ies –** ibs {ibz}

**Ias – **iabs {yabz}

**Ia –** ibs {ibz}

**I –** im {im}

Não precisa ter pressa em aprender os de baixo!

**So – **tei {tey}

**Toy –** tibs {tibz}

**Sa –** tay {tay}

**Tas –** tabs {tabz}

**Tod –** tei {tey}

**Ta –** tibs {tibz}

**Tel –** tal {tal}

**Ti –** tim {tim}

**Cis–** cei {tsey}

**Cies –** cibs {tsibz}

**Cia –** ciay {tsyay}

**Cias –** ciabs {tsyabz}

**Cid –** cei {tsey}

**Cia–** cibs {tsibz}

**Cel –** cial {tsyal}

**Ci –** cim {tsim}

**Qui –** quei {key}

**Quis –** quei

**Quoy –** quibs {kibz}

**Qua –** quay {kway}

**Quas –** quabs {kwabz}

**Quod –** quei

**Qua –** quibs

**Quel –** qual {kwal}

**Qui –** quim {kim}

**Neis – **nei {ney}

**Noy** – neibs {neybz}

**Nia –** niay {nyay}

**Nias –** niabs {nyabz}

**Neid** – nei {ney}

**Nia –** neibs {neybz}

**Nel –** nal {nal}

**Nei –** nim {nim}

**To – **ad to

**Quo –** ad quo

Caso você esqueça a forma dativa do pronome, você pode usar a preposição
“ad”.

**mi =** ad me

**tib =** ad te

**tal =** ad tel

**quei =** ad quod

CASO GENITIVO
-------------

É o caso do possuidor de alguma coisa. Primeiro vamos ver a lista de
genitivos e então vamos entendê-los melhor.

**Caso nominativo –** Caso genitivo

**Is –** ios {yos}

**Ia –** ias {yas}

**Id –** ios {yos}

**El –** al {al}

**Ies –** iom {yom}

**Ias –** iam {yam}

**Ia –** iom {yom}

**I –** im {im}

Não precisa ter pressa em aprender os de baixo!

**So – **tos {tos}

**Toy –** tom {tom}

**Sa –** tas {tas}

**Tas –** tam {tam}

**Tod –** tos

**Ta –** tom

**Tel –** tal {tal}

**Ti –** tim {tim}

**Cis–** cios {tsyos}

**Cies –** ciom {tsyom}

**Cia –** cias {tsyas}

**Cias –** ciam {tsyam}

**Cid –** cios

**Cia–** ciom

**Cel –** cial {tsyal}

**Ci –** cim {tsim}

**Qui –** quos {kwos}

**Quis –** quos

**Quoy –** quom {kwom}

**Qua –** quas {kwas}

**Quas –** quam {kwam}

**Quod –** quos

**Qua –** quom

**Quel –** qual {kwal}

**Qui –** quim {kim}

**Neis – **nios {nyos}

**Noy** – niom {nyom}

**Nia –** nias {nyas}

**Nias –** niam {nyam}

**Neid** – nios {nyos}

**Nia –** niom {nyom}

**Nel –** nal {nal}

**Nei –** nim {nim}

**To –** os to

**Quo –** os quo

Você me pergunta: *posso substituir, por exemplo, o “ios” por “os is”, o
“ias” por “as ia”, o “tal” por “os tel”, “quos” por “os quod” e assim
por diante?* É preferível que você não faça isso.

Vejamos alguns exemplos:

**Id apel ios dru {id Apël yos dru} –** a maçã da árvore

**Id hand al person {id hand al përsOn} –** a mão da pessoa

**Id augos cios wir – {id Aogos tsyos wir}** **–** o poder daquele homem

**Ia quitances tos munt {ya kitAntsës tos munt} –** as contas deste mês

DECLINAÇÕES
===========

Usadas como um complemento em diversas palavras.

SINGULAR
--------

**Masculino nominativo –** -o(s)

**Masculino acusativo –** -o / -um

**Masculino dativo –** -i

**Masculino genitivo –** -(io)s

**Feminino nominativo –** -a

**Feminino acusativo –** -u

**Feminino dativo –** -i

**Feminino genitivo –** -(ia)s

**Neutro nominativo –** -o / -um

**Neutro acusativo –** -o / -um

**Neutro dativo –** -i

**Neutro genitivo –** -(io)s

**Indeterminado nominativo –** -is

**Indeterminado acusativo –** -em

**Indeterminado dativo –** -i

**Indeterminado genitivo –** -(e)s

PLURAL
------

**Masculino nominativo –** -i

**Masculino acusativo –** -ens

**Masculino dativo –** -ims

**Masculino genitivo –** -(e)n

**Feminino nominativo –** -as

**Feminino acusativo –** -ens

**Feminino dativo –** -ims

**Feminino genitivo –** -(e)n

**Neutro nominativo –** -a

**Neutro acusativo –** -a

**Neutro dativo –** -ims

**Neutro genitivo –** -(e)n

**Indeterminado nominativo –** -i

**Indeterminado acusativo –** -ens

**Indeterminado dativo –** -ims

**Indeterminado genitivo –** -(e)n

Mas e então, como usamos as declinações? As vezes o seu uso é
obrigatório, como com as palavras “vasyo”(tudo) e “alyo”(um outro):

**Vasyas gwens –** todas as mulheres

**Alyo wir –** um outro homem

**Vasyi paters –** todos os pais

**Alyo stul –** uma outra cadeira

**Alya stuls –** umas outras cadeiras

**Vasyo est correcto –** tudo está correto

Para efeitos de eufonia ou propostas literárias, como poesia, você pode
usar essas declinações em outras palavras, como por exemplo o artigo
indefinido “un”, mas geralmente aplica-se essas declinações apenas às
palavras “vasyo” e “alyo”. Lembre-se que essas declinações eufônicas
podem ser usadas apenas se elas forem compatíveis com a acentuação
tônica da palavra.

A frase “un bell pwarn” {un bel pwarn}, que significa “um belo garoto”,
pode ser escrita como “uno bello pwarn” {Uno bElo pwarn}.

O plural de “bell plaj” pode ser escrito como “bella plaja”

Lembre-se que há também o caso genitivo, que representa a posse. Embora
não seja o modo mais simples, você pode dizer:

**Livro de Henrique –** Henriques buk

**Casa de Matheus –** Matheus*io*s dom *(perceba que usei a terminação
completa porque “Matheus” já termina com “s”)*

**Filho da mulher –** gwens son\
**Beleza do Brasil –** Brazilus beauteit

O uso da declinação no acusativo não tem muito mistério, você só a
aplica em palavras que estiverem na posição de objeto direto. Sobre a
declinação no genitivo, a ensinarei depois que você aprender os verbos,
acho melhor assim.

OS ARTIGOS
==========

ARTIGOS DEFINIDOS
-----------------

Uma característica do sambahsa que salta aos olhos são seus artigos
definidos, as palavras são as mesmas dos pronomes. Como assim? Veja as
traduções abaixo:

**O homem –** is wir {is wir}

**A mulher –** ia gwen {ya gwen}

**O cachorro –** el kwaun {el kwAon}

**A pessoa –** el anghen {el Angën} *ou* el person {el përsOn}

**A pessoa –** is anghen *(pessoa do sexo masculino)*

**O país –** id land {id land}

**O povo *****(de uma determinada cultura)***** – **id folk {id folk}

**O povo *****(de classe social baixa)***** –** id popule {id pOpül}

Percebeu como funciona a língua? Na lógica do sambahsa você não fala “a
secretária” ou “o homem”, mas “ela secretária” e “ele homem”. Tenha em
mente que mesmo esses artigos sofrem declinação assim como os pronomes.

***O***** cachorro ama *****a***** mulher – ***el* kwaun lieubht *iam*
gwen {el kwAon liöbt yam gwen}

Você não poderia traduzir “o cachorro ama a mulher” como “el kwaun
lieubht *ia* gwen” porque o artigo definido que se relaciona ao
substantivo “mulher” tem que sofrer a declinação para o acusativo.

Outro exemplo, mas também envolvendo o caso dativo:

***A***** gerente enviou *****o***** e-mail *****ao***** presidente –**
*ia* manager yisit *id* blixbrev *ei* president.

ARTIGO INDEFINIDO
-----------------

O artigo indefinido é “un”, parecido com o português.

**Uma maçã –** un apel {un Apël}

**Um muro –** un mur {un mur}

**Um garoto –** un pwarn {un pwarn}

**Uma garota –** un bent {un bent}

Não existe exatamente uma tradução direta de “uns”/“umas”, para isso
você usa a tradução de “algum”, que é “sem”, como em “sem apels”.

Como esse “sem” do sambahsa é um adjetivo, ele permanece dessa maneira
tanto no singular quanto no plural, observe:

**Sem apel {sem Apël} –** alguma maçã

**Sem apels {sem Apëls} –** algumas maçãs

Mas se esse “sem” estiver como substantivo, aí sim ele poderá receber
alguma terminação, logo a seguir está uma frase complexa, é claro que
você não precisa entendê-la agora, só quero que você veja como o “sem”
pode se comportar como substantivo.

**Sems credeihnt est neid global warmen –** alguns acreditam não haver
nenhum aquecimento global

COMO DIZER AS CONJUNÇÕES “E” E “OU”
===================================

Apenas quero me adiantar nessas duas conjunções básicas, porque até
chegarmos no capítulo de conjunções talvez já tenhamos visto alguns
exemplos com elas.

A conjunção “e” é traduzida como “ed” para o sambahsa.

**Ele e eu – **is ed io

Com “ou” você precisa prestar um pouquinho mais de atenção. Geralmente
você usará “au”:

**Ele ou eu – **is au io {is Ao yo}

Mas quando tratamos de duas orações, usamos o “we”. Vou usar uma frase
complexa, mas preste atenção apenas em como a conjunção é usada.

Minha crianças, você tem que escolher: brincar no parque *ou* nadar na
piscina

Mien purt, dehlcs chuses: likes in id park *we* snahe in id piscine

**POSSE** 
==========

Eis as preposições de posse:

**Masculino no singular –** os

**Masculino no plural –** om

**Feminino no singular –** as

**Feminino no plural –** am

**Neutro no singular –** os

**Neutro no plural –** om

**Indeterminado no singular –** es

**Indeterminado no plural –** em

Veja alguns exemplos:

**A morte de Louis –** id mohrt os Louis {id mo:rt os lU:is}

**A casa de Paulo e Peter –** id dom om Paulo ed Peter {id dom om pAolo
ed pEtër}

**O filho de Júlia –** is son as Julia {is son as jUlya}

**A boneca de Lara e Carla –** id pupp am Lara ed Carla {id pup am lAra
ed kArla}

Quando diversos elementos são donos de algo, como visto na última frase,
você também pode usar a palavra “sbei” {sbey}, assim “id dom om Paulo ed
Peter” e “id pupp am Lara ed Carla” podem ser reescritos como:

“Paulo ed Peter sbei dom”

“Lara ed Carla sbei pupp”

Você também pode fazer uso das declinações em conjunto com, logo a
seguir as frases “id mohrt os Louis” e “id dom om Paulo ed Peter”
reescritas de outra maneira.

Outra possibilidade é o uso de declinações:

**A morte de Louis –** Louisios mohrt {id luIzyos mo:rt}

**A casa de Paulo e Peter –** Paulos ed Peters dom {id pAolos ed pEtërs
dom}

Não poderíamos dizer “Paulo*io*s ed Peter*io*s dom” porque essa maneira
mudaria a acentuação tônica das palavras, ficaria {paolOyos} e
{petEryos}.

Você não precisa o artigo definido como “*id* Louisios mohrt” ou “*id*
Paulos ed Peters dom” porque os substantivos já são ‘definidos’ pelo
genitivo.

Eu lhe mostrei esses dois últimos exemplos com declinação porque tenho a
obrigação de ensinar tudo, mas seria melhor você preferir “id mohrt os
Louis” e “id dom om Paulo ed Peter”/“Paulo ed Peter sbei dom” por serem
mais simples. A pronúncia de “Louisios”{luIzyos}*(ou {luIyos} se formos
seguir a pronúncia francesa do nome “Louis”)* é simplesmente bizarra,
embora, gramaticalmente falando, possamos usar tal palavra. No outro
exemplo algumas pessoas poderiam não perceber que Paulo também é dono da
casa. Prefira o modo mais simples, deixe as declinações para momentos
mais apropriados.

DIFERENÇA DE USO DA PREPOSIÇÃO DE POSSE E CASO GENITIVO
-------------------------------------------------------

Talvez você esteja confuso e se pergunte por que o penúltimo exemplo não
foi traduzido como “id mohrt *i*os Louis”. Há uma diferença bem simples
entre a preposição de posse e o pronome no caso genitivo.

**Ios –** do *(preposição de + artigo o)*

**Os –** de

**Ias –** da *(preposição de + artigo a)*

**As – **de

**Tos –** daquele *(preposição de + pronome aquele)*

Perceba que, nas traduções do caso genitivo, usamos o artigo definido
junto com a preposição, enquanto a preposição de posse é traduzida
apenas para uma preposição. Vou lhe mostrar algumas frases inadequadas,
para que você entenda:

“Id mohrt ios Louis” seria traduzido como “a morte do Louis” e “Id apel
os dru” seria traduzida como “a maçã de árvore”. Conseguiu entender?

OUTROS PRONOMES DE POSSE
------------------------

**Mien {myen} –** meu

**Tien {tyen} –** teu

**Eys {eys} –** dele

**Ays {ays} –** dela

**Ids {Idz} –** dele, dela *(neutro)*

**Els {elz} –** dele, dela *(indeterminado)*

**Nies {nyes} –** nosso

**Noster {nOstër} –** nosso

**Vies {vyes} –** vosso

**Voster {vOstër} –** vosso

**Ir {ir} –** deles, delas *(masculino, feminino, neutro e
indeterminado)*

**Sien {syen} –** seu, sua. *Usado quando se refere ao sujeito da
frase.*

**Uns {uns} – **de um, de uns, de uma, de umas

**Minha casa –** mien dom {myen dom}

**A cidade dele –** eys urb {eys urb}

**Nosso mundo –** nies mund {nyes mund} / noster mund {nOstër mund}

**O país de vocês *****(ou “vosso país”)***** –** vies land {vyes land}
/ voster land {vOstër land}

“*Nies” e “noster”, assim como “vies” e “voster”, significam a mesma
coisa. *

*Eu vou ensinar como usar o pronome “sien” depois de eu ensinar os
verbos.*

Podemos combinar esses pronomes com as declinações para traduzir aquelas
situações onde usamos a preposição “de” com alguns pronomes possessivos.

**… dos meus homens –** … mienen wirs {miEnën wirs}

**… do seu filho –** … tien(io)s son {tiEn(yo)s son}

**… de nossas filhas –** … niesen dugters {nyEzën dUgtërs}

**… de nossa filha –** … nies(ia)s dugter {nyEz(ya)s dUgtër}

**Exemplo:** a casa do seu filho → id dom tiens son

É claro que você pode dizer algo como “id dom os tien son” ou “id pupp
am nies dugters”

NOMES DE LOCAIS COM CATEGORIA
=============================

Quando falamos de nomes de lugares que incluem sua categoria, como
“cidade de São Paulo” ou “Monte Everest”, normalmente decidimos a
posição dos nomes das categorias através do uso das línguas nacionais,
como por exemplo.

**Cidade de São Paulo –** citad São Paulo

**Monte Everest –** Mont Everest

**Rochedo de Gibraltar –** Perwnt Gibraltar

**Rochedo Casterly –** Casterly Rock *(porque o original em inglês é
“Casterly Rock”)*

**Rochedos de Liancourt –** Perwnts ios Liancourt *(este é interessante
porque, embora seus nomes origianis sejam “Dokdo” e “Takeshima”, no
Ocidente tem um nome diferente que se deve ao a um navio francês que
naufragou naquela região em 1849)*

Veja o exemplo abaixo:

**Lieubho brigvs, in mien safers ho kohgnet[^1] maung brigvs: Brigv JK,
Millennium Brigv, Brigv Alexandre III, Charles/Karluv Brigv, Kintai
Brigv ed Brigv os Rialto –** Amo pontes, em minhas viagens eu conheci
muitas pontes: Ponte JK, Ponte do Milênio, Ponte Alexandre III, Ponte
Carlos, Ponte Kintai e a Ponte de Rialto.

**Brigv JK =** Ponte Juscelino Kubitschek

**Millennium Brigv =** Millennium Bridge

**Brigv Alexandre III =** Pont Alexandre-III

**Charles/Karluv Brigv =** Karlův most

**Kintai Brigv =** 錦帯橋 Kintai-kyō

**Brigv os Rialto =** Ponte di Rialto

Em Sambahsa não se usa o genitivo quando o nome se refere ao local em si
porque isso seria uma apelação, não falamos “id citad os Montréal” mas
sim “id citad Montréal”, mas devemos falar “id tribunal os Montréal”
porque Montreal não é um tribunal. Outro exemplo é a tradução de
“Palácio de Versalhes” *(“Château de Versailles” no original)*, que é
“Chateau os Versailles” em sambahsa.

ADJETIVOS
=========

Um adjetivo é uma palavra que qualifica um substantivo, como em “a casa
bonita”, onde o adjetivo “bonita” qualifica o substantivo “casa”. Hmm,
acho que vou aproveitar o momento para explicar também o que é um
advérbio: advérbio qualifica adjetivos e verbos, como “muito rápido”, em
que o advérbio “muito” qualifica o adjetivo “rápido”. Estamos
entendidos?

A construção dos adjetivos é semelhante ao inglês, basicamente basta
colocar o adjetivo antes do substantivo. Vamos conhecer alguns adjetivos
e substantivos que usaremos nos nossos exemplos:

**Veut {vöt} –** velho

**Moll {mol} –** macio

**Pell {pel} –** pelo

**Wogh {wog} –** carro

**Bell {bel} –** bonito, bonita

**Buland {bulAnd} –** alto, alta

**Amin {amIn} –** confiável *(pessoas)*

**Wassic {wAsik} –** confiável *(coisas)*

**Yun {yun} –** jovem

**Smulk {smulk} –** pequeno, pequena

**Blou {blu:} –** azul

Eis os exemplos:

**Veut wogh –** carro velho

**Moll pel –** pelo macio

Naturalmente que você pode fazer construções do tipo:

**Id wogh est veut –** o carro é velho

Diferente do português, os adjetivos não recebem a terminação do plural
quando o substantivo que eles qualificam está no plural.

**Veut woghs –** carros velhos

**Moll pels –** pelos macios

Assim como no português, você pode usar o adjetivo sozinho e fazer dele
um substantivo como no caso abaixo:

**A poderosa –** ia staur / ia staura

Perceba que você precisa do artigo e você também pode fazer uso da
declinação, quando possível.

Também é possível fazer adjetivos a partir de verbos através de
particípios, mas isso eu vou explicar apenas no capítulo de particípios.

[]{#anchor-14}[]{#anchor-15}Para criar adjetivos a partir de
substantivos pode-se usar afixos como -ic ou -eus, como nos exemplos
abaixo:

**Bois sont cruor*****ic***** **[]{#anchor-16}**{boys sont krwOrik} –**
batalhas são sangrentas

**Is est wirt*****ic***** **[]{#anchor-17}**{is est wIrtik} –** ele é
digno

**Tod place est danger*****eus***** **[]{#anchor-18}**{tod plAts est
dangërÖs} –** este lugar é perigoso

**Ia gwen est nerv*****eus***** **[]{#anchor-19}**{ya gwen est nervÖs}
–** a mulher está nervosa

Não se preocupe muito com esses afixos porque muitas palavras podem ser
utilizados como substantivo ou adjetivo sem a necessidade de alterar a
palavra, como “infinitive”, que significa “infinitivo”, em “un
infinitive verb”*(um verbo infinitivo)* ou “smad studye id
infinitive”*(vamos estudar o infinitivo)*.

Uma *observação muito importante* é que como o vocabulário do sambahsa
vem de línguas de diferentes famílias, como os derivados do latim e
línguas germânicas, as regras de criação de adjetivos a partir de
substantivos se adaptam a origem da palavra, às vezes você usa outro
afixo, ou maneira, que não é através do -ic ou -eus. Veja os exemplos
abaixo:

[]{#anchor-20}[]{#anchor-21}[]{#anchor-22}[]{#anchor-23}**Cid est un
insuleus land –** []{#anchor-24}aquele é um país insular **{errado X}**

[]{#anchor-25}**Cid est un insulic land –** []{#anchor-26}aquele é um
país insular []{#anchor-27}**{errado X}**

[]{#anchor-28}[]{#anchor-29}**Cid est un insular land –**
[]{#anchor-30}aquele é um país insular **{certo Ѵ}**

[]{#anchor-31}**Som in un problemic situation –** estou numa situação
problemática []{#anchor-32}**{errado X}**

[]{#anchor-33}[]{#anchor-34}**Som in un problematic situation –** estou
numa situação problemática []{#anchor-35}**{certo Ѵ}**

[]{#anchor-36}**Un pateric amor –** um amor paternal
[]{#anchor-37}**{errado X}**

[]{#anchor-38}**Un patruw amor –** um amor paternal *(vinculação)*
[]{#anchor-39}**{certo Ѵ}**

*A palavra românica “amor” tem um significado ancestral do proto
indo-europeu que significa “ser vinculado” ou “ser ligado a” *(cf.
Grego: omnumi = eu juro)*. Então o primeiro real significado de “amor” é
“vinculação” (“conexão emocional entre dois indivíduos”)*.

[]{#anchor-40}**Tod buit monstereus –** aquilo foi monstruoso **{errado
X}**

[]{#anchor-41}**Tod buit monstrueus –** aquilo foi monstruoso
[]{#anchor-42}**{certo Ѵ}**

[]{#anchor-43}**Es baygh bayeus –** és muito medroso(a) **{aceitável,
mas prefira o de baixo}**

[]{#anchor-44}**Es baygh bayasen –** és muito medroso(a) **{certo Ѵ}**

Às vezes pode não ser interessante utilizar um afixo para transformar um
substantivo em um adjetivo, mas uma preposição como “os” ou “es” pode
ser uma boa alternativa.

[]{#anchor-45}**Kamo anon es kwaun –** gosto de comida de cachorro

**Lieubho likes RPGs os table –** amo jogar RPGs de mesa

Não faria sentido dizer algo como []{#anchor-46}“…kwaunic anon” ou
[]{#anchor-47}“… []{#anchor-48}tableus RPG”, porque “…kwaunic anon”
daria a impressão que a comida tem alguma característica canina, apesar
de ser uma comida voltada para cães, e “… []{#anchor-49}tableus RPG”
daria a impressão que o RPG tem alguma característica de mesa, embora
seja um jogo normalmente jogado sobre uma mesa.

A forma mais interessante de expressar as ideias dos dois últimos
exemplos é:

**Kamo kwaun-anon –** gosto de comida de cachorro

**Lieubho likes table-RPGs –** amo jogar RPGs de mesa

Perceba que []{#anchor-50}o “table” de “table-RPG” e o “kwaun” de
[]{#anchor-51}“kwaun-anon” não estão como adjetivos, mas como
componentes de uma palavra feita através da aglutinação de outras
palavras. Se, por exemplo, o “table” de []{#anchor-52}“table-RPG” fosse
um adjetivo, deveria ser possível a frase “id RPG est table”, o que não
faz o menor sentido. []{#anchor-53}Ah, e você poderia escrever
[]{#anchor-54}“kwaun-anon” e “table-RPG” sem os hífens, deixando-os
assim: []{#anchor-55}“kwaun anon” e “table RPG”.

Conheça também o []{#anchor-56}adjetivo predicativo sobre o objeto,
muito utilizado em frases como nos exemplos abaixo:

[]{#anchor-57}**Ho pict mien dom do glend –** pintei minha casa de verde

**Ia me hat kalen zani –** ela me chamou de adúltero(a)

**Tod anon kwehrt/beuwt me sieug –** esta comida me deixa doente

Aproveitando a oportunidade, deixe-me ensinar-lhe como dizer “Ana chamou
João de Roberto”: *Ana kiel João (ka) “Roberto”*. É interessante que
você uso o “ka” para evitar equívocos.

Vou usar o primeiro exemplo, []{#anchor-58}“[]{#anchor-59}ho pict mien
dom do glend”, para explicar mais sobre esse tipo de situação. Não
poderíamos ter escrito “[]{#anchor-60}ho pict mien dom glend” porque
assim o “glend” estaria no acusativo e a tradução seria algo como
“pintei meu verde caseiro”, não faria sentido, e lembre-se que os
adjetivos do sambahsa vêm antes do substantivo. Também não poderíamos
escrever “[]{#anchor-61}ho pict mien glend dom”, porque a tradução seria
“pintei minha casa verde”, que, embora estaja gramaticalmente perfeita,
definitivamente passa a ideia errada.

[]{#anchor-62}[]{#anchor-63}Acho pertinente fazer algumas observações
sobre esses últimos exemplos, eles são bem interessantes.

Perceba que, para construir o []{#anchor-64}[]{#anchor-65}adjetivo
predicativo sobre o objeto no primeiro exemplo foi feito uso da
preposição “do”, que você vai aprender com mais detalhes em capítulos
futuros, mas você pode reescrever essas frases de outra maneira, como
por exemplo: “Ho pict(o) glend(o) mien dom”, assim o “glend” não é
tomado para ser o adjetivo atributivo de “dom”.

[]{#anchor-66}No segundo exemplo eu poderia fazer da palavra “zani” um
substantivo ao adicionar um artigo indefinido antes dessa palavra: “Ia
me hat kalen *un* zani”; o significado praticamente ainda é o mesmo. Mas
nesse caso não seria mais um adjetivo predicativo sobre o objeto, mas
sim um *nominal* predicativo sobre o objeto.

[]{#anchor-67}[]{#anchor-68}[]{#anchor-69}Há uma forma melhor de
escrever a terceira frase, seria fazendo uso do sufixo -eih, que é um
factivo, você aprenderá mais sobre afixos em um capítulo específico. A
frase ficaria assim: “Tod anon sieugeiht me” *(esta comida me adoece)*.
Não o usei nos exemplos porque aí já não mais seria um adjetivo
predicativo sobre o objeto.

COMPARAÇÕES
-----------

O comparativo é feito sufixando o -er ao adjetivo (ou -ter caso a
palavra termine com uma vogal), mas só use essa terminação caso a
acentuação tônica da palavra não seja alterada, caso contrário utilize a
palavra “meis”, que significa “mais”; “quem” significa “que” no contexto
de uma comparação.

**Ele é mais velho do que eu –** is est veuter quem io {is est vÖtër kem
yo}

**Ele é mais alto do que eu –** is est meis buland quem io {is est meys
bulAnd kem yo}

Comparações de igualdade são feitas com “tem … quem”, que pode ser
traduzido como “tão … quanto”.

**Maria é tão bonita quanto Eliza – **Maria est tem bell quem Eliza

Comparações de inferioridade são feitas com “minter … quem”, que pode
ser traduzido como “menos … que”.

**Lobos são menos confiáveis do que cachorros –** wolfs sont minter amin
quem kwauns {wolfs sont mIntër amIn kem kwAons}

[]{#anchor-70}Uma observação importante sobre o adjetivo “amin” é que na
verdade ele é para humanos, mas o outro adjetivo para confiável do
sambahsa, que é “wassic”, é voltado para coisas. Qual usar? Você
escolhe, eu preferi “amin”, mas não seria errado usar “wassic”, isso vai
da visão de mundo da pessoa.

“Meis” e “minter” *(às vezes “mins” em vez de “minter”, mas isso é
raro)* são palavras comparativas, agora vamos falar sobre os
superlativos. Mas o que são superlativos? Veja os dois exemplos abaixo:

Carlos é mais alto do que Pedro

Mateus é o mais alto

Carlos é maior do que Pedro, mas isso não significa necessariamente que
não existam pessoas mais altas do que Carlos, o “mais” nesse caso só foi
usado para fazer uma comparação entre dois seres. Já a segunda frase diz
que Mateus é mais alto do que todo mundo no contexto onde ele se
encontra, o “mais” nesse caso é um superlativo. Naturalmente que o mesmo
raciocínio pode se aplicar para comparações de inferioridade:

Carlos é menos alto do que Pedro

Mateus é o menos alto

Mas diferente do português, o sambahsa não usa as mesmas palavras para o
comparativo e o superlativo, usa-se nesse caso o “meist” *(ou o sufixo
-st se possível)* e “minst”, veja os exemplos abaixo:

**Mateus é o mais alto –** Mateus est is meist buland

**Mateus é o menos alto –** Mateus est is minst buland

**Sou o mais novo em minha casa –** som is yunst in mien dom

**Lara é a mulher mais confiável –** Lara est ia aminst gwen

Os adjetivos “megil” {mEdjil} (grande) e “lytil” {lÜtil} (pequeno) são
os únicos que possuem formas comparativas e superlativas irregulares,
elas são:

**Comparativo:** “meger” {mEdjër} e “lyter” {lÜtër}

**Superlativo:** “megst” {megst} e “lytst” {lütst}

PLURAL
======

O plural é fácil e intuitivo, geralmente as palavras no plural terão um
-s adicionado no final, como se faz no português, essa é basicamente a
forma padrão.

**Prince {prints} *****(príncipe)***** –** princes {prIntsës}
*(príncipes)*

**Div {div} *****(deus) *****–** divs {divs} *(deuses)*

**Kwaun {kwAon} *****(cão)***** –** kwauns {kwAons} *(cães)*

**Land {land} *****(país) *****–** lands {landz} *(países)*

Palavras terminadas em -um tem seu final substituído por -a.

**Territorium {te(r)ritOryum} *****(território)***** –** territoria
{te(r)ritOrya} *(territórios)*

**Collegium {kolEdjyum} *****(faculdade, universidade)***** –** collegia
{kol(l)Edjya} *(faculdades, universidades)*

Para palavras terminadas em -es ou -os, mudam-se esses finais
respectivamente para -si e -sa.

**Daumos {dAomos} *****(maravilha)***** –** daumsa {dAomsa}
*(maravilhas)*

**Elkos {Elkos} *****(úlcera)***** –** elksa (Elksa) *(úlceras)*

**Bes {bes} *****(chefe)***** –** besi {bEzi} *(chefes)*

**Kames {kams} *****(magia)***** –** kamsa {kamsa} *(magias)*

Haverá situações que não será possível adicionar o -s no final por
questões de pronuncia, nesses casos adicionam-se os finais -i *(para
seres animados)* ou -a *(para seres inanimados)*.

**Magv {magv} *****(criança)***** – **magvi {mAgvi} *(crianças)*

**Kwax {kwaks} *****(coaxo)***** –** kwaxa {kwAksa} *(coaxos)*

**Urx {urks} *****(urso) *****–** urxi {Urksi} *(ursos)*

**Rawaj {rawAj} *****(unidade monetária)***** –** rawaja {rawAja}
*(unidades monetárias)*

**Aux {Aoks} (*****boi) *****–** auxi {Aoksi} *(bois)*

**Kwas {kwas} *****(tosse)***** –** kwasa {kwAza} *(tosses)*

**Musch {muc} *****(mosca)***** –** muschi {mUci} *(moscas)*

*A única exceção é que “ok” (olho), cujo pural pode ser “oks” {oks} ou
“okwi” {okwi}.*

Alguns exemplos com artigos:

**Os homens –** ies wirs {yes wirs}

**As mulheres –** ias gwens {yas gwens}

**Os carros –** ia woghs {ya wogz}

**As pessoas –** i anghens {i Angëns}

**Os cachorros –** i kwauns {i kwAons}

**As pessoas –** i leuds {i lödz}

Por favor, observe que mesmo os artigos são modificados, enquanto “os
homens” é “ies wirs”, “o homem” é “is wir”.

Para finalizar, um exemplo de plural com declinação:

**Plur millions brasileiren –** Vários milhões de brasileiros

Perceba que foi escrito “brasileiren” em vez de “brasileirs”, mas eu
poderia usar qualquer um dos dois sem problemas, mas se for usado o
“brasilers”, não se esqueça de usar o “em”, ficando “Plur millions em
brasileirs vahnt…”.

ALGUNS, POUCOS, MUITOS E OUTROS
===============================

**Nada mais –** neideti {neydEti}

**Nada mesmo, nada demais –** khich {qitc}

**Ninguém –** neanghen {neAngën}, nimen {nImën}

**Algum, alguns, alguma, algumas –** sem {sem}

**Alguém – **semanghen {semAngën}

**Algo, alguma coisa –** semject {semjekt}

**Muito, muitos, muitas *****(adjetivo ou advérbio)***** –** baygh
{bayg}

**Muito, muitos, muitas *****(adjetivo coloquial) *****–** maung {mAong}

**Muito, muitos, muitas *****(adjetivo literal)***** –** pelu {pElu}

**Muito, bastante *****(advérbio) *****–** meg {meg}

**Muitos, muitas *****(adjetivo)***** –** mult {mult}

**Demais, excessivamente *****(advérbio ou adjetivo)***** –** pior
{pyor}

**Relativamente, relativamente muito *****(advérbio)***** – **destull
{dëstUl}

**Diversos, diversas –** plur {plur}

**Mais *****(advérbio ou adjetivo) (comparativo)***** –** meis {meys}

**Menos *****(advérbio ou adjetivo)***** *****(comparativo)***** –**
minter {mIntër}

**Mais *****(advérbio ou adjetivo) (superlativo)***** –** meist {meyst}

**Menos** ***(advérbio)***** *****(superlativo)***** **– minst {minst}

**Tanto – **tant {tant}

**Cada**[]{#anchor-71}** *****(mais de dois)***** –** ielg {yElg}

**Cada**[]{#anchor-72}** *****(de dois)*****, um ou outro –** Ieter
{yEtër}

**Todos os / todos eles –** vasyi {vAzyi} *(masculino & plural
nominativo indeterminado)*

**Todas as / todas elas –** vasyas {vAzyas} *(plural nominativo
feminino)*

**Todos os / eles todos –** vasya {vAzya} *(nominativo plural nominativo
& acusativo)*

**Não muito de –** oik {oyk}

**Pouco, pouca *****(advérbio)***** –** pau {pAo}

**Pouco, pouca, poucos, poucas *****(adjetivo)***** –** pauk {pAok}

**Um pouco de *****(advérbio ou adjetivo)***** –** lyt {lüt}

**Pequeno, pequena –** smulk {smulk}

**Grande –** large {lardj}

**Meio, semi- – **pwol {pwol}

**Um e meio –** pwolter {pwOltër}

**O outro, a outra –** alter {Altër} *(não é necessário o artigo)*

**Um outro –** alyo {Alyo} *(não é necessário o artigo)*

**Outrem –** alyanghen {alyAngën}

**Ambos –** bo

**Os dois –** amb(o)

**Ou… ou… –** auter… au… {Aotër… Ao…}

**Se *****(diante da escolha entre um membro de um par)***** –** kweter
{kwEtër}

**Nem… e nem… –** neter… ni… {nEtër… ni…}

**Nenhum *****(dos dois)***** –** neuter {nÖtër}

**Um dos dois –** oiter {Oytër}

**Qual de ambos? –** quoter? {kwOtër}

**Um *****(artigo indefinido se referindo a um membro de um par)*****
–** uter {Utër}

**Pouco a pouco –** lyt ed lyt {lüt ed lüt}

**Alguns dias –** sem diens {sem dyens}

**Poucas horas –** pauk hors {pAok hors}

**Ela é um pouco pesada –** Ia est lyt gwaur {ya est lüt gwAur}

**O outro livro –** alter buk {Altër buk}

**Um outro livro –** alyo buk {Alyo buk}

UM POUCO CONFUSO COM TANTOS “MUITOS” e “POUCOS”?
------------------------------------------------

Você tem que observar quem é adjetivo, quem é advérbio e quem é ambas as
coisas, isso é muito importante! Vamos ver as implicações disso:

“Baygh” não pode ser usado antes de um adjetivo seguido de um
substantivo, uma frase como “baygh smulk magvi” significaria o quê?
“Muitas crianças pequenas” ou “crianças muito pequenas”? Lembre-se que
“baygh” pode servir tanto como adjetivo quanto advérbio, fica a dúvida
sobre qual palavra ele está qualificando. Nesses casos o ideal seria
utilizar outra palavra mais específica, uma que seja somente adjetivo ou
somente advérbio. *O “baygh” existe para evitarmos a repetição do
“maung” em um texto.*

Veja só: “maung belli leuds” {mAong bEli lödz}, essa frase eu sei que
significa “muitas pessoas bonitas” e *não* “pessoas muito bonitas”,
porque “maung” só tem função de adjetivo.

Acho que você já entendeu. Alguns bons exemplos para fixar:

***Maung***** smulk magvi –** muitas crianças pequenas

***Meg***** smulk magvi –** crianças muito pequenas

**Tod land hat *****pauk***** bella plaja –** este país tem poucas
praias bonitas

**Tod land hat *****pau***** bella plaja –** este país tem praias pouco
bonitas

Você pode vir que as palavras mencionadas acima trabalham às vezes como
adjetivos e às vezes como um advérbio. O que faz a diferença? É um
adjetivo quando se refere à quantidade do substantivo, mas é um advérbio
quando se refere à qualidade do adjetivo.

**Volo lyt cofie –** quero um pouco de café

**Lyt-ye bitter cofie –** café ligeiramente amargo

Para diferenciá-los quando há um risco de confusão, use a hifenada
terminação -ye para os advérbios e, se possível, as terminações
declesionais “eufônicas” para adjetivos.

Sobre o “destull”:

O que diferencia o “destull” de palavras como “meg” ou “baygh” é que
“destull” está um nível um pouco inferior, veja os exemplos abaixo que
você entenderá:

**Brasilu est meg/baygh bell –** o Brasil é muito belo *(na categoria
superior de beleza)*

**Brasilu est destull bell –** o Brasil é muito belo *(é bonito, mas há
outros países mais bonitos)*

NÚMEROS
=======

Eis os números de 0 a 10:

**0 – zero {dzEro} –** zero

**1 – oin {oyn} –** um

**2 – dwo {dwo} –** dois

**3 – tri {tri} –** três

**4 – quar {kwar} –** quatro

**5 – penk(we) {penk(wë)} –** cinco

**6 – six {siks} –** seis

**7 – sept(a) {sEpt(a)} –** sete

**8 – oct(o) {Okt(o)} –** oito

**9 – nev {nev} –** nove

**10 – dec {dek} –** dez

Entre 11 e 19 você adiciona o sufixo -dem.

**11 – oindem {Oyndëm} –** onze

**12 – dwodem {dwOdëm} –** doze

**13 – tridem {trIdëm} –** treze

**14 – quardem {kwArdëm} –** quatorze

**15 – penkdem {pEnkdëm} –** quinze

**16 – sixdem {sIksdëm} –** dezesseis

**17 – septdem {sEptdëm} –** dezessete

**18 – octdem {Oktdëm} –** dezoito

**19 – nevdem {nEvdëm} –** dezenove

Para números como 20, 30, 40, …, 90 você usa o sufixo -gim

**20 – dwogim {dwOdjim} –** vinte

**30 – trigim {trIdjim} – **trinta

**40 – quargim {kwArdjim} –** quarenta

**50 – penkgim {pEnkdjim} –** cinquenta

**60 – sixgim {sIksdjim} –** sessenta

**70 – septgim {sEptdjim} –** setenta

**80 – octgim {Oktdjim} –** oitenta

**90 – nevgim {nEvdjim} –** noventa

Eis os números 100, 1.000, 1.000.000 e 1.000.000.000:

**100 – cent(om) {tsent}/{tsëntOm} – **cem

**1.000 – mil {mil} –** mil

**1.000.000 – oin million {oyn milyOn} –** um milhão

**1.000.000.000 – oin milliard {oyn milyArd} –** um bilhão

Alguns exemplos de números:

**23 – dwogim tri –** vinte e três

**130 – cent trigim –** cento e trinta

**569 – penkcent sixgim nev –** quinhentos e sessenta e nove

**1992 – mil nevcent nevgim dwo –** mil novecentos e noventa e dois

Números cardinais são feitos com os sufixos -t (ou -im se o primeiro
sufixo for incompatível).

**Primeiro –** ~~oint~~ prest *(se for o primeiro de um conjunto de
dois, usa-se “preter” em vez de “prest”)*

**Segundo –** dwot *(ou “second”)*

**Terceiro – **trit

**Quarto –** quart

**Quinto –** penkt

**Sexto –** sixt

**Sétimo –** septim

**Oitavo –** octim

**Nono –** nevt

**Décimo –** dect

**Décimo primeiro –** oindemt

Sim, “primeiro” e “segundo” são irregulares, apesar de se admitir o
“dwot”.

Somente o último componente precisa da terminação:

**Vigésimo primeiro –** dwogim prest

Conheça também estes:

**Último –** senst *(superlativo)*

**Último –** senter *(comparativo)*

Um multiplicador é feito com o sufixo -(en)s:

**Uma vez –** oins

**Duas vezes –** dwis *(irregular)*

**Três vezes –** tris

Podemos também usar a palavra “ker”:

**Uma vez –** oin kers

**Duas vezes –** dwo kers

**Três vezes –** tri kers

O distributivo é feito com o sufixo -(e)n, mas existem formas
irregulares:

**1 –** ein

**2 –** dwin *(“dupla”) *(para a palavra “par” existe a palavra “pair”)

**12 – **douzen *(igual ao nosso “dúzia”!)*

**1000 –** tusent

Para números terminados em -dem ou -gim, você usa -tia no lugar de
-(e)n.

**20 –** dwogimtia *(como em um placar ou pontuação)*

Essa terminação -(e)n também é usada para contar substantivos que não
tem uma forma no singular:

**Mi ho kaupen trin bruks –** comprei três pares de calças

Se o distributivo é usado como quantidade, então os substantivos e
adjetivos estão no plural do genitivo.

**Un centen wolfen gwiviet in France –** uma centena de lobos poderia
viver na França

Lembra do “million” e “milliard”? Eles só existem na forma distributiva.
Mas quando o distributivo é seguido de outro número, o substantivo não
precisa mais estar no genitivo.

**1891400 humanos –** Oino million octcent nevgim oino mil quarcent
mensci

Partes podem ser indicadas pelo sufixo -del.

**Um terço –** tridel

Mas veja só:

**Metade *****(como substantivo) *****–** dwidel

**Metade *****(como adjetivo)***** –** pwol *(frequentemente usado como
prefixo)*

**Pwolter –** 1,5

O outro 0,5 é conseguido usando a forma ordinal do próximo número depois
de “pwol”. Veja um exemplo:

**Pwolpenkt – **4,5

**Pwolnevt –** 8,5

Sobre as pontuações em números, o sambahsa é igual ao português no que
diz respeito aos números fracionados, “dois e meio” é transcrito como
2,5 em sambahsa. Mas números superiores a 999 não recebem o ponto, então
“um milhão” não será transcrito como 1.000.000, mas sim como 1000000.

Operações matemáticas:

**2 + 2 = 4 –** dwo plus dwo egal (ad) quar

**2 − 2 = 0 –** dwo minus/ex dwo egal (ad) zero

**2 − 3 = −1** – dwo minus/ex tri egal (ad) minus oin

**2 ⨯ 2 = 4 –** dwo kers dwo egal (ad) quar

**2 ÷ 2 = 1 –** dwo dividen ab dwo egal (ad) oin

Placar ou resultado de votação, como por exemplo o resultado de um jogo
em que o Barcelona marcou dois gols e o Real Madrid marcou um.

**Dwo contra oin pro Barcelona –** dois a um para o Barcelona

**Oin contra dwo pro Real Madrid –** um a dois para o Real Madrid

Para indicar idade você usa o sufixo -at:

**Som dwogim sixat –** tenho vinte e seis anos

**Cid monument est milat –** aquele monumento tem mil anos

Observe que sambahsa segue a mesma lógica do inglês, por exemplo: você
não diz literalmente “tenho vinte e seis anos” (“ho dwogim sixat” em
tradução direta), mas sim “sou vinte e seis anos”.

Para saber como usar expressões como “…e tantos anos” ou “…e poucos
anos”, veja as frases abaixo:

**Sam-ye kam vasya bahsas evolve, id japanese evolvit, unte ia dwo
tusents om akhir yars, do un serie dialecten –** assim como todas as
línguas evoluem, o japonês evoluiu, durante os últimos 2000 e tantos
anos, em uma série de dialetos

**Gwens sont fecunder ye id end irs terwnia ed 20tiat, yed ne sont meis
quem crehscus magvas –** as mulheres são mais férteis no final da
adolescência e aos vinte e poucos anos, mas são pouco mais do que
crianças crescidas

TEMPO
=====

Para indicar o dia de um mês, nós colocamos o “dien” atrás do número
cardinal da data.

[]{#anchor-73}Dien sept september mil octcent dwogim dwo, Brasilu bihsit
independent

No dia sete de setembro de mil oitocentos e vinte e dois o Brasil se
tornou independente

Dien quar jul mil septcent septgim six, ia Uniet Stats bihr independent

No dia quatro de julho de mil setecentos e setenta e seis os Estados
Unidos se tornou independente

Eis os dias da semana:

**Mingo {mIngo} –** Domingo

**Mundie {mundI:} –** Segunda-feira

**Ardie **[]{#anchor-74}**{ardI:} –** Terça-feira

**Credie {krëdI:} –** Quarta-feira

**Khamsi **[]{#anchor-75}**{qAmsi} –** Quinta-feira

**Juma {jUma} –** Sexta-feira

**Sabd **[]{#anchor-76}**{sabd} –** Sábado

Eis os meses:

**Januar **[]{#anchor-77}**{januAr} –** Janeiro

**Februar {februAr} –** Fevereiro

**Mart {mart} –** Março

**Aprile {aprIl} –** Abril

**Mai {mä} – **Maio

**Jun {jun} –** Junho

**Jul {jul} – **Julho

**August {aogUst} –** Agosto

**September {sëptEmbër} –** Setembro

**October {oktObër} –** Outubro

**November {novEmbër} –** Novembro

**December {dëtsEmbër} –** Dezembro

Para indicar as horas, a melhor maneira é colocar o número da hora antes
de “saat” {saAt} seguido do número dos minutos.

**18:49 –** octdem saat quargim nev

**05:00 –** penk saat

Para indicar uma determina década, como “os anos 70”, você informa o ano
mais a terminação -tias.

**Ia 1970 –** **Ia mil nevcent septgimtias –** os anos (19)70

Um advérbio de período de tempo pode ser feito através da terminação -s.

**I beis, qui wey eiskwmos, appareihnt noct*****s***** –** as abelhas,
que nós queremos, aparecem à noite

O presente período de tempo pode ser indicado pelo prefixo ho-:

**Esta noite – **honoct

**Esta tarde –** hovesper

Mas “hoje” e “esta manhã” são feitos das seguintes formas:

**Hoje –** hoyd

**Esta manhã –** Todeghern

Um verbo que indica o período de tempo gasto pode ser feito com o
prefixo (u)per-.

**Passe ad upernocte in mien dom – **passe a noite em minha casa

**Uperdienam in id farm –** passamos o dia na fazenda

Eis as traduções para “amanhã”, “ontem” e variações:

**Amanhã –** cras

**Ontem – **ghes

**Depois de amanhã –** poscras

**De ontem –** ghestern

**Anteontem –** preghes {prëgEs}

Mais palavras importantes:

**De hora em hora –** horlong

**Semana passada –** perhvdi {pEr(h)vdi}

**Toda vez –** stets *(em geral usado para comparações)*

**Em tempos passados –** xiawngja {ksyAwngja}

**Noite passada –** yarlay {yarlAy}

[]{#anchor-78}**Dia & noite *****(24 horas)***** –** yawm {yAwm}

CORRELATIVOS
============

São palavras criadas afixando certas partículas aos pronomes
interrogativos ou outras palavras, como “anghen” *(pessoa)*, “ject”
*(coisa)* e “loc” *(lugar)*.

**Sem- {sem} – **algum

**-quid {kid} –** qualquer *(completa incerteza, geralmente é
pejorativo)*

> **Is lehct quodquid –** *uma boa tradução seria “ele não está dizendo
> coisa com coisa”*

**-gvonc {gvonk} –** está entre “algum” e “qualquer”. *É livremente
inspirado do -cumque do latim, é também como o -unque do italiano (como
em “chiunque” e “dovunque”), o -conque do francês (como em “quiconque”),
o -gen do alemão (como em “wegen”).*

> **Ne has clus id dwer. Quelgvonc ghehdiet entre id dom – **você não
> fechou a porta. Qualquer um poderia entrar na casa

> **Ne has tu dar mathen od mien intelligence est dar staurer quem
> quodgvonc eins? –** ainda não sabes que minha inteligência ainda é
> mais poderosa do que qualquer espada?

**-kwe {kwë} –** tanto faz, quer que seja, não importa o que, tudo

> **… ir gwelsa brungend quodkwe spalt iom armurs –** … suas lâminas
> tomando vantagem de cada divisão das armaduras

> **Est suwen ab id desperat naudh os convinces un public opinion
> skeptic de kweter ia EU institutions sont kwehrnd quodkwe valid ject
> –** é impulsionada pela necessidade desesperada de convencer uma
> opinião pública céptica de que as instituições europeias estão a fazer
> qualquer coisa – seja lá o que for – que valha a pena

> **Querkwe gwahm, io incontre prients –** onde quer que eu vá, eu
> encontro amigos

**-libt {libd} –** o que quiser, à vontade, a bel-prazer

> **Cheus quodlibt fustan {tcös kwOdlibt fustAn} – **Escolha qualquer
> saia *(o que você preferir)*

Uma comparação para melhor entendimento, as duas frases abaixo
significam “qualquer sapato é útil para mim” ou “qualquer sapato me
serve”:

> **Quodquid schou est util pro me:** significa que a pessoa não se
> importa qual sapato será usado pelo pé dela

> **Quodkwe schou est util pro me:** é mais sério

Outro exemplo:

> **Ia sayct quodquid – **ela diz coisa com coisa *(é uma maluca)*

> **Ia sayct quodkwe –** ela diz qualquer coisa *(significa
> provavelmente que ela é uma intelectual que conhece diversas áreas)*

**Semanghen, semquel {semAngën, semkEl} –** alguém

**Quelgvonc {këlgvOnk} –** alguém / qualquer um

**Cadanghen {kadAngën} –** todo mundo, cada um

**Quodquid {kwOdkid} –** qualquer coisa *(sentido pejorativo)*

**Quodgvonc {kwodgvOnk} –** qualquer coisa

**Quodkwe {kwOdkwë} –** qualquer, tanto faz, quer que seja

**Quiskwe {kIskwë} –** quem quer que, seja quem for

Para dizer frases como “não vejo nada”, você pode dizer “ne vido ject”,
usa-se o “ject” nesses casos quando em frases negativas. Mas uma melhor
tradução para essa frase seria “vido neid”. Quando se trata de pessoas
usamos a palavra “anghen”.

**Ma? {ma} –** por quê?

**Itak {itAk} –** é por isso

**Kam? {kam} –** como?; como *(comparativo de equidade)* *{essa palavra
também significa o verbo “gostar”}*

**Katha {kA§a} –** como isso, como aquilo

**Ka {ka} –** como (um(a)) *(qualidade, função)*

**It(han) {it}/{i§An} –** então, desse modo, dessa forma, deste jeito,
desta maneira

**Zowngschie {dzowngcI:} /dzowŋ'ʃi/ –** de qualquer maneira, de qualquer
forma, qualquer jeito

**-med {med} –** *sufixado a um pronome no genitivo, tem o significado
de “com”/“através de” + instrumento. quosmed {kwOsmëd} (com o quê? / Por
que meios?); tosmed {tOsmëd} (com isso / através desse meio)*

**Quayt {kwayt} –** quanto

**Tant {tant} –** tanto

**Semanghen, semquis, semqua {semAngën, semkis, semkwA} –** alguém

**Fulan {fulAn} –** qualquer, sem importância

**Vasyanghen {vasyAngën} –** todo mundo

**Nimen, neanghen {nImën, neAngën} –** ninguém

**Quer {ker} –** onde

**Quetro {kEtro} –** aonde, para onde

**Quetos / quois {kEtos / kwoys} –** de onde

**Her {her} –** aqui

**Hetro {hEtro} –** para cá

**Hetos / Hois {hEtos / hoys} –** daqui

**Ter {ter} –** lá

**Tetro {tEtro} –** acolá, até lá

**Tetos / tois {tEtos / toys} –** de lá, dali

**Cer {tser} –** além

**Cetro {tsEtro} –** ao além

**Cetos / ciois {tsEtos / tsyoys} –** do além

**Semloc/semquer, semtro, semtos {semlOk/semkEr, sEmtro, sEmtos} –**
algum lugar, alguma parte

**Quantloc/quanter, quantro, quantos/quantois {kwantlOk/kwAntër,
kwAntro, kwAntos/kwantOys} –** todo lugar, toda parte

**Neidloc/nequer, netro, netos {neydlOk/neker, nEtro, nEtos} –** nenhum
lugar, nenhuma parte

**Alyer, altro, altos/alyois {Alyer, Altro, Altos/alyOys} –** em outro
lugar

“Esquerda” e “direita” são representados respectivamente por “lev(ter)”
e “dex(ter)”, naturalmente que são possíveis formas como levtro/dextro,
levtos/dextos.

“Lado de fora” e “lado de dentro” são representados respectivamente por
“exo” e “eni” *(palavras irregulares)*, são possíveis formas como
extro/extos, entro/entos.

Um advérbio de direção pode ser feito com o sufix -worts, como em “Vasya
fluvs sreunt marworts” {vAsya fluvz srönt marwOrtz} (todos os rios fluem
em direção ao mar).

Um advérbio de localidade pode ser feito, raramente, com a terminação
-i.

**Ghomi {gOmi} – **no chão, em baixo

**Urbi {Urbi} –** na cidade

**Domi {dOmi} –** em casa

**Hemi {hEmi} –** no lar

*“Cima” é “ub” e “baixo” é “ghom”*

**Aptos {Aptos} –** de trás

**Unte {Untë} –** “por onde?”, “através” ou “durante”

**Qualg? {kwalg} –** que tipo?

**Solg, talg {solg, talg} –** tal

**Quod? {kwod} –** o quê?

**Semject, semquod {semjEkt, semkwOd} –** alguma coisa

**Quant(o/um) {kwAnt(o/um)} –** tudo

**Khich {qitc} –** nada mesmo, nada demais

Quant- pode servir como pronome relativo, basta adicionar os finais de
declinação a ele.

**Quan(do) {kwAn(do)}? –** quando?

**Kun {kun} –** quando, que *(expressão de tempo)*

**Kun gnahsim, mien scol dar ne existit –** quando nasci, minha escola
ainda não existia

**Ye id dien kun is mohr, vasyi buir trauric –** No dia em que ele
morreu, todos ficaram tristes

Como pronomes relativos, não há diferença entre “kun” e “quan(do)”, mas
somente “quan(do)” pode ser usado na forma interrogativa.

**Yando {yAndo} –** às vezes, algumas vezes

**Ops {ops} –** frequentemente

**Nun {nun} –** agora

**Yant {yant} –** assim que, tão logo, logo que

**Tun {tun} –** então

“Tun” é equivalente às expressões em sambahsa: “ye tod tid”, “in tod
condition”, etc

**Semper {sEmpër} –** sempre

**Naiw(o) {nÄw(o)} –** nunca

**Nuntos {nUntos} –** a partir de agora, desde agora, daqui em diante

**Tuntos {tUntos} –** por este tempo, para esta época

**Hol {hol} –** todo, completo, inteiro *(adjetivo)*

**Alnos {Alnos} –** completamente, totalmente *(advérbio)*

**Ceter {tsEtër} –** todos os outros, o restante

**Ielg {yelg} –** cada *(para mais de dois)*

**Ieter {yEtër} –** cada *(para um grupo de dois)*

**Cada {kAda} –** todos, qualquer

OS VERBOS
=========

Ah, os verbos…

Com exceção de três verbos principais, “es” {es}, “hab” {hab} e “woid”
{woyd}, os demais verbos são regulares, você só precisa prestar atenção
em alguns casos, previstos pelas regras ortográficas.

[]{#anchor-79}É comum, em outras línguas, os verbos serem representados
em sua forma no infinitivo, como em dicionários por exemplo, mas na
língua sambahsa a forma padrão de apresentação dos verbos é na forma de
*radical*. Eu quero que isso fique claro desde já, as traduções do
sambahsa para o portiguês serão disponibilizadas no infinitivo, mesmo
quando os verbos estejam em sua forma original (somente o radical). Por
exemplo: o verbo “sedd” é traduzido para “sentar” (infinitivo no
português), mas “sedd” não é a forma infinitiva da palavra, a forma
infinitiva é “sedde”. Não se preocupe que eu aviso quando o verbo estará
no infinitivo, eu só quero que você saiba que quando eu lhe mostrar algo
como “linekw = sair”, não pense que “linekw” já é o verbo no infinitivo.
Estamos entendidos?

Bom, vamos conhecer os verbos irregulares:

**Es {es} –** ser / estar

**Hab {hab} –** ter

**Woid {woyd} – **saber

[]{#anchor-80}[]{#anchor-81}“Es”, “hab” e “woid” são verbos na forma
radical, a forma infinitiva deles é respectivamente “ses”, “habe” e
“woide”. Você não precisa se preocupar muito com “es”, “hab” e “woid”
uma vez que esses verbos são irregulares.

ANTES DE COMEÇARMOS, ALGUNS COMENTÁRIOS ESPECIAIS
-------------------------------------------------

O verbo “woid” significa “saber”, mas há outro verbo com a mesma
tradução que é “gnoh”. A diferença entre “gnoh” e “woid” é que “gnoh”
tem um significado mais amplo que “woid”. “Ghoh” significa “saber” e
também “conhecer” enquanto “woid” significa apenas “saber”. É esperado
que você já saiba a diferença, mas caso ainda reste dúvidas, vejamos
alguns exemplos: “saber” (tanto “woid” quanto “gnoh”) significa que você
tem o conhecimento de um fato ou informação, como em “ele sabe quando
sair” ou “eu sei chinês”; “conhecer” (somente “woid”!) é para expressar
a familiaridade com alguém, algum lugar ou algo, como em “eles conhecem
Oliver” ou “eu conheço o Chile”. O verbo que significa apenas “conhecer”
é “kehgn”. Resumindo:

**Woid –** saber

**Kehgn –** conhecer

**Gnoh –** saber e conhecer *(em outras palavras: woid + kehgn)*

Sobre o verbo “es”, também são necessários alguns comentários. Não sei
se ficou claro, mas este verbo se refere tanto a “ser” quanto “estar”,
essa dualidade do verbo “es” basicamente só existe nas línguas ibéricas,
mais notadamente o português, espanhol e irlandês *(no irlandês “bí/tá”
equivale ao nosso “estar” e “is” ao nosso “ser”)*. Em 99,99% dos casos
um lusófono ou irlandês não terá problemas em traduzir bem suas
sentenças porque não há como ter uma interpretação diferente.\
**Som brasileir –** sou brasileiro *(acho difícil que alguém ache que
isso tenha possa ter o sentido de “estou brasileiro”)*

**Som kwehrend[^2] gohd –** eu estou fazendo bem *(novamente, alguém
imaginaria que essa frase significa “eu sou fazendo bem”?)*

Mas como nós falantes de português temos esse poder de expressar este
verbo tão importante de duas formas, de vez em quando fazemos bom uso
dessa possibilidade que infelizmente não existe em muitas outras
línguas, como o inglês e o francês. É raro, mas, às vezes, essa
distinção é muito importante, como por exemplo na frase “você não pode
dizer que ele é vereador, mas sim que está vereador, pois não sabe
sequer se vai ser reeleito”.

**Ela está bonita –** ia wehst jamile

**Não estou bonita, sou bonita! –** ne wehso jamile, io natural-ye som
jamile!

Para o verbo “estar” posso usar o verbo “wehs” (e também “lyehg” em
alguns casos, como em *“ter est maung corruption in id land, Brasilu
wehst/lyehct in id skaurnt!”, que significa “há muita corrupção, o
Brasil está na merda”, mas na dúvida fique com “wehs” que é mais
abrangente)*. No caso do verbo “ser”, você terá que usar um advérbio
para deixar claro que o verbo “es” não tem o sentido de “estar”.

Mas não abuse! Use as alternativas *(que na verdade são apenas um
quebra-galho!)* do verbo “es” somente quando isso for *realmente*
necessário, não faça disso um hábito, até porque, como eu já disse, em
99,99% dos casos o verbo “es” bastará! Pessoalmente eu traduziria tanto
“ela é bonita” quanto “ela está bonita” para “ia est jamile”, a não ser
que no texto original *(provavelmente em português, espanhol ou
irlandês)* haja de fato um questionamento sobre o uso do verbo, como em
“não estou bonita, sou bonita!”.

PRESENTE
--------

Você já conhece a conjugação do verbo “es” no presente, mas não custa
repeti-los:

[]{#anchor-82}**Som {som} –** sou / estou

**Es {es} –** és / estás

**Est {est} –** é / está

**Smos {smos} –** somos / estamos

**Ste {ste} –** sóis /estais

**Sont {sont} –** são / estão

O verbo “ste” é pronunciado {ste} porque {st} não é uma pronúncia
viável.

[]{#anchor-83}Os pronomes foram omitidos, mas você é livre para usá-los
também. Veja a mesmíssima conjugação da de cima, mas com os pronomes:

**Io som {yo som} –** sou / estou

**Tu es {tu es} –** és / estás

**Is/Ia/Id/El est {is/ya/id/el est} –** é / está

**Wey smos {wey smos} –** somos / estamos

**Yu Ste {yu ste} –** sóis /estais

**Ies/Ias/Ia/I sont {yes/yas/ya/i sont} –** eles são / estão

A partir daqui vou omitir os pronomes nas conjugações, mas saiba que
você pode usá-los! Isso é apenas uma escolha pessoal de minha parte.

Agora vamos conhecer a conjugação dos verbos “hab” e “woid”:

**Ho {ho} –** tenho

**Has {has} –** tens

**Hat {hat} –** tem

**Habmos / Hams {hAbmos / hams} –** temos

**Yu habte {yu habd} –** tendes

**Habent / Hant {hAbënt / hant} –** têm

**Woidim {wOydim} –** sei

**Woidst(a) {wOydst(a)} –** sabes

**Woidit {wOydit} –** sabe

**Woidam {woydAm} –** sabemos

**Woidat {woydAt} – **sabeis

**Woideer {woydEër} – **sabem

A conjugação do “woid” não é compulsória.

A conjugação dos verbos no presente é feito com as seguintes
terminações:

**1ª pessoa do singular:** -m *(quando depois de um som de vogal ou
{y})*, -o *(nos demais casos). Se o radical do verbo termina com -e ou
se a adicção do -o for incompatível com a acentuação tônica, não
adicione nada e use o pronome pessoa “io”.*

**2ª pessoa do singular:** -s

**3ª pessoa do singular:** -t

**1ª pessoa do plural:** -m(o)s *(use o que for compatível com a
acentuação tônica)*

**2ª pessoa do plural:** -t(e)

**3ª pessoa do plural:** -(e)nt, -e *(use o que for compatível com a
acentuação tônica)*

Vejamos um exemplo:

**Eu te amo –** te lieubho {te lyÖbo}

**Tu me amas –** me lieubhs {me lyÖbs}

**Ela o ama –** ia lieubht iom {ya lyÖbt yom}

**O cachorro ama a mulher –** el kwaun lieubht iam gwen {el kwAon lyöbt
yam gwen}

**Nós as amamos –** lieubhmos ians {lyÖbmos yans}

**Vós nos amais –** yu lieubhte nos {yu lyÖbt nos}

**Eles me amam –** ies lieubhent me {yes lyÖbënt me}

Uma vez que geralmente os pronomes são fáceis de identificar (se eles
são nominativos, acusativos, etc) você é livre para mudar a ordem deles
na frase, mas mantenha o bom senso para o bom entendimento. Você pode
dizer também: []{#anchor-84}[]{#anchor-85}[]{#anchor-86}lieubho te,
lieubhs me, []{#anchor-87}io []{#anchor-88}lieubho te, tu lieubhs me, te
[]{#anchor-89}lieubho io, me lieubhs tu.

Mais um exemplo para fixar bem:

**Aprendo sambahsa –** euco sambahsa {Öko sambA:sa}

**Aprendes sambahsa –** eucs sambahsa {öks sambA:sa}

**Aprende sambahsa –** euct sambahsa {ökt sambA:sa}

**Aprendemos sambahsa –** eucmos sambahsa {Ökmos sambA:sa}

**Aprendeis sambahsa –** yu eucte sambahsa {yu ökt sambA:sa}

**Aprendem sambahsa –** eucent sambahsa {Ötsënt sambA:sa}

BREVE EXPLICAÇÃO SOBRE INFIXOS NASAIS
-------------------------------------

Ainda falarei sobre os infixos nasais com mais detalhes em um capítulo
apropriado, mas como antes disso eu necessitarei que você tenha uma
noção desse assunto, eis aqui uma rápida explicação.

Um verbo com um infixo nasal é aquele que contém um “e” *não* acentuado
tonicamente que está junto às consoantes “m” ou “n”. Em todos os tempos
verbais, com exceção de uma forma do imperativo que usa o próprio
radical verbal, esse tipo de verbo sofrerá uma alteração, que
basicamente é a perda do “e” não tônico e, talvez, outras modificações
que explicarei no capítulo apropriado.

Veja como funciona:

**Lin*****e*****kw →** linkw

**Preg*****e*****n →** pregn

Você pode questionar: mas pra que isso? Sambahsa se comporta como uma
língua natural, se você analisar bem, fica mais agradável para falar com
as coisas desse modo, as pessoas naturalmente sentiriam vontade de
ignorar esse “e” não acentuado tonicamente. Sem essa regra até poderia
ficar um pouco mais fácil, mas não seria muito confortável para falar.

PASSADO
-------

Há dois tempos no passado para o verbo “es”, pretérito perfeito e
pretérito imperfeito, enquanto o primeiro informa uma situação pontual
no passado, o segundo trata de uma situação que acontecia durante um
tempo contínuo e acabou.

Pretérito perfeito

**Buim {bwim} –** fui / estive

**Buist(a) {bwIst(a)} –** foste / estiveste

**Buit {bwit} –** foi / esteve

**Buam {bwam} –** fomos / estivemos

**Buat {bwat} –** fostes / estivestes

**Buir {bwir} –** foram / estiveram

Pretérito imperfeito

**Eem {Eëm} –** eu era / estava

**Ees {Eës} –** tu eras / estavas

**Eet {Eët} –** ele era / estava

**Eemos {Eëmos} –** éramos / estávamos

**Yu eete {yu Eët} –** éreis / estáveis

**Eent {Eënt} –** eram / estavam

Tanto o verbo “hab” quanto o verbo “woid” só possuem o pretérito
perfeito.

**Hiebim {hyEbim} –** tive

**Hiebst(a) {hyEbst} –** tiveste

**Hiebit {hyEbit} –** teve

**Hiebam {hyebAm} –** tivemos

**Hiebat {hyebAt} –** tivestes

**Hiebeer {hyebEër} –** tiveram

**Woisim {wOyzim} –** eu soube

**Woisist {wOyzist} –** soubeste

**Woisit {wOyzit} – **ele/ela soube

**Woisam {woyzAm} – **soubemos

**Woisat {woyzAt} – **soubestes

**Woiseer {woyzEër} –** souberam

A conjugação do “wois” não é compulsória.

A conjugação dos verbos no passado é feita com as seguintes terminações:

**1ª pessoa do singular:** -im

**2ª pessoa do singular:** -(i)st(a) *(use o que for compatível com a
acentuação tônica)*

**3ª pessoa do singular:** -it

**1ª pessoa do plural:** -am

**2ª pessoa do plural:** -at

**3ª pessoa do plural:** -eer *(-r caso o verbo termine com um som de
vogal tônica)*

Entre o radical do verbo e a terminação da conjugação pode vir a ser
interessante o uso do -s-, caso o radical do verbo termine com o som de
vogal, mas não é obrigatório! Veja um exemplo abaixo:

**ghyah {gya:} –** bocejar

**ghyah + s + it = ghyahsit {gyA:sit} –** bocejou

Um exemplo para fixar:

**Franzi a testa –** brovim {brOvim}

**Franziste a testa – **brovist {brOvist} *ou* brovsta {brOvzta}

**Franziu a testa –** brovit {brOvit}

**Franzimos a testa –** brovam {brovAm}

**Franzistes a testa – **brovat {brovAt}

**Franziram a testa –** broveer {brovEër}

“Brovista” não é possível porque o “a” mudaria a acentuação tônica da
palavra.

A propósito, “brov” significa também o substantivo “sobrancelha”.

**Atenção:** a conjugação de verbos no passado, cujos radicais são
alterados, *não* é compulsória!

[]{#anchor-90}[]{#anchor-91}Sei que é meio ridículo repetir isso, mas é
para caso tenha restado dúvidas. Você poderia dizer “io buim”, “wey
brovam”, “tu ees”, etc, usando os pronomes. É a última vez que aviso
dessa possibilidade, minhas conjugações são feitas sem os pronomes, mas
isso não significa que eles não possam ser usados! Isso vale para
qualquer tempo verbal.

### NA VERDADE EXISTEM DUAS FORMAS DE REPRESENTAR O PASSADO

O que você acabou de ver é o passado simples, aquele em que as ações
começaram e terminaram no passado e ***não** deixaram consequências no
presente*. A outra forma verbal do passado é o presente perfeito, em que
as ações continuam no presente ou, embora as ações tenham terminado no
passado, *deixaram alguma consequência no presente*; o presente perfeito
é feito através do verbo “habe” + particípio no passado passivo (como só
vou ensinar particípios em um capítulo mais a frente, só me limitarei em
apresentar exemplos aqui, você não precisa saber agora como fazê-los,
mas quero que você ao menos saiba identificá-los).

Observe os exemplos abaixo para que você entenda as diferenças (note que
os (a)s estão no passado simples enquanto os (b)s estão no presente
perfeito):

**(1a) Is gwivit in Rome unte oct yars –** ele viveu em Roma por oito
anos

**(1b) Is hat gwiwt in Rome unte oct yars –** ele tem vivido em Roma por
oito anos

**(2a) Ia ghiet iom gouverneur –** ela conheceu o governador\
**(2b) Ia hat ghaten iom gouverneur –** ela conheceu o governador

**(3a) Lusim mien cleich –** perdi minha chave

**(3b) Ho lusen mien cleich –** perdi minha chave

Em (1a) a pessoa viveu em Roma no passado, mas hoje vive em outro lugar
ou não é mais vivo; em (1b) ele tem vivido em Roma há oito anos e até
hoje ele está lá. Em (2a) dá a entender que o governador daquela época
não mantém o cargo hoje em dia; em (2b) sabemos que o governador ainda
mantêm seu cargo. Em (3a) eu perdi minha chave no passado, mas ou eu a
encontrei depois ou isso já não é mais importante atualmente; em (3b) eu
perdi minha chave e não a encontrei ainda, de forma que ainda sofro as
consequências como a de ainda não poder entrar na minha própria casa.

INFINITIVO
----------

Estava esperando que eu fosse falar do tempo futuro agora, não foi?
Deixei para falar sobre o tempo futuro na próxima porque precisarei do
infinitivo para explicá-lo.

**Ses {ses} –** ser / estar

**Habe {hab} –** ter

**Woide {woyd} – **saber

Verbos terminados em “e” não tônico não mudam:

**Accepte {ak(t)sEpt(ë)} –** Accepte *(aceitar)*

Verbos com infixo nasal o “e” sem acentuação tônica é removido e é
adicionado o final -es.

Verbos com “ei”{ey} ou “eu”{ö} tem essas partes substituídas
respectivamente por “i” e “u” e é adicionado o final -es.

**Linekw {lInëkw}** – linkwes {lInkwës} *(deixar)*

**Dreiv {dreyv} –** drives {drivz} *(flutuar)*

**Ghieul {giÖl} –** ghiules {gyüls} *(carbonizar)*

Nos demais casos basta adicionar o final -e caso isso não altere a
sílaba tônica, se a sílaba tônica pode ser alterada, então a palavra
simplesmente não é modificada.

**Garabat {garabAt} –** garabate {garabAt} *(rabiscar)*

**Hinder {hIndër} – **hinder {hIndër} *(impedir)*

Vejamos um exemplo prático:

**Tehrbo kaupe anon {tE:rbo kAop anOn} –** preciso comprar comida

**Tehrb {te:rb} –** precisar, necessitar

***Kaup {kAop} –** comprar*

### O USO DO PREPOSIÇÃO “AD” COM VERBOS NO INFINITIVO

Você conheceu um exemplo do tipo:

**Ghohdim kaupe un wogh {gO:dim kAop un wog} –** pude comprar um carro

**Pitim nices un wir {pItim nItsës un wir} –** tentei matar um homem

Mas verbos que denotam movimento você deve usar a preposição “ad”, que
você conhecerá com mais detalhes em um capítulo mais a frente. Isso
acontece porque os infinitivos do sambahsa se comportam como
substantivos, exceto aqueles não usam nenhum artigo.

**Ia ihsit *****ad***** vide sien son {ya I:sit ad vid syen son} –** ela
foi ver o seu filho.

Uma situação onde o verbo é explícitamente usado como um substantivo:

**Is hehlpt iom *****ad***** tarjmes id dictionar do portughesche {is
he:lpt yom ad tArjmës id diktyonAr do portugEc} –** ele o ajudou a
traduzir o dicionário para o português

Outra situação parecida, mas com dois verbos no infinitivo:

**(Tu) has gwiven kay hehlpe *****ad***** cree un nov mund {(tu) has
gwIvën kay he:lp ad crEë un nov mund} –** você viveu para ajudar a criar
um novo mundo

FUTURO
------

As conjugações dos três verbos principais:

**Sessiem {sesyEm} –** serei / estarei

**Sessies {sesyEs} –** serás / estarás

**Sessiet {sesyEt} –** será / estará

**Sessiemos {sesyEmos} –** seremos / estaremos

**Yu sessiete {yu sesyEt} –** sereis / estareis

**Sessient {sesyEnt} –** serão / estarão

**Habsiem {habsyEm} –** terei

**Habsies {habsyEs} –** terás

**Habsiet {habsyEt} –** terá

**Habsiem(o)s {habsyEm(o)s} –** teremos

**Yu habsiete {yu habsyEt} –** tereis

**Habsient {habsyEnt} –** terão

**Woidsiem {woydsyEm} –** saberei

**Woidsies {woydsyEs} –** saberás

**Woidsiet {woydsyEt} –** saberá

**Woidsiem(o)s {woydsyEm(o)s} –** saberemos

**Yu woidsiete {yu woydsyEt} –** sabereis

**Woidsient {woydsyEnt} –** saberão

A conjugação dos verbos no futuro é feita de suas formas:

Na primeira forma você usa a forma verbal da 2ª pessoa do singular,
somado com a partícula “ie” e as conjugações do presente, veja um
exemplo abaixo:

**Permites –** permitts {permItz} *(2ª pessoa do singular no presente)*

**Permitirei – **permittsiem {përmitsyEm}

**Permitirás –** permittsies {permitsyEs}

**Permitirá –** permittsiet {permitsyEt}

**Permitiremos –** permittsiem(o)s {permitsyEm(o)s}

**Permitireis –** yu permittsiete {yu permitsyEt}

**Permitirão –** permittsient {permitsyEnt}

Na segunda forma você usa o auxiliar sie- (conjugado!) mais o verbo no
infinitivo, veja o mesmo exemplo do de cima conjugado dessa forma:

**Irei permitir –** siem permitte {syem permIt}

**Irás permitir –** sies permitte {syes permIt}

**Irá permitir –** siet permitte{syet permIt}

**Iremos permitir –** siem(o)s permitte {syEm(o)s permIt}

**Ireis permitir –** yu siete permitte {yu syet permIt}

**Irão permitir –** sient permitte {syent permIt}

Até que é bem parecido com o português.

O sambahsa tem uma versão negativa do sie-, que funciona da mesma forma
*(isto é, somente como uma partícula independente, mas nunca como um
sufixo)*, é o nie-.

**Não irei permitir –** niem permitte {nyem permIt}

### O FUTURO PRÓXIMO

Para um evento que está próximo de ocorrer, usamos o auxiliar vah-.
Funciona da seguinte forma: acrescentamos a esse auxiliar a terminação
adequada do tempo presente e em seguida colocamos o verbo no infinitivo.
Vou utilizar o verbo “orbat” {orbAt}, trabalhar, nesse exemplo.

**Vou trabalhar –** vahm orbate {va:m orbAt}

**Vais trabalhar –** vahs orbate {va:s orbAt}

**Vai trabalhar –** vaht orbate {va:t orbAt}

**Vamos trabalhar –** vahmos orbate {vA:mos orbAt}

**Ides trabalhar –** yu vahte orbate {yu va:t orbAt}

**Vão trabalhar –** vahnt orbate {vA:nt orbAt}

[]{#anchor-92}PASSADO DO FUTURO
-------------------------------

São eventos que futuros, mas que já ocorreram nos fatos mencionados.
Veja um exemplo abaixo:

**Quando eu retornar, ele já terá saído –** Quando reicsiem, is habsiet
ja likwn

Semelhante ao português

SUBJUNTIVO
----------

Indica desejo ou expectativa.

Somente o verbo “es” possui a conjugação completa nesse modo.

**Io sia {yo sya} –** seja / esteja

**Sias {syas} –** sejas / estejas

**Is/Ia/Id sia {is/ya/id sya} –** seja / esteja

**Siam(o)s {syAm(o)s} –** sejamos / estejamos

**Siate {syat} –** sejais / estejais

**Siant {syant} –** sejam / estejam

Nos demais verbos esse modo só existe no singular. As terminações são:

**1 ª pessoa do singular:** -a

**2 ª pessoa do singular:** -as

**3 ª pessoa do singular:** -a

Com essa terminação se pode fazer um imperativo negativo com o advérbio
proibitivo “mae” {may}.

**Mae kaupas {may kAopas} –** “não compre” *ou* “não comprarás”

IMPERATIVO
----------

Indica uma ordem ou pedido.

As conjugações dos três verbos principais:

**Sdi {sdi} –** sê *(afirmativo)* / sejas *(negativo)* // está
*(afirmativo)* / estejas *(negativo)*

**Estu {Estu} –** seja // esteja

**Smad ses {smad ses} –** sejamos // estejamos

**Ste {ste} –** sede *(afirmativo)* / sejais *(negativo) // estai
(afirmativo) / estejais (negativo)*

**Sontu {sOntu} –** sejam // estejam

**Hab(e) {hab} –** tem *(afirmativo)* / tenhas *(negativo)*

**Smad hab(e) {smad hab} –** tenhamos

**Habte {habt} –** tende *(afirmativo)* / tenhais *(negativo)*

**Woid(e) {woyd} – **sabe *(afirmativo)* / saibas *(negativo)*

**Smad woide {smad woyd} –** saibamos

**Woidte {woydt} –** sabei *(afirmativo)* / saibais *(negativo)*

O imperativo da 2ª pessoa do singular corresponde ao radical do verbo,
com ou sem o -e no final do verbo, simples assim.

**Saia! –** linekw! {lInëkw} ou linkwe! {linkw}

**Coma esta comida –** edd tod anon {ed tod anOn}

Na 1ª pessoa do plural, é utilizada a palavra “smad” *(vamos)* junto ao
verbo no infinitivo.

**Smad linkwes {smad lInkwës} –** vamos sair

**Smad edde {smad ed} –** vamos comer

Na 2ª pessoa do plural, a forma é a mesma do tempo presente, mas sem o
pronome pessoal.

**Linkwte {lInkut} –** deixai *(afirmativo)* / deixeis *(negativo)*

**Eddte {edt} -** comei *(afirmativo)* / comais *(negativo)*

CONDICIONAL
-----------

**Esiem {ezyEm} –** seria / estaria

**Esies {ezyEs} –** serias / estarias

**Esiet {ezyEt} –** seria / estaria

**Esiem(o)s {ezyEm(o)s} –** seríamos / estaríamos

**Yu esiete {yu ezyEt} –** seríeis / estaríeis

**Esient {ezyEnt} –** seriam / estariam

**Habiem {habyEm} –** teria

**Habies {habyEs} –** terias

**Habiet {habyEt} –** teria

**Habiem(o)s {habyEm(o)s} –** teríamos

**Yu habiete {yu habyEt} –** teríeis

**Habient {habyEnt} –** teriam

**Woidiem {woydyEm} –** saberia

**Woidies {woydyEs} –** saberias

**Woidiet {woydyEt} –** saberia

**Woidiem(o)s {woydyEm(o)s} –** saberíamos

**Yu woidiete {woydyEt} –** saberíeis

**Woidient {woydyEnt} –** saberiam

Esse modo verbal é feito juntando o radical do verbo + “ie” + terminação
do tempo verbal presente.

**Eu comeria –** eddiem {edyEm}

**Tu comerias –** eddies {edyEs}

**Ele/Ela comeria –** eddiet {edyEt}

**Nós comeríamos –** eddiemos {edyEm(o)s}

**Vós comeríeis –** yu eddiete {yu edyEt}

**Eles comeriam –** eddient {edyEnt}

Se o verbo terminar em -ye não tem problema:

**Sudye {sUdy(ë)} (processar (jur.)) →** sudyiet {sudyEt} (*ele/ela*
processaria (jur.))

### MAS E QUANTO AOS VERBOS QUE JÁ TERMINAM EM “IE”?

Quanto a esses, troque o “ie” por “icie”. Vejamos o verbo “edifie”
{edIfye}, que significa “construir” ou “edificar”.

**Eu edificaria –** edificiem {edifitsyEm}

**Tu edificarias –** edificies {edifitsyEs}

**Ele/Ela edificaria –** edificiet {edifitsyEt}

**Nós edificaríamos –** edificiemos {edifitsyEm(o)s}

**Vós edificaríeis –** yu edificiete {yu edifitsyEt}

**Eles edificariam –** edificient {edifitsyEnt}

CATEGORIAIS DE RADICAIS VERBAIS
-------------------------------

A depender do tipo do verbo, o radical pode sofrer alterações, todas as
regras estão explicadas neste subcapítulo. Embora eu vá falar muito aqui
de particípios, você não precisa se preocupar com eles, não é necessário
ainda saber como criá-los, basta saber que eles são formas nominais do
verbo *(como o particípio passivo do passado “comido” que vem do verbo
“comer”)*.

### INFIXOS NASAIS

A partir daqui até o final deste sub-capítulo de **INFIXO NASAL**, o
texto será praticamente uma tradução do livro *The Grammar Of
Sambahsa-Mundialect In English*, do Dr. Olivier Simon.

São verbos com infixo nasal os que seguem os seguintes requisitos:

-   Um “e” sem acentuação tônica como última vogal e esse “e” está entre
    consoantes
-   Uma dessas consoantes deve ser “m” ou “n”

Vamos trabalhar com as seguintes palavras:

**Linekw {lInëkw} –** deixar

**Pressem {prEsëm} –** pressionar

**Scinesd {sInësd} –** dividir *(verbo transitivo)*

**Annem {Anëm} –** respirar

**Pregen {prEdjën} –** imbuir, impregnar

No tempo presente, e em todos os tempos verbais derivados do tempo
presente, os verbos perdem o seu “e” sem acentuação tônica quando
possível. Se, com a deleção do “e”, um “s” ou “ss” se juntarem a uma
consoante, eles também serão deletados. Vamos ver o resultado disso:

Linkwo, linkws, linkwt, linkwm(o)s,yu linkwte, linkwnt {lInkunt}

Premo, prems, premt, premmos, yu premte, preme(nt)

Scindo, scinds, scindt, scindmos, yu scindte, scinde(nt)

Annmo, annems, annemt, annmmos, yu annemt, annment

(“annmt” e “annms” seriam impronunciáveis)

Pregno {prEnyo}, pregens {prEdjëns}, pregent, pregnems {prEnyëms}, yu
pregent, pregne(nt)

Todos os verbos, com ou sem infixo nasal, um “e” pode ser colocado entre
o radical do verbo e a terminação da conjugação para que a palavra seja
pronunciável. Veja esse caso com o verbo “storgn”.

Storgno, storgnes, storgnet, storgnems, yu storgnet, storgne(nt)

Verbos que tem um “e” sem acentuação tônica no final são conjugados da
seguinte maneira:

Io entre, entres, entret, entrems, yu entret, entre(nt)

É isso mesmo o que você viu, o verbo não se modifica na conjugação da 1ª
pessoa do singular, é até por isso que é utilizado o pronome “io”.

Verbos cuja última vogal não tem acentuação tônica e está atrás de uma
consoante seguem o mesmo padrão:

Io hinder, hinders, hindert, hinderms, yu hindert, hindernt

É “hinder” e não “hindere” porque “hindere” modificaria a acentuação
tônica {hindEr}

No tempo passado e derivados, o verbo, além de perder o “e” não
acentuado tônicamente, perde o infixo nasal.

**Linekw –** likwim, likwist, likwit, likwam, likwat, likweer

**Pressem –** pressim, pressist, pressit, pressam, pressat, presseer

**Annem – **annim, annist, annit, annam, annat, anneer

No português o infixo nasal meio que ocorre, como nas palavras
“corromper” e “corrupto”

### VERBOS COM “EH” + CONSOANTE

Em verbos com uma vogal “eh” *seguida de uma consoante*, o “eh” torna-se
“oh” quando usado no pretérito e particípio do passado.

**Ghehd –** poder, ser capaz de

**Presente:** ghehdo, ghehds, ghehdt, ghehdmos, yu ghehdte, ghehde(nt)

**Infinitivo:** ghehde

**Pretérito:** io ghohd, tu ghohd…

**Ou:** ghohdim, ghohdist/ghohdst(a), ghohdit, ghohdam, ghohdat,
ghohdeer

**Particípio no passado:** ghohdt / ghohden

### VERBOS COM “EU” E “EI”

Verbos com “eu” ou “ei”, no interior da palavra, tem essas partes
mudadas respectivamente para “i” e “u” quando usados no infinitivo,
pretérito ou particípio do passado.

**Kheiss –** sentir

**Presente:** kheisso, tu kheiss, kheisst, kheissmos, yu kheisste,
kheisse(nt)

“tu” é compulsório antes de “kheiss” porque a terminação não pode ser
ouvida

**Infinitivo:** khisses {qIsës}

**Pretérito:** io khiss, tu khiss…

**Ou:** khissim, khisst(a) / khissist, khissit, khissam, khissat,
khisseer

**Particípio no passado:** khisst / khissen

**Beud –** apelar para alguém, implorar

**Presente:** beudo, beuds, beudt, beudmos, yu beudte, beude(nt)

**Infintivo:** budes {büdz}

**Pretérito:** io bud, tu bud…

**Ou:** budim, budst(a) / budist, budit, budam, budat, budeer

**Particípio no passado:** budt / buden

**Credeih –** acreditar

**Presente:** credeihm, credeihs, credeiht, credeihm(o)s, yu credeihte,
credeihnt

**Infintivo:** credihes {krëdI:s}

**Pretérito:** io credih, tu credih…

**Ou:** credihsim, credihst(a), credihsit, credihsam, credihsat, credihr

**Particípio no passado:** crediht / credihn

### VERBOS COM “A”

Verbos com “a” no interior da palavra, o mesmo vale para os ditongos
“au” ou “ay”, tem o seu “a” trocado por “ie” quando no pretérito, mas o
“a” é mantido no particípio do passado.

**nak –** alcançar

**Presente:** nako, nacs, nact, nakmos, yu nacte, nake(nt)

**Infinitivo:** nake

**Pretérito:** io niek, tu niek…

**Ou:** niekim, niecst(a)/niekist, niekit, niekam, niekat, niekeer

Nakim, nacsta… são possíveis, mas na verdade nunca usados.

**Particípio no passado:** nact / naken

**sayg –** dizer

**Presente:** saygo, saycs, sayct, saygmos, yu saycte, sayge(nt)

**Infinitivo:** sayge

**Pretérito:** io sieyg, tu sieycst…

**Ou:** sieygim, sieycst(a)/sieygist, sieygit, sieygam, sieygat,
sieygeer

“Sieygim” é quase nunca usado.

**Particípio no passado:** sayct / saygen

**aur –** escutar

**Presente:** auro, aurs, aurt, aurm(o)s, yu aurte, aurnt

**Infinitivo:** aure

**Pretérito:** io ieur, tu ieurst…

**Ou:** ieurim, ieurst(a)/ieurist, ieurit, ieuram, ieurat, ieureer

**Particípio no passado:** aurt / aur(e)n

### AS REGRAS DE VON WAHL

O final desses verbos se modificam no pretérito e no particípio do
passado:

  ----------------- ----------------
  Original          Após a mudança
  -d                -s
  -dd / -tt         -ss
  -rt / -rr / -rg   -rs
  -lg               -ls
  -ct               -x
  ----------------- ----------------

Verbos terminados em -v seguem o mesmo procedimento que -t, mas apenas
para o seu particípio no passado. Se o -v vier depois de “o” ou “u”, ele
é removido, de outra forma, ele será trocado por -w.

**Solv –** solwt

**Lav –** lawt

Em outras formas o -v desaparece.

**mov – **mot

**Clud –** fechar

**Presente:** cludo, cluds, cludt, cludmos, yu cludte, clude(nt)

**Infinitivo:** clude

**Pretérito:** io clus, tu clusst…

**Ou:** clusim, clusst(a)/clusist, clusit, clusam, clusat, cluseer

**Particípio no passado:** clus / cluden

**Sedd –** sentar

**Presente:** seddo, sedds, seddt, seddmos, yu seddte, sedde(nt)

**Infinitivo:** sedde

**Pretérito**: io sess, tu sesst…

**Ou:** sessim, sesst(a)/sessist, sessit, sessam, sessat, sesseer

**Particípio no passado:** sess / sedden

**Permitt –** permitir

**Presente:** permitto, permitts, permitt, permittmos, yu permitte,
permitte(nt)

**Infinitivo:** permitte

**Pretérito:** io permiss, tu permisst…

**Ou:** permissim, permisst(a)/permissist, permissit, permissam,
permissat, permisseer

**Particípio no passado:** permiss / permitten

**Volg –** virar-se

**Presente:** volgo, volcs, volct, volgmos, yu volcte, volge(nt)

**Infinitivo:** volge

**Pretérito: **io vols, tu volsst…

**Ou:** volsim, volsst(a)/volsist, volsit, volsam, volsat, volseer

**Particípio no passado:** vols / volgen

**Curr –** correr

**Presente:** curro, currs, currt, currm(o)s, yu currte, curre(nt)

**Infinitivo:** curre

**Pretérito:** io curs, tu curst…

**Ou:** cursim, curst(a)/cursist, cursit, cursam, cursat, curseer

**Particípio no passado:** curs / curren

### TERMINADOS COM UMA VOGAL ACENTUADA TONICAMENTE

Verbos terminados com uma vogal acentuada tonicamente, ou com um -h logo
após a vogal, seguem o padrão abaixo:

**Gwah –** ir para

**Presente:** gwahm, gwahs, gwaht, gwahm(o)s, yu gwahte, gwahnt

**Infinitivo:** gwahe

**Pretérito:** gwahsim, gwahst(a)/gwahsist, gwahsit, gwahsam, gwahsat,
gwahr

**Particípio no passado:** gwaht / gwahn

### DEMAIS VERBOS

Seguem as regras normalmente e exigem a conjugação no pretérito.

**Duc –** liderar

**Presente:** duco, ducs, duct, ducmos, yu ducte, ducent

**Infinitivo: **duce {düts}

**Pretérito:** duxim, ducst(a)/duxist, duxit, duxam, duxat, duxeer

Você pode dizer também “ducim”, “ducit”. Foi escolhido o “duxim” aqui
por razões etimológicas, porque o latim tem o “dux-” como pretérito de
“duc-”.

**Particípio no passado: **duct / ducen

A acentuação tônica deve cair na mesma sílaba para todas as pessoas no
tempo presente.

[]{#anchor-93}Em todos os tempos verbais, com exceção dos infixos
nasais, o “e” não acentuado tonicamente entre duas consoantes, quando
este conjunto está no final da palavra, pode ser removido caso a junção
dessas duas consoantes muda a pronúncia da primeira consoante.

AFIXOS VERBAIS
--------------

Nos exemplos abaixo usarei palavras no gerúndio e também farei uso de
pronomes reflexivos, não se preocupe com eles, foque-se apenas nos
afixos verbais e como eles mudam os verbos. Falarei depois sobre
gerúndios e pronomes reflexivos.

**bi- –** começar a *(incoativo)*

**Id luce biattract insects –** a luz começou a atrair insetos

**na- –** manter-se a + *gerúndio (continuativo)*

**Is nieudh urgent-ye namove –** Ele precisou urgentemente manter-se
movendo

**re- –** a mesma coisa do português

[]{#anchor-94}[]{#anchor-95}**Ho tohrben rescribe mien buk –** tive que
reescrever o meu livro

**vi- –** corresponde ao advérbio “finalmente”

**Is viemers ex id wed –** Ele finalmente emergiu para fora da água

**za- –** parar de fazer

**Ibs sclavs buit permitten za-ermes –** Os escravos tiveram permissão
para parar de remar

### AFIXOS QUE MUDAM O MODO DO VERBO

**ee- –** costumava a

**Hermann ee(-)gwaht id scol siens urb –** Hermann costumava ir para a
escola de sua cidade

Perceba que o verbo “gwaht” está no presente

**sa(l)- –** estar prestes a *(sempre escrito com hífen)*

**Is wir sa-khierk –** O homem estava a ponto de se afogar

**-skw –** desiderativo. Ter intenção de, querer fazer *(pode ser
adicionado a verbos caso não altere a acentuação tônica do verbo)*

**Ies nauts gwahskweer id maykhana –** Os marinheiros queriam ir para a
pousada

**-eih –** factivo

**Kaupeihm iom tod wogh – **eu o fiz comprar esse carro

Verbos terminados em “ie” tem essa parte trocada por “iceih”. Radicais
em “ei” mudam essa parte por “i” por questões de eufonia.

Cuidado que nem todo verbo terminado em “eih” é um factivo, “credeih” e
“vergeih” são dois exemplos.

Esse afixo pode ser adicionado a adjetivos. “rudh” significa “vermelho”,
“rudheih” significa “avermelhar”.

ALGUMAS FORMAS VERBAIS LITERÁRIAS
---------------------------------

Devido à sua herança indo-europeia, sambahsa tem algumas formas verbais
que encontram apenas no uso literário.

### FINAIS OPCIONAIS DO PRESENTE DO INDICATIVO

Se compatível com a acentuação tônica, os verbos podem receber as
seguintes terminações:

**1ª pessoa do singular:** -mi

**2ª pessoa do singular:** -si

**3ª pessoa do singular:** -ti

**3ª pessoa do plural:** -nti

**O verbo “ses”:** esmi, essi, esti, sonti

O verbo deve estar na posição inicial da oração e esta oração não deve
conter advérbio, nem mesmo negação! Esta formação verbal só serve para
descrever os eventos que ocorrem na verdade, não para declarações
gerais. Essas condições são raramente cumpridas.

**Stahnti medsu iens peinds –** cá eles ficam no meio dos inimigos

**Acquiseihmi denars kay likes videogames –** ganho dinheiro para jogar
videogames

### FORMAS ANTIGAS DO IMPERATIVO

Na 2ª pessoa do singular do imperativo, uma opção pode ser o uso do
sufixo -di ao radical do verbo. Não esquecer de trocar “eu” e “ei” por
“u” e “i” respectivamente e observar o infixo nasal.

**Kludi! –** ouça! *em vez de* “kleu(e)!”

**Ihdi! –** vá! *em vez de* “eih(e)!”

Um imperativo nas 3ª pessoas é possível sufixando o -u ao verbo
conjugado, se não alterar a acentuação tônica.

**Is maurdher nehct –** o assassino perece

**Is maurdher nehctu! –** deixe o assassino perecer!

**I slougs behrnt gwaur bermens –** os servos carregam cargas muito
pesadas

**I slougs behrntu gwaur bermens! –** deixe os servos carregarem cargas
muito pesadas!

É possível usar o radical do verbo com o final -e e um sujeito na frase:

**Gwive is roy! –** vida longa ao rei!

### PARTICÍPIO NO FUTURO

Para formar o particípio usa-se a forma sintética do futuro mais a
terminação -nd.

**gwehmsie + nd = gwehmsiend –** *o que ou quem* virá

O futuro próximo ativo é marcado com o final “tur”, para formá-lo:

-   Adicione -ur para os verbos no particípio que seguem as regras de
    Von Wahl, ou aqueles que terminem com “v”.

**Cedd –** cessur (vai ceder)

**Emov –** emotur (vai se emocionar)

-   Adicione -ur para a 3ª pessoa do singular do presente.

**Baht –** bahtur (*o que ou quem* vai falar)

O futuro ativo do verbo “ses” é “butur”.

### AS VELHAS FORMAÇÕES DO INFINITIVO

Formações do velho infinitivo são possíveis de duas formas: seguindo a
mesma base do futuro ativo visto anteriormente (uso do particípio
passivo em “t” ou da 3ª pessoa do singular do presente), se isso não
alterar a acentuação tônica.

A primeira formação termina com -(t)um e expressa a ideia de propósito.

**Abgwahsit pinctum in id widu –** ele sai para pintar na mata

**Gwehmo essum con vos –** vim comer com você

Verbos substantivados.

**Deictum exact reuls sienims almens –** mostrando regras exatas para os
próprios alunos

**sienims =** “sien” + plural dativo “ims”

As demais formações terminam com -tu e podem ser traduzidas como “ser
…ado(a)” ou “ser …ido(a)”. Aparece frequentemente depois de adjetivos de
qualidade.

**Un garden amat spehctu –** um jardim que é agradável de ser olhado

Às vezes aparece como um adjetivo de obrigação.

**Ia kwehrtu opsa** – as tarefas a serem realizadas

### O DURATIVO

O sufixo durativo -neu corresponde a ações que começaram no passado e
continuam até o presente, envolve situações onde normalmente se usaria
palavras como “desde” ou “por”.

**Stahneum her pon trigim minutes –** eu estive aqui por trinta minutos

O passado é feito com ee-.

**Eeghangneut apter iom pon Orleans –** ele estava andando atrás dele
desde Orleans

### O EVENTIVO

Significa “não parar de fazer”, consiste em repetir a primeira consoante
(ou sC, Cw, Cy ou Cv) antes do radical com a adição de i- ou ei-. O
imperfeito é feito com ee-.

**Dehm *****(treinar)***** –** didehm *(treinar *sem parar*)*

### O INTENSIVO

Significa “fazer pouco a pouco”. Consiste na reduplicação do radical
(com a deleção de oclusivos no meio). Raramente usado. O imperfeito é
feito com ee-.

**Wehrt →** wehrwehrt

### O ITERATIVO

Significa “começar a continuar a fazer”. Há apofonias a serem
observadas:

**eh:** oh

**ei:** oi

**eu:** ou

Você usa o sufixo -ye. O imperfeito é feito com ee-.

**Kwehr- sien itner –** fazer do seu modo

**Kwohrye- sien itner –** *indica que parou a ação por um tempo (por
exemplo: para o agente descançar) e continua essa ação depois. Não seria
como em “rekwehr- sien itner”, que indica que está sendo refeita toda a
ação novamente.*

### O PERFEITO

Representa uma ação feita no passado cujos efeitos ou consequencias
ainda tem alguma influência no presente. É feito prefixando a primeira
letra do radical seguido do “e” e então o verbo conjugado no passado

**Lelikwst id vetus wastu –** tu deixaste a antiga cidade *(“lelikwst”
vem do verbo “linekw”)*

MAIS SOBRE O USO DAS DECLINAÇÕES
================================

Como você já conhece os verbos, sinto me mais confortável em lhe ensinar
mais sobre o uso das declinações.

ACUSATIVO
---------

É bem fácil, alguns exemplos:

**Tenho todos os maridos –** ho vasyens mann(en)s

**Desculpe-me, mas passei a amar uma outra mulher –** maaf, bet ho
biliubht alyu gwen

**biliubht =** bi (afixo verbal que significa “começar a”) + liubht
(particípio no passado)

DATIVO
------

**Manoel dá mel aos ursos –** Manoel daht mielt im urxims\
**Comprei um presente para minha esposa –** ho kaupen un hadia mieni
esori\
**Faço móveis para as casas –** kwehro meublar ibs domims

ADVÉRBIOS
=========

Advérbios são palavras que qualificam verbos e adjetivos, como em
“viajou tranquilamente” e “comeu bastante”, onde os verbos “viajou” e
“comeu” são qualificados pelos advérbios “tranquilamente” e “bastante”.
Para criar um advérbio basta adicionar o sufixo -ye (com hífen!!!). Veja
alguns exemplos:

**End –** fim, final

**End-ye –** finalmente

**Enpace –** tranquilo, tranquila

**Enpace-ye –** tranquilamente

Pode-se dizer que o -ye é como o -mente do português. Mas, assim como no
português, nem todos os advérbios do sambahsa terminam com o mesmo
final, pois algumas palavras por si só já são advérbios, veja alguns
exemplos:

**Tik –** somente

**Ops –** frequentemente

**Just –** apenas, há pouco

***Ho just nicen un wir –** acabei de matar um homem*

**Just barwakt –** na hora!

**It(han) {it / i§An} –** então, dessa forma, deste jeito, desta maneira

**Bfuyi {bfUyi} –** continuamente

**Sigwra {sIgura} –** com certeza, certamente, indubitavelmente

**Oku {Oku} –** depressa, rapidamente

**Ja** **{ja} –** já

**Semper {sEmpër} –** sempre

**Tun {tun} –** então, depois, em seguida, aí

**(Ya)schi {(yA)ci} –** também, além disso, do mesmo modo, igualmente,
da mesma forma

Advérbios também podem ser feitos adicionando o prefixo a- a
substantivos.

**Part –** parte

**Top(p) –** topo

**Apart –** à parte, independentemente, separadamente

**Atop –** em cima,

Advérbios de qualidade como “baygh” e “pior” podem se comportar como
adjetivos quando eles se referem a um substantivo, podendo (não é
compulsório) um final declesional, mas como um advérbio (com a possível
adjunção do -ye) quando eles se referem a um adjetivo atribuitivo.

**Piora kowpic chifans sont vierdnic pro sieune –** muitas refeições
copiosas são prejudiciais para a saúde

**Pior-ye kowpic chifans sont vierdnic pro sieune –** refeições bastante
copiosas são prejudiciais para a saúde

GERÚNDIO
========

Para formar o gerúndio usa-se o particípio ativo no presente mais o
sufixo -ye (com hífen!). Muito simples, mas brasileiros precisam ter uma
atençãozinha especial aqui, porque você viu que a frase “estou comendo”
é traduzida como “som eddend”, não como “som eddend-ye”! Com o verbo
“ses”, você só usa o particípio ativo no presente, nos demais casos você
usa o gerúndio. Veja alguns exemplos:

**Ele comeu o bolo olhando para mim –** Is essit id torte spehcend-ye me

**Elas morreram trabalhando –** Ias mohreer orbatend-ye

Se eu escrevesse “Is essit id torte spehcend me” *(sem o -ye no
gerúndio)* significaria que é a torta que está olhando para mim, espero
que assim você aprenda a diferenciar melhor o gerúndio do particípio
ativo do presente.

Se isso facilita o entendimento: os dois exemplos acima podem ser
traduzidos como “ele comeu o bolo *enquanto* olhava para mim” e “elas
morreram *enquanto* trabalhavam”.

PRONOMES REFLEXIVOS E O PRONOME POSSESSIVO “SIEN”
=================================================

Os pronomes são: “se” (acusativo), “sib” (dativo) e “sien” (pronome
possessivo).

**Se vidmos in id specule –** nos vemos no espelho

**“…”, sib sieyg is lytil prince –** “…”, disse o pequeno príncipe para
si mesmo

O pronome “sien” sempre se refere ao sujeito da oração, mas nunca
aparece junto ao grupo nominal do sujeito.

**Un pater alt sien purts –** um pai educa seus filhos

**Julio ed eyso prient siefreer do Brasilu –** Júlio e seu amigo
viajaram para o Brasil

Perceba que não poderia ser “Julio ed sien prient”, lembre-se do que
falei antes.

O “mesmo” é traduzido como -swo, veja só.

**Gnohdi teswo –** Conheça a si mesmo

Ah, é oportuno dizer aqui que “entre si” ou “um ao outro” é traduzido
como “mutu” no sambahsa.

**Martin ed eyso prient tolke con mutu in Sambahsa –** Martin e seu
amigo conversam um com o outro em sambahsa

**PRONOME RELATIVO E CONJUNÇÃO INTEGRANTE** 
============================================

Veja as frases abaixo, preste atenção no uso do “que”:

\(1) Esse é o cavalo *que* eu comprei

\(2) Eu tinha uma filha *que* queria ser cantora

\(3) Eu disse *que* não gosto de café

\(4) Ele era tão preguiçoso *que* seus pais colocaram rodas na cama dele

Embora eles sejam a mesma palavra, eles têm funções diferentes, tanto
que se fossem traduzidos para outras línguas os “que”s dessas frases
poderiam ser traduzidos de forma bem diferente.

Os “que”s das frases (1) e (2) são pronomes relativos, porque eles são
relativos a algum substantivo utilizado anteriormente, que no caso da
frase (1) é “cavalo” e na frase (2) é “filha”.

Já os “que”s das frases (3) e (4) são conjunções integrantes, pois eles
iniciam uma nova oração.

Talvez você já conheça os pronomes relativos, pois eles estão lá naquela
lista de casos de declinação, só preste atenção sobre qual caso usar,
porque o “que” da frase (1) é um objeto direto (do verbo “comprei”)
enquanto o “que” da frase (2) é um sujeito.

**Esse é o cavalo *****que***** eu comprei – **Tel est el cavall *quel*
kieupim

**Eu tinha uma filha *****que***** queria ser cantora –** hiebim un
dugter *qua* iskwit ses sehngver

A conjunção integrante geralmente é traduzida como “od”.

**Eu disse *****que***** não gosto de café –** Ho saygen *od* ne kamo
cofie

**Ele pensou *****que***** eu sou da África –** is mohnit *od* som ex
África

Como as vezes só uma conjunção integrante não é clara o suficiente para
indicar se o que foi dito é um desejo ou uma afirmação, existe também a
conjunção integrante “ke” *(ou “kem”)*, que expressa um desejo e pode
ser utilizada no lugar de “od”.

Seria o nosso “que” *(conjunção integrante)* mais um verbo no
subjuntivo.

**Eu desejo que ele aprenda sambahsa –** vanscho kem is euct Sambahsa

**Que nosso time vença o time inimigo e torne-se o campeão de futebol
europeu –** Ke nies swoin vinct id peindswoin ed biht Europes football
champion

**Vamos fingir que o vidro se tornou todo macio como gaze para que
possamos passar para o outro lado –** smad simule kem id glas hat biht
tem moll quem kaz kay ghehdiemos lites ocolo

**Eu te peço que saia –** te prehgo ke linkws

OS PRONOMES RELATIVOS “YO(S)”, “YA” E “YOD”
-------------------------------------------

Eu pensei muito se eu deveria realmente trazer esse assunto para aqui,
porque, como você vai ver, provavelmente você nunca irá usá-los, mas
como esta é uma gramática *completa*, me senti na obrigação de
explicá-los.

Basicamente a relação é:

**Qui –** yo(s)

**Qua – **ya

**Quod –** yod

Os pronomes relativos yo(s)/ya/yod são para formarem orações
independentes, como assim? Veja o exemplo abaixo:

**Latif, gwivt-yo in Afghanistan, baht Sambahsa –** Latif, que vive no
Afeganistão, fala sambahsa

Percebe que mesmo se retirássemos o “gwivt-yo in Afghanistan” (“que vive
no Afeganistão”), a frase não perderia o significado original, uma vez
que “Latif baht Sambahsa” continua passando a mesma mensagem? Podemos
entender que os pronomes relativos yo(s)/ya/yod inserem uma informação
meramente adicional que – gramaticalmente falando – pode ser omitida.

Agora vejamos um exemplo que não caberia os pronomes relativos
yo(s)/ya/yod.

**Id bahsa, quod Latif baht, est Sambahsa –** a língua, que Latif fala,
é sambahsa

Eles são, na verdade, enclíticos; você saberá mais sobre eles em um
capítulo mais apropriado, basta saber que eles devem ser sufixados – com
hífen – após o verbo.

Se retirarmos o “quod Latif baht” (“que Latif fala”), a frase perderia
seu significado original.

Você me questionaria: “mas eu poderia usar muito esses pronomes
relativos que você diz serem raramente usados, porque eu faço muitas
frases onde eles caberiam”.

Mas o detalhe é que você poderia usar os pronomes qui/qua/quod no lugar
de yo(s)/ya/yod, seria perfeitamente válida uma frase como: “Latif, qui
gwivt in Afghanistan, baht Sambahsa”

O único motivo para usar os pronomes relativos yo(s)/ya/yod seria por
propostas literárias.

PERGUNTAS
=========

Há diversas formas de fazer uma pergunta, a primeira é adicionando a
palavra “kwe” {kwe} no início da frase.

**Kwe ghehdo ghende tod? –** posso segurar isso?

**Kwe siem ghehdo ghende tod? –** poderei segurar isso?

**Kwe ter sont leuds her? –** há pessoas aqui?

**Kwe sont leuds her? –** há pessoas aqui?

O “kwe” também pode ser utilizado no final de perguntas junto ao “no” na
função de “não é”.

**Safersiemos do ia Uniet Stats, kwe no? –** viajaremos para os Estados
Unidos, não é?

Você também pode fazer uma pergunta com invertendo a posição do verbo
principal.

**Ghehdo io ghende tod? –** posso segurar isso?

**Siem io ghehde ghende tod? –** poderei segurar isso?

Perceba que é necessário o uso do pronome.

**Sont ter leuds her? –** há pessoas aqui?

Perceba que é necessário o uso do “ter”.

Perguntas com “o que”, “quem”, “quando” e outros pronomes do gênero,
você pode fazer a pergunta do mesmo modo que você faz no português.

**Quis ste yu? –** quem é você? *(pergunta feita para uma pessoa do sexo
masculino)*

**Quando safersies –** quando viajarás?

NEGAÇÃO
=======

Para responder uma pergunta de forma negativa, você pode simplesmente
dizer “no”.

Para criar um verbo negativo, basta colocar o “ne” antes do verbo.

**Ne eddo leckereits –** não como doces

O “mae” {may} é um proibitivo.

**Mae eddo leckereits –** eu não deveria comer doces

**Mae eddas leckereits –** não coma doces *(não entendeu o porquê da
terminação -as no verbo “edd”? Relembre o motivo no capítulo sobre o
verbo subjuntivo)*

Para facilitar, é só lembrar daqueles mandamentos bíblicos:

**Mae neicas –** não matarás

Não podemos nos esquecer do verbo de “não posso” que é “khako” {qAko}.

**Khako kaupe un wogh – **não posso comprar um carro

“Khak” é o antônimo de “ghehd”.

SOBRE O USO DO “SI”
-------------------

Para que não haja dúvidas quanto ao uso do “si”, que significa “sim” e é
utilizado para responder questões negativas. Observe a seguinte
situação:

**Ne has tu purts? –** você não tem filhos?

Si

O “si” não corrobora a linha de pensamento de quem pergunta.

**Ne has tu purts? –** você não tem filhos?

**Si, ho purts –** sim, tenho filhos

EVITANDO REPETIÇÕES
===================

Em uma conversa, pode acontecer de necessitarmos usar um substantivo já
utilizado logo anteriormente, para evitar a repetição de palavras o
sambahsa oferece basicamente os mesmos recursos que o português. Veja os
exemplos:

Se você gosta de chá, eu vou te dar *algum*

Sei tu kams chay, vahm tib schehnke *sem*

Trago o chá e seus copos.

Por favor, dê me *um*!

Bringho id chay ed ia tasses.

Plais, schehnk mi *uno*!

Um curso de faculdade é composto por várias disciplinas, e *cada uma*
tem uma certa quantidade de horas

Un facultat curs est composen ab/med multa matiers, ed *ielg* hat un
certain quantitat om hors

Pegue seu livro e passe o *meu*

Ghendte vies buk ed anancte mi *mieno*

Você poderia dizer apenas "mien" ou “id mien”, mas "mieno" deixa a
mensagem mais clara

Qual camisa você vai usar?

Eu vou usar *a minha (mais) nova*

Qualg tischert vahte yu vehse?

Vahm vehse *mien nov(er)*

Meu peixinho está como *o* do vizinho antes de morrer

Mien lytil pisk est lik *tal* ios nieber pre mehre

Eis a frase completa: Mien lytil pisk est lik al pisk ios nieber pre
mehre

PARTICÍPIOS
===========

Particípios são a forma nominal do verbo.

PRESENTE ATIVO
--------------

Para formar o particípio ativo do presente, bastar adicionar o sufixo
-(e)nd *(-(e)nt também é possível, mas raramente usado porque pode
causar confusão com a terceira pessoa do plural)*.

Usando o verbo “ser” mais o particípio ativo no presente você pode criar
frases como:

**Id sol eet bleigend –** o sol estava brilhando

**Id bleigend sol –** o sol brilhante

**Es –** esend

**Hab –** Habend

**Woid –** woidend

Em teoria os particípios adjetivos podem ser usados como substantivos,
mas, na prática, isso nunca ocorre, o que acontece é o uso de
terminações como -(e)nt e -(a)nt, como em “president”, “studyent” e
“immigrant”.

PASSADO ATIVO
-------------

 Para formar o particípio ativo do pretérito, bastar adicionar o sufixo
-us ou vs *(vai depender do que ficar melhor pronunciável na palavra e
dos seus padrões de acentuação tônica)*.

**Dank spollay ob gwehmus hetro –** muito obrigado por ter vindo aqui

**Ia minst antslehnkus lands –** os países menos desenvolvidos

**Es –** esus

**Hab –** Habus

**Woid –** woidus

PRESENTE PASSIVO
----------------

Dos particípios passivos, o sambahsa usa apenas o passado, mas é
possível usar o usar o presente ativo em palavras compostas. O sufixo
seria -men com o radical do verbo no presente.

**Al –** criar *(no sentido de criar um filho)*

**Almen –** pupilo ou pupila

PASSADO PASSIVO
---------------

Para formar o particípio passivo do pretérito, bastar adicionar o sufixo
-t ou -(e)n. Julgo esse particípio o mais interessante e você vai
entender o porquê.

**Es –** est / esen *(sido)*

**Hab –** habt / haben *(tido)*

**Woid –** wois / woiden *(sabido)*

### OUTRA MANEIRA DE EXPRESSAR O PASSADO, O PRESENTE PERFEITO

Através do verbo “habe” você pode expressar o passado, você conjuga esse
verbo como no presente e depois utiliza o verbo desejado no particípio
passivo do passado.

**Ho edden –** eu comi

**Ho kaupen –** eu comprei

**Has liubhen / liubht –** você amou

**Habte yu edden? / –** você comeu?

**Habte yu kaupen? / –** você comprou?

Mas preste atenção, ele não é igual ao verbo do passado simples. Verbos
no presente representam ações feitas que tem consequências até o
presente, enquanto verbos no pretérito simples as ações não deixaram
consequências no presente. Veja os dois exemplos abaixo:

**(1) Qui hant uct sambahsa –** aqueles que aprenderam sambahsa

**(2) Qui uceer sambahsa –** aqueles que aprenderam sambahsa

\(1) dá a entender que os alunos aprenderam sambahsa e ainda sabem a
língua *(mas não que dizer necessariamente que eles ainda aprendem a
língua, significa apenas que os seus efeitos – saber a língua – ainda
permanecem)*, em (2) dá a entender que eles aprenderam sambahsa, mas
esqueceram a língua tempos depois ou morreram.

### UMA MANEIRA DE FAZER ADJETIVOS COM VERBOS

Vamos usar os seguintes verbos e substantivos nos exemplos:

**Sneigv {sneygv} –** neve

**Calive {kalIv} –** cabine

**Lyegher {lyEgër} –** camada

**Tenu {tEnu} –** fino, fina

**Myehrs {mye:rs} – **esquecer

**Covehr {kovE:r} –** cobrir

**Covohr {kovO:r} –** *verbo “covehr” no passado. No próximo capítulo
explico por que isso ocorre*.

Agora vejamos a aplicação dos particípios:

**Myohrsen land –** país esquecido *(ou terra esquecida)*

**Uno sneigvcovohrn calive –** uma cabine coberta de neve

**Un calive covohrno med un tenu sneigvlyegher –** Uma cabine coberta
com uma fina camada de neve

CONJUNÇÕES E OUTRAS PALAVRAS INVARIÁVEIS
========================================

Tanto neste capítulo quanto no capítulo seguinte, o das preposições, eu
vou trabalhar no seguinte modo: eu vou mostrar algumas traduções diretas
das palavras, mas nos exemplos eu poderei representar essas palavras
invariáveis do sambahsa através de palavras ou expressões da língua
portuguesa que eu não mostrei na tradução direta. Como assim? Eu vou
falar sobre a palavra invariável “yeji”, que na tradução está como “de
acordo”, mas eu não usei essa expressão nos exemplos! Fiz isso
propositalmente com o objetivo de mostrar-lhe que o que você tem que
aprender são as ideias por trás das palavras, não as suas traduções
diretas! Isso não vale só para o sambahsa, mas sim para qualquer outra
língua que você for aprender.

Não se assuste com o tamanho do capítulo, eu apenas procurei colocar
*todas* as palavras invariáveis possíveis aqui, de forma que esta
gramática seja um bom material de consulta.

**Aiw(o) {Äw(o)} –** jamais *(sem negação)*, sempre, já

**Ays kays eet id bellst quod is hieb aiwo vis –** o cabelo dela era o
mais bonito que ele já tinha visto

**Est stragn od tod ilaj aiw hieb esen usen –** é estranho que este
tratamento jamais tenha sido usado

Como substantivo, “aiwo” significa “era”, como em “Petraiwo”, que
significa “Idade da Pedra”.

**Id memorandum comprindt oino iom meist exhaustive playcts aiwo signen
ab bo lands, markend uno major wehnd in ira relations –**
[]{#anchor-96}O memorando abrange um dos acordos mais abrangentes jamais
assinados entre os dois países, marcando um ponto de virada nas suas
relações

**Id khakst film aiwo –** o pior filme jamais feito / o pior filme de
todos os tempos

**Agar {agAr} –** se, em caso de

**In 2005, id Chinese Parlament hat widen un "anti-secession" leg
autorisend silahneud agar Taywan declariet sien independence –** em
2005, o Parlamento chinês aprovou uma lei "anti-secessão", que autoriza
o uso da força em caso de Taiwan declare sua independência

**Albatt(a) {albAt(a)} – **de fato, realmente

**Albatt sambahsa ne est id meist facil bahsa –** de fato sambahsa não é
a língua mais fácil

**Als {als} –** mais *(preste atenção como o “mais” é utilizado)*

**Kwe semanghen als volt anon? –** alguém mais quer comida?

**Also {Also} –** além disso *(introduz outra frase, outra ideia)*

**Ne ho denars kay safer, also tehrbo studye –** não tenho dinheiro para
viajar, além disso tenho que estudar

**Maria ghi hieb neid magho kay lises ments. Also ia contentit-se med
abgires ed chehxe proscher cada face –** Maria, na verdade, não tinha
poder para ler mentes. Então ela contentou-se em virar-se e olhar, mais
de perto, cada rosto

**Is fauran mohn de stuppe sieno nas; also sternues ne lambh iom –** de
repente, ele pensou em parar o nariz; além disso, ele não foi pego por
espirros

**Amalan {amalAn} –** praticamente

**Ti bell animals sont amalan extinct –** esses belos animais estão
praticamente extintos

**An {an} –** se *(introduzindo uma interrogação, uma dúvida)*

***Ne woidim an poitto drehnke hoyd –** não sei se posso beber *(bebida
alcoólica)* hoje*

***Ia ihsit ad vide an ays wogh hieb likwt id reparation service –** ela
foi ver se o seu carro saiu do conserto*

***Daumo an i insects tant kame plukes ambh kiers –** me pergunto se os
insetos gostam tanto de voar em volta de velas*

**Anter {Antër} –** em vez, prefira

**Sieycst “torche”, bet kad eiskws sayge anter “torte” –** você disse
“tocha”, mas talvez você queira dizer “torta” em vez disso

**Api {Api} –** ora, mas, agora *(para introduzir um contra-argumento)*

**Est saygen od pisks ne maghe ses daht im leuds, i dehlge bihe bedarst
ad piskes, api leuds ne hant access ei fluv –** dizem que peixes não
pode ser dados às pessoas, deve-se ensiná-las a pescar, mas o povo não
tem acesso ao rio

**Gulf lands eiskwnt conserve ir teutisk ed religieus traditions. Api,
id interpretation ios Coran ed ia reuls in gwis ios scharia
inferioreihnt gwens dia wirs –** países do Golfo querem conservar as
suas tradições tribais e religiosas. Ora, a interpretação do Alcorão e
as regras em vigor da Sharia fazem as mulheres inferiores em relação aos
homens

**Aproposs {apropOs} –** a propósito

**Dank ob aurdhens id meja. Aproposs, kwohrst tien almentask? –**
obrigado por arrumar a mesa. A propósito você fez o seu dever de casa?

**Som un serieus christian ed, aproposs, som gai. Sem leuds pohnd
difficil ghabe to –** sou um cristão sério e, a propósito, sou gay.
Algumas pessoas acharam difícil compreender isso

**Ar {ar} –** para, porque *(usado quando o “ghi” não é conveniente)*

**Kartvelia est ayt un perodhkala os Occident ar, pos dwo secules os
russiano militaro presence, Moskva hat dohlgen evacue in 2005 sien
sensta militar bases –** []{#anchor-97}Geórgia (o país caucasiano) é
considerado um bastião avançado do Ocidente para, depois de dois séculos
de presença militar russa, Moscou teve de evacuar em 2005 suas últimas
bases militares

Perceba que o uso do “ghi” não seria prático por causa do longo
complemento “pos dwo secules os Russiano militaro presence”.

**Arasih {arazI:} –** acidentalmente

**Arasih sorbim un bei –** acidentalmente eu engoli uma abelha

**Au {Ao} –** ou *(separa substantivos, adjetivos e verbos)*

**Dah ei un apel au un vinber –** dê a ele uma maçã ou uma uva

**Aus {Aos} –** cedo

***Is gwohmit aus –** ele chegou cedo*

**Autah {aotA:} –** ou *(é apenas mais forte do que “au”)*

***China est tienxia dwot plautsto Stat yeji id land superficie, autah
trit au quart plautsto yeji id total superficie, sekwent id meid methode
–** A China é o segundo maior país do mundo em massa de terra, ou (caso
contrário), terceiro ou quarto maior de superfície total, seguindo o
método de medição*

**Auti {Aoti} – **ou também, ou mesmo

***Tod militar potential ghehdt ses nudt eni id quader om UNO au NATO
operations, auti ob id maidehsa om certain lands au organisations –**
este potencial militar pode ser usado no âmbito das operações da ONU ou
OTAN, ou mesmo / também por causa do pedido de ajuda de determinados
países ou organizações*

**Bad {bad} –** finalmente, afinal

**Bad gwahsiem hem –** finalmente irei para casa

**Ia gwenak me spohc med sien okwi meg-ye ghyanen ob staunos, pre bad
sprehge –** a jovem olhou para mim com seus olhos bem abertos de
espanto, antes de finalmente perguntar

**Ne … bad {ne … bad} –** ainda não

**Ne ho bad perichohxen id hol Sambahsa-Portughesche kamus –** ainda não
chequei completamente todo o repertório de dicionário do
sambahsa-português

perichohxen = peri + chohxen (pretérito de “chehx”)

**Bariem {baryEm} – **pelo menos, de toda maneira

**Som orm, bet bariem weiko in un riche land –** sou pobre, mas, pelo
menos, vivo num país rico

**Besonters {bezOntërs} –** especialmente, principalmente

***Kamo magvi, besonters i tamijdars –** gosto de crianças,
especialmente as bem-educadas*

Extra: “essas bem-educadas” poderia ser “ti tamijdar”, porque o “ti”
substitui o substantivo.

**Bet {bet} –** mas

**Ho un bell gvibh, bet ne som noroct –** tenho uma bela esposa, mas não
sou feliz

**Bfuyi {bfUyi} –** repetidamente, para sempre, eternamente

**El Manticore henslit iom bfuyi –** a Mantícora não parou perturbá-lo

**Biadet {byAdët} –** normalmente, usualmente, geralmente

**Biadet eihm hem jumas –** normalmente vou para casa às sextas

**Bilax {bilAks} –** pelo contrário, ao contrário

**Ne orbato con bandits, bilax, tik orbato con honeste leuds –** não
trabalho com bandidos, pelo contrário, só trabalho com gente honesta

**(Bil)hassa {(bil)hAsa} –** na maioria das vezes, geralmente

**Bilhassa ghango in id forest nocts –** na maioria das vezes eu caminho
na floresta a noite

**Cadadien {kadadyEn} – **diariamente, todo dia

**Cadadien puwno id dom –** limpo a casa diariamente

**Casu quo {kAzu kwo} – **se necessário, em caso de necessidade, se for
caso disso

**Casu quo, pehrnsiem id dom, men neti sessiem makrouse –** se for
preciso, venderei a casa, mas não ficarei mais endividado

**Chiowdeo {tcyowdEo} –** exatamente

**Quod volst sayge chiowdeo? –** o que você quis dizer exatamente?

**Chunke {tcunk} –** desde *(condicional)*

**Chunke yu xeihte id magh os kyukes iom Mighelekwo, ne ghehdiete yu
sprehge iom quer wehst eys poti? –** Desde que você mantenha o poder de
convocar o Cavalo da Névoa, você não poderia lhe perguntar onde está seu
mestre?

**Circa {tsIrka} –** cerca de, aproximadamente

**Is est circa dwo meters buland –** ele tem cerca de dois metros de
altura

**Com(samen) {kom(sAmën)} –** juntamente

**Hovesper, sessiemos com –** esta tarde estaremos juntos

**Daanistah {daanista:} –** intencionalmente

**Daanistah brohgim id machine –** quebrei a máquina intencionalmente

**Dalg {dalg} –** longe

**Weiko dalg –** moro longe

**Dalger {dAldjër} –** mais adiante, mais a frente *(sentido espacial,
nunca temporal)*

**Tetos dalger, vidsies un phar –** dali mais adiante você verá um farol

**Seghlim dalger kay trehve id noroc **– naveguei mais longe para
encontrar a felicidade

**Dar {dar} –** ainda *(geralmente usado no meio das frases, atrás de um
verbo. Veja o exemplo abaixo para você não confundir com o “yed”)*

**Quan gwahm lict, mien mann dar est wehrgend –** quando vou pra cama o
meu marido ainda está trabalhando

**Dat {dat} – **dado que

**Dat id noct hieb gwohmen, is Marjban ess oin lembas pre swehpe –**
dado que a noite chegou, o guardião (da Terra Média de J. R. R. Tolkien)
comeu um lembas antes de dormir

**Dat is hieb nia denars kay tules un eustwort porm, Tosten dohlgit
linkwes id urb –** []{#anchor-98}Dado que ele não tinha dinheiro para
pagar uma tarifa para o leste, Tosten teve que deixar a cidade

**Daydey {daydEy} – **em geral

**Daydey ia semens teukent quan (sont) madhen –** de um modo geral as
sementes germinam quando (elas são) molhadas

Obs.: neste caso, escrever apenas “…quan madhen” não é gramaticalmente
errado, mas é preferível que se coloque o verbo no meio para fins de
clareza

**Dayim {dayIm} –** repetidamente

***Is fabric orbater premt ia scruvs dayim –** o operário aperta os
parafusos repetidamente*

É sinônimo de “bfuyi”

**Dexios {dEksjos} –** rapidamente, agilmente, vigorosamente,
energicamente

**Id Caroline snohg dexios inter ia enflammen vracks ed intercepit oino
iom feugend vecels –** o Caroline (um navio de guerra) esgueirou-se
habilmente entre os destroços flamejados e interceptou um dos navios de
fuga

**Dind {dind} –** depois, então, em seguida

***Gwahsim id mercat, dind gwahsim kyrk –** fui ao mercado, depois fui à
igreja*

**Iran speht ithan crisces sien financial ressurces –** O Irã espera
então aumentar seus recursos financeiros

É sinônimo de “poskwo” e “pos to”

**Diu {dyu} – **muito tempo

**Unte baygh diu, ho esen her –** durante muito tempo, estive aqui

**Diutos is est un prisoner – **há muito tempo que ele é um prisioneiro

**Diuper {dyÜpër} –** muito tempo atrás

**Diuper ia gwivit in mien dom –** muito tempo atrás ela viveu na minha
casa

**Ed {ed} –** e

**Io ed mien son –** eu e meu filho

**Entrim {Entrim} –** nesse ínterim, enquanto isso

**Vahm soke discret-ye, bet entrim, to dehlct remane inter nos –** vou
investigar às escondidas, mas enquanto isso, isto precisa ficar entre
nòs

**Esdi {Ezdi} –** mesmo se, mesmo que, ainda se, ainda que

**Ne weiko in tod dom esdi i payghent me –** não moro nessa casa mesmo
que me paguem

**I men Chineses name ir land Zhōngguó, quo hat dahn id Sambahsa
Giungkwok, esdi tod nam est neter nudt in id official nam ios Popules
Respublic China, ni in tod ios Respublic China (Taywan). China de gwehmt
ex id nam ios megil cesar Qin Shi Huangdi (246-210 pre JC)… –** os
chineses chamam seu país de Zhōngguó, o que deu ao sambahsa “Giungkwok”,
mesmo se este nome não seja usado na República Popular da China e nem no
nome da República da China (Taiwan). “China” vem do nome do grande
imperador Qin Shi Huangdi (246-210 A.C.)…

Se você não entendeu o uso do “… men (…) de …”, no capítulo “Enclíticos
e Proclíticos” existe uma explicação.

**Eni {Eni} –** dentro

***El kwaun est eni id dom –** o cachorro está do lado de dentro da
casa*

**Eti {Eti} –** além disso, ademais

**Ne eddo hamburger, eti som vegetarian – **não como hambúrguer, e além
disso sou vegetariano

**En id plan ios edifice. Tod plan est ja veut. Kad id edifice hat est
modifiet mulayim-ye tuntos. Eti khact ses vis ep id an fulan dwer est
ghyanen au cluden –** aqui está o plano do edifício. Este plano já está
velho. Talvez o edifício tenha sido ligeiramente modificado desde então.
Além disso, não pode ser visto nele se qualquer porta está aberta ou
fechada

**Tod vestibule compris eti dwo dwers, uter wester, alter euster –** o
hall de entrada comportava ainda mais duas portas, uma para o oeste, o
outro para o leste

**Exo {Ekso} –** do lado de fora

**El kwaun est exo id dom –** o cachorro está do lado de fora da casa

Podemos dizer também: “el kwaun est exter id dom”

**Fauran {faorAn} –** imediatamente

**Kehrzsiem mien kays fauran, iey(it) is orbater ei bes –** cortarei meu
cabelo imediatamente, disse o trabalhador ao patrão

**Filan {filAn} –** este ou aquele

**Ia religieus autoritats hant tolcto kay woide kweter filan buk ios
Bible eet we ne inspiret ab Div –** as autoridades religiosas têm
discutido para saber se este ou aquele livro da Bíblia foi inspirado por
Deus ou não

**Fujatan {fujatAn} –** de repente

**Fujatan eem trigimat –** de repente eu tinha trinta anos

**Gairn {gärn} –** de boa vontade

**Is commander reservit id access ibs “inner kyals” (kam is gairn kiel
ia) sibswo –** o comandante reservou o acesso às “salas internas” (como
ele gostava de chamá-las) para se mesmo

**Gontro {gOntro} (gon + tro) –** para o lado *(advérbio de movimento)*

**Reusch gontro! –** Correr para o lado

**Per noroc is ekwos ios hussar, se kheissend protietragen ghomtro ab id
ansia is colonel dier, movit gontro, ed it id longo miech ios saber os
gwaur cavalerie os Fabrice slid engwn id vest ios hussar ed passit alnos
sub eys okwi –** Por acaso o cavalo do hussardo, sentiu-se puxado para
baixo pela rédea que o coronel estava segurando firmemente, moveu-se
para o lado, e, desta forma, a longa lâmina de sabre de cavalaria pesada
de Fabrício deslizou ao longo do vestido do hussardo e passado pouco
abaixo dos seus olhos

**Ghi {gi} –** *sem significado definido, essa palavra frequentemente
aparece em segunda posição numa oração e serve para enfatizar a palavra
anterior. Algumas vezes é sufixado ao advérbio ou pronome anterior. Pode
ser traduzido como “então”, “por conta disso”, “porque”.*

**Is ne kieup id wogh, isghi ne hieb denars –** ele não comprou o carro
porque ele não tinha dinheiro

**Eiskwo woide id ghi payghen pris –** quero saber o preço realmente
pago

**Id probleme tom prabhils est od pauk ghi anghens brunge ia –** o
problema dessas regras é que elas, na verdade, beneficiam poucas pessoas

**Gwaru {gwAru} –** severamente, gravemente *(se referindo a um
ferimento)*

**Id ecosysteme os Amazonia buit gwaru taraght ab illegal reuydens –** o
ecossistema da Amazônia foi fortemente perturbado por explorações
ilegais

**Hatta {hAta} –** mesmo, até

**Hatta i smulkst aranks ghehde nices –** Até as menores aranhas podem
matar

**Pedro ne hat hatta smautert tod film –** Pedro nem viu esse filme

**Tu hatta ghyienst id hadia –** você nem abriu o presente

**Hatta habend-ye piten, ne kamyabim –** mesmo tendo tentado, eu não
consegui

**Magho sayge od hatta Suomi hat falls os foll gwow siuge –** posso
dizer que até a Finlândia tem casos de vaca louca

**Hakan {hakAn} –** realmente, verdadeiramente

***Is hakan lieubht te –** ele realmente te ama*

**Iawod {yawOd} –** esperar que

**Hol grupps bihnt autoriset ad page, conservend ir bahsa, ir mores, ir
social organisation iawod obedeihnt ia loys ios Roman Stat –** grupos
inteiros ficaram autorizados a instalar-se, conservando sua língua, seus
costumes, sua organização social, desde que obedeça às leis do Estado
Romano

**Ib {ib} –** para que não, com receio de

**Eti, un ieuster daysa iom opnos tehrbiet bihe instohlen in multa
lands, ibo vide udbrehge grave social troubles –** além disso, uma
partilha mais justa da riqueza tem que ser implementada em vários
países, para que não sejam vistos problemas sociais graves

**Ilhali {ilhAli} –** enquanto que, ao passo que

**Ilhali id recognition ios inherent decos vasyims members ios
menscfamilia ed iren egal edinalienable rects constituet id sul om lure,
justice ed pace tienxia –** considerando que o reconhecimento da
dignidade inerente a todos os membros da família humana e seus
inalienáveis direitos iguais constituem a fundação da liberdade, justiça
e paz em todo lugar.

**Inkaptos {inkAptos} –** do começo, a partir do início

**Sayg mi quanto wakyit inkaptos –** conte me tudo o que aconteceu desde
o começo

**Intant {intAnt} –** enquanto isso, ao mesmo tempo, nesse meio tempo

**Bet intant, kwehr quodlibt –** mas enquanto isso, faça o que quiser

**Iter {Itër} –** de novo, outra vez, novamente

**Som con mien família iter –** estou com minha família outra vez

**In unisson {in unisOn} –** em uníssono

**I brasileirs obswihr is corrumepen president in unisson –** todos
juntos como uma única pessoa, os brasileiros vaiaram o presidente
corrupto

**Ja {ja} –** já

**Tu biscripst todeghern ed tu ja finihst id wehrg –** você começou a
escrever esta manhã e já terminou o trabalho

**Jaldi {jAldi} –** rapidamente, o mais rápido *(passa noção de
velocidade)*

**Ia cursit meg jaldi quando ia eet yuner –** ela corria muito rápido
quando mais nova

**Id nivell ios wed est steighend jaldi –** o nível da água está subindo
depressa

**Ka {ka} –** como (um(a)) *(quando se referindo a uma qualidade)*

**Tod werd wehrct ka adjective ed adverb –** essa palavra se comporta
como um adjetivo e um advérbio

**Kad {kad} –** talvez, pode ser que

**Kad wehdsiem iam –** talvez eu me case com ela

**Kafi {kAfi} –** o bastante, suficiente

**Sat {sat} –** o bastante, suficiente

**Ho edden kafi –** comi o suficiente

**Kam {kam} –** como?, em que maneira?, como

**Kam has tu arriven her? –** como você chegou aqui?

**Kam leits tu? –** como vai você?

Som kam tu – sou como você

**Kam adet {kam Adët} –** como de costume

**Kam adet ia oisbud aus –** ela acordou cedo, como costume

**Kamsei {kamsEy} –** como se

**Is ee-sispehct me kamsei is esiet/eet un lion sispehcend un owu –**
ele olhava para mim como se ele fosse um leão olhando uma ovelha

**Kariban {karibAn} –** daqui a pouco, em seguida

**Linkwsiemos (hetos) kariban –** sairemos daqui a pouco

**Kathalika {ka§alIka} –** da mesma forma, do mesmo modo

**Tony ed Sandro sont kerabs, kathalika sont Otavio ed Clarissa –** Tony
e Sandro são parentes, da mesma forma são Otávio e Clarissa.

**Kay {kay} –** para, para que, em ordem para

**Gwahsiem weir kay defende mien land –** irei para a guerra para
defender meu país

**Eiskwo un wogh kay safer –** quero um carro para viajar

**Khaliban {qalibAn} –** principalmente

**Is orbietit khaliban ka swobod orbater –** ele trabalhava
principalmente como freelancer

**Kheptenn {qëptEn} –** completamente, definitivamente

**Is kheptenn est gai –** ele definitivamente é gay

**Kjiawxieng {kjyawksyEng} –** por acaso

**Kjiawxieng kwe has cigarettes? –** por acaso você tem cigarros?

**Kongcio {kOngtsyo} –** doravante, de agora em diante, em direção ao
futuro, a partir de agora

**Kongcio sessiem un gohd pater, promitto –** daqui pra frente eu serei
um bom pai, eu prometo

“Nuntos” tem o mesmo significado e geralmente é muito mais usado, já que
o “kongcio” aparenta ser antiquado.

**Kwecto {kwEcto} –** aparentemente, ao que parece, como parece

**Id phial, quei poskwo dahsim mien attention, kwecto pwolpohld med un
cruorrudh liqueur –** o frasco, ao qual eu voltei a minha atenção,
poderia ter sido meio cheio de um licor de sangue vermelho

**Hatta eys prientias kwecto kihr ep un samliko catholicitat os suabuhsa
–** mesmo as amizades dele pareciam fundadas em uma catolicidade
semelhante de boa natureza

**Kweid {kweyd} –** mesmo se, mesmo que, ainda se, ainda que *(passa a
ideia de: é necessário que, ao preço de)*

**Iaschi EU lands maghe bihe tenten ab bringhes wahid-ye securitara
responses ei probleme os terrorisme, kweid biht limitet id bungos iom
civil lures –** os próprios países da UE podem ser tentados a introduzir
respostas de segurança apenas ao problema do terrorismo, ainda (ao preço
de) que a função das liberdades civis seja limitada

**Kweter {kwEtër} –** *muito parecido com “an”, mas se refere a uma
escolha ou dúvida entre duas opções *

***Ay buit impossible tarctum kweter ia hieb vanien in id hava we ia
hieb curren baygh oku in id bosc***** **(“tarctum” é a opcional forma
antiquada do infinitivo)** – ***era impossível para ela supor se ela
desapareceu no ar ou ela tinha corrido muito rápido no bosque.*

***Quo ia druve-ye gnohskwit eet kweter el stohng we ne –** o que ela
realmente quer se ele* (um inseto) *pica ou não*

**Lakin {lakIn} –** entretanto, contudo, todavia, ainda assim

**Kamo te, lakin tu dehlcs change –** gosto de você, entretanto você
deve mudar

**Libter {lIbtër} –** de bom grado, com prazer

**Libter kwehrsiem tod pro te –** com prazer farei isso para você

**Lika {lIka} –** semelhantemente, parecido

**Daydey, ho piten vergihes werds qua swehnient pior lika alyi –** em
geral, tenho tentado evitar as palavras que soariam bastante do mesmo
modo que outra

**Makar {makAr} –** por mais que

**Makar ei saygo, is ne kaurt de –** por mais que lhe diga, ele não faz
caso

**Makar io ielgv denars, na kwahsim spare –** por mais dinheiro que
ganhasse, não conseguia poupar

**Makar kauro, ne kwahm vergihes howkscheces –** por mais cuidado que
tenha, não consigo evitar os buracos da estrada

**Makhsus {mAqsus} –** propositalmente, intencionalmente,
deliberadamente, com o propósito de

**Makhsus ho scriben id texte samt erros –** escrevi o texto com erros
de propósito

**Mathalan {ma§alAn} –** por exemplo

**Kamo aw fantasia buks, mathalan “Is Lytil Prince” ed Lovecrafts buks
–** gosto de livros antigos de fantasia, como por exemplo “O Pequeno
Príncipe” e livros de Lovecraft.

**Meist-ye {meyst ye} –** no máximo

**Eiskwo meist-ye dwo purts, ne meis quem to –** eu quero, no máximo,
dois filhos, não mais do que isso

**Men {men} –** mas

**Men, weidwos, to ne est tien fault –** mas (além disso), é claro, isso
não é culpa sua

**Menxu {mEnksu} –** enquanto

**Eem in alyo land menxu mien land eet invaden –** eu estava em outro
país enquanto meu país foi invadido

**Minst-ye, lytst-ye –** pelo menos *(“tehrb” também pode ser usado
aqui)*

**Naudhsies minst-ye six hevds kay plane adequat-ye –** você precisará
de, pelo menos, seis semanas para planejar adequadamente

**Mudam {mudAm} – **constantemente, continuamente

**Myen machine orbat mudam –** minha máquina trabalha sem parar

***Id seuy fallt mudam – **a chuva cai constantemente*

**Mutlak {mutlAk} –** absolutamente

**Ia est mutlak khiter –** ela é absolutamente má

**Mox(u) {mOks(u)} –** breve, logo

**Vidsiem te mox –** verei você em breve

**Naiw(o) {nÄw(o)} –** nunca

**Naiwo likwim mien land –** nunca sai do meu país

**Naturelika {natürëlIka} –** naturalmente

**Naturelika kamo uces bahsas –** naturalmente que eu gosto de aprender
idiomas

**Nepunei {nepÜney} –** com impunidade, impunimentemente

**“Niem permitte od quoy serve iom Demon-Roy safernt nepunei unte mien
land, Castelian”, grohm Beruh –** “não permitirei que aqueles que servem
o Rei Demônio viagem sem punição pelo meu país, Casteliã”, rugiu Berú.

**Neti {nEti} –** não mais, nunca mais

**Neti eddo her –** nunca mais como aqui

**Nib(o) {nIb(o)} –** a não ser

**Orbatsiem in id farm nibo kamyabo in un public concurs – **trabalharei
na fazenda a não ser que eu tenha sucesso em um concurso público

**Nisbatan {nisbatAn} –** relativamente

**Ia ruines sont nisbatan salver quem id forest –** as ruínas são
relativamente mais seguras do que a floresta

**Nun {nun} –** agora

**Som noroct nun –** estou feliz agora

**Nundiens {nundyEns} –** hoje em dia, atualmente

**Nundiens, leuds sont suagramat –** hoje em dia, as pessoas são bem
alfabetizadas

**Nuper {nÜpër} – **recentemente

**Gnahsit nupe –** (ele/ela) nasceu recentemente

**Nuptos {nUptos} –** não muito tempo atrás

**Nuptos mien dugter ghiemt –** há não muito tempo atrás minha filha se
casou

**Oku {Oku} –** depressa, rapidamente

**Gwehm oku –** venha depressa

**Ne ghehdo antwehrde tib oku –** não posso te responder por agora

**Okwivid-ye {okwivId-ye} –** obviamente

**Okwivid-ye i ne surviveer –** obviamente eles não sobreviveram

**Payn {payn} – **dificilmente

**Payn kwahsiemos fuges –** dificilmente conseguiremos fugir

**Perodh {perOd} –** para a frente, adiante

**Ghango perodh –** ando para a frente

**Plus {plus} –** mais (+), adicional

**Dwo plus dwo est egal ad quar –** dois mais dois é igual a quatro

**Ho addihn plus mathmoun ad mien buk –** adicionei mais conteúdo ao meu
livro

Nesta última frase você pode usar o “meis” no lugar do “plus” se quiser

**Poskwo {pOskwo} –** depois, posteriormente, após

**Ghamsiemos poskwo stajernsiemos un dom –** nos casaremos e após isso
alugaremos uma casa

**Prevst {prevst} –** uma vez (no passado)

**Visim un fee prevst –** eu vi uma fada uma vez (no passado)

**Protiapo {protyApo} –** contra a corrente

**Snahm protiapo –** nado contra a corrente

**Punor {punOr} –** por outro lado

**Habe purts est gohd, punor dehlcs dedie tien hol gwiv pro i –** ter
filhos é bom, por outro lado você deve dedicar toda a sua vida para eles

**Quayque {kwAyk(ë)} –** embora, apesar de

**Eddo mult leckereits, quayque som diabetic –** como muitos doces,
apesar de eu ser diabético

**Quasi {kwAzi} –** quase

**Quasi mohrim honoct –** quase morri esta noite

**Quodlibt {kwOdlibd} –** qualquer um/coisa que você gosta/quer

**Vols tu un orange, un banana au un mankay? Quodlibt – **você quer uma
laranja, uma banana ou uma manga? Tanto faz

**Quoterlibt {kwOtërlibd} –** *o mesmo que “quodlibt”, mas se trata de
uma escolha entre duas opções*

**Vols tu un orange au un mankay? Quoterlibt – **você quer uma laranja
ou uma manga? Tanto faz

**Saat-ye {saAt ye} –** no sentido horário

**Id wogh gwaht saat-ye –** o carro corre no sentido horário

**Sammel {sAmël} –** ao mesmo tempo

**Nies purts vanier sammel –** nossos filhos sumiram ao mesmo tempo

**Sat {sat} –** o bastante, suficientemente

**Essim sat – **comi o suficiente

**Schawxwen {xAwkswën} –** []{#anchor-99}momentaneamente

***Eem schawxwen dusiht –** estive momentaneamente tonto*

**Schowi {cOwi} –** portanto

**Id institutional division iom maghs est schowi necessar iri mutual
control –** a divisão institucional dos poderes é consequentemente
necessária para o seu controle mútuo

**Sei {sey} –** se *(introduzindo uma condição ou suposição)*

**Sei seuyt, mansiem domi –** se chover, ficarei em casa

**Kaupsiem tien hadia, bet sei tien pater ne payght mi, cheidsiem con
iom –** comprarei seu presente, mas se seu pai não me pagar, brigarei
com ele

**Sekwent {sEkwënt} –** segundo, de acordo a

**Sekwent ids Constitution, official bahsa Ukraines est ukrainsk –** de
acordo a sua constituição, a língua oficial da Ucrânia é o ucraniano

***Sekwent id Tyrk statistic institut, id population ios landios mikdier
74,7 millions leuden in 2011 –** segundo o instituto de estatística
turca, a população do país somou 74,7 milhões de pessoas em 2011*

**Seni {sEni} –** à parte, separadamente

**Crohscim seni ud mien braters ed swest**ers – cresci separado dos meus
irmãos e irmãs

**Ser {ser} –** seriamente *(quando se refere a ferimentos,
machucados…)*

***Buim ser vurnt unte id accident –** eu fiquei gravemente ferido(a)
durante o acidente*

**Serter {sErtër} –** mais tarde

**Wano wakt nun, ghehdsiemos vide mutu serter –** preciso de um tempo,
mais pra frente a gente se vê

**Shayad {xayAd} –** provavelmente

**Credeihm od ne ter sessient meis large ubnuwa nuntos. Shayad naudhsiem
kaure tik de id werdskaut capitel ed adverbs –** acredito que não haverá
mais grandes atualizações a partir de agora. Provavelmente precisarei me
preocupar apenas em relação ao capítulo de vocabulário e advérbios

**Sigwra {sIgura} –** seguramente, certamente

**Sigwra eucsiem sambahsa –** seguramente aprenderei sambahsa

**Sonst {sonst} –** senão, de outra forma, caso contrário

**Is ne hat daken vies message, sonst habiet gwohmt –** ele não recebeu
sua mensagem, caso contrário teria vindo

**Sontern {sOntërn} –** mas *(depois de uma negação)*

**“Ne ho saygen od neid est gohder,” jawieb is Roy, “sontern od neid est
meis lecker –** “eu não disse que nada é melhor”, respondeu o rei, “mas
que nada é mais delicioso”

**Stayg {stayg} =** fujatan *(mas este também pode ser usado como
adjetivo)*

**Strax {straks} =** fauran

**Tadrijan {tadrIjan} –** gradualmente, pouco a pouco

**Tadrijan i beis construgent ir alvey –** aos poucos as abelhas
constroem sua colmeia

**Taiper {tÄpër} –** atualmente, presentemente

**Taiper id mund est baygh dangereus –** atualmente o mundo está muito
perigoso

**Takriban {takribAn} –** quase, aproximadamente

**Ho takriban penkwe milliards in id bank –** tenho quase 5 bilhões no
banco

**Tan(do) {tAn(do)} –** contanto que, sob a condição de

**“Ghehdo te hehlpe ad kwehre id sam”, is iey “tan es asli-ye zakir” –**
“eu posso te ajudar a fazer o mesmo”, disse ele, “contanto que você seja
realmente piedoso”

**Tienxia {tyEnksya} –** em todo lugar no mundo

**Tienxia est khitert, betschi tienxia est karam –** em todo lugar há
maldade, mas também em todo lugar há bondade

**Tik {tik} – **somente, apenas

**Som tik octat –** tenho apenas oito anos

**Towsraen {towsrAyn} –** ao acaso, aleatoriamente

**Ia numers prehpent towsraen –** os números aparecem aleatoriamente

**Tsay {tsay} –** de novo, mais uma vez, novamente, de volta, para trás,
atrás

**I peinds gwehment tsay –** os inimigos estão vindo novamente

**Tun {tun} –** então, naquele momento

**Tun el magv visit un piurneus serpent ep id charagah –** então a
criança viu uma cobra de fogo sobre a grama

**Wa {wa} –** ou *(para indicar nomes alternativos)*

**Feira de Santana wa “Princesse Urb” est id second mierst urb os Bahia
–** Feira de Santana ou “Cidade Princesa” é a segunda maior cidade da
Bahia

**Wakea {wakEa} –** definitivamente

**Is est wakea is meist preparet **– ele é definitivamente o mais
preparado

**Way {way} –** infelizmente, lamentavelmente, lastimavelmente

**Way ia mohrit –** infelizmente ela morreu

**We {we} –** ou *(separa orações)*

***Ne woidim an kaupo un wogh we io safer do Tyrkia –** não sei se
compro um carro ou viajo para a Turquia.*

**Weidwos {wEydwos} –** é claro, naturalmente, certamente

**Weidwos sambahsa est facil –** é claro que sambahsa é fácil

**Yadi {yAdi} –** se ao menos, quem me dera

***Yadi Jorge hieb esen perodhsedd, hol esiet different –** se somente
Jorge tivesse sido presidente, tudo seria diferente.*

**Yani {yAni} –** isto é, quer dizer que *(para trazer uma precisão)*

**Babys sont pur, yani, i ne hant synt –** bebês são puros, isto é, eles
não têm pecado

**…i hant neid synt –** eles não têm nenhum pecado

**(Ya)schi {(yA)ci} –** também, além disso *(“schi” pode ser sufixado a
um pronome, a um artigo ou a qualquer outra palavra invariável se ela
for foneticamente compatível e se é a palavra concernida pela
repetição)*

**Iaschi buit aunstohmen ad mohrt –** ela também foi condenada a morte

**Cavalls yaschi ghehdent ses usen ka transport forme – **cavalos podem
ser usados como meio de transporte também

**Yed {yed} –** contudo, no entanto

**Yed, is postalion ne gwohmit –** contudo, o carteiro não chegou

**Yeji {yEji} –** de acordo

**Yeji id Traiteit os Amritsar, is Radja os Jammu, Gulab Singh, bihsit
is nov wanak os Kashmir –** nos termos do tratado de Amritsar, o Ralah
de Jammu, Gulab Singh, tornou-se o novo governante da Caxemira

**Strehcend uper circa 9,6 millions km², China est tienxia dwot plautsto
Stat yeji id land superficie –** abrangendo um tanto mais de 9,6 milhões
de km², a China é o segundo ou terceiro maior país do mundo de acordo
com a superfície terrestre

**Yeji Einstein, dwo jects sont aunfin, id universe ed mensc cacavania
–** para Einstein, duas coisas são infinitas, o universo e a estupidez
humana

Se você quiser dizer uma frase com “por mim” no sentido de desejo, como
em “por mim ele seria demitido”, você deve usar uma expressão como “ye
mien desire/eiskwen/vansch”.

**Yunyun {yunyUn} –** e assim por diante

**Un cyclist safert 20Km in id prest hor, 16 in id second yunyun –** um
ciclista percorre 20Km na primeira hora, 16Km na segunda e assim por
diante

ALGUMAS DÚVIDAS QUE PODEM SURGIR
--------------------------------

Tentei explicar bem as conjunções nos exemplos, eu inclusive usei
exemplos muito parecidos para que as diferenças ficassem bem evidentes,
mas, ainda assim, podem restar algumas dúvidas. Neste subcapítulo eu
tento prever algumas delas e tento respondê-las.

### QUAL É A DIFERENÇA ENTRE “BET” E “MEN”?

Enquanto “bet”, o mais comum, apresenta uma real oposição, o “men” não
apresenta uma real oposição. Veja os exemplos abaixo:

**(1) Ne eddo leckereits, MEN ne som diabetic –** não como doces, mas
não sou diabético

**(2) Ia orbat in un kiekwrschop, MEN ia ne est veterinar –** ela
trabalha em um pet shop, mas não é veterinária

**(3) Kamo sports, BET ne som un athlete –** gosto de esportes, mas não
sou um atleta

**(4) Gwivs in id eust, BET eiskws gwive in id west –** você vive no
leste, mas você quer viver no oeste

Observe, em (1), se eu não como doces, não significa necessariamente que
eu seja diabético; em (2) a mulher trabalha em um pet shop, mas ela não
precisa ser necessariamente uma veterinária para trabalhar lá. Já em
(3), se eu digo que gosto de esportes, muita gente pode presumir que eu
seja um atleta; em (4) a pessoa vive no leste, então supõe-se que ela
queria viver lá, mas na realidade o desejo dela é outro.

Não se preocupe tanto, até porque a escolha entre o “men” e o “bet” é
muito *subjetivo*, alguns podem achar que se você trabalha num pet shop,
muito provavelmente você é um veterinário, assim teríamos que usar “bet”
em vez de “men” em (2). Em (3), se fosse em um lugar onde as pessoas
gostam muito de esportes, mas, mesmo assim, é comum que quase ninguém
pratique uma atividade desse tipo, o “men” poderia ser usado.

PREPOSIÇÕES
===========

Preposições são palavras que estabelecem uma relação, do tipo
subordinativa, entre dois termos de uma oração. A maioria das
preposições do sambahsa podem ser usadas como conjunções também. As
preposições são:

**Ab {ab} –** de *(após particípio passivo).* *Essa preposição se torna
“af” quando antes de palavra que inicie com “h”. Também pode significar
“a partir de”, “começando de”*

**Id torte buit edden ab me –** o bolo foi comido por mim

**Af hoyd –** a partir de hoje

**Ad {ad} –** para, no, na. *Muito útil quando o dativo não pode ser
usado.*

**Yeiso un blixbrev ad Henrique –** Envio um e-mail para Henrique

***Ia ihsit ad vide an ays wogh hieb likwt id reparation service –** ela
foi ver se o seu carro saiu do conserto*

**Ant {ant} –** em frente de, perante

**Ant id scol –** em frente da escola

**Ant id thron** – diante do trono

**Apo {Apo} –** fora. *Pode ser reduzido para “ap” quando antes de
vogais.*

**Grand Britain lyehct apo id Europay dayluk –** Grã-Bretanha
encontra-se fora do continente europeu

**Apter {Aptër} –** atrás

**Apter id dru –** atrás da árvore

**Apter iam mater –** atrás da mãe

**Id urb est apter id forest – **a cidade fica atrás/depois da floresta

**Aun {Aon} – **sem

**Som aun chifan –** estou sem refeição

**Bayna {bAyna} –** entre, no meio de *(no sentido de cercado(a) por)*

**Smos bayna i peinds –** estamos cercados por inimigos

**Id vallee est bayna ia ghyors –** o vale está entre as montanhas

**Is planit id competition bayna i daskals –** Ele planejou a competição
junto aos professores

**Bi {bi} –** perto do, no *(local de trabalho, loja)*, em *(trabalho de
um(a) autor(a))*, próximo de *(não implica proximidade espacial
permanente)*. *Essa preposição frequentemente se funde ao pronome nas
formas dativa e acusativa. bi + ei = bei; bi + ay = bay; bi + el = bil;
bi + im = bim.*

**Fruits sont kaupen bi mercat –** frutas são compradas no mercado

**Manskwo bi mien sokwi ep tod walu –** quero permanecer junto ao meu
companheiro neste campo de batalha

**Sei yed id dayi progress dehlct ses, bi Kant kam bi Condorcet, id
norme quod permitt judce Historia… –** Se, contudo, a ideia geral de
progresso deve ser, em Kant e em Condorcet, a norma que permite julgar
História…

**Bila {bIla} –** salvo, na ausência de

**Bila satisfacend communicationzariyas ed efficienta
transportsnetwehrgs, ia electoral campagnes sont difficil ductu –** na
ausência de meios de comunicação e de redes de transportes eficientes,
as campanhas eleitorais são difíceis de conduzir

**Cis {tsis} –** deste lado de

**Cis tod fluv –** deste lado do rio

**Con {kon} –** com *(no sentido de companhia)*

**Som con te –** estou contigo / estou com você

**Safersiem con iom –** viajarei com ele

**Weiko con mien esor – **vivo com minha esposa / moro com minha esposa

**Tolko con Olivier –** falo com Olivier

**Contra {kOntra} –** contra

**I katuer contra nos –** eles lutaram contra nós

**Circa {tsIrka} – **cerca de, por volta de, em torno de

**Is est circa dwo meters buland–** ele tem cerca de dois metros

**Dayir {dAyir} –** concernente, relativo a, a respeito de, acerca de,
referente a, tocante a

**Ia conflicts dayir wed bihnt schowi imminent –** os conflitos
relativos à água estão se tornando consequentemente iminentes

**De {de} –** sobre, quanto a

**Tod buk est de drus –** este livro é sobre árvores

**Dia {dya} –** rumo a, em direção a *(em um senso moral, figurativo)*

**Id majoritat os Kashmirs population esiet nuntos ryowkhowo dia
independence –** a maioria da população da Caxemira seria, doravante,
favorável à independência

**Som allergic dia penicillin –** sou alérgico à penicilina

**Do {do} –** em, para dentro, de

**Wey safersiemos do Espania –** nós viajamos para a Espanha

***Ho adapten sem reconstruct nams em antique hindeuropay divs do
Sambahsa – **adaptei alguns nomes reconstruídos dos antigos deuses
indo-europeus para o sambahsa*

**Due {dü:} –** devido a

**Due id seuy, khako linkwes mien dom – **devido a chuva, não posso sair
da minha casa

**En {en} –** eis, aqui está *+ acusativo*, aqui estão *+ acusativo*

**En id antwehrd –** eis a resposta / aqui está a resposta

**Ender {Endër} –** abaixo, debaixo *(não implica contato)*

**Ter est un kwaun ender id meja –** há um cachorro debaixo da mesa

**Ender nebhs sont id land ed id mar –** Debaixo das nuvens está a terra
e o mar

**Id stohm ender id nas –** a boca debaixo do nariz

**Endo {Endo} –** em direção ao interior

**El monster fugit endo antro –** o monstro fugiu para o interior da
caverna

**Engwn {Engun} – **ao longo de, junto a

**Tod permitt mette precis-ye engwn ia walls ed flors **– Isso permite
cortar com precisão ao longo das paredes e flores

**Eni {Eni} –** dentro

**Eni mien corpos ter sont vehrms –** dentro do meu corpo há vermes

**Ep {ep} –** sobre, em cima de *(toca o objeto no qual ele está em
cima)*. *Antes de “h” essa preposição tornasse “ef”, como se faz com o
“ab”*

**Ter est un rat ep id meja –** há um rato sobre a mesa

**Epter {Eptër} –** pouco mais de

**Ia dohm-se ep id bors om sien peds, ed, glanzend epter id leizdo ios
mur, ayso spect incontrit tod uns buland wir –** ela esticou-se na ponta
dos pés e espiou por cima da borda do muro, e seus olhos encontraram
imediatamente aqueles de um homem alto

**Ex {eks} –** fora, de, do, da

**Som ex Brasilu –** sou do Brasil

**Un helm ex stal –** um elmo de aço

**El kwaun gwehmt ex id dom –** o cachorro veio para fora da casa

Veja a diferença de “ex” e “exo”

***El kwaun est ex****o**** id dom –** o cachorro está do lado de fora
da casa*

Quando não dá para expressar a ideia de “feito de” ou “de tal lugar”, a
preposição “ex” passa uma ideia de movimento.

**In {in} – **em

**Som in id strad X –** estou na rua X

**Ghehdo bahe in dwodem bahsas –** posso falar em doze línguas

**Credeihm in fees –** acredito em fadas

**Instet {instEt} –** em vez (de)

**Instet swehpe, ma ne orbats? –** em vez de dormir, por que você não
trabalha?

**Inter {Intër} –** entre

**Ia bent est inter dwo geronts –** a garota está entre dois anciãos

***Id joyel eet inter ia colunns –** a joia ficava entre as colunas*

**Kata {kAta} –** a partir do topo de

**Mathalan, ia pinegs hangend kata id mur nieb id ogwn quanta tengier
gwiv –** por exemplo, os quadros pendurados na parede perto da lareira,
tudo parecia estar vivo

**Kye {kye} –** em direção a. *Essa preposição se mistura com o
determinante seguinte ou o pronome na 3ª pessoa, não é difícil encontrar
algo como “kyid” (kye + id).*

**Vahm gwahe kye mien dom –** vou para minha casa

**Is fallit kya dubes ios hayd –** ele caiu nas profundezas do inferno

**To ne est bad perfection, sontern to est un itner kye –** esta não é a
perfeição ainda, mas este é um caminho para isso

**Kyant {kyant} = kye + ant –** em direção a *(com intenção de
encontro)*

**Io vis iom gwehme kyant me –** o vi chegar ao meu encontro

**Med {med} –** com *(um instrumento)*

**Is magician construgit un castell med sien magia – **O mago construiu
um castelo com sua magia

**Saferam med wogh –** viajamos de carro

**Med alya werds –** ou seja; em outras palavras

**Medsu {mEdsu} –** no meio de

**Eduarda khiek sib stambhes glihes medsu sien dakrus –** Eduarda não se
conteve de rir no meio de suas lágrimas

**Arriveer bad in vid unios mier menegh medsu quod is Lion ed is Unicorn
eent katuend –** eles finalmente chegaram a visão de uma grande
multidão, no meio da qual o Leão e o Unicórnio estavam lutando

**Mustathna {mustA§na} –** para além, além de

**3.729.500 leuds gwiveer dien 1sto Januar 2015 in Kartvelia (mustathna
Abkhasia ed Sud-Iristan) –** 3.729.500 pessoas viviam em 1º de janeiro
de 2015, na Geórgia (para além da Abkházia e da Ossétia do Sul)

**Na {na} –** em continuação a

**J. K. Rowling scripsit alya Harry Potter buks na “The Philosopher's
Stone” –** J. K. Rowling escreveu outros livros de Harry Potter em
continuação a “A Pedra Filosofal”

**Nespekent {nëspEkënt} –** apesar de

**Nespekent sien situation, Sophia biey meis pro iom quem pro se ye cid
moment –** Apesar de sua situação, Sophia temia mais por ele do que para
si mesma naquele momento

**Nieb {nyeb} –** ao lado de

**Id dru est nieb id dom –** a árvore está ao lado da casa

**Ob {ob} –** porque, por causa de

**Eddo sabzi ob id suasmehct –** como vegetais porque eles tem um bom
sabor

**Dank ob id antwehrd –** obrigado pela resposta

**Ois {oys} –** vindo de. *As vezes escrito como “oys”.*

**Tuntos, ad pelu Stats ois id decolonisation moliet trehve ir tula –**
desde então, uma série de Estados nascidos da descolonização tiveram
dificuldade para encontrar seu equilíbrio

Acho melhor eu explicar o que o “ad” está fazendo em “ad pelu Stats”
para quem não entendeu. No exemplo foi usado o verbo “molie”, que
significa “ser/estar difícil para”, por isso que foi necessário o uso do
“ad” para indicar o objeto indireto. O verdadeiro sujeito de “moliet” é
“trehve”

**Pati {pAti} –** incluindo, inclusive

**Ceters hieb esen lusen unte id naufrage, pati vasya mehmens os eys
gwito tuntro –** todas as outras coisas tinham sido perdidas durante o
naufrágio, incluindo todas as memórias de sua vida até então

**Sayg mi tien adresse, pati tien postal code –** diga me seu endereço,
inclusive o seu código postal

**Per {per} –** por meio de, através de, de

**Skapam per cid passage –** escapamos por meio daquela passagem

**Presaygo id future per mien cristall ball –** prevejo o futuro através
da minha bola de cristal

**Dec kilometers per hor –** dez quilômetros por hora

**(Per)ambh {(për)Amb} –** por aí, em torno de, em volta

***Piends sont ambh nos –** inimigos estão a nossa volta*

**Ia ghyors perambh id vallee –** as montanhas cercam o vale

Ter est wed perambh id insule – há água em torno da ilha

**Ploisko {plOysko} –** exceto

**Eddo quant genos os miems, ploisko pork –** como todo tipo de carne,
exceto carne de porco

**Po {po} –** para *(em troca de, a fim de obter, em busca de)*

**Io exchange un apel po un banane –** troco uma maçã por uma banana

**Pon {pon} –** desde

**Som her pon id auror –** estou aqui desde o amanhecer

**Pon kamdiu reidneute yu in plen diewo? –** Por quanto tempo você tem
montado em plena luz do dia? (perceba o uso do durativo -neu-, falarei
sobre ele no capítulo apropriado)

**Pos {pos} –** depois

**Tolko tib pos io arrive –** falo com você depois que eu chegar

**Ho corregen werd pos werd –** corrigi palavra por palavra

**Pre {pre} –** antes *(preposição temporal)*

**Tolko tib pre linkwo –** falo com você antes de eu sair

**Pri {pri} –** passar ao longo de, passar perto *(ideia de movimento)*

**Is vis un bloudun reider qui galoppit pri iom dextos levtro –** ele
viu um cavaleiro vestido de azul que passou galopando perto dele da
direita para a esquerda

**Quando yu gwahsiete pri id cocin –** quando passar pela cozinha

**El shamyu snahsit pri me –** o tubarão passou nadando ao meu lado

**Pro {pro} –** para (o benefício de), para que, pra

**Kieupim un cavall pro te –** comprei um cavalo para você

**Eiskwo un wogh pro miena safers – **quero um carro para as minhas
viagens

**El behrger mohrit pro vies gwits –** o salvador morreu pela vida de
vocês

Dec kilometers per hor – dez quilômetros por hora

**To est nearyo pro me –** isso é grego pra mim

**Prod {prod} –** adiante, à frente

**Noster chef semper ghanct prod nies grupp –** nosso chefe anda sempre
à frente do nosso grupo

**Prokwe(m) {prOkwë(m)} –** perto, próximo

**El animal est prokwe me –** o animal está próximo de mim

**Prosch {proc} –** aproximando-se de

**Id wogho gwehmt prosch me –** o carro está de aproximando de mim

**Protie {protI:} –** contra. *O mesmo que a preposição do sambahsa
“contra”.*

**I katueer protiev nos –** eles lutaram contra nós

**Protiev {protyEv} = **Protie

**Samt {samt} –** com *(denota circunstância, descrição)*

**Un wir samt blou okwi –** um homem com olhos azuis

**Cid familia gwivt samt maungo noroc –** aquela família vive com muita
alegria

**Cheuso mien werds samt kaur –** escolho minhas palavras com cuidado

**Sekwos {sEkwos} –** seguinte, depois

**Sekwos id swodetermination referendum os 1sto Jul 1962, Algeria
proclamet sien independence –** na sequência do referendo de
autodeterminação do 01 de julho de 1962, a Argélia proclamou sua
independência

**Speit {speyt} –** apesar de *(com uma ideia de desapontamento, pode
ser usado como substantivo também)*

**Id tank, speit gwaur leusa, tadrijan upertrehsit id squadron –** o
tanque, apesar de pesadas perdas, esmagou o esquadrão pouco a pouco

**Sub {sub} –** logo embaixo, debaixo *(geralmente implica contato)*

**Ter est un arank sub id meja –** há uma aranha debaixo da mesa

**Sub un peruca ter est un calv cap –** debaixo de uma peruca há uma
cabeça careca

**Subpoena {subpOyna} –** sob pena de, sob medo de *(penalidade)*

**Uno mukhla al est tun dahn kay linkwes id land subpoena expulsion –**
um prazo é então dado a ele para deixar o país sob a ameaça de expulsão

**Suisraen {swisrAyn} –** não obstante

**Diplomatia ghehdeiht ia Stats suisraen ia differences iren
constitutional ed social systemes, do mutual comprehension –**
diplomacia permite que os Estados, não obstante as diferenças de seus
sistemas constitucionais e sociais, alcancem a compreensão mútua

**Suppose {supOzë} –** supondo que

**Suppose mathalan yu subscripte un contract bi un pharmaceutic firma
kay behrge vos ud diabetes –** vamos supor, por exemplo, que você assine
um contrato com uma empresa farmacêutica para mantê-lo da diabetes

**Swod {swod} –** no caminho de, na forma de

***Swod iens atavs (salaf) sokwis ios Prophet –** seguindo os costumes
dos seguidores antepassados (salafis) do Profeta*

**Tiel {tyel}–** até. *Mas antes de um verbo nós usamos “hin(a)”.*

**Id verdia start ex id urb ed continuet tiel id mar –** a estrada
começa da cidade e continua até o mar

**Cursim tiel mien dom –** corri até minha casa

**Trans {trans} –** do outro lado, além de

**Trans tod fluv –** do outro lado do rio

**Trans destruge doms, hurcan Irma hat nicen leuds –** além de destruir
casas, o furacão Irma matou pessoas

**Ud {ud} –** de *(origem)*

**Un brev ud Marta –** uma carta de Marta

**Ia kieupit un dom med denars ud ays pater –** ela comprou uma casa com
o dinheiro do pai dela

**Ulter {Ultër} –** em adição, além da

**Ulter id marche wehlen ios roy Huan, is conservit precieus-ye id royal
titule yeji quod is poiss mutalbe id noble nam Vasconcelos –** além da
ordem do rei Huan, ele conservou preciosamente o título real, segundo a
qual ele poderia reivindicar o nome nobre de Vasconcelos

**Uper {Üpër} –** acima *(não toca o objeto no qual ele está em cima)*

**Un plav pleuct uper id tor –** um avião voa sobre a torre

**Unte {Untë} –** durante, no espaço de* (sentido temporal ou espacial)*

**Unte id wer –** durante a primavera

**Unte id hall – **através do corredor

**I construgeer id tor unte ia yars –** Eles construíram a torre ao
longo dos anos

***Unte tod wakt, in Washington –** enquanto isso, em Washington*

**Sub id schock, is Waktprince remien aunmov unte un khvil –** Sob o
choque, o Príncipe do Tempo permaneceu imóvel por um tempo

**Vice {vits} –** em vez de, no lugar de

**Henrique ne hat ghohden dake ia buks. Vice to, Olivier hat ei yist ia
odt-documents –** Henrique não teve como receber os livros. Ao invés
disso, Olivier enviou para ele os documentos .odt.

**Witerom {witërOm} –** contra, em frente de, diante de, em face de
*(raramente usado como preposição)*

**Witerom id dikhliz, is daskal skohpt me –** Na (extremidade oposta do)
corredor, o professor estava esperando por mim

**Yant {yant} –** assim que, tão logo *(pode ser utilizado com
substantivos)*

**Yant ho denars, kaupo un leuyk tib –** assim que eu tiver dinheiro eu
compro um brinquedo para você

**Ye {ye} –** *não tem significado definido, expressa uma circunstância
ou condição, você usa essa preposição quando nenhuma outra mais serve, é
a nossa preposição coringa*

**Ye mien surprise –** para minha surpresa

**Ye mieno mayn –** na minha opinião

**Io arrive do hem ye noct –** eu chego em casa à noite

**Za {dza} –** na parte de trás

**Za te! –** atrás de você!

ALGUMAS DÚVIDAS QUE PODEM SURGIR
--------------------------------

Tentei explicar bem as preposições nos exemplos, eu inclusive usei
exemplos muito parecidos para que as diferenças ficassem bem evidentes,
mas, ainda assim, podem restar algumas dúvidas. Neste subcapítulo eu
tento prever algumas delas e tento respondê-las.

### DIFERENÇA ENTRE “MED” E “PER”

Qual a diferença entre as duas frases abaixo?

**Is magician hat construgen un castell med sien magia –** o mago
construiu um castelo com sua magia

**Is magician hat construgen un castell per sien magia – **o mago
construiu um castelo através de sua magia

A segunda frase em sambahsa está gramaticalmente correta, mas dá a
impressão de que o mago construiu o castelo com ele em um a outra
dimensão ou lugar distante, enquanto na primeira frase o mago construiu
o castelo no lugar onde ele estava. Veja se com o exemplo abaixo você
consegue entender:

Glego per id fenster med mien durbin iam bikini-vehsend gwenak

Espiono através da janela com meus binóculos a moça de biquíni.

Percebeu o porquê do meu segundo exemplo *(o do mago que construiu o
castelo “per” sua magia)* ficou meio estranho?

### DIFERENÇA ENTRE “POS” E “TRANS”

“Pos” é inicialmente um advérbio temporal, ou seja, se você disser a
frase abaixo

**Id urb est pos id forest – **a cidade fica depois da floresta

A frase acima significa algo como: tem-se que primeiro cruzar a floresta
antes de chegar à cidade.

### DIFERENÇA ENTRE “EX” E “UD”

Vejamos três exemplos:

**Som ex Brasilu –** sou do Brasil

**Un helm ex stal –** um elmo de aço

**Un brev ud Clara –** uma carta de Clara

O “ex”, quando não no sentido de “fora”, a gente usa quando é para algo
feito a partir do objeto mencionado ou que veio de dentro dele, como o
elmo de aço ou uma pessoa que veio de um país. O “ud” você usa para
coisas que não vieram de dentro do objeto mencionado, como se espera que
a carta não tenha sido feita do corpo de Clara, usamos o “ud” em vez do
“ex”.

### DIFERENÇA ENTRE “VICE” E “INSTET”

Como preposições ambos significam a mesma coisa, mas “instet” pode ser
também um advérbio enquanto “vice” também pode ser um prefixo, como em
“vice-president”.

Henrique ne hat ghohden dake ia buks. *Instet*, Olivier hat ei yist ia
odt-documents

Henrique ne hat ghohden dake ia buks. *Vice to*, Olivier hat ei yist ia
odt-documents

ENCLÍTICOS E PROCLÍTICOS
========================

Algumas dessas palavras não são simples de serem traduzidas, mas este
capítulo ensinará como usá-las. Enclíticos são palavras postas depois de
outra palavra enquanto proclíticos são palavras postas antes de outra
palavra.

O enclítico “tun” é o demonstrativo de “kun” ou “quando”.

**Herodotos extendt tod nam do id continental hinterland, beuwend it
Asia id trit part ios gnoht mundios tun –** Heródoto estende esse nome
para o interior continental, tornando, assim, a Ásia a terceira parte do
mundo então conhecido *(na época)*

**Id Occidental nam ios land gwehmt sigwra ex id Medieval Chinese
pronunciation tom khanjis, quod tun buit launto do id bahsa Indonesias
ka “Jepang”, ed dind ab Portughesche nauts –** o nome ocidental do país
seguramente veio da pronúncia medieval chinesa desses kanjis, que foram
então emprestado ao indonésio como “Jepang”, e depois disso por
navegantes portugueses

Quando você tem que usar algum clítico, mas não tens uma palavra com a
acentuação tônica adequada que suporte o clítico, você pode usar o “nu”
como apoio, como em “nughi” *(nu + ghi)* ou com os pronomes relativos
“yos”, “ya”, “yod”, “yel” *(por exemplo: nu- yos, nu-yel)*.

**Chanda ex Zambia, nu-yos suabaht Sambahsa, gnoht maung bahsas –**
Chanda da Zâmbia, que fala sambahsa muito bem, sabe muitas línguas

“ge” e “tar” são como “ghi”, mas como, às vezes, ocorre muito a
repetição da partícula “ghi”, usa-se essas duas partículas. O
“ge”enfatiza pronomes pessoais e artigos, enquanto o “tar” enfatiza
pronomes interrogativos.

**Ia gehnsiet un son, ed ei dahsies id nam Jesus; is ge salvsiet sien
popule ex idsa synts –** ela conceberar um filho, a quem chamarás de
Jesus; porque ele salvará o seu povo dos seus pecados

**Yed, isge ieg kam weysmee –** entretanto, ele agiu como nós *(agimos)*

-ge é atrelado a um pronome pessoal, nesta sentença do exemplo o -ge
está aqui para enfatizar a oposição com “wey-smee”

**Quod tar tengicit is? –** como ele *(realmente)* se parece?

Geralmente “tar” e “ge” não são diretamente traduzidos para outro
idioma, especialmente o “tar”

Um excelente exemplo com “nu” e “tar”:

**Sei nu id luce quod est in te est temos, kam tar megil sessiet tod
temos! –** se, portanto, a luz que em ti há são trevas, quão grandes são
tais trevas!

O enclítico “pet” reforça identidade, geralmente traduzido como o nosso
“mesmo”.

**Henrique weict in idpet centre ios citad –** Henrique mora no próprio
centro da cidade

Escrever “…in idswo centre…” seria estranho porque significaria “… no
centro em si …”

**Id brev hieb gwohmt unte idpet dwer ios laboratorium –** a carta
chegara pela própria porta do laboratório

**Est un prientlik geront quospet ghalv buit trohven medsu eys bustan
–** é um homem velho amigável cujo o próprio crânio foi encontrado no
meio do seu jardim

**I reuschus leuds eent ipet parents ias magv –** as pessoas que tinham
batido eram os próprios pais da criança *(após aquela criança ter tido
um acidente)*

*ipet = i *(o artigo definido)* + pet*

Quando “pet” é usado depois de um verbo ele tem o sentido de “estar na
capacidade de”.

**Sei yu plais steighte con me do mien fiaker, duco-pet vos ad eys
baytel –** Se você aceitar entrar comigo no meu táxi, eu *(posso)*
conduzi-lo até sua casa

Com advérbios de tempo e espaço a palavra “kye”, quando usada como
enclítico”, reforça o advérbio.

**… ed terkye id sinister masse uns bina forstilb ids ghebel uper id
strad –** … e justo nesse ponto, um certo bloco sinistro de construção
empurrou para a frente seu frontão na rua

**Ed mehnte ghi od is nos duxit tei stet querkye staht id dwer?! –** e
aonde você acha que ele nos levou para este lugar com a porta?!

**“Nunkye”, bahsit alter, antwehrdte mi: “quois gnohte yu me?” –** “e
agora”, falou o outro, responda me: “de onde você me conhece?”

Perceba que “antwehrdte” e “gnohte yu” se referem a uma pessoa, não a um
grupo como possa aparentar em um primeiro momento. É porque foi usado o
pronome “yu” de cortesia, realmente você só pode saber disso pelo
contexto.

**Tunkye eet circum nev saat ios aghyern, ed id prest nebule ios saison
–** foi por esta altura por volta das nove horas da manhã, e o primeiro
nevoeiro da estação

**Gheskye ia ne eevevis iom pon quasi dwo munts –** eram quase dois
meses desde que ela o vira até ontem

O enclítico “wert” indica fala relatada.

**Sambahsa estwert baygh interessant –** sambahsa – como é dito – é
muito interessante

**Sekwent qui ghieng pri iens unte ir mingo promenades, bowert sieyg
neid –** segundo os que passaram por eles em seus passeios de domingo,
ambos não disseram nada

**Sekwent tom mertikwol, ia bent comwert hieb haben meis dekhschat quem
gvol –** bom, a criança não estava tão pior, mais assustada, de acordo
com os Sawbones

**Iswert liek stets meis confinit-se in id practis uper id laboratorium,
quer is hatta swohp yando –** o médico, aparentemente, agora mais do que
nunca se limitou ao gabinete no laboratório, onde às vezes até dormia
*(na frase anterior da tradução, o que sabemos sobre o médico é relatado
por seu mordomo)*

O proclítico “proe” geralmente é traduzido como “já”.

**Kun id nam os Hyde buit swohrt, Ustad Utterson proe-akowsicit, bet kun
is chohx id klor, is khiek etidwoie –** quando o nome de Hyde foi
pronunciado, o Sr. Utterson já estava ouvindo, mas quando ele checou o
pedaço de madeira, ele não duvidava mais

**Mien kerd hat tem aghnuet quem ioschi proe-plangiem –** meu coração
tem doído tanto que eu também poderia ter chorado

**… proeghi tetyohc pelu dwogimtias –** … uma coisa que tinha acontecido
muitas dezenas de vezes

O conjunto “… men (…) de …” ou “… nu (…) de …” apresenta uma oposição
entre duas sentenças:

**Olivier men weict in France; Henrique de in Brasilu –** Olivier vive
*(por sua vez)* na França; Henrique *(por outro lado)* no Brasil

**Central ed Eust Asia nu est befolct ab mongoloids. Indonesia de
superpont uni aborigine substrat malays populations –** A Ásia Central e
Oriental é povoada *(por seu lado)* por mongoloides. Indonésia *(por seu
lado)* sobrepõe, a um substrato aborígene, populações malaias

uni = un + i = ad un (singular dativo)

“… men (…) de …” e “… nu (…) de …” tem a mesmíssima função.

Mas quando o “nu” é usado sozinho, isto é, não é usado em conjunto com
“de”, ele é um advérbio que significa “nomeadamente” ou “a saber”.

**Un public wesnum est un contracto conclus ab un contragend magh, nu id
Stat, ia regional au local autoritats –** uma aquisição pública é um
contrato celebrado por uma entidade contratante, ou nomeadamente / a
saber o Estado, as autoridades locais ou regionais (etc …)

O enclítico afixo -schi já foi explicado e o enclítico afixo -smee será
explicado logo.

AFIXOS ÚTEIS
============

Diferente de outras línguas auxiliares, o sambahsa depende mais palavras
emprestadas do que em palavras compostas. O sambahsa tem tantas línguas
como fonte que é impossível ter um sistema de derivação regular, mas
alguns afixos úteis podem ser listados.

Algumas regras básicas podem ser levadas em conta em formas derivadas
das línguas românicas, uma característica importante delas é que elas
são baseadas no “radical perfeito”, que é predito das seguintes
maneiras:

Para verbos terminados com “e” não acentuado se usa -at- no lugar.

form*e* → form*at-* → formation

Para verbos terminados com “ie” usasse -icat- no lugar.

publ*ie* → publ*icat-* → publication

Para verbos terminados com “ue” usasse -ut- no lugar. Também se aplica a
verbos cujos particípios do passado em “t” terminam em -wt.

constit*ue* → constitut*ut*- → constitution

Para verbos terminados com “eih” usasse -it- no lugar.

add*eih* → add*it-* → addition

O radical perfeito de outros verbos corresponde aos seus particípios em
“t”.

PREFIXOS
--------

**ab- –** longe, distante

**Abcurr –** correr para longe

**apo- – ***indica quarta geração*

**Apopater – **bisavô

**be- –** *cria verbos transitivos*

**begh(i)s- –** privado de

**bfu- –** *prefixo negativo antes de palavras de origem semita*

**cum- –** *é parecido com o prefixo com- de “comadre”*

**Cumpater –** compadre

**Cummater –** comadre

**dus- –** donte, ruim

**Dusmenos –** indisposto

**en- –** para colocar em

**Enquestion –** colocar em questão, questionar

**ender- –** *diminutivo de ação*

**Endervid –** vislumbrar

**Enderghyan –** entreabrir

**eti- – **Indica a quinta geração

**Etipater –** tataravô

**for- – **extraviar, seguir mau caminho

**ga- –** *com um verbo com apofonia: indica o resultado de uma ação; se
tiver o sufixo -os indica algo pejorativo. Já com um substantivo indica
soma.*

***Gabehrg –** cadeia de montanhas* (behrg = penhasco)

**ken- –** vazio de

**mu- –** *pode ser usado para indicar o executor de uma ação quando
prefixado a uma palavra de origem “muçulmana”*

***Mussafer –** viajante* (safer = viajar)

**muta- –** mudar

**Mutamayn –** mudar opinião

**Ni- –** *“baixo” em um sentido figurado*

***Niklad –** fazer download, baixar* (klad = carregar)

**(oi)s-** – o oposto de en-, significa “colocar para fora”

**oischalt –** desligar

**(en)schalt –** ligar

**or- –** original, primitivo

**Oraryo – **pré-ariano ou proto indo-europeu (aryo = ariano)

**par- –** conclusão, ação cumprida

***parkwehr –** realizar* (wehr = fazer)

**peri- –** completo, minucioso

**Perigumt –** trânsito (gumt = vinda, chegada, advento)

**pro- –** *indica a terceira geração. Antes de um verbo significa
“perante” “precedendo” e aciona o dativo. *

**Pronepot –** bisneto

**Is mi proghieng –** ele estava andando na minha frente

**rhayr –** *prefixo negativo usado antes de palavras de origem árabe,
mas é frequentemente usado com um advérbio independente*

**Rhayr yakin –** incerto, inseguro

**step- –** *família após um segundo casamento*

**Steppater –** padrasto

**Stepmater –** madrasta

**Steppurt –** enteado

**sua- –** bem

***Suakwohrt – **bem-feito* (kwohrt = pretérito de “kwohr”, que
significa “feito” ou “construído” )

**ud- –** capacidade de fazer melhor do que os outros

**Ho udsnaht iom –** eu nadei melhor do que ele

Com o pronome reflexivo “sib” indica uma forma de conseguir algo.

**Id mafia sib udtehrct id silence schahiden –** a máfia consegue o
silêncio das testemunhas ameaçando-as

SUFIXOS
-------

### EXPRESSAR QUALIDADE OU ESTADO

Em alguns casos adiciona-se o sufixo -e submete-se a apofonia se
possível.

**Long \[long\] (grande) –** longe \[londj\] (grandeza)

**Deub \[döb\] (profundo, profunda) –** dube \[düb\] (profundidade)

**Slab \[slab\] (fraco, fraca) –** sliebe \[slyeb\] (fraqueza)

Esse sistema só funciona se tiver uma diferença fonética entre o
adjetivo e a forma derivada. Para outros adjetivos se usa o sufixo
-(e)t.

**Mild \[mild\] (suave) –** mildet \[mIldët\] (suavidade)

**Mynder \[mÜndër\] (orgulhoso, orgulhosa) –** Myndert \[mÜndërt\]
(orgulho)

É possível o uso de outros sufixos como -os e -ia. Para palavras de
origem românica usa-se -or e (i)tat.

### EXPRESSAR UMA AÇÃO

Se o próprio radical verbal não é o suficiente, a terminação -(e)n pode
ser adicionada *(ou -sa para verbos terminados em som de vogal acentuada
tonicamente)*. Verbos de origem românica adiciona-se -(t)ion ao radical
verbal.

### INDICAR O EXECUTOR DE UMA AÇÃO

O mais usado é o -er, o seu equivalente das línguas românicas é o -or.
Um conjunto prático é o -ant para quem realiza a ação, -eit para quem
sofre essa ação e -at para indicar o resultado ou objeto dessa ação.

Is telephonant kwehrt un telephonat pro iom telephoneit

O telefonante faz um telefonema para o telefonado

### OUTROS SUFIXOS

**-ar –** coleção. Para nomes de profissão significa “fazedor de”

**-asc –** tornar-se

**-at –** idade

**-av –** num radical verbal significa “inclinado a”, “propenso a” (os
verbos se modificam por conta do infixo nasal e outras razões)

**-ber –** fruta

***Vinber – **uva* (vin = videra)

**-ble –** possibilidade. Se há risco de confusão com outra forma
conjugada, usa-se -et (-im caso o -t não seja possível).

***Dyehrcet –** vislumbrável *(dyehrc = vislumbrar)

**-dem –** região

***Roydem –** reino* (roy = rei)

**-eus –** *adjetivo de qualidade. *Corresponde ao -oso do português

***Bulbeus –** bulboso* (bulbe = bulbo)

**-en –** *adjetivo de substância*

***Golden –** dourado *(gold = ouro)

**-fred –** livre de

**-ia –** qualidade, ciência, país

**-ic –** forma adjetivos. Corresponde ao -ico do português

***Cubic –** cúbico* (cube = cubo)

**-iev –** fruta, grão

**-ika –** jovem *(feminino) *

***Potnika –** senhorita* (potnia = senhora)

**-iko –** jovem *(masculino) *

***Potiko –** galante* (poti = senhor)

**-il – **susceptível a, aberto para.

**-in –** sufixo feminino *ou* “floresta de”

**-(i)sk – **adjetivo de origem

***Coschmarisk –** de pesadelo* (coschmar = pesadelo)

**-isme –** estudo de uma coisa, teoria, ideologia

**-iste –** participantes ou apoiadores de um estudo, teoria ou
ideologia

**-ka –** faz um diminutivo feminino em uma sílaba tônica

**Ritka & Hanko –** Hansel & Gretel

**-ko –** faz um diminutivo masculino em uma sílaba tônica

**Ritka & Hanko –** Hansel & Gretel

[]{#anchor-100}**-lik –** semelhança

**Ays face est cavallik –** a face dela é como a de um cavalo

**-log –** logista

**Astrolog –** astrólogo

**Cardiolog –** cardiologista

**-ment –** corresponde a palavras emprestadas (não advérbios) que
terminem com -mente. Por questões de acentuação tônica, esse sufixo
conta como um substantivo separado dentro de uma palavra composta

**-mon –** quem pode. *Para radicais verbais*

**-ner –** sufixo masculino

***Eireanner –** irlandês* (eirean = pessoa ou coisa da Irlanda)

**-nic – **pejorativo *(masculino)*

**Drehnknic –** bêbado

**-nica –** pejorativo *(feminino)*

**-on –** precisão *(com hífen)*

**Segim mien diemens. Bet ye tod-on momento, ies Orks vols ed
exkardkiesch ir simitars ed dagas –** eu cortei minhas ligações. Mas
neste exato momento, os orcs se viraram e puxaram suas cimitarras e
adagas

**-os –*** *em um verbo significa “jogo de”

**Skeulkos –** esconde-esconde

**-smee –** sempre com pronomes pessoais, enfatiza oposição

**Weysmee habmos naiwo likwno nies parents –** nós (não você!) nunca
deixamos nossos pais

**-ster –** sufixo feminino. Use este somente se não houver problemas
com mudança de acentuação tônica.

**-ure –** em radicais perfeitos isso significa “resultado” ou
“qualidade”

**-went –** faz adjetivos, algo como “tendo …”*. *Por questões de
acentuação tônica, esse sufixo conta como um substantivo separado dentro
de uma palavra composta

***Dakruwent –** choroso (dakru = lágrima)*

ESTILO
======

Diferente das línguas nacionais, que possuem falantes nativos inseridos
dentro de um determinado contexto social, o sambahsa é pra ser falado
entre pessoas de diferentes origens. Um mongol vai ter dificuldades em
entender uma pessoa que fala do jeito alemão, por mais que esta segunda
pessoa fale um sambahsa gramaticalmente correto e bem pronunciado.

Você tem que tomar cuidado para não traduzir uma frase como “eu não devo
nada” como “io ne deulgo neid”, perceba que na frase em sambahsa você
negou vezes, o certo seria “io ne deulgo ject”. Se trabalhe para evitar
transportar características do português para o sambahsa. Um outro
exemplo: traduza “essa história se passa em São Paulo” para “tod storia
wakyet in São Paulo” em vez de “tod storia uperleit in São Paulo”.

Sabe o que é idiotismo? Idiotismo é um termo linguístico usado para
descrever termos que só são usados numa determinada língua e se forem
traduzidos diretamente para outras línguas eles não farão o menor
sentido. Vou mostrar alguns exemplos da língua francesa:

**Cherchez la femme –** gíria policial para quando há uma complicação e
não se sabe quem foi. Em tradução literal “procure a mulher”

**Pourboire –** é a nossa gorjeta. Em tradução literal “para beber”.

Não é que os idiotismos são intraduzíveis, o que não é viável é a sua
tradução direta. Está entendendo aonde eu quero chegar?

Não estou dizendo que é para você falar um sambahsa pobre e sem
personalidade! Mas tenha o cuidado de evitar idiotismos e outras formas
de falar próprias dos brasileiros.

**O que estou tentando dizer é:** converse de maneira simples, mas sem
perder a personalidade que a língua nos permite ter e a riqueza que ela
nos oferece.

ESCOLHENDO NOVAS PALAVRAS
=========================

A fonte primária é, sem dúvida, o que foi reconstruído do ancestral
(proto-)indo-europeu. Como nem todas as palavras foram reconstruídas,
muitas ideias podem ser obtidas das antigas línguas indo-europeias como
sâncrito, grego antigo e latim.

Outras fontes são palavras emprestadas de, pelo menos, dois ramos
linguísticos, da Europa Ocidental até a Ásia Oriental.

Não há muito o que se preocupar com novas palavras, o sambahsa já conta
com um bom lexico e novas palavras virão da absorção de neologismos.
Caso não seja possível criar uma tradução através do léxico existente no
sambahsa, pode se usar a palavra em sua forma original, os termos da
informática são um bom exemplo disso. Palavras que denotam um item
cultural específico, como uma arte marcial de um povo ou um prato
típico, será usado a própria palavra. É como aconteceria no português,
por exemplo, a palavra 武術 *(wushu)* foi traduzida da forma mais
próxima do que é pronunciado no original, respeitando os limites do
português que não tem as entonações do chinês e outros sons diferentes.

Como você pode ver, naturalmente que as novas palavras serão adaptadas à
ortografia do sambahsa, que devido a sua flexibilidade tenta ao máximo
manter as palavras na sua forma e pronúncia original.

ERROS E DÚVIDAS COMUNS
======================

No decorrer de toda a gramática eu tentei explicar todos os pormenores
da língua, mas sempre pode existir algo que alguém não tenha entendido.
Então criei este capítulo para listar os erros e dúvidas que eu tive no
decorrer do meu aprendizado da língua sambahsa, espero que este capítulo
lhe seja muito útil.

Naturalmente que também será adicionado, em versões futuras desta
gramática, dúvidas de outras pessoas.

COMO FUNCIONAM PRONOMES GENÉRICOS COMO “TO” E “QUO”?
----------------------------------------------------

Palavras como “to” e “quo” são pronomes genéricos que se referem
notadamente a coisas já ditas, eles são para o nominativo/acusativo
singular neutro. Eles não sofrem declinação.

Henrique hat creet un nov automatic dictionar. To*d* suawehrct!

Aqui o “to*d*” se refere a “automatic dictionar”.

Henrique hat creet un nov automatic dictionar. To est un khauris khabar!

Aqui o “to” se refere à frase anterior, ao fato de Henrique ter criado
um novo dicionário.

**“To hat duren pior diu!!!!” sieyg is ob impatience**.

Aqui o “to” se refere à situação que criou esta impaciência.

El prient: “Volo kaupe un ieftin deluxe auto, bet ne trehvo semject
interessant in id journal”.

Serter: Est her un annunce quod correspondt quo tu paurskes

Aqui “quod” se refere a “annunce” e “quo” se refere a algo não dito na
frase.

É possível mais pronomes genéricos? Em teoria seria possível pronomes
como “cio” e “eno”, mas quase não são usados.

O QUE REALMENTE SIGNIFICA O VERBO “LEIT”
----------------------------------------

Tive um pouco de dificuldade com esse verbo, principalmente porque eu me
baseava no dicionário de sambahsa para o inglês, que apontava para o
verbo “to go, to run” no sentido figurado. Mas aprendi depois de Olivier
ter me concedido quatro exemplos, analise cada um deles, talvez assim
você consiga entender o verbo leit.

**Is vohs un armur quod baygh duslit ei –** ele usou uma armadura que
não ficou muito bem nele

**duslit = **dus (prefixo para “mau” ou “doente”) + lit (pretérito de
“leit”)

**In ielg poesis, lit meis au minter dayir pisk –** Em cada poesia, o
assunto era mais ou menos sobre peixes

**Kam leitte yu? –** como vai você? *(*ou* “como você está?”)*

**Bet tod conversation leit lyt pior oku –** mas essa conversa está indo
um pouco rápida demais

**Id grance leit engwn id Rhen –** as fronteiras vão ao longo do Reno

Percebeu que o verbo “leit” é como os verbos “ser”, “estar” e “ir” no
sentido figurativo?

Mais alguns exemplos, mas com a palavra “uperleit”:

**Hind uperleit ex un agrent societat eni un democratic quader –** A
Índia faz a transição de uma sociedade agrária num quadro democrático

**Id economic crosct nilent, uperleitend ex 9% pro yar in 2010 do 6% in
2011 –** o crescimento econômico está desacelerando, passando de 9% por
ano para 6% em 2011

**Mehnent maghe uperlites fauran do id politic nivell –** eles pensam
que podem passar imediatamente para o nível político

**Serter uperlit is do id camp ios Papp –** mais tarde, ele se juntou ao
campo do Papa

**Ghehdt uperlites id hol territorium uns land –** pode ir *(além)* de
todo o território de um país

COMO PEDIR ALGO
---------------

Como traduzir “pedi dinheiro ao meu pai”. A tradução mais correta *não*
é “prohgim denars ad mien pater”, vamos ver os problemas dessa frase. A
frase na sua forma passiva:

**\* Denars buir prohgen ab me ud mien pater –** dinheiro foi rezado a
mim do meu pai

Lembre-se que o verbo “prehg”, no sentido de “pedir” é usado com a
palavra “ke(m)” e nesse caso deveria ser usado o “ud” em vez do “ad”.
Vejamos como seria a tradução dessa frase absurda.

**\* Prohgim denars ad mien pater –** rezei dinheiro para o meu pai

Agora veja a frase como deveria ser traduzida:

**Pedi dinheiro ao meu pai –** prohgim mien pater ke mi daht denars
*(**literalmente:** pedi meu pai que me desse dinheiro)*

Perceba que “mi” está no caso dativo

COMO DIZER QUE ALGO VAI DEMORAR OU VAI SE ATRASAR
-------------------------------------------------

Vamos começar com o verbo “chitay”. Você pode dizer frases do tipo:

**Tod saat chitayt –** esse relógio atrasou

**Id machine chitayt pre schalte –** a máquina demora para ligar *(**em
tradução direta:** “a máquina demora antes de ligar”. Você não deve usar
o “kay” aqui)*

Como verbo transitivo o verbo “chitay” pode ser usado com pessoas:

**Id seuy hat chitayt mien prient –** A chuva atrasou meu amigo

Você pode usar o adjetivo “skept”.

**Mien prient est skept ob id seuy –** meu amigo está atrasado devido a
chuva

Há diversas formas de expressar essa ideia:

**Id machine tehrpt wakt pre biwehrge – **a máquina precisa de tempo
antes de começar a trabalhar

SUJEITO INDETERMINADO
---------------------

Como dizer algo como “roubaram o carro do meu irmão”? Seria “raubheer id
wogh os mien brater”?

O sambahsa não tem nenhum sistema determinado de como deve funcionar
tudo, se as regras são respeitadas e não há risco de mal-entendido,
então geralmente não há problema.

Quanto a frase do exemplo, e infinitas outras que seguem o mesmo estilo,
recomenda-se que se evite o uso dessa forma, porque ela implica que a
ação foi feita por diversas pessoas ou coisas, e não por uma pessoa ou
coisa. Para frases desse tipo é mais interessante usar a forma passiva:

“Id wogh os mien brater hat est raubht”.

VERBOS DE DIREÇÃO COMO “GWAH”
-----------------------------

Verbos como “gwah” (ir para) dispensam preposições, pois seus objetos
diretos já informam que eles são a direção do movimento. A preposição
pode servir para informar algo a mais, como no exemplo abaixo:

**Vahm gwahe kye mien dom –** vou para minha casa *(vou até a casa, mas
não planejo entrar nela)*

**Vahm gwahe mien dom –** vou para minha casa *(entro na casa)*

QUAL É A DIFERENÇA ENTRE “YAKIN” E “WEIDWOS”?
---------------------------------------------

Enquanto “yakin” é um adjetivo, “weidwos” é um advérbio.

Sempre preste atenção no dicionário à classificação gramatical das
palavras.

SOBRE QUESTÕES GRAMATICAIS MENORES
==================================

NOMES DE LÍNGUAS E NACIONALIDADES
---------------------------------

Observe o seguinte texto: “Você sabia que o espanhol é falado por mais
de quinhentos milhões de pessoas? Mas só existem quarenta e sete milhões
de espanhóis”.

Esse mesmo texto em inglês: “Did you know that Spanish is spoken by more
than five hundred million people? But there are only forty-seven million
Spaniards”.

Perceba que em português o nome da língua foi escrito em letra minúscula
e recebeu um artigo definido, o gentílico também foi escrito em letra
minúscula. Mas em inglês tanto o nome da língua quanto o seu gentílico
são tem sempre sua primeira letra escrita em maiúscula, e o nome da
língua nunca recebe artigo.

Qual modelo sambahsa segue? Ambos são válidos, escreva como você achar
melhor.

PALAVRAS RELACIONADAS A NÚMEROS MENORES QUE DOIS
------------------------------------------------

Qual é o correto, “1,5 liter” ou “1,5 liters” *(“liter” significa a
unidade de medida de volume “litro”)*? Em sambahsa ambos estão corretos.

CONCORDÂNCIA VERBAL DO VERBO “ES”
---------------------------------

Observe as seguintes frases:

\(1) – Id bratriya est i Purts ios Desert

\(2) – Id bratriya sont i Purts ios Desert

Qual seria o correto neste caso, “est” ou “sont”? O verbo “es”
preferencialmente concordaria com quem, “Id bratriya” ou “i Purts ios
Desert”? Nesses casos, se houver um elemento no plural, o verbo a ser
usado estará no plural, portanto o correto seria (2).

Sim, sei que a melhor maneira de escrever essa frase seria “id bratriya
est composen ab i Purts ios Desert”, mas forcei uma frase mais informal
para mostrar para você o que fazer em situações desse tipo.

PALAVRAS E SEUS SIGNIFICADOS
============================

As palavras possuem camadas de significados, como por exemplo a palavra
“periferia” *(no contexto de cidade)*, que significa “bairros mais
afastados do centro”, mas popularmente “periferia” significa “bairros
pobres mais afastados do centro”, apesar de que uma periferia pode ser
um bairro rico. Muitos brasileiros achariam estranho chamar de
“periferia” um bairro rico só porque este está afastado do centro.

Se você vir uma palavra como “farm” *(“fazenda” em sambahsa)*, não
assuma de imediato que se trata necessariamente de uma propriedade rural
com milhares de hectares, até mesmo uma propriedade rural com 3 hectares
onde se trabalhe a agricultura ou pecuária pode ser considerada como uma
“farm”.

Quando você estiver consultando o dicionário tenha cuidado com esse tipo
de coisa para que a mensagem não seja passada ou recebida da maneira
errada. Claro que sempre haverá o esforço de fazer dicionários mais
explicativos, mas na dúvida consulte um dicionário da língua portuguesa
para ter certeza do significado da palavra.

VOCABULÁRIO
===========

Você aprendeu muito do que necessita para utilizar essa bela língua
chamada sambahsa. Não serei imodesto em afirmar que passei tudo que
possa ser ensinado, pois mesmo línguas auxiliares exigem volumes e mais
volumes se forem analisadas a fundo, mas espero que esta gramática tenha
lhe ensinado a usar a língua em pelo menos 99.90% das situações.

Este último capítulo tem como objetivo expandir um pouquinho mais o seu
vocabulário, é óbvio que não ensinarei todas as palavras, até porque
este livro teria que ser também um dicionário para que isso fosse
possível, mas quero que você termine esse livro com um vocabulário
razoável, de forma que você possa iniciar a leitura de textos com um
pouco mais de conforto.

Há diversos subcapítulos que atendem diferentes áreas, como corpo humano
e informática, mas serão trabalhadas apenas conceitos básicos, não há
uma pretensão aqui de listar os nomes em sambahsa de todos os músculos e
ossos do corpo humano, da mesma forma que não serão traduzidos todos os
termos utilizados na área da Tecnologia da Informação; você terá que
procurar pelas palavras em um dicionário ou, caso sejam termos muito
específicos, em dicionários destinados a uma área em especial como
dicionários de termos médicos.

Também é importante que você preste atenção em que subcapítulo as
palavras se encontram para que você não tenha dúvidas desnecessárias a
respeito do real significado das palavras. Eu quero evitar situações
como você vir a palavra “stupid”, cuja tradução é “burro”, e pensar que
se trata do animal doméstico jumento; se essa palavra está no
subcapítulo ADJETIVOS, provavelmente ela se refere a um insulto em vez
de um animal da subespécie *Equus africanus asinus*. Resumindo: através
do subcapítulo onde a palavra se encontra você pode deduzir em que
contexto ela pode ser empregada.

Para que você possa, de fato, expandir o seu vocabulário – e consolidar
seus conhecimentos da gramática – é essencial que você leia, escute e
pratique a língua.

VERBOS
------

Embora as palavras portuguesas estejam na forma infinitiva, as palavras
do sambahsa apresentadas estão em sua forma de radical. Por exemplo: o
verbo no **infinitivo** “adicionar” é “addihes” em sambahsa, não
“addeih”. Estamos entendidos?

**Acquiseih {akizE:y} –** adquirir, obter *(como, por exemplo, em um
sentido legal)*

**Addeih {adE:y} –** adicionar

[]{#anchor-101}**Adore {adOr} –** adorar *(tanto no sentido de gostar
quanto no sentido de venerar)*

**Al {al} –** criar *(no sentido de “eu crio galinhas”)*

**Annem {Anëm} –** respirar

**Antwehrd {antwE:rd}–** responder

**Appareih {aparE:y} –** tornar-se aparente, tornar-se evidente *(este
verbo é pouco usado, não o confunda com “prehp”)*

**Arrive {arIv} –** chegar *(verbo intransitivo)*

**Au {Ao} –** faltar, estar sem

**Aur {Aor} –** ouvir

**Ay {ay} –** considerar como *(**me ays tien prient** = você me
considera como seu amigo)*; dizer/contar *(“…” **iey is =** “…” disse
ele)*

**Aygve {aygv} –** sentir vergonha de

**Bah {ba:} –** falar

**Balbel {bAlbël} –** conversar, bater papo

**Balnye {bAlnyë} –** se banhar, tomar banho

**Bay {bay} –** temer, ter medo

**Behrg {be:rg} –** salvar *(a vida, de um perigo)*

**Beud {böd} –** apelar *(para alguém)*, implorar, prestar atenção a
*(alguém(no dativo))*, exigir *(algo)*

**Beuw {böw} –** fazer crescer, ajudar a se tornar; cultivar *(uma
determinada cultura)*

**Bih {bi:} –** tornar-se

**Brinegh {brInëg} –** trazer

**Bruneg {brÜnëg} –** aproveitar

**Cheid {tceyd} –** brigar, disputar

**Cheus {tcös} –** escolher

**Chitay {tcitAy} – **demorar, se atrasar *(quando se refere a máquinas
ou coisas)*

**Clud {klud} –** fechar

**Comprehend {komprEhënd} –** compreender *(ter compreensão para)*

**Credeih {krëdE:y} –** acreditar

**Cultive {kultIv} –** cultivar

**Curr {kur} –** correr

**Dagh {dag} – **acender

**Dah {da:} –** dar

**Dak {dak} – **receber

**Daum {dAom} – **imaginar, se perguntar

**Deh {de:} –** colocar, botar

**Dehbh {de:b} –** se adequar, ser conveniente

**Dehlg {de:lg} –** dever *(no sentido de “você deve fazer isso”, “devo
comprar comida hoje”)*

**Dehm {de:m} –** erigir, suspender, levantar; domar, treinar
*(especificamente um animal)*

**Deik {deyk} –** mostrar, indicar

**Desire {dezIr} –** desejar, cobiçar

**Destrug {dëstrUg} –** destruir

**Deulg {dölg} –** dever *(no sentido de dívida)*

**Disradh {disrAd} –** aconselhar Y contra X

**Tib disradho namore ciom yuwen –** eu te aconselho a não namorar
aquele rapaz

**Drehnk {dre:nk} – **beber *(especificamente bebida alcoólica)*

**Edd {ed} –** comer

**Eih {e:y} –** ir

**Eiskw {Eyskw} –** querer *(no sentido mais genérico da palavra)*, ter
intenção de

**Em {em} –** pegar *(no sentido de “pegar um trem”)*, tomar *(no
sentido de “tomar uma decisão”)*

**(En)schalt {(en)cAlt} –** ligar *(uma máquina)*

**Entre {Entrë} –** entrar

**Euc {ök} –** aprender

**Exporte {ekspOrt} –** exportar

**Fall {fal} –** cair

**Faungmoen {faongmOyn} –** visitar

**Feug {fög} –** fugir (de)

**Fineih {finE:y} –** finalizar, terminar

**Fleurt {flört} –** flertar

**Folossie {folosI:} –** servir-se de

**Fortrehc {fortrE:k} –** sair *(para uma viagem)*

**Gahab {gahAb} –** poupar *(um(a) inimigo(a))*

**Gehn {dje:n} (+ acc) –** parir, dar a luz *(para propostas literárias,
você também pode dizer “bringhes do luce”)*

**Ghab {gab} –** entender *(um fato ou situação)*

**Gham {gam} –** se casar

**Ghang {gang} –** andar, caminhar

**Ghat {gat} –** encontrar/conhecer (uma pessoa)

**Ghehd {ge:d} –** poder *(capacidade)*, ser capaz de

**Ghend {gend} –** pegar *(literalmente alguma coisa com as mãos)*

**Ghohd {go:d} –** *pretérito de “ghehd”*

**Ghyan {gyan} –** abrir

**Gnah {nya:} –** nascer

**Gnoh {nyo:} –** conhecer, saber

**Gugheir {gugEyr} –** bagunçar, desordenar

**Gvaedd {gvayd} –** achar *(no sentido de “acho que é esse o caminho
certo”)*

**Gvehd *****&lt;alguma coisa&gt; &lt;de alguém&gt;*****{gve:d} –**
rezar *(por alguma coisa de alguém, como em “I nowngmins gvehde seuy ud
Div” que significa “os fazendeiros rezam por chuva de Deus”)*

**Gwah {gwa:} –** ir para

**Gwehm {gwe:m} –** vir

**Gweup {gwöp} –** guardar, manter, conservar

**Gwiv {gwiv} –** viver, estar vivo, estar viva

**Gwohm {gwo:m} – ***pretérito de “gwehm”*

**Heih {he:y} –** alcançar *(um ponto preciso)*, acertar

**Hock {hok} –** agachar-se

**Iey {yey} –** *pretérito de “ay”*

**Ih {i:} –** *pretérito de “eih”*

**Importe {impOrt} –** importar *(uma coisa, como um produto do
exterior)*

**Istifsar {istifsAr} –** pedir *(uma informação)*

**Jinkdou {jinkdU:} –** acontecer, continuar *(no sentido de “o segundo
filme acontece/continua na China”)*

**Jlampoh {jlampO:} –** beber *(especificamente água)*

**Kam {kam} –** gostar de

**Kamyab {kamyAb} –** ter sucesso em

**Kan {kan} –** tocar *(um instrumento)*

**Kau {kAo} –** notar, reparar

**Kaup {kAop} –** comprar

**Kaur {kAor} –** importar *(kaur de = “se importar com”)* *(no sentido
de “você importa para mim”)*

**Keul {köl} –** valorizar; cultivar *(uma terra)*

**Keung {köng} –** hesitar

**Keup {köp} –** exigir, demandar, requerer

**Khak {qak} –** não poder, não ser capaz de. *Essa palavra também
significa o adjetivo “mau”*.

**Kheiss {qeys} –** sentir

**Kieup {kyöp} –** *pretérito de “kaup”*

**Klehpt {kle:pt} –** furtar

**Kleu {klö} –** escutar

**Kleuster {klÖstër} –** escutar *(mais cuidadosamente)*

**Klu {klu} –** *pretérito de “kleu”*

**Kurihen {kurIhën} –** comprar

**Kussen {kUsën} –** beijar

**Kwah {kwa:} –** conseguir, ter sucesso em; acolher, recolher

**Kwehk {kwe:k} –** parecer *(não confunda com “aparecer”! Esse verbo é
usado em situações como “o filho se parece mais com a mãe”)*

**Kwehr {kwe:r} –** fazer

**Lass {las} –** deixar *(como em “deixe-me em paz)*

**Leik {leyk} –** jogar *(um jogo)*

**Leis {leys} –** ler

**Leips {leypz} –** esquecer *(um prazo, um ônibus…)*

**Leit {leyt} –** ser, estar, ir *(todas as três palavras no sentido
figurado, no final do livro há um subcapítulo que trata somente desse
verbo)*

**Lever {lEvër} –** entregar *(como em “entregar uma pizza”)*

**Lieubh {lyÖb} –** amar

***Se en*****lieubh *****in/med/in/… ***** {lyÖb} –** se apaixonar

**Linekw {lInëkw} –** deixar *(o ambiente onde se encontra)*, sair

**Lis {lis} –** *pretérito de “leis”*

**Lites {lits} –** *forma infinitiva de “leit”*

**Localise {lokaliz} –** localizar

**Magh {mag} –** poder *(capacidade ou permissão)*, ter permissão para

**Maghses {mAgsës} –** pode ser *(como em: “você quer uma maçã agora?
Pode ser”)*

**Man {man} –** ficar, permanecer

**Manage {manAdj} –** gerenciar, cuidar bem de

**Mank {mank} –** a faltar, estar faltando *(verbo intransitivo)*

**Maximise {maksimIz} –** maximizar

**Mayn {mayn} – **significar, ter o significado de

**Mehld {me:ld} –** salientar *(algo para alguém)*, sinalizar algo para
*(João sinaliza para seu filho que o dicionário contém erros)*, rezar

**Mehm {me:m} –** lembrar, recordar

**Mehn {me:n} –** pensar

**Meil {meyl} –** preferir, gostar

**Meuk {mök} –** soltar, largar

**Miegh {myeg} –** *pretérito de “magh”*

**Minimise {minimIz} –** minimizar

**Miss {mis} –** sentir falta *(de alguém)*

**Myehrs {mye:rz} –** esquecer

**Mohn {mo:n} –** *pretérito de “mehn”*

**Mutt {mut}** – aconteça, caminhar *(no sentido figurado, como em
“espero que os acontecimentos caminhem nessa direção”)*, andar *(no
sentido figurado de “como andam as coisas?”)*, ter razão para, ter
motivos para

**Nak {nak} –** alcançar, atingir *(verbo transitivo)*

**Namor {namOr} –** namorar

**Naudh {nAod} –** necessitar, precisar

**Nayd {nayd} –** caçoar, zoar

**Neic {neyk} –** matar, assassinar

**Niklad {niklAd} – **baixar *(um arquivo)*, fazer download

**Neud {nöd} –** utilizar *(no sentido geral da palavra)*

**Nieudh {nyöd} –** *pretérito de “naudh”*

**Obsok {obsOk} –** buscar *(investigação)*, remexer *(investigação)*

**Oisschalt {oyscAlt} –** desligar *(uma máquina)*

**Orbat {orbAt} –** trabalhar

**Paursk {pAorzk} –** procurar

**Pehrd {pe:rd} –** peidar *(com barulho)*

**Pehrn {pe:rn} –** vender

**Pehzd {pe:zd} –** peidar *(com mau cheiro)*

**Peit {peyt} –** tentar

**Permitt {përmIt} –** dar autorização a

**Permiss {përmIs} –** *pretérito de “permitt”*

**Phone ad {fon ad} –** ligar para, telefonar para

**Plan {plan} –** planejar

**Pleuk {plök} –** voar

**Poh {po:} –** beber *(no sentido geral da palavra)*

**Poitt {poyt} –** poder *(permissão)*, ter permissão para, ter o
direito a

**Prehg *****&lt;alguém&gt;***** ke(m) {pre:g &lt;&gt; ke(m)} –** pedir,
rezar *(como por exemplo “Henrique prehct Olivier kem (is) ei
antwehrdt”, que significa “Henrique pergunta a Olivier por uma
resposta”)*, conversar *(no sentido de “conversar com alguém sobre uma
questão”)*

**Prehp {pre:p} –** aparecer *(no sentido de tornar-se visível)*

**Preim {preym} –** receber *(recepção intencional, como em “receber
alguns amigos”)*

**Prete {pret} –** entender *(um idioma ou o que foi dito)*

**Prodah {prodA:} –** entregar *(um criminoso)*

**Puwen {pÜwën} –** limpar

**Raubh {rAob} –** roubar

**Recep {rëtsEp} –** receber *(geralmente em um sentido mais técnico,
como por exemplo: aqui na minha vila recebemos o sinal de TV da
Alemanha)*

**Reik {reyk} –** retornar para

**Reiss {reys} – **desenhar

**Resid {rëzId} –** residir, viver, morar

**Sagv {sagv} –** saber como

**Salg {salg} –** sair de

**Salg con {salg kon} –** sair com *(adicionei este porque pode
significar “namorar”)*

**Sayg {sayg} –** dizer

**Schehnk {ce:nk} –** dar, oferecer *(ambas as traduções podem se
referir a um presente)*

**Schikay {cikAy} –** reclamar

**Scrib {skrib} – **escrever

**Sedd {sed} – **sentar

**Sgwesen {sgwEzën} –** apagar

**Skand {skand} –** pular

**Skap {skap} –** escapar de, se afastar de

**Skeul {sköl} –** ter a obrigação, ser obrigado

**Skehpt {ske:pt} –** esperar por

**Sleu {slö} –** lançar, liberar

**Sisen {sIzën} –** deixar *+ verbo no infinitivo (como em “ele me
deixou usar o emblema de sua casa”)*

**Smauter {smAotër} –** assistir a *(TV, filme, teatro, partida de
futebol, etc)*

**Snumeb {snÜmëb} –** casar (um homem)

**Sok {sok} –** investigar

**Soll {sol} –** deve *(possibilidade)*

**Spar {spar} –** poupar *(como em “poupar dinheiro”)*

**Speh {spe:} – **esperar, ter esperança de

**Spehc {spe:k} –** olhar para

**Spend {spend} –** gastar (tempo), perder (tempo)

**Spraneg {sprAnëg} –** saltar, explodir *(verbo transitivo)*

**(oi)Sprehg *****&lt;pessoa perguntada&gt; &lt;questão&gt;*****
{(oy)sprE:g &lt;&gt;&lt;&gt;} –** perguntar *(para alguém uma questão.
Como em “Leandro sprehct Carlos quer is weict in Brasilu”, que significa
“Leandro pergunta a Carlos onde ele mora no Brasil”)*

**Sprehng {spre:ng} –** saltar, explodir *(verbo intransitivo)*

**Stehm {ste:m} –** suportar, apoiar *(uma pessoa, uma ideia, alguma
coisa)*

**Stehnk {ste:nk} –** feder

**Studye {stUdye} – **estudar

**Substitue {substitÜ:} –** substituir

**Suicide {switsId} –** cometer suicídio

**Swehk {swe:k} – **cheirar

**Tarjem {tArjëm} –** traduzir

**Tehl –** suportar, apoiar *(ambas as palavras se referem em sustentar
alguma coisa, como um telhado)*; tolerar

**Tehr {te:r} –** atravessar, cruzar

**Tehrb {te:rb} –** precisar *(no sentido de “preciso fazer o jantar”)*,
ter *(no sentido de “tenho que trabalhar”), *necessitar

**Tekhnass {teqnAs} –** se virar *(sentido figurado, como em “sei me
virar sozinho”)*

**Tengie {tëndjI:} –** se parecer como, se parecer com

**Teup {töp} –** se esconder

**Tohrb –** *pretérito de “tehrb”*

**Tolk {tolk} –** falar de, explicar, conversar

**Trehc {tre:k} –** mover, deslocar-se

**Trehv {tre:v} –** achar, encontrar

**Ubklad {ubklAd} –** subir *(um arquivo)*, fazer upload

**Uc {uk} –** *pretérito de “euc”*

**Uperleit do {üpërlEyt do} –** passar a(o)*, *ir a *(sentido figurado)*

**Use {Üz} –** usar

**Van {van} –** buscar, trazer

**Vansch {vanc} –** desejar, exprimir votos a

**Vid {vid} –** ver

**Vol {vol} –** querer

**Wan {wan} –** necessitar *(algo que está faltando)*, requerer *(algo
que está faltando)*, precisar de *(algo que está faltando)*

**Wehd {we:d} –** se casar (especificamente com uma mulher)

**Wehkw {wE:kw} –** falar com, se expressar com, conversar com

**Wehl {we:l} –** querer alguém para, dar uma ordem para alguém

**Wehn {we:n} –** sentir vontade de fazer

**Wehrg {we:rg} –** se comportar, trabalhar, funcionar *(essas duas
últimas no sentido de “aquela palavra trabalha/funciona como um adjetivo
e advérbio ao mesmo tempo”)*

**Wehs {we:s} –** estar *(no sentido de localização)*

**Weik {weyk} –** morar, viver *(no sentido de morar)*, habitar

**Yeis {yeys} –** enviar

**Yis {yis} –** *pretérito de “yeis”*

ADJETIVOS
---------

**Akster {Akstër} –** vívido, animado, forte *(sentido figurado; cores;
senso, como em “álcool forte”)*

**Bell {bel} –** belo(a)

**Bert {bert} –** brilhante

**Biaur {byAor} –** feio(a)

**Biedan {biedAn} –** infeliz

**Bridek {brIdëk} –** feio(a) *(como em “tempo feio/ruim”)*

**Buland {bulAnd} –** alto(a)

**Chald {tcald} –** quente

**Cherkin {tcërkIn} –** feio(a) *(passa uma ideia de sordidez)*

**Clever {klEvër} – **inteligente

**Clus/cluden {klus / klÜdën} –** fechado(a)

**Cort {kort} –** curto(a)

**Deusk {dösk} –** escuro(a)

**Difficil {difItsil} –** difícil

**Dorgv {dorgv} –** estimado, caro; caro *(antônimo de barato)*

**Facil {fAtsil} –** fácil

**Ghem {gem} –** baixo, inferior

**Ghyan(t/en) {gyAn(t/ën)} –** aberto(a)

**Gohd {go:d} –** bom, boa *(contrapartida de algo de má qualidade)*

**Hog {hog} –** alto(a), elevado(a), superior

**Ieftin {yeftIn} –** barato(a)

**Jadide {jadId} –** não desgastado(a)

**Jamile {jamIl} – **bonito(a)

**Kaurd {kAord} –** duro(a)

**Kiest {kyest} –** limpo(a)

**Latif {lAtif} –** puro(a)

**Lent {lent} –** lento(a)

**Leur {lör} –** livre

**Long {long} –** longo(a)

**Madh {mad} –** úmido(a), molhado(a)

**Maschkhoul {macqU:l} –** comprometido(a) *(emprestado, usado)*

**Mliak {mlyak} –** suave

**Murdar {murdAr} –** sujo(a)

**Muzlim {mUdzlim} –** fraco(a) *(sentido figurado)*

**Nert {nert} –** forte (fisicamente)

**Noroct {norOkt} –** feliz, alegre

**Nov {nov} –** novo(a)

**Oku {Oku} –** rápido

**Orm {orm} –** pobre

**Peigher {pEygër} –** desagrável, nojento(a)

**Prev {prev} –** anterior

**Riche {ritc} –** rico(a)

**Saluber {salÜbër} –** sadio(a), saudável, salubre

**Sell {sel} –** bom, boa *(contrapartida de ruim, mal)*, bendito,
bem-aventurado(a)

**Siuk {syuk} –** seco(a)

**Slab {slab} –** fraco(a) (fisicamente)

**Smulk {smulk} – **baixo(a)

**Srig {srig} –** frio

**Staur {stAor} –** poderoso(a)

**Stupid {stUpid} –** burro(a), estúpido(a)

**Swad {swad} –** doce

**Trauric {trAorik} –** triste *(em um contexto de luto)*

**Trist {trist} –** (coisa) triste

**Dussaun {dusAon} –** insalubre, pouco saudável

**Veut {vöt} –** velho(a)

**Wahid {wahId} –** único(a), solitário(a), só, singular

**Walik {wAlik} –** forte, poderoso(a)

**Yui {yuI} –** triste

**Yun {yun} –** jovem

CORPO
-----

**Anus {Anus} –** ânus

**Aur {Aor} –** orelha

**Badan {badAn} –** organismo *(obviamente quando se referindo ao
corpo)*

**Bagu {bAgu} –** antebraço

**Bemern {bEmërn} –** coxa

**Berd {berd} –** barba

**Brakh {braq} –** braço

**Brov {brov} –** sobrancelha

**Cap {kap} –** cabeça

**Chol {tcol} –** testa

**Cloin {kloyn} –** nádega

**Coll {kol} –** pescoço

**Corpos {kOrpos} –** corpo

**Dent {dent} –** dente

**Dingv {dingv} –** língua

**Fingher {fIngër} –** dedo

**Genu {djEnu} –** joelho

**Gharn(a) {gArn(a)} –** intestino

**Gian {djyan} – **bochecha

**Glesen {glEzën} –** tornozelo

**Gumos {gUmos} –** secreção corporal

**Gurgule {gUrgül} –** garganta

**Gventer {gvEntër} –** cintura

**Hand {hand} –** mão

**Hank {hank} –** quadril

**Jamb {jamb} –** perna

**Kays {kays} –** cabelo (da cabeça)

**Kers {kers} –** cérebro

**Kert {kert} –** coração

**Krew(os) {krEw(os)} –** carne

**Lid {lid} –** pálpebra

**Lige {lidj} –** face

**Lip {lip} –** lábio

**Lyekwrnt {lyEkurnt} –** fígado

**Mant(u) {mAnt(u)} –** queixo

**Mems {mems} –** membro

**Moustache – **bigode

**Muscle {muskl} –** músculo

**Nabh {nab} –** umbigo

**Nas {nas} –** nariz

**Naster {nAstër} –** focinho

**Nayv {nayv} –** corpo (de um morto)

**Nugver {nUgvër} –** rim

**Ok {ok} –** olho

**Oklid {Oklid} –** pálpebra

**Okwi {Okwi} –** olhos

**Olan {olAn} –** cotovelo

**Oms {oms} –** ombro

**Onkh {onk} –** unha

**Ors {ors} –** bunda

**Ost {ost} –** osso

**Papil {pApil} –** mamilo

**Ped {ped} –** pé

**Penis {pEnis} –** pênis

**Pizdan {pizdAn} –** seio *(peito da mulher)*

**Pod {pod} –** pata (de um animal)

**Poimen {pOymën} –** leite da mãe

**Pulmon {pulmOn} –** pulmão

**Regv {regv} –** costas

**Runc {runk} –** pulso

**Sehrg {se:rg} –** sangue

**Skeletum {skëlEtum} –** esqueleto

**Smokru {smOkru} –** barbicha

**Snap {snap} –** bico

**Stohm {sto:m} –** boca

**Stomak {stomAk} –** estômago

**Veine {veyn} –** veia

**Wagin {wadjIn} –** vagina

**Xiongbu {ksyOngbu} –** peito

**Yowkjitia {yowkjItya} –** corpo (quando se refere a um ser vivente)

CORES
-----

**Albh {alb} –** branco

**Argavan {argavAn} –** lilás

**Blou {blu:} –** azul

**Brun {brun} –** marrom

**Gehlb {dje:lb} –** amarelo

**Glend {glend} –** verde

**Greis {greys} –** cinza

**Kenek {kEnëk} –** amarelo dourado

**Kwit {kwit} –** branco. *Tanto “albh” quanto “kwit” significam a mesma
coisa, “branco”, mas como “kwit” vem de “kweit” (purificar), “kwit” pode
passar o sentimento de algo mais brilhante, mas ambas as palavras têm o
mesmo significado.*

**Orange {orAndj} –** laranja

**Pemb {pemb} –** rosa

**Rudh {rud} –** vermelho

**Sword {sword} –** preto

**Violett {violEt} –** violeta

INFORMÁTICA
-----------

Como certos termos, especialmente as siglas, oriundas da língua inglesa
tem ampla aceitação mundial, talvez seja mais conveniente utilizar os
termos originais, como por exemplo VGA, que em sambahsa é VGT. Bom, faça
como achar melhor.

Ah, e você se lembra do que eu falei a respeito dos verbos no
infinitivo? Caso não, por favor volte até o subcapítulo VERBOS deste
capítulo VOCABULÁRIO.

Caso você queira traduzir um software para o sambahsa e não sabe se deve
traduzir os comandos para a forma infinitiva ou imperativa, aqui vai a
explicação de como deve ser feito:

-   Comandos dados do usuário para o computador são traduzidos na forma
    infinitiva. ***Exemplo:** abonne in forum *(se inscrever no fórum)*;
    rename archive *(renomear arquivo)*; *[]{#anchor-102}*yises
    blixbrev* (enviar email)
-   Comandos dados do computador para o usuário são traduzidos na forma
    imperativa, com a fórmula de cortesia da 2ª pessoa do plural.
    ***Exemplo:** contacte nos (contate-nos); clickte her *(clique
    aqui)*; yeiste nos un blixbrev *(envie-nos um e-mail)

[]{#anchor-103}[]{#anchor-104}**Abonn {abOn} –** se inscrever

**Abonnent {abOnënt} –** assinante

**Abonnment {abOnmënt} –** inscrição

**Adresse bar {adrEs bar} –** barra de endereço

**AI (Artificial Inteligence) {artifitsiAl intëlidjEnts} –** AI
(*Artificial Inteligence*), IA *(Inteligência artificial)*

**Alat bar {alAt bar} –** barra de ferramentas

[]{#anchor-105}**Alatengjia {alAtëngjya} –** barra de ferramentas

**Algorithme {algorI§m(ë)} –** algorítimo

**Alveycomputing {alveykompUting} –** cloud computing - computação na
nuvem

**Analogic {analOdjik} –** analógico

**Antivirus {antivIrus} –** antivírus

**Anyow {anyOw} –** password - senha

**Application {aplikatyOn} –** aplicativo

**Archive {artcIv} –** arquivo

**Arrange {arAndj} – **formatar (um texto)

**Attache {atAtc} –** anexar (um arquivo)

**Aunviel {aonvyEl} –** wireless

**Aunkabel {aonkAbël} –** wireless

**Aurphone {aorfOn} –** fone de ouvido

**BD *****(Blou-rai)***** {blu:-rä} –** BD *(Blue-ray)*

**Bichoun {bitcU:n} –** layout

**Binar {binAr} –** binário

**Blixbrev {blIksbrëv} –** e-mail

**Blog {blog} –** Blog

**Bluetooth {blu:tU§} –** bluetooth *(uma vez que se trata de uma marca
registrada, a palavra em sambahsa mantém a pronúncia inglesa)*

**Browser {brOwsër} –** navegador

**Bukmark {bukmArk} –** favorito ou marcador (do navegador ou página de
documento)

**Cadre per secunde {kAdr per sekUnd} –** frame per second

**CD *****(compacto disk)***** {kompAkto disk} –** CD *(compact disk)*

**Cell(ule) {tsEl(ül)} –** célula de planílha

**(Central) processor {(tsentrAl) protsesOr} –** CPU *(Central
Processing Unit)*

**Channel {tcAnël} –** canal (de rádio, televisão, etc)

**Chip {tcip} –** chip eletrônico

**Claviatolk {klavyatOlk} –** CHAT

**Claviature {klaviatÜr} –** teclado

**Cleichwerd {klEytcwërd} –** palavra-chave

**Click {klik} –** clicar

**Codec {kOdëk} –** codec

**Colunn {kolUn} –** coluna

**Compiler {kompIlër} –** compilador

**Computer {kompÜtër} –** computador

**Computerdorak {kompütërdorAk} –** gabinete

**Configuration {konfiguratyOn} –** configuração

**Connect {konEkt} –** conectar *(verbo transitivo)*

**Cont(o) {kOnt(o)} –** conta

**Converter {konvErtër} –** conversor

**Convert {konvErt} –** converter

**Copie {kopI:} –** copiar

**Cursive texte {kursIv text} –** texto em itálico

**Daftarnukta {daftarnUkta} – **marcador *(sinal gráfico como •, ◦, ‣)*

**Dakhelpage {dakhëlpAdj} –** página inicial

**Data {dAta} –** dados

**Databank {databAnk} –** banco de dados

**Database {databAz} –** banco de dados

**Datenbank {datënbAnk} – **banco de dados

**Datensklad {datënsklAd} –** DRIVE *(Data storage device)*

**Datia {dAtya} –** arquivo

**Datum {dAtum} –** dado

**DD - darm disk {darm disk} –** HD - disco rígido *(Hard Disk Drive)*

**Defect {dëfEkt} – **BUG

**Desinstalle {dësinstAl} –** desinstalar

**Desfragmente {dësfragmEnt} –** desfragmentar (uma partição)

**Desktop {dësktOp} –** área de trabalho; computador de mesa

**Diaporama {dyaporAma} –** diaporama, apresentação de slides

**Digital {didjitAl} –** digital

**Disayn {dizAyn} –** design

**Disayner {dizAynër} –** designer

**Diskette {diskEt} –** disquete

**Diskwehr {diskwE:r} –** desfazer

**DM *****(darmo memoria)***** {dArmo memOrya} –** ROM *(Read-Only
Memory)*

**Document {dokÜmënt} –** documento

**Documentation {dokümëntatyOn} –** documentação

**Dossier {dosyEr} –** pasta

**Drucken {drUkën} –** impressão

**Ducer {dÜtsër} –** driver

**DVD (*****Digital Video Disk*****) {didjitAl vidEo disk} –** DVD
*(Digital Video Disc)*

**E-buk {ebUk} –** e-book

**Ecran {ekrAn} –** tela

**(Ecran)decor {(ekran)dëkOr} –** wallpaper - papel de parede

**Ecransparer {ekranspArër} –** protetor de tela

**Edeih {edE:y} –** editar

**Editor {editOr} –** editor

**Emulator {emulatOr} –** emulador

[]{#anchor-106}**Enmercatnotes {enmErkatnots} –** notas de lançamento

**error {erOr} –** error

**Extension {ekstënsyOn} –** complemento, extensão (add-on); extensão de
arquivo

**Exter ligne {ekstër lInye} –** offline

**Extrag {ekstrAg} –** recortar

**Fenster {fEnstër} –** janela

**Format {formAt} –** formatar (um disco rígido ou um texto)

**Formulire {formulIr} –** fomulário

**Forum {fOrum} –** fórum

**Fraumbar {fraombAr} –** barra de rolagem

[]{#anchor-107}**Frequent-ye Anacta Questions (F.A.Q.) {frEkwënt-ye
anAkta këstyOns} –** perguntas frequentes

**Funed {fÜnëd} –** mesclar (células)

**Gadabeih {gadabE:y} –** formatar (um texto)

**Gadget {gAdjët}–** gadget

**Galerie {galërI:} – **galeria

**Ghyanen code {gyAnën kod} –** open source – código aberto

**Gleim {gleym} –** colar

**GNI *****(graphic (neuder) interface)***** {grAfik (nÖdër) intërfAts}
–** GUI *(Graphical User Interface)*

**Graphic {grAfik} –** gráfico *(adjetivo)*

**Graphic ambient {grAfik ambyEnt} –** ambiente gráfico

**Graphic pianji {grAfik pyAnji} –** mesa digitalizadora

**Graphique {grafIk} –** gráfico *(substantivo)*

**Hacker {hAkër} –** hacker

**Hardware {hardwAr} –** hardware

**Hissabpianji {hisabpyAnji} –** planilha

**HDMI *****(Hog-definition multimedia interface)***** {hog dëfinityOn
multimEdya intërfAts} –** HDMI *(High-Definition Multimedia Interface)*

**Hol ecran {hol ekrAn} –** tela inteira, tela cheia

**Hyperlink {hüpërlInk} –** hiperlink

**IA (integren antplehcensambient) {intEgrën antple:tsënsambyEnt} –**
IDE *(Integrated Development Environment)*

**Icon {ikOn} –** ícone

**Indentation {indëntatyOn} –** indentação

**Informatique {informatIk} –** informática

**Infrarudh {infrarUd} –** infravermelho

**In ligne {in lInye} –** online

**Installe {instAl} –** instalar

**Internet {intërnEt} –** internet

**Interpreter {intërprEtër} –** interpretador *(como interpretador de
Python ou Java)*

**(Computer) joystick {(compÜtër) joystIk} –** joystick

**Kabel {kAbël} –** cabo

**Khatem {qAtëm} –** Game Over

**Klad {klad} –** carregar

**Klink {klink} –** aba / guia de uma janela

**Knop {knop} –** botão

**Laptop {laptOp} –** notebook (laptop)

**Leikstyr {lEykstür} –** controle de videogame

**Leiser {lEyzër} –** leitor

**Leur software {lör softwAr} –** software livre

**Ligne {lInye} –** linha

**Log in {log in} –** login, logon

**Log ex {log eks} –** logout, logoff

**Materplack {matërplAk} –** placa-mãe

**Mediatolker {medyatOlkër} –** reprodutor de media

**Memoria carte {memOrya kart} –** cartão de memória

**Metadata {metadAta} –** metadados

**Metadatum {metadAtum} –** metadado

**Microphone {mikrofOn} –** microfone

**Miniature {miniatÜr} –** thumbnail - miniatura

**Mobilphone {mobilfOn} –** celular

**Modem {mOdëm} –** modem

**Monitor {monitOr} –** monitor

**Mus {mus} –** mouse

**Musictolker {muziktOlkër} –** reprodutor de música

**Muspad {muspAd} –** mousepad

**Mov {mov} –** mover

**Netwehrg {netwE:rg} –** network - rede

**Niklad {niklAd} –** download, baixar (um arquivo)

**Ogwnschirm {Oguncirm} –** firewall

**Oisschalt {oyscAlt} –** desligar

**Operationsysteme {opëratyonsüstEm} –** sistema operacional

**Operative systeme {opëratIv süstEm} –** sistema operacional

**Pack {pak} –** pacote *(de um conjunto de softwares)*

**Panell {panEl} –** painel

**Partition {partityOn} –** partição (de um disco rígido)

**Personalise {përsonalIz} –** personalizar

**Pfehrster {pfE:rstër} –** cursor, ponteiro do mouse

**(Computer) pianji {(kompÜtër) pyAnji} –** tablet

**Piwer texte {pIwër text} –** texto em negrito

[]{#anchor-108}**Plug-in {plug-In} –** plugin

**Podcast {podkAst} –** podcast

**Post {post} –** post(agem)

**Primark lurhat {primArk lurhAt} –** linguagem de marcação

Printer {prIntër} – impressora

Processor {protsesOr} – processador

[]{#anchor-109}Profile {profIl} – perfil

Programmation lurhat {programatyOn lurhAt} – linguagem de programação

Programme {progrAm} – programa; programar

**Pung {pung} –** botão

**Puwen {pÜwën} –** debug / depurar

**RAM *****(random access memoria)***** {randOm ak(t)sEs mëmOrya} –**
RAM *(Random Access Memory)*

**Reclame {rëklAm} –** anúncio / publicidade

**Recorde {rëkOrd} –** salvar (um documento ou progresso de um software)

**Recorder {rëkOrdër} –** gravador

**Renam {rënAm} –** renomear

**Retrovert {rëtrovErt} –** downgrade *(trazer de volta uma antiga
versão ou estado)*

**Rekwehr {rëkwE:r} –** refazer

**Rewos bar {rEwos bar} –** barra de espaço

**Rinkap {rinkAp} –** resetar

**Roig {royg} – **linha (no caso, de uma planilha)

**Router {rU:tër} –** roteador

**Salvguarde {salvgwArd} –** backup

**Scanner {skAnër} –** scanner

**Schalter {cAltër} –** botão de liga e desliga

**Schrift {crift} –** fonte *(typeface)*

**Server {sErvër} –** servidor

**Smartphone {smartfOn} –** smartphone

**Smauter {smAotër} –** visualizar

**Social netwehrg {sotsiAl nëtwE:rg} –** rede social

**Software {softwAr} –** software

**Sokmotor {sokmotOr} –** motor de busca

**SPAM –** SPAM

**Sroviswor {sroviswOr} –** fonte de alimentação

[]{#anchor-110}**Stahgjia {stA:gjya} –** barra lateral

**Strehmen {strE:mën} –** streaming - transmissão

**Substrichen texte {substrItcën tekst} –** texto sublinhado

**Suppressem {suprEsëm} –** deletar

**Surce code {surts kod} –** código fonte

**Swoglehmber {swoglE:mbër} –** booting

**Swoncarte {swonkArt} –** placa de som

**Swonkwatel {swonkwAtël} –** caixa de som

**Synchronise {sünkronIz} –** sincronizar

**Tab(ulation) {tab(ulatyOn)} –** tabulação

**Texte futliar {text futlyAr} –** caixa de texto

**Textenbeorbater {tekstënbeorbAtër} – **processador de texto

**Topic {tOpik} –** tópico (de um fórum)

**Touche {tu:tc} –** tecla (de um teclado)

**Touchecran {tu:tcëkrAn} –** touchscreen

**Trackliste {traklIst} –** playlist

**Transistor {transistOr} –** transistor

**TV carte {TV kart} –** placa de captura

**Ubklad {ubklAd} –** fazer upload, subir (um arquivo)

**Ubnuw {ubnUw} –** atualização (de dados)

**Ubnuwen {ubnÜwën} –** atualizar (dados)

**Ubstiumep {ubstyÜmëp} –** atualizar (versão)

**Uperswehp {üpërswE:p} –** hibernar

**Utilitat {utilitAt} –** utilidade

**USB (*****Universal Serial Bus*****) {univërsAl sëriAl bus} –** USB
*(Universal Serial Bus)*

**USB cleich {USB kleytc}–** pendrive

**Vector image {vëktOr imAdj} –** imagem vetorial

**Vectorial image {vëktoryAl imAdj} –** imagem vetorial

**VGT *****(Video Graphique Tehxen)***** {vidEo grafIk tE:ksën} –** VGA
*(Video Graphics Array)*

**Video {vidEo} –** vídeo

**Videogame {videogAm} –** console de videogame

**Videocarte {videokArt} –** placa de vídeo

**Videoleik {videolEyk} –** console de videogame

**Videotolker {videotOlkër} –** reprodutor de vídeo

**Wi-fi {wi-fi} –** wi-fi

**Virtual {virtuAl} –** virtual

**Virus {vIrus} –** vírus

**Viel {vyel} –** cabo

**Voltage-tuler {voltAdj tÜlër} – **estabilizador

**Volume {volÜm} –** volume

**Web {web} –** web, rede

**Webcam {wëbkAm} –** webcam

**Website {wëbsAyt} –** website, site

[]{#anchor-111}**Webtyohc {wëbtyO:k} –** feed

[]{#anchor-112}**Wehrgeih {we:rdjE:y} –** dar play, rodar, fazer
funcionar, operar

**Windows portfeuyl {windOws portfÖyl} – **Porta-Arquivos *(do Microsoft
Windows)*

**Zoum apo {zu:m Apo} –** Zoom out

**Zoum prosch {zu:m proc} –** Zoom in

ANIMAIS
-------

**Arank {arAnk} –** aranha

**Aux(an) {Aoks / aoksAn} –** boi

**Av {av} –** pássaro

**Ayg {ayg} –** cabra

**Aygur {aygUr} –** garanhão

**Baleina {balEyna} –** baleia

**Baul {bAol} –** morcego

**Bei {bey} –** abelha

**Bock {bok} –** bode

**Brank {brank} –** besouro

**Cat {kat} –** gato

**Cavall {kavAl} –** cavalo

**Cuincule {kwInkül} –** coelho

**Delphin {dëlfIn} –** golfinho

**Gall {gal} –** galo

**Gaydh {gayd} –** bode / cabra *(pode ser usado com adjetivo)*

**Ghelon {gëlOn} –** tartaruga

**Ghimer {gImër} –** filhote *(um inverno de idade)*

**Gwow {gwow} –** vaca, bovino

**Katelsqual {katëlskwAl} –** orca

**Kiep {kyep} –** macaco grande

**Kierk {kyErk} –** galinha

**Kunia {kUnya} –** cadela, cachorra

**Kwaun {kwAon} –** cão, cachorro

**Leopard {leopArd} –** leopardo

**Lion {lyon} –** leão

**Maymoun {maymU:n} – **macaco pequeno

**Murm {murm} –** formiga

**Mus {mus} –** camundongo

**Musch {muc} –** mosca

**Ornd {ornd} –** águia

**Ow {ow} –** ovelha / carneiro

**Paymen {pAymën} –** criador *(de animais)*

**Pelpel {pElpël} –** borboleta

**Pinguin {pingwIn} –** pinguim

**Pisk {pisk} –** peixe

**Pork {pork} –** porco

**Rat {rat} –** rato

**Schebeck {cëbEk} –** macaca

**Scorpion {skorpyOn} –** escorpião

**Serpent {sErpënt} –** serpente, cobra

**Shamyu {xAmyu} –** tubarão

**Sill {sil} –** foca

**Taur {tAor} –** touro

**Tigher {tIgër} –** tigre

**Urx {urks} –** urso

**Vulp(ek) {vUlp(ëk)} –** raposa

**Wersi {wErsi} –** filhote *(uma primavera de idade)*

**Wolf {wolf} –** lobo

MEDIDAS
-------

**Absorpt energia dose {absOrpt enErdjya doz} –** dose absorvida

**Ampere {ampEr} (A) –** ampere

**Becquerel {bëkErël} (Bq**) – becquerel

**Candela {kandEla} (cd) –** candela

**Catalytic activitat {katalÜtik aktivitAt} –** atividade catalítica

**Celsius grade {tsElsyus grAd} (°C) –** grau Celsius

**Dwinegh {dwInëg} –** força *(no contexto de pressão)*

**Electric capacitat {elEktrik kapatsitAt} –** capacitância

**Electric charge {elEktrik tcardj} –** carga elétrica

**Electric conductance {elEktrik konduktAnts} –** condutância

**Electric inductance {elEktrik induktAnts} –** indutância

**Electric resistence {elEktrik rezistEnts} –** resistência elétrica

**Electromot force {elëktromot forts} –** tensão elétrica

**Energia {enErdjya} –** energia

**Equivalent dose {ekivAlënt doz} –** dose equivalente

**Farad {farAd} (F) –** farad

**Force {forts} –** força

**Frequence {frëkEnts} – **frequência

**Gray {gray} (Gy) –** gray

**Gwayder flux {gwAydër fluks} –** fluxo luminoso

**Gwayder intensitat {gwAydër intënsitAt} –** intensidade luminosa

**Henry {hEnri} (H) –** henry

**Hertz {hertz} (Hz) –** hertz

**Joule {ju:l} (J) –** joule

**Katal {katAl} (kat) –** katal

**Kelvin {kelvIn} (k) –** kelvin

**Kilogramme {kilogrAm} (kg) –** quilograma

**Kweitergwis {kwEytërgwis} –** luminosidade

**Liter {lItër} –** litro

**Longe {londj} –** comprimento

**Lumen {lÜmën} (lm) –** lúmen

**Lux {luks} (lx) –** lux

**Magnetic induction {manyEtik induktyOn} –** intensidade de campo
magnético

**Magnetic induction flux {manyEtik induktyOn fluks} –** fluxo magnético

**Masse {mas} –** massa

**Meter {mEtër} (m)–** metro

**Mole {mol} (mol) –** mol

**Newton {nëwtOn} (N) –** newton

**Ohm {o:m} (Ω) –** ohm

**Pascal {paskAl} (Pa) –** pascal

**Plane angule {plan Angül} –** ângulo plano

**Pressem {prEsëm} –** pressão

**Radian {radyAn} (rad) –** radiano

**Radioactivitat {radyoaktivitAt} –** atividade radioativa

**Secunde {sekUnd} (s) –** segundo

**Siemens {syEmëns} (S) –** siemens

**Sievert {syEvërt} (Sv) –** sievert

**Solid angule {sOlid Angül} – **ângulo sólido

**Srov {srov} –** corrente elétrica

**Steradian {stëradyAn} (sr) – **esferorradiano

**Stieure {styÖr} –** potência

**Temperature {tëmpëratÜr} –** temperatura

**Tesla {tEsla} (T) –** tesla

**Volt {volt} (V) –** volt

**Wakel {wAkël} –** quantidade, carga

**Wakt {wakt} –** tempo

**Weber {wEbër} (Wb) –** weber

**Wehrg {we:rg} –** trabalho

**Watt {wat} (W) –** watt

FAMÍLIA
-------

**Aja {Aja} –** vovó

**Amma {Ama} –** mãe adotiva

**Atta {Ata} –** padrasto

**Bent {bent} –** garota, menina

**Brater {brAtër} –** irmão

**Cousin {ku:zIn} –** primo, prima

**Cummater {kummAtër} –** madrinha

**Cumpater {kumpAtër} –** padrinho

**Daiwer {dÄwër} –** cunhado

**Dugter {dUgtër} –** filha

**Eln {eln} –** criança, filhote

**Esor {ezOr} –** esposa, mulher

**Ewo {Ewo} –** vovô

**Familia {famIlya} –** família

**Fiancee {fyantsEë} –** noiva *(durante o noivado)*

**Fianceo {fyantsEo} –** noivo *(durante o noivado)*

**Gelou {djëlU:} –** cunhada

**Gemer {djEmër} – **marido da irmã

**Genealogic dru {djënealOdjik dru} –** árvore genealógica

**Grandmater {grandmAtër} –** avó

**Grandpater {grandpAtër} –** avô

**Gvibh {gvib} –** esposa, mulher

**Ienter {yEntër} –** esposa do irmão do marido

**Kerab {kërAb} –** parente

**Kerdprient {kërdpryEnt} –** namorado

**Kerdprientin {kërdpryentIn} –** namorada

**Kweil {kweyl} –** garota, menina

**Mann {man} –** esposo, marido

**Mater {mAtër} –** mãe

**Matruw {matrUw} –** tio materno

**Nanander {nanAndër} –** irmã do marido, cunhada

**Nepot {nëpOt} –** sobrinho

**Neptia {nEptya} –** sobrinha

**Nitia {nItya} –** *circulo familiar e de amigos*

**Novsta {nOvsta} –** noiva *(durante o casamento)*

**Novsto {nOvsto} –** noivo *(durante o casamento)*

**Oncle {onkl} –** tio

**Parent {pArënt} –** pai / mãe

**Pater {pAtër} –** pai

**Prient {pryent} –** amigo, amiga

**Samgwelbh –** *nascido da mesma mãe*

**Siour {syu:r} –** irmão da esposa, cunhado

**Son {son} –** filho

**Swelion {swëlyOn} –** marido da irmã da esposa

**Swesgen {swEsdjën} –** irmão / irmã

**Swes(ter) {swEs(tër)} –** irmã

**Swoker {swOkër} –** sogro

**Swokru {swOkru} –** sogra

**Tante {tant} –** tia

**Vidva {vIdva} –** viúva

**Vidvo(s) {vIdvo(s)} –** viúvo

**Wehdmen {wE:dmën} –** dote (da noiva)

DIVERSOS
--------

**Alphabet {alfAbd} – **alfabeto

**Anua {Anwa} –** anciã

**Aunfin {aonfIn} –** infinito

**Buk {buk} –** livro

**Dienkia {dyEnkya} –** eletrodoméstico

**Distance-control {distAnts kontrOl} –** controle remoto

**Dru {dru} –** árvore

**Fall {fal} –** caso *(situação)*

**Forest {forEst} –** floresta

**Future {futÜr} –** futuro

**Geront {djërOnt} –** ancião

**Grammatic {gramAtik} –** gramática

**Gwen {gwen} –** mulher

**Gwenak {gwënAk} –** moça

**Ject {jekt} –** coisa

**Ker {ker} –** vez

**Kindergarten {kindërgArtën} –** jardim de infância

**Kwehrmen {kwE:mën} –** maneira, forma de fazer alguma coisa

**Majoritat {majoritAt} –** maioria

**Memoria {mëmOrya}** – memória

**Mensc {mensk} – **humano(a), homem *(“homem” no sentido de espécie)*

**Menue {mënÜ:} – **menu

**Message {mesAdj} –** mensagem

**Minoritat {minoritAt} –** minoria

**Pieg {pyeg} –** garota, menina

**Present {prEzënt} –** presente

**Prev {prev} –** passado

**Primar talim {primAr tAlim} –** *estágio no qual, crianças aprendem o
básico, como escrever e fazer contas*

**Pwarn {pwarn} – **garoto, menino

**Secundar talim {sëkundAr tAlim} –** *estágio no qual, adolescentes
aprendem o fundamental*

**Suedos {sÜ:dos} –** propriedade *(qualidade)*

**Television {tëlëvizyOn} – **televisão

**Veneg {vEnëg} –** vendedor

**Videocassette {videokasEt} – **videocassete

**Videoteip {videotEyp} –** videocassete

**Wakt {wakt} –** tempo *(duração)*

**Wir {wir} –** homem

**Yeudmo {yÖdmo} –** homem *(como combatente)*

**Yuwen {yÜwën} –** rapaz

**Zaman {dzamAn} –** época, período, tempo *(quando referente a um
determinado período)*

INTERJEIÇÕES
------------

**Ajaban! {ajabAn} –** oh! ah, sério? mesmo?

**Al-hamdulillah! {al hamdulilA:} –** graças a Deus!

**Aman! {amAn} –** tenha misericórdia! tenha piedade!

**Anchoa! {antcOa} –** oh! o quê? *(incredulidade)*

**Asafa! {azAfa} –** credo! cruzes! macacos me mordam!

**Bast(a)! {bAst(a)} –** basta! é o bastante!

**Bravo! {brAvo} –** bravo!

**Bre! {bre} –** ei!

**Chao! {tcAo} –** tchau!

**Chiba! {tcIba} –** psiu! *(para um cachorro se afastar)*

**Chinchay! –** não é nada!

**Coul! {ku:l} –** legal! bacana! que massa!

**Dank! {dank} –** valeu! obrigado!

**E? {ë} –** hein? hã?

**Ehh {e:} –** err *(dúvida)*

**Gwivtu! / Gwive! {gw} –** salve! viva!

**Ha ha ha! {ha ha ha} –** Há! Há! Há!

**Hay(te)! {hay(t)} –** vamos lá! *(encorajamento)*

**Hue! {hü:} –** *para um cavalo*

**Hurrah! {hurA:} –** hurra! *(aplauso)*

**Iblis! {Iblis} –** mas o quê? mas que diabos? que porra é essa?

**Interjection {intërjëktyOn} –** interjeição

**Lassamahallah {lasamahalA:} –** Deus me ajude!

**Lutfan {lutfAn} –** seja gentil! seja amável!

**Maideh {maydE:} –** socorro! alguém me ajude! SOS!

**Marba {mArba} –** é um prazer de conhecer

**Mersie! {mërsI:} –** obrigado!

**O Deiwes! {o dEywës} **– graças aos Deuses!

**Pawiropeku! {pawiropEku} –** tchau! adeus!

**Sayang! {sayAng} –** que pena!

**Servus! {sErvus} –** pois não! com muito prazer! a seu serviço!

**Scha! {ca} –** silêncio! psiu!

**Sieune {syön} –** saúde!

**Sikhtir {sIqtir} –** saia daqui! saia!

**Stop! {stop} –** pare!

**Toi! {toy} –** *decepção*

**Uff! {uf} –** ufa!

**Ups! {ups} –** ops!

**Vedim! {vEdim} –** vamos ver! *(para trazer um contra-argumento)*

**Yallah! {yalA:} –** meu Deus!

**Way! {way} –** ai! ui!

**Zinhaar! {dzinhaAr} –** cuidado!

REFERÊNCIAS
===========

SIMON, Olivier. RICE, Stephen L (ed.). **The Grammar Of
Sambahsa-Mundialect In English**. 7 ed. Maio 2012.

A Starter to Sambahsa

RICE, Stephen L. **Sambahsa\_Phrasebook**

WINTER, Robert. **Sambahsa: Guide to Pronouns and Articles**.

**Muitas e muitas conversas com Olivier Simon**.

LIMA, Henrique Matheus da Silva. **Manual de Alfabeto Fonético
Internacional**. 2017.

OFFICIAL DAKHELPAGE OS SAMBAHSA-MUNDIALECT. **FrontPage**. Disponível
em: &lt;http://sambahsa.pbworks.com/w/page/10183084/FrontPage&gt;.
Acesso em: jul. 2016.

OFFICIAL DAKHELPAGE OS SAMBAHSA-MUNDIALECT. **IPA Sambahsa phonetics**.
Disponível em:
&lt;http://sambahsa.pbworks.com/w/page/10183089/IPA%20Sambahsa%20phonetics&gt;.
Acesso em: jul. 2016.

DIARIO DOS CAMPOS. **Idiotismos da Língua Portuguesa**. 01 set. 2012.
Disponível em:
&lt;http://www.diariodoscampos.com.br/variedades/2009/05/idiotismos-da-lingua-portuguesa/1123815/&gt;.
Acesso em: jul. 2016.

RACHEL. Canal de YouTube da Rachel's English.
&lt;https://www.youtube.com/user/rachelsenglish&gt;. Acesso em: jan.
2017.

[^1]: Não é “kohgnt” porque {ko:nyt} não é pronunciável, por isso que
    aqui escrevemos “kohgnt” {kO:nyët}.

[^2]: Esse -end no final denota o particípio ativo no presente, você
    aprenderá sobre eles em capítulos futuros, não precisa se preocupar
    com ele agora.
