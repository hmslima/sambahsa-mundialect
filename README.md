# Sambahsa Mundialect

***Sambahsa Mundialect*** *is an auxiliary language created by the French Dr. Olivier Simon, it was launched in the internet on July of 2007, its base is the Proto-Indo-European language, language spoken about 5000 years ago, whose daughter languages widespread from Southern Russia up to the British Isles and India. Sambahsa counts with many contributions from other languages, especially Arabic, but also Chinese, Indonesian and many others.*

This project aims to develop many kinds of materials for *(and in)* Sambahsa Mundialec, like grammars, translations of books, etc.